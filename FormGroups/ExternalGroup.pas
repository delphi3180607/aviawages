unit ExternalGroup;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FormsGroup, Vcl.ExtCtrls, FMAVertPages;

type
  TFormExternalGroup = class(TFormFormsGroup)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormExternalGroup: TFormExternalGroup;

implementation

uses workers, departments, payments;

{$R *.dfm}

procedure TFormExternalGroup.FormCreate(Sender: TObject);
begin
  inherited;
  vpSections.AddForm(FormWorkers);
  vpSections.AddForm(FormDepartments);
  vpSections.AddForm(FormPayments);
  vpSections.ActivatePage(0);
end;

end.
