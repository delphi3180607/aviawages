unit CalculationGroup;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FormsGroup, Vcl.ExtCtrls, FMAVertPages;

type
  TFormCalcGroup = class(TFormFormsGroup)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormCalcGroup: TFormCalcGroup;

implementation

uses calculation, export;

{$R *.dfm}

procedure TFormCalcGroup.FormCreate(Sender: TObject);
begin
  inherited;
  vpSections.AddForm(FormCalculation);
  vpSections.AddForm(FormExport);
  vpSections.ActivatePage(0);
end;

end.
