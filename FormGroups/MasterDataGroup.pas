unit MasterDataGroup;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FormsGroup, Vcl.ExtCtrls, FMAVertPages;

type
  TFormMasterDataGroup = class(TFormFormsGroup)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormMasterDataGroup: TFormMasterDataGroup;

implementation

uses JumpKinds, Planes, PayGroups, FlightTarifs, Forestries;

{$R *.dfm}

procedure TFormMasterDataGroup.FormCreate(Sender: TObject);
begin
  inherited;
  vpSections.AddForm(FormForestries);
  vpSections.AddForm(FormJumpKinds);
  vpSections.AddForm(FormPlanes);
  vpSections.AddForm(FormPayGroups);
  vpSections.AddForm(FormFlightTarifs);
  vpSections.ActivatePage(0);
end;

end.
