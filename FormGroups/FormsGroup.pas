unit FormsGroup;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, FMAVertPages, InitStated;

type
  TFormFormsGroup = class(TForm)
    vpSections: TFMAVertPages;
    procedure vpSectionsChangeActiveForm(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    {}
  public
    procedure Init;
  end;

var
  FormFormsGroup: TFormFormsGroup;

implementation

uses dmucurrentperiod;

{$R *.dfm}

{ TFormFormsGroup }

procedure TFormFormsGroup.FormShow(Sender: TObject);
begin
  Init;
end;

procedure TFormFormsGroup.Init;
begin
  if not vpSections.Parent.Visible then exit;
  if not vpSections.ActiveForm.Visible then exit;
  if vpSections.ActiveForm is TFormInitStated then
  begin
    TFormInitStated(vpSections.ActiveForm).Init;
    dmcurrentperiod.RegisterInitializedForm(vpSections.ActiveForm);
  end;
end;

procedure TFormFormsGroup.vpSectionsChangeActiveForm(Sender: TObject);
begin
  inherited;
  Init;
end;

end.
