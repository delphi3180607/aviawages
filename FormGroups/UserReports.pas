unit UserReports;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Mask,
  DBCtrlsEh, FMALookUp, Vcl.Buttons, PngSpeedButton, DBGridEhGrouping,
  ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh,
  DBGridEh, dmureports, MemTableDataEh, Data.DB, MemTableEh, FMACrudDisp,
  EXLReportExcelTLB, EXLReportBand, EXLReport, EXLReportDictionary, JSON,
  EditReportParams, StrUtils, FMACRUDAgent, System.Generics.Collections,
  dmureportcolumns, dmureportparams, dmuutils, dmudedscale;

type

  TFormUserReports = class(TForm)
    plCommand: TPanel;
    sbClose: TPngSpeedButton;
    luReport: TFMALookUp;
    Grid: TDBGridEh;
    Label1: TLabel;
    btPerform: TButton;
    btUpload: TButton;
    DS: TDataSource;
    EXLReport: TEXLReport;
    MemData: TMemTableEh;
    procedure btPerformClick(Sender: TObject);
    procedure luReportKeyValueChange(Sender: TObject);
    procedure sbCloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btUploadClick(Sender: TObject);
  private
    template: string;
    group_field1: string;
    group_field2: string;
    function GetParamsDataSet(ReportId: Variant): TDataSet;
    function GetColumnsDataSet(ReportId: Variant): TDataSet;
    function PrepareParamForm(params: TDataSet): TFormEditReportParams;
    procedure PlaceParamControls(params: TDataSet; form: TFormEditReportParams);
    function MakeControl(param_id: integer; code, caption, param_type: string; param_length, param_decimals: integer;
    getlist_method, display_field_name, key_field_name: string; master_param_id: integer; filter_expression: string;
    form: TFormEditReportParams): integer;
    function GetCrudAgent(getlist_method: string): TFMACRUDAgent;
    procedure GenerateColumns(params: TDataSet);
  public
    procedure GenerateReport;
  end;

var
  FormUserReports: TFormUserReports;

implementation

{$R *.dfm}

uses dmu, dmucalcperiods, dmufirecases, dmudepartments, dmuworkers, dmupayments;

procedure TFormUserReports.btPerformClick(Sender: TObject);
begin
  try
    Screen.Cursor := crHourGlass;
    GenerateReport;
  except
    on E: Exception do
    MessageDlg(E.Message, mtError, [mbOK], 0);
  end;
  Screen.Cursor := crDefault;
end;

procedure TFormUserReports.btUploadClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  MemData.DisableControls;
  try
    exlReport.Template := ExtractFilePath(Application.ExeName) +'/templates/'+template;
    exlReport.Show('', false, group_field1, group_field2);
  except
    on E: Exception do
    MessageDlg(E.Message, mtError, [mbOK], 0);
  end;
  MemData.EnableControls;
  Screen.Cursor := crDefault;
end;

procedure TFormUserReports.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dm.DataSnapConnection.Close;
  MemData.Close;
end;

procedure TFormUserReports.GenerateColumns(params: TDataSet);
var errormessage: string; c: TColumnEh;
begin
  Grid.Columns.Clear;
  params.First;
  while not params.Eof do
  begin
    c := Grid.Columns.Add;
    c.Title.Caption := params.FieldByName('title').AsString;
    c.FieldName := params.FieldByName('field_name').AsString;
    c.AutoFitColWidth := false;
    c.Width := params.FieldByName('display_width').AsInteger;
    params.Next;
  end;
end;

procedure TFormUserReports.GenerateReport;
var ds, params, columns, reportdata, preparedata: TDataSet;
sqltext, errormessage: string;
result_state: boolean; f: TFormEditReportParams;
rf: TEXLReportField;
i: integer;
p: Pointer;
begin
  with luReport.CrudAgent do
  begin
    ParamTransform.ResetContext;
    ParamTransform.AddPatternContext('/*wherefilter*/', 'id = :id');
    ParamTransform.AddParameterContext('id', ftInteger, luReport.KeyValue);
    ds := GetList(result_state, errormessage);
  end;
  if ds.Eof then exit;
  MemData.DisableControls;
  try
    params := GetParamsDataSet(luReport.KeyValue);
    f := PrepareParamForm(params);
    PlaceParamControls(params, f);
    f.ShowModal;
    if f.ModalResult = mrOk then
    begin
      EXLReport.Dictionary.Clear;
      for p in f.ControlList.Values do
      begin
        rf := EXLReport.Dictionary.Add;
        rf.FieldName := TDBEditEh(p).DataField;
        rf.ValueAsString := TDBEditEh(p).Text;
      end;

      sqltext := ds.FieldByName('prepare_command_sql').AsString;
      if Trim(sqltext)<>'' then
      begin
        dmutils.CustomQueryAgent.ParamTransform.ResetContext;
        dmutils.CustomQueryAgent.ParamTransform.AddSQLTextContext(sqltext);
        dmutils.CustomQueryAgent.ParamTransform.AddParametersFromDataSet(f.ParamBuffer);
        preparedata := dmutils.CustomQueryAgent.GetList(result_state, errormessage);
        if not preparedata.Eof then
        begin
          for i := 0 to preparedata.Fields.Count-1 do
          begin
            rf := EXLReport.Dictionary.Add;
            rf.FieldName := preparedata.Fields[i].FieldName;
            rf.ValueAsString := preparedata.Fields[i].AsString;
          end;
        end;
      end;
      sqltext := ds.FieldByName('query_sql').AsString;
      template := ds.FieldByName('template').AsString;
      group_field1 := ds.FieldByName('group_field_level1').AsString;
      group_field2 := ds.FieldByName('group_field_level2').AsString;
      dmutils.CustomQueryAgent.ParamTransform.ResetContext;
      dmutils.CustomQueryAgent.ParamTransform.AddSQLTextContext(sqltext);
      dmutils.CustomQueryAgent.ParamTransform.AddParametersFromDataSet(f.ParamBuffer);
      reportdata := dmutils.CustomQueryAgent.GetList(result_state, errormessage);
      if not result_state then
      begin
        ShowMessage('������ ���������� ������� ������: '+errormessage);
      end else
      begin
        Grid.Columns.Clear;
        MemData.Close;
        Memdata.EmptyTable;
        MemData.LoadFromDataSet(reportdata, 0, lmCopyStructureOnly, false);
        MemData.LoadFromDataSet(reportdata, 0, lmAppend, false);
      end;
      columns := GetColumnsDataSet(luReport.KeyValue);
      GenerateColumns(columns);
    end;
  finally
    MemData.EnableControls;
    Grid.Show;
    btUpload.Enabled := true;
    f.Free;
  end;
end;

function TFormUserReports.GetColumnsDataSet(ReportId: Variant): TDataSet;
var errormessage: string; result_state: boolean;
begin
  with dmreportcolumns.CrudAgent do
  begin
    ParamTransform.ResetContext;
    ParamTransform.AddPatternContext('/*wherefilter*/','report_id = :id');
    ParamTransform.AddParameterContext('id', ftInteger, ReportId);
    result := GetList(result_state, errormessage);
  end;
end;

function TFormUserReports.GetCrudAgent(getlist_method: string): TFMACRUDAgent;
begin
  if getlist_method = 'calc_periods' then result := dmcalcperiods.CrudAgent
  else if getlist_method = 'fire_cases' then result := dmfirecases.CrudAgent
  else if getlist_method = 'departments' then result := dmdepartments.CrudAgent
  else if getlist_method = 'workers' then result := dmworkers.CrudAgent
  else if getlist_method = 'payments' then result := dmpayments.CrudAgent
  else
    result := nil;
end;

function TFormUserReports.GetParamsDataSet(ReportId: Variant): TDataSet;
var errormessage: string; result_state: boolean;
begin
  with dmreportparams.CrudAgent do
  begin
    ParamTransform.ResetContext;
    ParamTransform.AddPatternContext('/*wherefilter*/','report_id = :id');
    ParamTransform.AddParameterContext('id', ftInteger, ReportId);
    result := GetList(result_state, errormessage);
  end;
end;

procedure TFormUserReports.luReportKeyValueChange(Sender: TObject);
begin
  if luReport.KeyValue = null then
    btPerform.Enabled := false
 else
    btPerform.Enabled := true;
end;


function TFormUserReports.MakeControl(param_id: integer; code, caption, param_type: string; param_length, param_decimals: integer;
getlist_method, display_field_name, key_field_name: string; master_param_id: integer; filter_expression: string;
form: TFormEditReportParams): integer;
var c: TCustomDBEditEh;
begin

  result := 0;

  if param_type = 'String' then
  begin
    c := TDbEditEh.Create(form);
  end else
  if param_type = 'Number' then
  begin
    c := TDbNumberEditEh.Create(form);
  end else
  if param_type = 'Date' then
  begin
    c := TDbDateTimeEditEh.Create(form);
  end else
  if param_type = 'List' then
  begin
    c := TFMALookUp.Create(form);
    TFMALookUp(c).CrudAgent := GetCrudAgent(getlist_method);
    TFMALookUp(c).KeyFieldName := key_field_name;
    TFMALookUp(c).DisplayFieldName := display_field_name;
    TFMALookUp(c).OnBeforeGetList := form.OnBeforeGetList;
    if master_param_id>0 then
    form.MasterFilterList.Add(TFMALookUp(c), form.MasterFilterSet(master_param_id, filter_expression));
  end else
  begin
    ShowMessage('������� ��������� �������� ������.');
    exit;
  end;

  if Assigned(c) then
  begin
    c.Name := 'ed'+UpperCase(code);
    c.ControlLabel.Caption := caption;
    c.ControlLabelLocation.Position := lpLeftCenterEh;
    c.ControlLabel.Visible := true;
    c.DataField := code;
    c.DataSource := form.dsLocal;
    c.Top := 1000;
    c.Parent := form;
    c.Align := alTop;
    c.AlignWithMargins := true;
    c.Margins.Top := 10;
    c.Margins.Left := 200;
    c.Margins.Bottom := 10;
    c.Margins.Right := 10;
    c.Show;
    form.ControlList.Add(param_id, c);
    result := c.Height+20;
  end;

end;


procedure TFormUserReports.PlaceParamControls(params: TDataSet; form: TFormEditReportParams);
var h: integer;
begin
  params.First;
  h := 50;
  form.ControlList.Clear;
  while not params.Eof do
  begin
    h := h + MakeControl(
      params.FieldByName('id').AsInteger,
      params.FieldByName('code').AsString,
      params.FieldByName('caption').AsString,
      params.FieldByName('param_type').AsString,
      params.FieldByName('param_length').AsInteger,
      params.FieldByName('param_decimals').AsInteger,
      params.FieldByName('getlist_method').AsString,
      params.FieldByName('display_field_name').AsString,
      params.FieldByName('key_field_name').AsString,
      params.FieldByName('master_param_id').AsInteger,
      params.FieldByName('filter_expression').AsString,
      form
    );
    params.Next;
  end;
  form.AutoSize := false;
  form.Height := form.plBottom.Height + h;
end;

function TFormUserReports.PrepareParamForm(params: TDataSet): TFormEditReportParams;
begin
  Application.CreateForm(TFormEditReportParams, result);
  result.ParamBuffer.Close;
  while not params.Eof do
  begin
    result.ParamBuffer.FieldDefs.Add(
        params.FieldByName('code').AsString,
        ftString,
        params.FieldByName('param_length').AsInteger,
        false
      );
    params.Next;
  end;
  result.ParamBuffer.CreateDataSet;
  result.ParamBuffer.Open;
  result.ParamBuffer.Append;
end;

procedure TFormUserReports.sbCloseClick(Sender: TObject);
begin
  Grid.Hide;
  btUpload.Enabled := false;
end;

end.
