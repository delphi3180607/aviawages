unit InputGroup;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, PeriodGroup, Vcl.ExtCtrls, FMAVertPages,
  Vcl.WinXPickers, Vcl.StdCtrls, FormsGroup, InitStated;

type
  TFormInputGroup = class(TFormPeriodGroup)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormInputGroup: TFormInputGroup;

implementation

{$R *.dfm}

uses FireCases, Fightings, Jumps, Flights, OtherPays, CommonPays;

procedure TFormInputGroup.FormCreate(Sender: TObject);
begin
  inherited;
  vpSections.AddForm(FormFireCases);
  vpSections.AddForm(FormFightings);
  vpSections.AddForm(FormJumps);
  vpSections.AddForm(FormFlights);
  vpSections.AddForm(FormOtherPays);
  vpSections.AddForm(FormCommonPays);
  vpSections.ActivatePage(0);
end;

end.
