object FormFormsGroup: TFormFormsGroup
  Left = 0
  Top = 0
  Caption = 'FormFormsGroup'
  ClientHeight = 439
  ClientWidth = 612
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  OnShow = FormShow
  TextHeight = 15
  object vpSections: TFMAVertPages
    Left = 0
    Top = 0
    Width = 612
    Height = 439
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    ShowCaption = False
    TabOrder = 0
    ShowButtons = True
    OnChangeActiveForm = vpSectionsChangeActiveForm
  end
end
