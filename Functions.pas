unit Functions;

interface

uses System.Classes, Vcl.Forms, Splash,
DB, DBGridEh, ComObj, Vcl.Controls, System.Variants, System.SysUtils, Dialogs;

procedure PrepareForm(InstanceClass: TComponentClass; var Reference);
procedure ExportExcel(grid: TDbGridEh; title: string);

implementation

procedure PrepareForm(InstanceClass: TComponentClass; var Reference);
begin
  Application.CreateForm(InstanceClass, Reference);
  FormSplash.pgLoad.Position := FormSplash.pgLoad.Position+1;
  Application.ProcessMessages;
end;

procedure ExportExcel(grid: TDbGridEh; title: string);
var i,j,index: Integer; ds: TDataSet;
ExcelApp,sheet: Variant;
Range, Cell1, Cell2, ArrayData: Variant;
BeginCol, BeginRow: integer;
RowCount, ColCount : integer;
begin
 Screen.Cursor := crHourGlass;
 try
   Application.ProcessMessages;
   ds := grid.DataSource.DataSet;
   ds.DisableControls;
   ExcelApp := CreateOleObject('Excel.Application');
   ExcelApp.Visible := False;
   ExcelApp.WorkBooks.Add(-4167);
   ExcelApp.WorkBooks[1].WorkSheets[1].name := 'Export';
   sheet:=ExcelApp.WorkBooks[1].WorkSheets['Export'];

   sheet.cells[1,1] := title;
   sheet.cells[1,1].Font.Bold:=1;
   sheet.cells[1,1].Font.Size:=14;

   for i := 1 to grid.Columns.Count do
   begin
     sheet.columns.columns[i].ColumnWidth := IntToStr(trunc(grid.Columns[i-1].Width/7));
     if (grid.Columns[i-1].Field.DataType = ftBCD) then
       sheet.columns.columns[i].NumberFormatLocal := '# ##0,00;-# ##0,00';
     sheet.columns.columns[i].WrapText:= true;
     sheet.cells[2,i] := grid.Columns[i-1].Title.Caption;
     sheet.cells[2,i].Interior.Color :=$00DDFFFF;
     sheet.cells[2,i].WrapText := true;
     sheet.cells[2,i].Borders.LineStyle := 1;
     sheet.cells[2,i].Borders.Weight := 2;
     sheet.cells[2,i].Font.Bold := true;
     sheet.cells[2,i].VerticalAlignment := -4108;
     sheet.cells[2,i].HorizontalAlignment := -4108;
   end;

   sheet.cells[1,1].WrapText:= false;

   index := 3;
   ds.First;

   BeginRow := 3;
   BeginCol := 1;

   RowCount := ds.RecordCount;
   ColCount := grid.Columns.Count;

  // ������� ���������� ������, ������� �������� ��������� �������
  ArrayData := VarArrayCreate([1, RowCount, 1, ColCount], varVariant);

   // ��������� ������
   for i:=1 to  ds.RecordCount do
   begin
      for j:=1 to grid.Columns.Count do
      begin
        try
          if grid.Columns[j-1].FieldName<>'' then
           if (grid.Columns[j-1].Field.DataType = ftBCD) then
             ArrayData[i, j] := ds.FieldByName(grid.Columns[j-1].FieldName).Value
           else
            ArrayData[i, j] := ds.FieldByName(grid.Columns[j-1].FieldName).AsString;
        except
         //
        end;
      end;
      inc(index);
      ds.Next;
   end;

  // ����� ������� ������ �������, � ������� ����� �������� ������
    Cell1 := sheet.Cells[BeginRow, BeginCol];

  // ������ ������ ������ �������, � ������� ����� �������� ������
    Cell2 := sheet.Cells[BeginRow  + RowCount - 1, BeginCol + ColCount - 1];

  // �������, � ������� ����� �������� ������
    Range := sheet.Range[Cell1, Cell2];

  // � ��� � ��� ����� ������
    // ������� ������� ����������� ����������
    Range.Value := ArrayData;
    Range.Borders.LineStyle:=1;
    Range.Borders.Weight:=2;
    Range.VerticalAlignment := -4108;

    ExcelApp.Visible := true;
    ds.EnableControls;

 except
    on E : Exception do begin
      ds.EnableControls;
      ShowMessage('������: '+E.Message);
    end;
 end;
 Screen.Cursor := crDefault;
end;

end.
