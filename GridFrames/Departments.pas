unit Departments;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, InitStated, BaseSimpleGrid, dmudepartments,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.StdCtrls, Vcl.ExtCtrls,
  MemTableDataEh, MemTableEh;

type
  TFormDepartments = class(TFormInitStated)
    FrameDepartments: TFrameBaseSimpleGrid;
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormDepartments: TFormDepartments;

implementation

{$R *.dfm}



{ TFormDepartments }

procedure TFormDepartments.Init;
begin
  if IsInited then exit;
  FrameDepartments.CRUDDisp.CrudAgent.ParamTransform.ResetContext;
  if not FrameDepartments.CRUDDisp.GetList(nil,errormessage) then
  begin
    MessageDlg(errormessage, mtError, [mbOK], 0);
    exit;
  end;
  inherited;
end;

end.
