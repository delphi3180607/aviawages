unit Reports;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, InitStated, MemTableDataEh, Data.DB,
  MemTableEh, Vcl.StdCtrls, Vcl.ExtCtrls, BaseGrid, Vcl.ComCtrls,
  dmureports, dmureportcolumns, dmureportparams,
  EditReport, EditReportColumn, EditReportParam, Vcl.Menus,
  Xml.xmldom, Xml.XMLIntf, Xml.XMLDoc;

type
  TFormReports = class(TFormInitStated)
    FrameReports: TFrameBaseGrid;
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    TabSheet1: TTabSheet;
    FrameColumns: TFrameBaseGrid;
    FrameParams: TFrameBaseGrid;
    od: TOpenDialog;
    N100: TMenuItem;
    N101: TMenuItem;
    N201: TMenuItem;
    procedure FrameColumnsCRUDDispBeforeInsert(Sender: TObject);
    procedure FrameReportsMemDataAfterScroll(DataSet: TDataSet);
    procedure FrameParamsCRUDDispBeforeInsert(Sender: TObject);
    procedure N101Click(Sender: TObject);
    procedure N201Click(Sender: TObject);
    procedure FrameParamsCRUDDispBeforeGetList(Sender: TObject);
    procedure FrameColumnsCRUDDispBeforeGetList(Sender: TObject);
  private
    xmlReport: TXMLDocument;
    function DataSetToXMLNode(xmlDoc: TXMLDocument; dataset: TDataSet; node_name: string): IXMLNode;
    procedure XMLNodeToDataSet(var dataset: TMemTableEh; xmlNode: IXMLNode);
    procedure SaveReport(filename: string);
    procedure RestoreReport(filename: string);
  public
    { Public declarations }
  end;

var
  FormReports: TFormReports;

implementation

{$R *.dfm}

procedure TFormReports.FrameColumnsCRUDDispBeforeGetList(Sender: TObject);
begin
  FrameColumns.CRUDDisp.ApplyMasterDetailParams(FrameReports.CRUDDisp, 'report_id', 'id');
end;

procedure TFormReports.FrameColumnsCRUDDispBeforeInsert(Sender: TObject);
begin
  FrameColumns.MemData.FieldByName('report_id').Value := FrameReports.MemData.FieldByName('id').Value;
end;

procedure TFormReports.FrameParamsCRUDDispBeforeGetList(Sender: TObject);
begin
  FrameParams.CRUDDisp.ApplyMasterDetailParams(FrameReports.CRUDDisp, 'report_id', 'id');
end;

procedure TFormReports.FrameParamsCRUDDispBeforeInsert(Sender: TObject);
begin
  FrameParams.MemData.FieldByName('report_id').Value := FrameReports.MemData.FieldByName('id').Value;
end;

procedure TFormReports.FrameReportsMemDataAfterScroll(DataSet: TDataSet);
begin
  FrameParams.acRefresh.Execute;
  FrameColumns.acRefresh.Execute;
end;

procedure TFormReports.N101Click(Sender: TObject);
begin
  if od.Execute then
  begin
    SaveReport(od.FileName);
  end;
end;

procedure TFormReports.N201Click(Sender: TObject);
begin
  if od.Execute then
  begin
    RestoreReport(od.FileName);
  end;
end;

procedure TFormReports.RestoreReport(filename: string);
var XMLDoc: TXMLDocument; node_root, node: IXMLNode; errormessage: string;
begin

  try
    XMLDoc := TXMLDocument.Create(self);
    XMLDoc.LoadFromFile(filename);
  except
    MessageDlg('�� ������� ��������� ���� '+filename, mtError, [mbOK], 0);
    exit;
  end;

  node_root := XMLDoc.ChildNodes.FindNode('report');

  if node_root = nil then
  begin
    MessageDlg('���� '+filename+' �� �������� ������ �������� ������.', mtError, [mbOK], 0);
    exit;
  end;

  with FrameReports do
  begin
    MemData.Edit;
    XMLNodeToDataSet(MemData, node_root);
    CRUDDisp.InnerSave;
  end;

  node := node_root.ChildNodes.FindNode('parameters');
  node := node.ChildNodes.FindNode('parameter');
  with FrameParams do
  begin
     while not (node = nil) do
     begin
        MemData.Append;
        XMLNodeToDataSet(MemData, node);
        CRUDDisp.InnerInsert;
        node := node.NextSibling;
     end;
     CRUDDisp.GetList(nil, errormessage);
  end;

  node := node_root.ChildNodes.FindNode('columns');
  node := node.ChildNodes.FindNode('column');
  with FrameColumns do
  begin
     while not (node = nil) do
     begin
        MemData.Append;
        XMLNodeToDataSet(MemData, node);
        CRUDDisp.InnerInsert;
        node := node.NextSibling;
     end;
     CRUDDisp.GetList(nil, errormessage);
  end;

end;


procedure TFormReports.SaveReport(filename: string);
var node_root, node_spec, node: IXMLNode;
begin
  xmlReport := TXMLDocument.Create(self);
  xmlReport.Active := True;
  node_root := DataSetToXMLNode(xmlReport, FrameReports.MemData, 'report');
  xmlReport.ChildNodes.Add(node_root);

  node_spec := xmlReport.CreateElement('parameters', '');
  node_root.ChildNodes.Add(node_spec);
  with FrameParams do
  begin
    MemData.First;
    while not MemData.Eof do
    begin
      node := DataSetToXMLNode(xmlReport, MemData, 'parameter');
      node_spec.ChildNodes.Add(node);
      MemData.Next;
    end;
  end;

  node_spec := xmlReport.CreateElement('columns', '');
  node_root.ChildNodes.Add(node_spec);
  with FrameColumns do
  begin
    MemData.First;
    while not MemData.Eof do
    begin
      node := DataSetToXMLNode(xmlReport, MemData, 'column');
      node_spec.ChildNodes.Add(node);
      MemData.Next;
    end;
  end;

  xmlReport.SaveToFile(filename);
  ShowMessage('�������� ������ ��������� �������.');
  xmlReport.Free;
end;

function TFormReports.DataSetToXMLNode(xmlDoc: TXMLDocument; dataset: TDataSet; node_name: string): IXMLNode;
var i: integer;
begin
  result := xmlDoc.CreateElement(node_name, '');
  for i := 0 to dataset.FieldCount-1 do
  begin
    result.Attributes[dataset.Fields[i].FieldName] := dataset.Fields[i].AsString;
  end;
end;

procedure TFormReports.XMLNodeToDataSet(var dataset: TMemTableEh; xmlNode: IXMLNode);
var i: integer;
begin
  for i := 0 to dataset.FieldCount-1 do
  begin
    if dataset.Fields[i].FieldName='id' then continue;
    if xmlNode.Attributes[dataset.Fields[i].FieldName]='' then
      dataset.Fields[i].Value := null
    else
    begin
      dataset.Fields[i].Value := xmlNode.Attributes[dataset.Fields[i].FieldName];
    end;
  end;
end;


end.
