inherited FormPayments: TFormPayments
  Caption = #1042#1099#1087#1083#1072#1090#1099
  ClientHeight = 538
  ClientWidth = 881
  ExplicitWidth = 897
  ExplicitHeight = 577
  TextHeight = 15
  object Splitter1: TSplitter [0]
    Left = 294
    Top = 0
    Width = 5
    Height = 497
    ExplicitLeft = 300
    ExplicitHeight = 538
  end
  inherited pl1: TPanel
    Top = 497
    Width = 881
    TabOrder = 1
    ExplicitTop = 497
    ExplicitWidth = 881
    inherited btOk: TButton
      Left = 678
      ExplicitLeft = 678
    end
    inherited btCancel: TButton
      Left = 781
      ExplicitLeft = 781
    end
  end
  inline FrameCatalogs: TFrameBaseTree [2]
    Left = 0
    Top = 0
    Width = 294
    Height = 497
    Align = alLeft
    TabOrder = 0
    ExplicitWidth = 294
    ExplicitHeight = 497
    inherited plTitleDetails: TPanel
      Width = 294
      Caption = #1050#1072#1090#1072#1083#1086#1075#1080
      ExplicitWidth = 294
    end
    inherited tvData: TVirtualStringTree
      Width = 288
      Height = 471
      ExplicitWidth = 288
      ExplicitHeight = 471
    end
    inherited CRUDDisp: TFMACRUDDisp
      CrudAgent = dmCatalogs.CrudAgent
    end
  end
  object plAll: TPanel
    Left = 299
    Top = 0
    Width = 582
    Height = 497
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Splitter2: TSplitter
      Left = 0
      Top = 259
      Width = 582
      Height = 5
      Cursor = crVSplit
      Align = alBottom
      ExplicitTop = 260
      ExplicitWidth = 586
    end
    inline FramePayments2Payments: TFrameBaseGrid
      Left = 0
      Top = 264
      Width = 582
      Height = 233
      Align = alBottom
      TabOrder = 0
      ExplicitTop = 264
      ExplicitWidth = 582
      ExplicitHeight = 233
      inherited dgGrid: TDBGridEh
        Width = 576
        Height = 184
        Columns = <
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'payment_code'
            Footers = <>
            Title.Caption = #1050#1086#1076' '#1074#1099#1087#1083#1072#1090#1099
            Width = 189
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'payment_name'
            Footers = <>
            Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1074#1099#1087#1083#1072#1090#1099
            Width = 331
          end>
      end
      inherited tbTools: TToolBar
        Width = 576
        ExplicitWidth = 576
      end
      inherited plTitleDetails: TPanel
        Width = 576
        Caption = #1047#1072#1074#1080#1089#1080#1084#1099#1077' '#1074#1099#1087#1083#1072#1090#1099
        ExplicitWidth = 576
      end
      inherited IL: TPngImageList
        Bitmap = {}
      end
      inherited AL: TActionList
        Left = 164
      end
      inherited DataSource: TDataSource
        Left = 300
      end
      inherited CRUDDisp: TFMACRUDDisp
        Fields = <
          item
            Name = 'id'
            Precision = 0
            FieldNo = 1
            Size = 0
            DataType = ftInteger
            Attributes = [faHiddenCol]
          end
          item
            Name = 'parent_payment_id'
            Precision = 0
            FieldNo = 2
            Size = 39
            DataType = ftString
            Attributes = [faHiddenCol]
          end
          item
            Name = 'payment_id'
            Precision = 0
            FieldNo = 3
            Size = 39
            DataType = ftString
            Attributes = [faHiddenCol]
          end
          item
            Name = 'payment_code'
            Precision = 0
            FieldNo = 4
            Size = 102
            DataType = ftWideString
            Attributes = []
            LongCaption = #1042#1099#1087#1083#1072#1090#1072
            ShortCaption = #1050#1086#1076' '#1074#1099#1087#1083#1072#1090#1099
            KeyFieldName = 'payment_id'
          end
          item
            Name = 'payment_name'
            Precision = 0
            FieldNo = 5
            Size = 502
            DataType = ftWideString
            Attributes = []
            LongCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1074#1099#1087#1083#1072#1090#1099
            ShortCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1074#1099#1087#1083#1072#1090#1099
            KeyFieldName = 'payment_id'
          end>
        CrudAgent = dmPayments2Payments.CrudAgent
        EditFormDataSource = FormEditPayment2Payment.dsLocal
        OnBeforeGetList = FramePayments2PaymentsCRUDDispBeforeGetList
        OnBeforeInsert = FramePayments2PaymentsCRUDDispBeforeInsert
        Left = 374
      end
      inherited MemData: TMemTableEh
        FieldDefs = <
          item
            Name = 'id'
            DataType = ftInteger
          end
          item
            Name = 'parent_payment_id'
            DataType = ftString
            Size = 39
          end
          item
            Name = 'payment_id'
            DataType = ftString
            Size = 39
          end
          item
            Name = 'payment_code'
            DataType = ftWideString
            Size = 102
          end
          item
            Name = 'payment_name'
            DataType = ftWideString
            Size = 502
          end>
        StoreDefs = True
        Left = 227
      end
      inherited pmGridMenu: TPopupMenu
        Left = 224
        Top = 176
      end
    end
    inline FramePayments: TFrameBaseSimpleGrid
      Left = 0
      Top = 0
      Width = 582
      Height = 259
      Align = alClient
      TabOrder = 1
      ExplicitWidth = 582
      ExplicitHeight = 259
      inherited dgGrid: TDBGridEh
        Width = 576
        Height = 232
        Columns = <
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'num'
            Footers = <>
            Title.Caption = #1053#1086#1084#1077#1088
            Width = 75
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'code'
            Footers = <>
            Title.Caption = #1050#1086#1076
            Width = 150
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'name'
            Footers = <>
            Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
            Width = 391
          end>
      end
      inherited plTitleDetails: TPanel
        Width = 576
        Caption = #1042#1099#1087#1083#1072#1090#1099
        ExplicitWidth = 582
      end
      inherited MemData: TMemTableEh
        FieldDefs = <
          item
            Name = 'id'
            DataType = ftString
            Size = 39
          end
          item
            Name = 'catalog_id'
            DataType = ftString
            Size = 39
          end
          item
            Name = 'num'
            DataType = ftBCD
            Precision = 6
            Size = 4
          end
          item
            Name = 'code'
            DataType = ftWideString
            Size = 102
          end
          item
            Name = 'name'
            DataType = ftWideString
            Size = 502
          end>
        AfterScroll = FramePaymentsMemDataAfterScroll
      end
      inherited CRUDDisp: TFMACRUDDisp
        Fields = <
          item
            Name = 'id'
            Precision = 0
            FieldNo = 1
            Size = 39
            DataType = ftString
            Attributes = [faHiddenCol]
          end
          item
            Name = 'catalog_id'
            Precision = 0
            FieldNo = 2
            Size = 39
            DataType = ftString
            Attributes = [faHiddenCol]
          end
          item
            Name = 'num'
            Precision = 6
            FieldNo = 3
            Size = 0
            DataType = ftBCD
            Attributes = []
            LongCaption = #1053#1086#1084#1077#1088
            ShortCaption = #1053#1086#1084#1077#1088
          end
          item
            Name = 'code'
            Precision = 0
            FieldNo = 4
            Size = 102
            DataType = ftWideString
            Attributes = []
            LongCaption = #1050#1086#1076
            ShortCaption = #1050#1086#1076
          end
          item
            Name = 'name'
            Precision = 0
            FieldNo = 5
            Size = 502
            DataType = ftWideString
            Attributes = []
            LongCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
            ShortCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          end>
        CrudAgent = dmPayments.CrudAgent
      end
    end
  end
end
