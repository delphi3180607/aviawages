inherited FormPlanes: TFormPlanes
  Caption = #1042#1080#1076#1099' '#1042#1057
  ClientWidth = 622
  ExplicitWidth = 638
  TextHeight = 15
  inherited pl1: TPanel
    Width = 622
    TabOrder = 1
    ExplicitWidth = 622
    inherited btOk: TButton
      Left = 419
      ExplicitLeft = 419
    end
    inherited btCancel: TButton
      Left = 522
      ExplicitLeft = 522
    end
  end
  inline FrameGrid: TFrameBaseGrid [1]
    Left = 0
    Top = 0
    Width = 622
    Height = 406
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 622
    ExplicitHeight = 406
    inherited dgGrid: TDBGridEh
      Width = 616
      Height = 357
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'code'
          Footers = <>
          Title.Caption = #1050#1086#1076
          Width = 270
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'name'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          Width = 458
        end>
    end
    inherited tbTools: TToolBar
      Width = 616
      ExplicitWidth = 616
      inherited ToolButton13: TToolButton
        Visible = False
      end
    end
    inherited plTitleDetails: TPanel
      Width = 616
      ExplicitWidth = 616
    end
    inherited IL: TPngImageList
      Bitmap = {}
    end
    inherited CRUDDisp: TFMACRUDDisp
      Fields = <
        item
          Name = 'id'
          Precision = 0
          FieldNo = 1
          Size = 0
          DataType = ftInteger
          Attributes = [faHiddenCol]
        end
        item
          Name = 'code'
          Precision = 0
          FieldNo = 2
          Size = 162
          DataType = ftWideString
          Attributes = []
          LongCaption = #1050#1086#1076
          ShortCaption = #1050#1086#1076
        end
        item
          Name = 'name'
          Precision = 0
          FieldNo = 3
          Size = 502
          DataType = ftWideString
          Attributes = []
          LongCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          ShortCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
        end>
      CrudAgent = dmplanes.CrudAgent
      EditFormDataSource = FormEditPlane.dsLocal
    end
    inherited MemData: TMemTableEh
      FieldDefs = <
        item
          Name = 'id'
          DataType = ftInteger
        end
        item
          Name = 'code'
          DataType = ftWideString
          Size = 162
        end
        item
          Name = 'name'
          DataType = ftWideString
          Size = 502
        end
        item
          Name = 'created_at'
          DataType = ftTimeStamp
        end
        item
          Name = 'updated_at'
          DataType = ftTimeStamp
        end>
      StoreDefs = True
    end
  end
end
