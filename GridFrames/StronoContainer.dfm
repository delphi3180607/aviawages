inherited FormStornoContainer: TFormStornoContainer
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = #1054#1090#1084#1077#1085#1072'/'#1082#1086#1088#1088#1077#1082#1090#1080#1088#1086#1074#1082#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072' '#1079#1072' '#1087#1088#1086#1096#1083#1099#1081' '#1087#1077#1088#1080#1086#1076
  ClientHeight = 558
  ClientWidth = 909
  ExplicitWidth = 925
  ExplicitHeight = 597
  TextHeight = 15
  inherited pl1: TPanel
    Top = 517
    Width = 909
    ExplicitTop = 517
    ExplicitWidth = 909
    inherited btOk: TButton
      Left = 706
      ExplicitLeft = 706
    end
    inherited btCancel: TButton
      Left = 809
      ExplicitLeft = 809
    end
  end
  object plWorkSpace: TPanel [1]
    Left = 0
    Top = 73
    Width = 909
    Height = 444
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitTop = 89
    ExplicitHeight = 428
  end
  object plOperation: TPanel [2]
    Left = 0
    Top = 0
    Width = 909
    Height = 31
    Align = alTop
    TabOrder = 2
    object lbOperation: TLabel
      AlignWithMargins = True
      Left = 157
      Top = 4
      Width = 748
      Height = 23
      Align = alClient
      AutoSize = False
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = False
      StyleElements = [seFont, seBorder]
      ExplicitTop = 0
      ExplicitWidth = 754
      ExplicitHeight = 29
    end
    object Label1: TLabel
      AlignWithMargins = True
      Left = 4
      Top = 6
      Width = 147
      Height = 17
      Margins.Top = 5
      Align = alLeft
      Caption = #1042#1099#1087#1086#1083#1085#1103#1077#1084#1072#1103' '#1086#1087#1077#1088#1072#1094#1080#1103':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
  end
  object plTop: TPanel [3]
    Left = 0
    Top = 31
    Width = 909
    Height = 42
    Align = alTop
    BevelOuter = bvNone
    Color = 15584687
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 3
    StyleElements = [seFont, seBorder]
    ExplicitTop = 30
    ExplicitWidth = 838
    object Shape1: TShape
      Left = 0
      Top = 0
      Width = 909
      Height = 1
      Align = alTop
      Pen.Color = 10635825
      ExplicitWidth = 838
    end
    object plCalcPeriod: TPanel
      AlignWithMargins = True
      Left = 3
      Top = 6
      Width = 225
      Height = 33
      Margins.Top = 5
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object Label5: TLabel
        AlignWithMargins = True
        Left = 4
        Top = 7
        Width = 120
        Height = 17
        Margins.Top = 6
        Margins.Bottom = 0
        Align = alLeft
        Caption = #1056#1072#1089#1095#1077#1090#1085#1099#1081' '#1087#1077#1088#1080#1086#1076
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dtCalcPeriod: TDBDateTimeEditEh
        AlignWithMargins = True
        Left = 130
        Top = 4
        Width = 91
        Height = 25
        ControlLabel.Font.Charset = DEFAULT_CHARSET
        ControlLabel.Font.Color = clWindowText
        ControlLabel.Font.Height = -12
        ControlLabel.Font.Name = 'Segoe UI'
        ControlLabel.Font.Style = []
        ControlLabel.ParentFont = False
        Align = alClient
        BevelInner = bvNone
        BevelOuter = bvNone
        Ctl3D = True
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        Visible = True
        OnChange = dtCalcPeriodChange
        ExplicitLeft = 132
        ExplicitWidth = 89
        ExplicitHeight = 28
        EditFormat = 'MM/YYYY'
      end
    end
    object plPeriod: TPanel
      AlignWithMargins = True
      Left = 234
      Top = 6
      Width = 391
      Height = 33
      Margins.Top = 5
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 1
      object Label3: TLabel
        AlignWithMargins = True
        Left = 4
        Top = 7
        Width = 167
        Height = 17
        Margins.Top = 6
        Margins.Bottom = 0
        Align = alLeft
        Caption = #1055#1077#1088#1080#1086#1076' '#1086#1090#1073#1086#1088#1072' '#1089#1086#1073#1099#1090#1080#1081' '#1089
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        AlignWithMargins = True
        Left = 273
        Top = 7
        Width = 16
        Height = 17
        Margins.Top = 6
        Margins.Bottom = 0
        Align = alLeft
        Caption = #1087#1086
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dtBegin: TDBDateTimeEditEh
        AlignWithMargins = True
        Left = 177
        Top = 4
        Width = 90
        Height = 25
        Align = alLeft
        BevelInner = bvNone
        BevelOuter = bvNone
        Ctl3D = True
        DynProps = <>
        EditButton.Visible = False
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        Visible = True
        ExplicitLeft = 178
        EditFormat = 'DD/MM/YYYY'
      end
      object dtEnd: TDBDateTimeEditEh
        AlignWithMargins = True
        Left = 295
        Top = 4
        Width = 91
        Height = 25
        Align = alLeft
        BevelInner = bvNone
        BevelOuter = bvNone
        Ctl3D = True
        DynProps = <>
        EditButton.Visible = False
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
        Visible = True
        ExplicitLeft = 296
        EditFormat = 'DD/MM/YYYY'
      end
    end
  end
end
