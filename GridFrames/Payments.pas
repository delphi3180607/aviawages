unit Payments;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, InitStated, BaseTree, Vcl.ExtCtrls, DB, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, BaseGrid, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.StdCtrls, BaseSimpleGrid, dmucatalogs, dmupayments,
  dmuPayments2Payments, EditPayment2Payment, MemTableDataEh, MemTableEh, System.UITypes;

type
  TFormPayments = class(TFormInitStated)
    FrameCatalogs: TFrameBaseTree;
    Splitter1: TSplitter;
    plAll: TPanel;
    FramePayments2Payments: TFrameBaseGrid;
    Splitter2: TSplitter;
    FramePayments: TFrameBaseSimpleGrid;
    procedure FramePayments2PaymentsCRUDDispBeforeInsert(Sender: TObject);
    procedure FramePaymentsMemDataAfterScroll(DataSet: TDataSet);
    procedure FramePayments2PaymentsCRUDDispBeforeGetList(Sender: TObject);
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormPayments: TFormPayments;

implementation

{$R *.dfm}

{ TFormPayments }

procedure TFormPayments.FramePayments2PaymentsCRUDDispBeforeGetList(Sender: TObject);
begin
  FramePayments2Payments.CRUDDisp.ApplyMasterDetailParams(FramePayments.CRUDDisp, 'parent_payment_id', 'id');
end;

procedure TFormPayments.FramePayments2PaymentsCRUDDispBeforeInsert(Sender: TObject);
begin
  FramePayments2Payments.MemData.FieldByName('parent_payment_id').Value :=
  FramePayments.MemData.FieldByName('id').Value;
end;

procedure TFormPayments.FramePaymentsMemDataAfterScroll(DataSet: TDataSet);
begin
  FramePayments2Payments.acRefresh.Execute;
end;

procedure TFormPayments.Init;
begin
  if IsInited then exit;
  with FrameCatalogs do
  begin
    if not Assigned(CrudDisp.CrudAgent) then exit;
    key_field_name := 'id';
    parent_field_name := 'parent_id';
    display_field_name := 'name';
    CrudDisp.CrudAgent.ParamTransform.ResetContext;
    CrudDisp.CrudAgent.ParamTransform.AddPatternContext('/*wherefilter*/', 'parent_id is null');
    if not CRUDDisp.GetList(nil,errormessage) then
    begin
      MessageDlg(errormessage, mtError, [mbOK], 0);
      exit;
    end;
    start_id := MemData.FieldByName(key_field_name).Value;
    CrudDisp.CrudAgent.ParamTransform.ResetContext;
    CrudDisp.CrudAgent.ParamTransform.AddPatternContext('/*wherefilter*/', 'parent_id = :parent_id and name = ''������� � ���������''');
    CrudDisp.CrudAgent.ParamTransform.AddParameterContext('parent_id', ftGuid, start_id);
    if not CRUDDisp.GetList(nil,errormessage) then
    begin
      MessageDlg(errormessage, mtError, [mbOK], 0);
      exit;
    end;
    start_id := MemData.FieldByName(key_field_name).Value;
    Init;
  end;
  FramePayments.CRUDDisp.CrudAgent.ParamTransform.ResetContext;
  if not FramePayments.CRUDDisp.GetList(nil,errormessage) then
  begin
    MessageDlg(errormessage, mtError, [mbOK], 0);
    exit;
  end;
  IsInited := true;
end;

end.
