inherited FormDepartments: TFormDepartments
  Caption = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1103
  ClientWidth = 632
  ExplicitWidth = 648
  TextHeight = 15
  inherited pl1: TPanel
    Width = 632
    TabOrder = 1
    ExplicitWidth = 632
    inherited btOk: TButton
      Left = 431
      ExplicitLeft = 429
    end
    inherited btCancel: TButton
      Left = 534
      ExplicitLeft = 532
    end
  end
  inline FrameDepartments: TFrameBaseSimpleGrid [1]
    Left = 0
    Top = 0
    Width = 632
    Height = 406
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 632
    ExplicitHeight = 406
    inherited dgGrid: TDBGridEh
      Width = 626
      Height = 379
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'parent_code'
          Footers = <>
          Title.Caption = #1042#1099#1096#1077#1089#1090#1086#1103#1097#1077#1077' ('#1084#1085#1077#1084#1086#1082#1086#1076')'
          Width = 218
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'parent_name'
          Footers = <>
          Title.Caption = #1042#1099#1096#1077#1089#1090#1086#1103#1097#1077#1077' ('#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077')'
          Width = 409
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'code'
          Footers = <>
          Title.Caption = #1050#1086#1076
          Width = 135
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'name'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          Width = 393
        end>
    end
    inherited plTitleDetails: TPanel
      Width = 626
      ExplicitWidth = 632
    end
    inherited MemData: TMemTableEh
      FieldDefs = <
        item
          Name = 'id'
          DataType = ftString
          Size = 39
        end
        item
          Name = 'parent_id'
          DataType = ftString
          Size = 39
        end
        item
          Name = 'parent_code'
          DataType = ftWideString
          Size = 162
        end
        item
          Name = 'parent_name'
          DataType = ftWideString
          Size = 502
        end
        item
          Name = 'code'
          DataType = ftWideString
          Size = 162
        end
        item
          Name = 'name'
          DataType = ftWideString
          Size = 502
        end>
    end
    inherited CRUDDisp: TFMACRUDDisp
      Fields = <
        item
          Name = 'id'
          Precision = 0
          FieldNo = 1
          Size = 39
          DataType = ftString
          Attributes = [faHiddenCol]
        end
        item
          Name = 'parent_id'
          Precision = 0
          FieldNo = 2
          Size = 39
          DataType = ftString
          Attributes = [faHiddenCol]
        end
        item
          Name = 'parent_code'
          Precision = 0
          FieldNo = 3
          Size = 162
          DataType = ftWideString
          Attributes = []
          LongCaption = #1042#1099#1096#1077#1089#1090#1086#1103#1097#1077#1077' ('#1084#1085#1077#1084#1086#1082#1086#1076')'
          ShortCaption = #1042#1099#1096#1077#1089#1090#1086#1103#1097#1077#1077' ('#1084#1085#1077#1084#1086#1082#1086#1076')'
        end
        item
          Name = 'parent_name'
          Precision = 0
          FieldNo = 4
          Size = 502
          DataType = ftWideString
          Attributes = []
          LongCaption = #1042#1099#1096#1077#1089#1090#1086#1103#1097#1077#1077' ('#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077')'
          ShortCaption = #1042#1099#1096#1077#1089#1090#1086#1103#1097#1077#1077' ('#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077')'
        end
        item
          Name = 'code'
          Precision = 0
          FieldNo = 5
          Size = 162
          DataType = ftWideString
          Attributes = []
          LongCaption = #1050#1086#1076
          ShortCaption = #1050#1086#1076
        end
        item
          Name = 'name'
          Precision = 0
          FieldNo = 6
          Size = 502
          DataType = ftWideString
          Attributes = []
          LongCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          ShortCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
        end>
      CrudAgent = dmdepartments.CrudAgent
    end
  end
end
