unit PayLinks;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, InitStated, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.StdCtrls, Vcl.ExtCtrls, BaseVertGrid, EditPayLinks, dmuPayLinks,
  MemTableDataEh, MemTableEh;

type
  TFormPayLinks = class(TFormInitStated)
    FrameVertGrid: TFrameBaseVertGrid;
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormPayLinks: TFormPayLinks;

implementation

{$R *.dfm}

{ TFormPayLinks }

procedure TFormPayLinks.Init;
begin
  if not FrameVertGrid.CRUDDisp.GetList(nil,errormessage) then
  begin
    MessageDlg(errormessage, mtError, [mbOK], 0);
  end;
  inherited;
end;

end.
