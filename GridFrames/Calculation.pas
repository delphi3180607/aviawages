unit Calculation;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, InitStated, FireDAC.VCLUI.Controls,
  Vcl.ExtCtrls, BaseSimpleGrid, Vcl.ComCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FMACrudDisp, dmudepartments, dmuworkers, dmuaccurals,
  BaseTree, Vcl.StdCtrls, Vcl.Tabs, MemTableDataEh, MemTableEh,
  GridsEh, DBGridEh;

type
  TFormCalculation = class(TFormInitStated)
    plAll: TPanel;
    FrameWorkers: TFrameBaseSimpleGrid;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    plCalc: TPanel;
    btCalc: TButton;
    plBottom: TPanel;
    FrameCalculation: TFrameBaseSimpleGrid;
    btExport: TButton;
    tsFireCases: TTabControl;
    Splitter3: TSplitter;
    plLeft: TPanel;
    FrameDepsTree: TFrameBaseTree;
    cbAll: TCheckBox;
    procedure btCalcClick(Sender: TObject);
    procedure FrameCalculationCRUDDispAfterGetList(Sender: TObject);
    procedure FrameWorkersMemDataAfterScroll(DataSet: TDataSet);
    procedure FrameDepsTreeMemDataAfterScroll(DataSet: TDataSet);
    procedure FrameCalculationCRUDDispBeforeGetList(Sender: TObject);
    procedure cbAllClick(Sender: TObject);
    procedure FrameWorkersCRUDDispCheckState(Sender: TObject);
    procedure FrameWorkersCRUDDispBeforeGetList(Sender: TObject);
    procedure FrameDepsTreeCRUDDispBeforeGetList(Sender: TObject);
    procedure FrameDepsTreetvDataCheckStateChanging(Sender: TCustomTreeView;
      Node: TTreeNode; NewCheckState, OldCheckState: TNodeCheckState;
      var AllowChange: Boolean);
    procedure FrameDepsTreetvDataCustomDrawItem(Sender: TCustomTreeView;
      Node: TTreeNode; State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure btExportClick(Sender: TObject);
    procedure tsFireCasesChange(Sender: TObject);
    procedure FrameWorkersdgGridDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumnEh;
      State: TGridDrawState);
  private
    FUpdating: boolean;
    procedure ExportOne(nCalcPeriod: integer; sWorkerId: string);
  public
    procedure Init; override;
  end;

var
  FormCalculation: TFormCalculation;

implementation

uses dmuutils, dmucurrentperiod, dmucalcperiods;

{$R *.dfm}

{ TFormCalculation }

procedure TFormCalculation.btCalcClick(Sender: TObject);
begin
   if Application.MessageBox(PWideChar('����� ���������� ������ ����������.'+#13#10+'���������� ������ ����� ������!'+#13#10+'��������� ������?'),'��������', MB_YESNO)=IDYes then
   begin
    Screen.Cursor := crHourGlass;
    dmutils.smCalculate.Params[0].Value := context_data_module.CalcPeriodId;
    dmutils.smCalculate.ExecuteMethod;
    if not FrameWorkers.CRUDDisp.GetList(nil,errormessage) then
    begin
      Screen.Cursor := crDefault;
      MessageDlg(errormessage, mtError, [mbOK], 0);
      exit;
    end;
    if not FrameCalculation.CRUDDisp.GetList(nil,errormessage) then
    begin
      Screen.Cursor := crDefault;
      MessageDlg(errormessage, mtError, [mbOK], 0);
      exit;
    end;
    FrameDepsTree.Init;
    ShowMessage('������ ������� ��������.');
   end;
   FrameCalculation.CRUDDisp.CrudAgent.ParamTransform.ResetContext;
   FrameCalculation.CRUDDisp.GetList(nil, errormessage);
   FrameCalculation.MemData.Filter := '';
   FrameCalculation.MemData.Filtered := false;
   Screen.Cursor := crDefault;
end;

procedure TFormCalculation.btExportClick(Sender: TObject);
var i: integer;
begin
  if Application.MessageBox(PWideChar('��������� ������� ������ �� ������� ������ �� ���������� �������?'), '��������', MB_YESNO) = IDYes then
  begin
    if FrameWorkers.dgGrid.Selection.SelectionType = gstRecordBookmarks then
    begin
      for i := 0 to FrameWorkers.dgGrid.Selection.Rows.Count-1 do
      begin
        FrameWorkers.MemData.Bookmark := FrameWorkers.dgGrid.Selection.Rows[i];
        ExportOne(context_data_module.CalcPeriodId, FrameWorkers.MemData.FieldByName('id').AsString);
      end;
      ShowMessage('������� ��������.');
    end;

    if FrameWorkers.dgGrid.Selection.SelectionType = gstAll then
    begin
      FrameWorkers.MemData.First;
      while not FrameWorkers.MemData.Eof do
      begin
        ExportOne(context_data_module.CalcPeriodId, FrameWorkers.MemData.FieldByName('id').AsString);
        FrameWorkers.MemData.Next;
      end;
      ShowMessage('������� ��������.');
    end;
  end;
end;

procedure TFormCalculation.ExportOne(nCalcPeriod: integer; sWorkerId: string);
begin
  try
    Screen.Cursor := crHourGlass;
    Application.ProcessMessages;
    dmutils.smExport.Params.ParamByName('ncalcperiodid').Value := context_data_module.CalcPeriodId;
    dmutils.smExport.Params.ParamByName('sworkerid').Value := sWorkerId;
    dmutils.smExport.ExecuteMethod;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFormCalculation.cbAllClick(Sender: TObject);
begin
  inherited;
  FrameDepsTreeMemDataAfterScroll(FrameDepsTree.MemData);
  if not FrameWorkers.CRUDDisp.GetList(nil,errormessage) then
  begin
    MessageDlg(errormessage, mtError, [mbOK], 0);
  end;
end;

procedure TFormCalculation.FrameCalculationCRUDDispAfterGetList(Sender: TObject);
var old_fire_case_id: integer;
begin
  with FrameCalculation do
  begin
    FUpdating := true;
    MemData.DisableControls;
    MemData.First;
    old_fire_case_id := 0;
    tsFireCases.Tabs.Clear;
    tsFireCases.Tabs.Add('���');
    while not MemData.Eof do
    begin
      if (MemData.FieldByName('fire_case_id').AsInteger<>old_fire_case_id) then
      begin
        tsFireCases.Tabs.Add(MemData.FieldByName('fire_case_name').AsString);
        old_fire_case_id := MemData.FieldByName('fire_case_id').AsInteger;
      end;
      MemData.Next;
    end;
    MemData.EnableControls;
    FUpdating := false;
  end;
end;

procedure TFormCalculation.FrameCalculationCRUDDispBeforeGetList(Sender: TObject);
begin
  FrameCalculation.CRUDDisp.ApplyMasterDetailParams(FrameWorkers.CRUDDisp, 'worker_id', 'id');

  with FrameCalculation.CRUDDisp.CRUDAgent do
  begin
    ParamTransform.AddPatternContext('/*wherefilter*/','calc_period_id = :calc_period_id');
    ParamTransform.AddParameterContext('calc_period_id', ftInteger, context_data_module.CalcPeriodId);
  end;
end;

procedure TFormCalculation.FrameDepsTreeCRUDDispBeforeGetList(Sender: TObject);
begin
  inherited;
  with FrameDepsTree.CRUDDisp do
  begin
    CrudAgent.ParamTransform.AddParameterContext('calc_period_id', ftInteger, context_data_module.CalcPeriodId);
  end;
end;

procedure TFormCalculation.FrameDepsTreeMemDataAfterScroll(DataSet: TDataSet);
begin
  FrameWorkers.acRefresh.Execute;
end;

procedure TFormCalculation.FrameDepsTreetvDataCheckStateChanging(
  Sender: TCustomTreeView; Node: TTreeNode; NewCheckState,
  OldCheckState: TNodeCheckState; var AllowChange: Boolean);
begin
  inherited;
  AllowChange := FrameDepsTree.IsUpdating;
end;

procedure TFormCalculation.FrameDepsTreetvDataCustomDrawItem(
  Sender: TCustomTreeView; Node: TTreeNode; State: TCustomDrawState;
  var DefaultDraw: Boolean);
begin
  if Node.Checked then
  Sender.Canvas.Font.Style := [fsBold];
end;

procedure TFormCalculation.FrameWorkersCRUDDispBeforeGetList(Sender: TObject);
begin
  if cbAll.Checked then
    FrameWorkers.CRUDDisp.CrudAgent.ParamTransform.ResetContext
  else
    FrameWorkers.CRUDDisp.ApplyMasterDetailParams(FrameDepsTree.CRUDDisp, 'department_id', 'id');

  FrameWorkers.CRUDDisp.CrudAgent.ParamTransform.AddParameterContext('calc_period_id', ftInteger, context_data_module.CalcPeriodId);

end;

procedure TFormCalculation.FrameWorkersCRUDDispCheckState(Sender: TObject);
begin
  inherited;
  btCalc.Enabled := not dmcurrentperiod.IsPeriodClosed;
end;

procedure TFormCalculation.FrameWorkersdgGridDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
    if (not (gdSelected in State)) or (not FrameWorkers.dgGrid.Focused)
    then
    begin
      if FrameWorkers.MemData.FieldByName('summ_calc').AsFloat>0 then
        FrameWorkers.dgGrid.Canvas.Brush.Color := $00B5FE2E;
      FrameWorkers.dgGrid.DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end;

    if FrameWorkers.MemData.FieldByName('isdeleted').AsInteger>0 then
    if Column.FieldName = 'code' then
    begin
      FrameWorkers.dgGrid.Canvas.Brush.Color := clRed;
      FrameWorkers.dgGrid.DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end;

end;

procedure TFormCalculation.FrameWorkersMemDataAfterScroll(DataSet: TDataSet);
begin
  FrameCalculation.MemData.Filter := '';
  FrameCalculation.MemData.Filtered := false;
  FrameCalculation.acRefresh.Execute;
end;

procedure TFormCalculation.Init;
begin
  if not IsInited then
  begin
    context_data_module := dmcurrentperiod;
    FrameDepsTree.key_field_name := 'id';
    FrameDepsTree.parent_field_name := 'parent_id';
    FrameDepsTree.display_field_name := 'code_marked';
    FrameDepsTree.Init;
    if not FrameWorkers.CRUDDisp.GetList(nil,errormessage) then
    begin
      MessageDlg(errormessage, mtError, [mbOK], 0);
      exit;
    end;
    FrameDepsTree.tvData.Selected[FrameDepsTree.tvData.GetFirst] := true;
  end;
  inherited;
end;

procedure TFormCalculation.tsFireCasesChange(Sender: TObject);
var NewTab: integer;
begin
  inherited;
  if FUpdating then exit;
  NewTab := tsFireCases.TabIndex;
  FrameCalculation.MemData.Filter := '';
  FrameCalculation.MemData.Filtered := false;
  if NewTab>0 then
  FrameCalculation.MemData.Filter := 'fire_case_name='''+tsFireCases.Tabs[NewTab]+'''';
  FrameCalculation.MemData.Filtered := true;
end;

end.
