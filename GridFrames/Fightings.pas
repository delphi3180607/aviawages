unit Fightings;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, InitStated, Vcl.ExtCtrls, DB, FMACrudAgent,  BaseGrid,
  Vcl.StdCtrls, EditFighting, EditFightingDetail, BaseFireLinked, BaseSimpleGrid, dmufirecases,
  workers, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, EditFightingDays, System.DateUtils,
  MemTableDataEh, MemTableEh, System.UITypes, System.Actions, Vcl.ActnList, GridsEh, DBGridEh;

type
  TFormFightings = class(TFormBaseFireLinked)
    plAll: TPanel;
    FrameFightings: TFrameBaseGrid;
    plBottom: TPanel;
    FrameDetails: TFrameBaseGrid;
    Splitter1: TSplitter;
    procedure FrameDetailsCRUDDispBeforeInsert(Sender: TObject);
    procedure FrameFightingsCRUDDispBeforeInsert(Sender: TObject);
    procedure FrameFightingsCRUDDispBeforeGetList(Sender: TObject);
    procedure FrameFightingsacMultyExecute(Sender: TObject);
    procedure FrameFightingsCRUDDispBeforeMultipleInsert(Sender: TObject);
    procedure FrameFightingsCRUDDispMultipleInsert(Sender: TObject);
    procedure FrameFireLinkedMemDataAfterScroll(DataSet: TDataSet);
    procedure FrameFightingsMemDataAfterScroll(DataSet: TDataSet);
    procedure FrameFightingsCRUDDispAfterInsert(Sender: TObject);
    procedure cbAllClick(Sender: TObject);
    procedure FrameDetailsCRUDDispBeforeGetList(Sender: TObject);
    procedure FrameFightingsdgGridDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumnEh;
      State: TGridDrawState);
  private
    { Private declarations }
  public
    procedure InsertStronoDocument; override;
  end;

var
  FormFightings: TFormFightings;

implementation

uses dmufightings, dmucurrentperiod;

{$R *.dfm}

procedure TFormFightings.cbAllClick(Sender: TObject);
begin
  FrameFireLinkedMemDataAfterScroll(FrameFireLinked.MemData);
  if not FrameFightings.CRUDDisp.GetList(nil,errormessage) then
  begin
    MessageDlg(errormessage, mtError, [mbOK], 0);
  end;
end;

procedure TFormFightings.FrameDetailsCRUDDispBeforeGetList(Sender: TObject);
begin
  FrameDetails.CRUDDisp.ApplyMasterDetailParams(FrameFightings.CRUDDisp, 'fighting_id', 'id');
end;

procedure TFormFightings.FrameDetailsCRUDDispBeforeInsert(Sender: TObject);
begin
  inherited;
  FrameDetails.MemData.FieldByName('fighting_id').Value := FrameFightings.MemData.FieldByName('id').Value;
end;

procedure TFormFightings.FrameFightingsacMultyExecute(Sender: TObject);
begin
  FrameFightings.DoMutipleAdd('worker_id','id',FormWorkers.FrameWorkers.CRUDDisp);
end;

procedure TFormFightings.FrameFightingsCRUDDispAfterInsert(Sender: TObject);
var date_from, date_to: TDateTime;
begin
  with FormEditFightingDays do
  begin
    MemData.Close;
    MemData.Open;
    MemData.EmptyDataSet;
    date_from := FrameFightings.MemData.FieldByName('date_from').AsDateTime;
    date_to := FrameFightings.MemData.FieldByName('date_to').AsDateTime;
    if date_to < date_from then
      date_to := date_from;
    while date_from<=date_to do
    begin
      MemData.Append;
      MemData.FieldByName('day').Value := date_from;
      MemData.Post;
      date_from := date_from+1;
    end;
    MemData.First;
    ShowModal;
    if ModalResult = mrOk then
    begin
      MemData.First;
      while not MemData.Eof do
      begin
        FrameDetails.CRUDDisp.InsertFromSource(MemData);
        MemData.Next;
      end;
    end;
  end;
end;

procedure TFormFightings.FrameFightingsCRUDDispBeforeGetList(Sender: TObject);
begin
  if cbAll.Checked then
    FrameFightings.CRUDDisp.CrudAgent.ParamTransform.ResetContext
  else
    FrameFightings.CRUDDisp.ApplyMasterDetailParams(FrameFireLinked.CRUDDisp, 'fire_case_id', 'id');

  with FrameFightings.CRUDDisp.CRUDAgent do
  begin
    ParamTransform.AddPatternContext('/*wherefilter*/','calc_period_id = :calc_period_id');
    ParamTransform.AddParameterContext('calc_period_id', ftInteger, context_data_module.CalcPeriodId);
  end;
end;

procedure TFormFightings.FrameFightingsCRUDDispBeforeInsert(Sender: TObject);
begin
  inherited;
  FrameFightings.MemData.FieldByName('calc_period_id').Value := context_data_module.CalcPeriodId;
  if StornoFireCaseId>0 then
    FrameFightings.MemData.FieldByName('fire_case_id').Value := StornoFireCaseId
  else
    FrameFightings.MemData.FieldByName('fire_case_id').Value := FrameFireLinked.MemData.FieldByName('id').Value;
end;

procedure TFormFightings.FrameFightingsCRUDDispBeforeMultipleInsert(Sender: TObject);
var date_from, date_to: TDateTime;
begin
  with FormEditFightingDays do
  begin
    MemData.Open;
    MemData.EmptyDataSet;
    date_from := FrameFightings.MemData.FieldByName('date_from').AsDateTime;
    date_to := FrameFightings.MemData.FieldByName('date_to').AsDateTime;
    if date_to < date_from then
      date_to := date_from;
    while date_from<=date_to do
    begin
      MemData.Append;
      MemData.FieldByName('day').Value := date_from;
      MemData.Post;
      date_from := date_from+1;
    end;
    MemData.First;
    ShowModal;
    if ModalResult <> mrOk then
    FrameFightings.CRUDDisp.Terminate := true;
  end;
end;

procedure TFormFightings.FrameFightingsCRUDDispMultipleInsert(Sender: TObject);
begin
  with FormEditFightingDays do
  begin
    MemData.First;
    while not MemData.Eof do
    begin
      FrameDetails.CRUDDisp.InsertFromSource(MemData);
      MemData.Next;
    end;
  end;
end;

procedure TFormFightings.FrameFightingsdgGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
begin
   if (not (gdSelected in State)) or (not FrameFightings.dgGrid.Focused)
   then
   begin
     if FrameFightings.MemData.FieldByName('isstorno_num').AsInteger>0 then
        FrameFightings.dgGrid.Canvas.Brush.Color := $00D6D1FC;
      FrameFightings.dgGrid.DefaultDrawColumnCell(Rect, DataCol, Column, State);
   end;
end;

procedure TFormFightings.FrameFightingsMemDataAfterScroll(DataSet: TDataSet);
begin
  FrameDetails.acRefresh.Execute;
end;

procedure TFormFightings.FrameFireLinkedMemDataAfterScroll(DataSet: TDataSet);
begin
  FrameFightings.acRefresh.Execute;
end;

procedure TFormFightings.InsertStronoDocument;
begin
  inherited;
  FrameFightings.CRUDDisp.Insert;
end;

end.
