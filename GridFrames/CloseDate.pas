unit CloseDate;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, InitStated, Vcl.StdCtrls, Vcl.Mask,
  DBCtrlsEh, Data.DB, FMACrudDisp, Vcl.ExtCtrls;

type
  TFormCloseDate = class(TFormInitStated)
    dtCloseDate: TDBDateTimeEditEh;
    Button1: TButton;
    dsLocal: TDataSource;
    Bevel1: TBevel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormCloseDate: TFormCloseDate;

implementation

{$R *.dfm}

end.
