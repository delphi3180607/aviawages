inherited FormReports: TFormReports
  Caption = #1054#1090#1095#1077#1090#1099
  ClientHeight = 653
  ClientWidth = 946
  ExplicitWidth = 962
  ExplicitHeight = 692
  TextHeight = 15
  inherited pl1: TPanel
    Top = 612
    Width = 946
    ExplicitTop = 612
    ExplicitWidth = 946
    inherited btOk: TButton
      Left = 743
      ExplicitLeft = 743
    end
    inherited btCancel: TButton
      Left = 846
      ExplicitLeft = 846
    end
  end
  inline FrameReports: TFrameBaseGrid [1]
    Left = 0
    Top = 0
    Width = 946
    Height = 353
    Align = alTop
    TabOrder = 1
    ExplicitWidth = 946
    ExplicitHeight = 353
    inherited dgGrid: TDBGridEh
      Width = 940
      Height = 304
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'name'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          Width = 336
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'template'
          Footers = <>
          Title.Caption = #1064#1072#1073#1083#1086#1085
          Width = 197
        end>
    end
    inherited tbTools: TToolBar
      Width = 940
      ExplicitWidth = 940
      inherited ToolButton13: TToolButton
        Visible = False
      end
    end
    inherited plTitleDetails: TPanel
      Width = 940
      Caption = #1057#1087#1080#1089#1086#1082' '#1085#1072#1089#1090#1088#1072#1080#1074#1072#1077#1084#1099#1093' '#1086#1090#1095#1077#1090#1086#1074
      ExplicitWidth = 940
    end
    inherited IL: TPngImageList
      Bitmap = {}
    end
    inherited CRUDDisp: TFMACRUDDisp
      Fields = <
        item
          Name = 'id'
          Precision = 15
          FieldNo = 1
          Size = 0
          DataType = ftInteger
          Attributes = [faHiddenCol]
        end
        item
          Name = 'name'
          Precision = 0
          FieldNo = 2
          Size = 502
          DataType = ftWideString
          Attributes = []
          LongCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          ShortCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
        end
        item
          Name = 'template'
          Precision = 0
          FieldNo = 3
          Size = 502
          DataType = ftWideString
          Attributes = []
          LongCaption = #1064#1072#1073#1083#1086#1085
          ShortCaption = #1064#1072#1073#1083#1086#1085
        end
        item
          Name = 'prepare_command_sql'
          Precision = 0
          FieldNo = 4
          Size = 0
          DataType = ftWideMemo
          Attributes = []
          LongCaption = #1050#1086#1084#1072#1085#1076#1072' '#1087#1086#1076#1075#1086#1090#1086#1074#1082#1080
          ShortCaption = #1050#1086#1084#1072#1085#1076#1072' '#1087#1086#1076#1075#1086#1090#1086#1074#1082#1080
        end
        item
          Name = 'query_sql'
          Precision = 0
          FieldNo = 5
          Size = 0
          DataType = ftWideMemo
          Attributes = []
          LongCaption = #1047#1072#1087#1088#1086#1089' '#1076#1083#1103' '#1087#1086#1083#1091#1095#1077#1085#1080#1103' '#1086#1090#1095#1077#1090#1072
          ShortCaption = #1047#1072#1087#1088#1086#1089' '#1076#1083#1103' '#1087#1086#1083#1091#1095#1077#1085#1080#1103' '#1086#1090#1095#1077#1090#1072
        end>
      CrudAgent = dmReports.CrudAgent
      EditFormDataSource = FormEditReport.dsLocal
    end
    inherited MemData: TMemTableEh
      Active = True
      AfterScroll = FrameReportsMemDataAfterScroll
      object MemTableData: TMemTableDataEh
        object DataStruct: TMTDataStructEh
          object id: TMTNumericDataFieldEh
            FieldName = 'id'
            NumericDataType = fdtIntegerEh
            AutoIncrement = False
            DisplayLabel = 'id'
            DisplayWidth = 10
            currency = False
            Precision = 15
          end
          object name: TMTStringDataFieldEh
            FieldName = 'name'
            StringDataType = fdtWideStringEh
            DisplayLabel = 'name'
            DisplayWidth = 502
            Size = 502
            Transliterate = True
          end
          object template: TMTStringDataFieldEh
            FieldName = 'template'
            StringDataType = fdtWideStringEh
            DisplayLabel = 'template'
            DisplayWidth = 502
            Size = 502
            Transliterate = True
          end
          object prepare_command_sql: TMTBlobDataFieldEh
            FieldName = 'prepare_command_sql'
            DisplayLabel = 'prepare_command_sql'
            DisplayWidth = 10
            BlobType = ftWideMemo
            Transliterate = False
          end
          object query_sql: TMTBlobDataFieldEh
            FieldName = 'query_sql'
            DisplayLabel = 'query_sql'
            DisplayWidth = 10
            BlobType = ftWideMemo
            Transliterate = False
          end
        end
        object RecordsList: TRecordsListEh
        end
      end
    end
    inherited pmGridMenu: TPopupMenu
      inherited N3: TMenuItem [1]
      end
      inherited N2: TMenuItem [2]
      end
      object N100: TMenuItem [3]
        Caption = '-'
      end
      object N101: TMenuItem [4]
        Caption = #1042#1099#1075#1088#1091#1079#1080#1090#1100
        OnClick = N101Click
      end
      object N201: TMenuItem [5]
        Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100
        OnClick = N201Click
      end
      inherited N10: TMenuItem [10]
      end
      inherited N8: TMenuItem [11]
      end
      inherited Excel1: TMenuItem [12]
      end
      inherited N9: TMenuItem [13]
      end
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 353
    Width = 946
    Height = 259
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 2
    object TabSheet1: TTabSheet
      Caption = #1050#1086#1083#1086#1085#1082#1080
      inline FrameColumns: TFrameBaseGrid
        Left = 0
        Top = 0
        Width = 938
        Height = 229
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 938
        ExplicitHeight = 229
        inherited dgGrid: TDBGridEh
          Width = 932
          Height = 180
          Columns = <
            item
              AutoFitColWidth = False
              CellButtons = <>
              DynProps = <>
              EditButtons = <>
              FieldName = 'field_name'
              Footers = <>
              Title.Caption = #1048#1084#1103' '#1087#1086#1083#1103
              Width = 150
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              DynProps = <>
              EditButtons = <>
              FieldName = 'title'
              Footers = <>
              Title.Caption = #1047#1072#1075#1086#1083#1086#1074#1086#1082
              Width = 150
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              DynProps = <>
              EditButtons = <>
              FieldName = 'display_width'
              Footers = <>
              Title.Caption = #1064#1080#1088#1080#1085#1072
              Width = 150
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              DynProps = <>
              EditButtons = <>
              FieldName = 'display_align'
              Footers = <>
              Title.Caption = #1042#1099#1088#1072#1074#1085#1080#1074#1072#1085#1080#1077
              Width = 150
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              DynProps = <>
              EditButtons = <>
              FieldName = 'order_num'
              Footers = <>
              Title.Caption = #1055#1086#1088#1103#1076#1086#1082
              Width = 150
            end>
        end
        inherited tbTools: TToolBar
          Width = 932
          ExplicitWidth = 932
        end
        inherited plTitleDetails: TPanel
          Width = 932
          ExplicitWidth = 932
        end
        inherited IL: TPngImageList
          Bitmap = {}
        end
        inherited CRUDDisp: TFMACRUDDisp
          Fields = <
            item
              Name = 'id'
              Precision = 15
              FieldNo = 1
              Size = 0
              DataType = ftInteger
              Attributes = [faHiddenCol]
            end
            item
              Name = 'report_id'
              Precision = 15
              FieldNo = 2
              Size = 0
              DataType = ftInteger
              Attributes = [faHiddenCol]
            end
            item
              Name = 'field_name'
              Precision = 0
              FieldNo = 3
              Size = 162
              DataType = ftWideString
              Attributes = []
              LongCaption = #1048#1084#1103' '#1087#1086#1083#1103
              ShortCaption = #1048#1084#1103' '#1087#1086#1083#1103
            end
            item
              Name = 'title'
              Precision = 0
              FieldNo = 4
              Size = 162
              DataType = ftWideString
              Attributes = []
              LongCaption = #1047#1072#1075#1086#1083#1086#1074#1086#1082
              ShortCaption = #1047#1072#1075#1086#1083#1086#1074#1086#1082
            end
            item
              Name = 'display_width'
              Precision = 15
              FieldNo = 5
              Size = 0
              DataType = ftInteger
              Attributes = []
              LongCaption = #1064#1080#1088#1080#1085#1072
              ShortCaption = #1064#1080#1088#1080#1085#1072
            end
            item
              Name = 'display_align'
              Precision = 0
              FieldNo = 6
              Size = 42
              DataType = ftWideString
              Attributes = []
              LongCaption = #1042#1099#1088#1072#1074#1085#1080#1074#1072#1085#1080#1077
              ShortCaption = #1042#1099#1088#1072#1074#1085#1080#1074#1072#1085#1080#1077
            end
            item
              Name = 'order_num'
              Precision = 15
              FieldNo = 7
              Size = 0
              DataType = ftInteger
              Attributes = []
              LongCaption = #1055#1086#1088#1103#1076#1086#1082
              ShortCaption = #1055#1086#1088#1103#1076#1086#1082
            end>
          CrudAgent = dmReportColumns.CrudAgent
          EditFormDataSource = FormEditReportColumn.dsLocal
          OnBeforeGetList = FrameColumnsCRUDDispBeforeGetList
          OnBeforeInsert = FrameColumnsCRUDDispBeforeInsert
        end
        inherited MemData: TMemTableEh
          Active = True
          object MemTableData: TMemTableDataEh
            object DataStruct: TMTDataStructEh
              object id: TMTNumericDataFieldEh
                FieldName = 'id'
                NumericDataType = fdtIntegerEh
                AutoIncrement = False
                DisplayLabel = 'id'
                DisplayWidth = 10
                currency = False
                Precision = 15
              end
              object report_id: TMTNumericDataFieldEh
                FieldName = 'report_id'
                NumericDataType = fdtIntegerEh
                AutoIncrement = False
                DisplayLabel = 'report_id'
                DisplayWidth = 10
                currency = False
                Precision = 15
              end
              object field_name: TMTStringDataFieldEh
                FieldName = 'field_name'
                StringDataType = fdtWideStringEh
                DisplayLabel = 'field_name'
                DisplayWidth = 162
                Size = 162
                Transliterate = True
              end
              object title: TMTStringDataFieldEh
                FieldName = 'title'
                StringDataType = fdtWideStringEh
                DisplayLabel = 'title'
                DisplayWidth = 162
                Size = 162
                Transliterate = True
              end
              object display_width: TMTNumericDataFieldEh
                FieldName = 'display_width'
                NumericDataType = fdtIntegerEh
                AutoIncrement = False
                DisplayLabel = 'display_width'
                DisplayWidth = 10
                currency = False
                Precision = 15
              end
              object display_align: TMTStringDataFieldEh
                FieldName = 'display_align'
                StringDataType = fdtWideStringEh
                DisplayLabel = 'display_align'
                DisplayWidth = 42
                Size = 42
                Transliterate = True
              end
              object order_num: TMTNumericDataFieldEh
                FieldName = 'order_num'
                NumericDataType = fdtIntegerEh
                AutoIncrement = False
                DisplayLabel = 'order_num'
                DisplayWidth = 10
                currency = False
                Precision = 15
              end
            end
            object RecordsList: TRecordsListEh
            end
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099
      ImageIndex = 1
      inline FrameParams: TFrameBaseGrid
        Left = 0
        Top = 0
        Width = 938
        Height = 229
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 938
        ExplicitHeight = 229
        inherited dgGrid: TDBGridEh
          Width = 932
          Height = 180
          Columns = <
            item
              AutoFitColWidth = False
              CellButtons = <>
              DynProps = <>
              EditButtons = <>
              FieldName = 'code'
              Footers = <>
              Title.Caption = #1050#1086#1076' '#1087#1072#1088#1072#1084#1077#1090#1088#1072
              Width = 150
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              DynProps = <>
              EditButtons = <>
              FieldName = 'caption'
              Footers = <>
              Title.Caption = #1047#1072#1075#1086#1083#1086#1074#1086#1082
              Width = 150
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              DynProps = <>
              EditButtons = <>
              FieldName = 'param_type'
              Footers = <>
              Title.Caption = #1058#1080#1087' '#1087#1072#1088#1072#1084#1077#1090#1088#1072
              Width = 150
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              DynProps = <>
              EditButtons = <>
              FieldName = 'param_length'
              Footers = <>
              Title.Caption = #1044#1083#1080#1085#1072
              Width = 150
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              DynProps = <>
              EditButtons = <>
              FieldName = 'param_decimals'
              Footers = <>
              Title.Caption = #1044#1077#1089#1103#1090#1080#1095#1085#1099#1093' '#1079#1085#1072#1082#1086#1074
              Width = 150
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              DynProps = <>
              EditButtons = <>
              FieldName = 'getlist_method'
              Footers = <>
              Title.Caption = #1055#1086#1083#1085#1086#1077' '#1080#1084#1103' '#1084#1077#1090#1086#1076#1072' GETLIST'
              Width = 150
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              DynProps = <>
              EditButtons = <>
              FieldName = 'display_field_name'
              Footers = <>
              Title.Caption = #1048#1084#1103' '#1087#1086#1083#1103' '#1076#1083#1103' '#1086#1090#1086#1073#1088#1072#1078#1077#1085#1080#1103
              Width = 150
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              DynProps = <>
              EditButtons = <>
              FieldName = 'key_field_name'
              Footers = <>
              Title.Caption = #1048#1084#1103' '#1087#1086#1083#1103' '#1080#1076#1077#1085#1090#1080#1092#1080#1082#1072#1090#1086#1088#1072
              Width = 150
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              DynProps = <>
              EditButtons = <>
              FieldName = 'order_num'
              Footers = <>
              Title.Caption = #1055#1086#1088#1103#1076#1086#1082
              Width = 150
            end>
        end
        inherited tbTools: TToolBar
          Width = 932
          ExplicitWidth = 932
        end
        inherited plTitleDetails: TPanel
          Width = 932
          ExplicitWidth = 932
        end
        inherited IL: TPngImageList
          Bitmap = {}
        end
        inherited CRUDDisp: TFMACRUDDisp
          Fields = <
            item
              Name = 'id'
              Precision = 15
              FieldNo = 1
              Size = 0
              DataType = ftInteger
              Attributes = [faHiddenCol]
            end
            item
              Name = 'report_id'
              Precision = 15
              FieldNo = 2
              Size = 0
              DataType = ftInteger
              Attributes = [faHiddenCol]
            end
            item
              Name = 'code'
              Precision = 0
              FieldNo = 3
              Size = 102
              DataType = ftWideString
              Attributes = []
              LongCaption = #1050#1086#1076' '#1087#1072#1088#1072#1084#1077#1090#1088#1072
              ShortCaption = #1050#1086#1076' '#1087#1072#1088#1072#1084#1077#1090#1088#1072
            end
            item
              Name = 'caption'
              Precision = 0
              FieldNo = 4
              Size = 502
              DataType = ftWideString
              Attributes = []
              LongCaption = #1047#1072#1075#1086#1083#1086#1074#1086#1082
              ShortCaption = #1047#1072#1075#1086#1083#1086#1074#1086#1082
            end
            item
              Name = 'param_type'
              Precision = 0
              FieldNo = 5
              Size = 42
              DataType = ftWideString
              Attributes = []
              LongCaption = #1058#1080#1087' '#1087#1072#1088#1072#1084#1077#1090#1088#1072
              ShortCaption = #1058#1080#1087' '#1087#1072#1088#1072#1084#1077#1090#1088#1072
            end
            item
              Name = 'param_length'
              Precision = 15
              FieldNo = 6
              Size = 0
              DataType = ftInteger
              Attributes = []
              LongCaption = #1044#1083#1080#1085#1072
              ShortCaption = #1044#1083#1080#1085#1072
            end
            item
              Name = 'param_decimals'
              Precision = 15
              FieldNo = 7
              Size = 0
              DataType = ftInteger
              Attributes = []
              LongCaption = #1044#1077#1089#1103#1090#1080#1095#1085#1099#1093' '#1079#1085#1072#1082#1086#1074
              ShortCaption = #1044#1077#1089#1103#1090#1080#1095#1085#1099#1093' '#1079#1085#1072#1082#1086#1074
            end
            item
              Name = 'getlist_method'
              Precision = 0
              FieldNo = 8
              Size = 502
              DataType = ftWideString
              Attributes = []
              LongCaption = #1055#1086#1083#1085#1086#1077' '#1080#1084#1103' '#1084#1077#1090#1086#1076#1072' GETLIST'
              ShortCaption = #1055#1086#1083#1085#1086#1077' '#1080#1084#1103' '#1084#1077#1090#1086#1076#1072' GETLIST'
            end
            item
              Name = 'display_field_name'
              Precision = 0
              FieldNo = 9
              Size = 162
              DataType = ftWideString
              Attributes = []
              LongCaption = #1048#1084#1103' '#1087#1086#1083#1103' '#1076#1083#1103' '#1086#1090#1086#1073#1088#1072#1078#1077#1085#1080#1103
              ShortCaption = #1048#1084#1103' '#1087#1086#1083#1103' '#1076#1083#1103' '#1086#1090#1086#1073#1088#1072#1078#1077#1085#1080#1103
            end
            item
              Name = 'key_field_name'
              Precision = 0
              FieldNo = 10
              Size = 162
              DataType = ftWideString
              Attributes = []
              LongCaption = #1048#1084#1103' '#1087#1086#1083#1103' '#1080#1076#1077#1085#1090#1080#1092#1080#1082#1072#1090#1086#1088#1072
              ShortCaption = #1048#1084#1103' '#1087#1086#1083#1103' '#1080#1076#1077#1085#1090#1080#1092#1080#1082#1072#1090#1086#1088#1072
            end
            item
              Name = 'order_num'
              Precision = 15
              FieldNo = 11
              Size = 0
              DataType = ftInteger
              Attributes = []
              LongCaption = #1055#1086#1088#1103#1076#1086#1082
              ShortCaption = #1055#1086#1088#1103#1076#1086#1082
            end>
          CrudAgent = dmReportParams.CrudAgent
          EditFormDataSource = FormEditReportParam.dsLocal
          OnBeforeGetList = FrameParamsCRUDDispBeforeGetList
          OnBeforeInsert = FrameParamsCRUDDispBeforeInsert
        end
        inherited MemData: TMemTableEh
          Active = True
          object MemTableData: TMemTableDataEh
            object DataStruct: TMTDataStructEh
              object id: TMTNumericDataFieldEh
                FieldName = 'id'
                NumericDataType = fdtIntegerEh
                AutoIncrement = False
                DisplayLabel = 'id'
                DisplayWidth = 10
                currency = False
                Precision = 15
              end
              object report_id: TMTNumericDataFieldEh
                FieldName = 'report_id'
                NumericDataType = fdtIntegerEh
                AutoIncrement = False
                DisplayLabel = 'report_id'
                DisplayWidth = 10
                currency = False
                Precision = 15
              end
              object code: TMTStringDataFieldEh
                FieldName = 'code'
                StringDataType = fdtWideStringEh
                DisplayLabel = 'code'
                DisplayWidth = 102
                Size = 102
                Transliterate = True
              end
              object caption: TMTStringDataFieldEh
                FieldName = 'caption'
                StringDataType = fdtWideStringEh
                DisplayLabel = 'caption'
                DisplayWidth = 502
                Size = 502
                Transliterate = True
              end
              object param_type: TMTStringDataFieldEh
                FieldName = 'param_type'
                StringDataType = fdtWideStringEh
                DisplayLabel = 'param_type'
                DisplayWidth = 42
                Size = 42
                Transliterate = True
              end
              object param_length: TMTNumericDataFieldEh
                FieldName = 'param_length'
                NumericDataType = fdtIntegerEh
                AutoIncrement = False
                DisplayLabel = 'param_length'
                DisplayWidth = 10
                currency = False
                Precision = 15
              end
              object param_decimals: TMTNumericDataFieldEh
                FieldName = 'param_decimals'
                NumericDataType = fdtIntegerEh
                AutoIncrement = False
                DisplayLabel = 'param_decimals'
                DisplayWidth = 10
                currency = False
                Precision = 15
              end
              object getlist_method: TMTStringDataFieldEh
                FieldName = 'getlist_method'
                StringDataType = fdtWideStringEh
                DisplayLabel = 'getlist_method'
                DisplayWidth = 502
                Size = 502
                Transliterate = True
              end
              object display_field_name: TMTStringDataFieldEh
                FieldName = 'display_field_name'
                StringDataType = fdtWideStringEh
                DisplayLabel = 'display_field_name'
                DisplayWidth = 162
                Size = 162
                Transliterate = True
              end
              object key_field_name: TMTStringDataFieldEh
                FieldName = 'key_field_name'
                StringDataType = fdtWideStringEh
                DisplayLabel = 'key_field_name'
                DisplayWidth = 162
                Size = 162
                Transliterate = True
              end
              object order_num: TMTNumericDataFieldEh
                FieldName = 'order_num'
                NumericDataType = fdtIntegerEh
                AutoIncrement = False
                DisplayLabel = 'order_num'
                DisplayWidth = 10
                currency = False
                Precision = 15
              end
            end
            object RecordsList: TRecordsListEh
            end
          end
        end
      end
    end
  end
  inherited MainDataSet: TMemTableEh
    Left = 232
    Top = 592
  end
  object od: TOpenDialog
    Left = 480
    Top = 96
  end
end
