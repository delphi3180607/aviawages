inherited FormJumps: TFormJumps
  Caption = #1055#1088#1099#1078#1082#1080'/'#1057#1087#1091#1089#1082#1080
  ClientHeight = 582
  ClientWidth = 972
  ExplicitWidth = 988
  ExplicitHeight = 621
  TextHeight = 15
  inherited splFireEng: TSplitter
    Left = 430
    Height = 513
    ExplicitLeft = 371
    ExplicitHeight = 541
  end
  inherited FrameFireLinked: TFrameBaseGrid
    Width = 430
    Height = 513
    ExplicitWidth = 430
    ExplicitHeight = 513
    inherited dgGrid: TDBGridEh
      Width = 424
      Height = 464
    end
    inherited tbTools: TToolBar
      Width = 424
      ExplicitWidth = 424
      inherited ToolButton13: TToolButton
        Visible = False
      end
    end
    inherited plTitleDetails: TPanel
      Width = 424
      Caption = #1055#1086#1078#1072#1088#1099' '#1079#1072' '#1087#1077#1088#1080#1086#1076
      ExplicitWidth = 424
    end
    inherited IL: TPngImageList
      Bitmap = {}
    end
    inherited CRUDDisp: TFMACRUDDisp
      Fields = <
        item
          Precision = 0
          FieldNo = 0
          Size = 0
          DataType = ftUnknown
          Attributes = []
        end>
    end
    inherited MemData: TMemTableEh
      AfterScroll = FrameFireLinkedMemDataAfterScroll
    end
    inherited pmGridMenu: TPopupMenu
      Left = 253
      Top = 176
    end
  end
  inherited pl1: TPanel
    Top = 541
    Width = 972
    ExplicitTop = 541
    ExplicitWidth = 972
    inherited btOk: TButton
      Left = 769
      ExplicitLeft = 769
    end
    inherited btCancel: TButton
      Left = 872
      ExplicitLeft = 872
    end
  end
  object plAll: TPanel [3]
    Left = 433
    Top = 28
    Width = 539
    Height = 513
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    inline FrameGrid: TFrameBaseGrid
      Left = 0
      Top = 0
      Width = 539
      Height = 513
      Align = alClient
      TabOrder = 0
      ExplicitWidth = 539
      ExplicitHeight = 513
      inherited dgGrid: TDBGridEh
        Width = 533
        Height = 464
        OnDrawColumnCell = FrameGriddgGridDrawColumnCell
        Columns = <
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'fire_case_name'
            Footers = <>
            Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1086#1078#1072#1088#1072
            Width = 194
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'jump_kind_name'
            Footers = <>
            Title.Caption = #1042#1080#1076' '#1087#1088#1099#1078#1082#1072'/'#1089#1087#1091#1089#1082#1072
            Width = 157
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'worker_code'
            Footers = <>
            Title.Caption = #1057#1086#1090#1088#1091#1076#1085#1080#1082
            Width = 367
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'amount'
            Footers = <>
            Title.Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
            Width = 87
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            Checkboxes = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'isstorno_num'
            Footers = <>
            Title.Caption = #1057#1090#1086#1088#1085#1086
            Width = 65
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'date_begin'
            Footers = <>
            Title.Caption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
            Width = 80
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'date_end'
            Footers = <>
            Title.Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
            Width = 80
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DisplayFormat = '### ### ##0.00'
            DynProps = <>
            EditButtons = <>
            FieldName = 'summa'
            Footers = <>
            Title.Caption = #1057#1091#1084#1084#1072
            Width = 77
          end>
      end
      inherited tbTools: TToolBar
        Width = 533
        ExplicitWidth = 533
        inherited ToolButton13: TToolButton
          Action = acStorno1
        end
      end
      inherited plTitleDetails: TPanel
        Width = 533
        Caption = #1055#1088#1099#1078#1082#1080' '#1080' '#1089#1087#1091#1089#1082#1080' '#1074' '#1090#1077#1082#1091#1097#1077#1084' '#1088#1072#1089#1095#1077#1090#1077
        ExplicitWidth = 533
      end
      inherited IL: TPngImageList
        Bitmap = {}
      end
      inherited AL: TActionList
        inherited acMulty: TAction
          Visible = True
          OnExecute = FrameGridacMultyExecute
        end
      end
      inherited CRUDDisp: TFMACRUDDisp
        Fields = <
          item
            Name = 'id'
            Precision = 0
            FieldNo = 1
            Size = 0
            DataType = ftInteger
            Attributes = [faHiddenCol]
          end
          item
            Name = 'fire_case_id'
            Precision = 0
            FieldNo = 2
            Size = 0
            DataType = ftInteger
            Attributes = [faHiddenCol, faRequired]
          end
          item
            Name = 'worker_id'
            Precision = 0
            FieldNo = 3
            Size = 38
            DataType = ftGuid
            Attributes = [faHiddenCol, faRequired]
          end
          item
            Name = 'jump_kind_id'
            Precision = 0
            FieldNo = 4
            Size = 0
            DataType = ftInteger
            Attributes = [faHiddenCol, faRequired]
          end
          item
            Name = 'amount'
            Precision = 0
            FieldNo = 5
            Size = 0
            DataType = ftInteger
            Attributes = [faRequired]
            LongCaption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
            ShortCaption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
          end
          item
            Name = 'calc_period_id'
            Precision = 0
            FieldNo = 6
            Size = 0
            DataType = ftInteger
            Attributes = [faHiddenCol]
          end
          item
            Name = 'date_begin'
            Precision = 0
            FieldNo = 7
            Size = 0
            DataType = ftDate
            Attributes = [faRequired]
            LongCaption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
            ShortCaption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
          end
          item
            Name = 'date_end'
            Precision = 0
            FieldNo = 8
            Size = 0
            DataType = ftDate
            Attributes = []
            LongCaption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
            ShortCaption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
          end
          item
            Name = 'fire_case_name'
            Precision = 0
            FieldNo = 11
            Size = 250
            DataType = ftWideString
            Attributes = []
            LongCaption = #1055#1086#1078#1072#1088
            ShortCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1086#1078#1072#1088#1072
            KeyFieldName = 'fire_case_id'
          end
          item
            Name = 'worker_code'
            Precision = 0
            FieldNo = 12
            Size = 50
            DataType = ftWideString
            Attributes = []
            LongCaption = #1057#1086#1090#1088#1091#1076#1085#1080#1082
            ShortCaption = #1057#1086#1090#1088#1091#1076#1085#1080#1082
            KeyFieldName = 'worker_id'
          end
          item
            Name = 'jump_kind_name'
            Precision = 0
            FieldNo = 13
            Size = 250
            DataType = ftWideString
            Attributes = []
            LongCaption = #1042#1080#1076' '#1087#1088#1099#1078#1082#1072'/'#1089#1087#1091#1089#1082#1072
            ShortCaption = #1042#1080#1076' '#1087#1088#1099#1078#1082#1072'/'#1089#1087#1091#1089#1082#1072
            KeyFieldName = 'jump_kind_id'
          end>
        CrudAgent = dmJumps.CrudAgent
        EditFormDataSource = FormEditJump.dsLocal
        OnBeforeGetList = FrameGridCRUDDispBeforeGetList
        OnBeforeInsert = FrameGridCRUDDispBeforeInsert
      end
    end
  end
  inherited cbAll: TCheckBox
    Width = 966
    OnClick = cbAllClick
    ExplicitWidth = 966
  end
  inherited MainDataSet: TMemTableEh
    Left = 104
    Top = 264
  end
  inherited MainActionList: TActionList
    Images = FrameFireLinked.IL
  end
end
