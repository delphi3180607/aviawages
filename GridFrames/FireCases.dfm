object FormFireCases: TFormFireCases
  Left = 0
  Top = 0
  Caption = #1055#1086#1078#1072#1088#1099' '#1079#1072' '#1087#1077#1088#1080#1086#1076
  ClientHeight = 440
  ClientWidth = 608
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  TextHeight = 15
  inline FrameGrid: TFrameBaseGrid
    Left = 0
    Top = 0
    Width = 608
    Height = 440
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 608
    ExplicitHeight = 440
    inherited dgGrid: TDBGridEh
      Width = 602
      Height = 391
      DataGrouping.Active = True
      DataGrouping.DefaultStateExpanded = True
      DataGrouping.GroupLevels = <
        item
          ColumnName = 'Column_3_department_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = [fsBold]
          ParentFont = False
        end>
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'name'
          Footers = <>
          HighlightRequired = True
          Title.Caption = #1053#1086#1084#1077#1088
          Width = 93
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'forestry_name'
          Footers = <>
          Title.Caption = #1051#1077#1089#1085#1080#1095#1077#1089#1090#1074#1086
          Width = 219
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'square'
          Footers = <>
          Title.Caption = #1050#1074#1072#1088#1090#1072#1083
          Width = 72
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'department_name'
          Footers = <>
          Title.Caption = #1054#1090#1076#1077#1083#1077#1085#1080#1077
          Width = 248
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'datefrom'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
          Width = 92
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'dateto'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
          Width = 121
        end>
    end
    inherited tbTools: TToolBar
      Width = 602
      ExplicitWidth = 600
      inherited ToolButton13: TToolButton
        Visible = False
      end
    end
    inherited plTitleDetails: TPanel
      Width = 602
      Caption = #1055#1086#1078#1072#1088#1099' '#1079#1072' '#1091#1089#1090#1072#1085#1086#1074#1083#1077#1085#1085#1099#1081' '#1087#1077#1088#1080#1086#1076
      ExplicitWidth = 600
    end
    inherited IL: TPngImageList
      Bitmap = {}
    end
    inherited DataSource: TDataSource
      Left = 284
    end
    inherited CRUDDisp: TFMACRUDDisp
      Fields = <
        item
          Name = 'id'
          Precision = 0
          FieldNo = 1
          Size = 0
          DataType = ftInteger
          Attributes = [faHiddenCol]
        end
        item
          Name = 'name'
          Precision = 0
          FieldNo = 2
          Size = 250
          DataType = ftWideString
          Attributes = [faRequired]
          LongCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          ShortCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
        end
        item
          Name = 'forestry_id'
          Precision = 0
          FieldNo = 3
          Size = 0
          DataType = ftInteger
          Attributes = [faHiddenCol, faRequired]
        end
        item
          Name = 'forestry_name'
          Precision = 0
          FieldNo = 4
          Size = 250
          DataType = ftWideString
          Attributes = []
          LongCaption = #1051#1077#1089#1085#1080#1095#1077#1089#1090#1074#1086
          ShortCaption = #1051#1077#1089#1085#1080#1095#1077#1089#1090#1074#1086
          KeyFieldName = 'forestry_id'
        end
        item
          Name = 'square'
          Precision = 0
          FieldNo = 5
          Size = 0
          DataType = ftSmallint
          Attributes = [faRequired]
          LongCaption = #1050#1074#1072#1088#1090#1072#1083
          ShortCaption = #1050#1074#1072#1088#1090#1072#1083
        end
        item
          Name = 'department_id'
          Precision = 0
          FieldNo = 6
          Size = 38
          DataType = ftGuid
          Attributes = [faHiddenCol, faRequired]
        end
        item
          Name = 'department_name'
          Precision = 0
          FieldNo = 7
          Size = 255
          DataType = ftWideString
          Attributes = []
          LongCaption = #1054#1090#1076#1077#1083#1077#1085#1080#1077
          ShortCaption = #1054#1090#1076#1077#1083#1077#1085#1080#1077
          KeyFieldName = 'department_id'
        end
        item
          Name = 'datefrom'
          Precision = 0
          FieldNo = 8
          Size = 0
          DataType = ftDate
          Attributes = [faRequired]
          LongCaption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
          ShortCaption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
        end
        item
          Name = 'dateto'
          Precision = 0
          FieldNo = 9
          Size = 0
          DataType = ftDate
          Attributes = []
          LongCaption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
          ShortCaption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
        end>
      CrudAgent = dmFireCases.CrudAgent
      EditFormDataSource = FormEditFireCase.dsLocal
      OnBeforeGetList = FrameGridCRUDDispBeforeGetList
      Left = 441
    end
    inherited pmGridMenu: TPopupMenu
      Left = 365
    end
  end
end
