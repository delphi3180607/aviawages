unit Workers;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, InitStated, Vcl.ExtCtrls, BaseSimpleGrid,
  dmudepartments, dmuworkers, dmugrounds, dmufot, BaseTree, Vcl.StdCtrls,
  Data.DB, MemTableDataEh, MemTableEh, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.ComCtrls;

type
  TFormWorkers = class(TFormInitStated)
    Splitter1: TSplitter;
    FrameDepsTree: TFrameBaseTree;
    plAll: TPanel;
    FrameWorkers: TFrameBaseSimpleGrid;
    Splitter2: TSplitter;
    dsMain: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    FrameGrounds: TFrameBaseSimpleGrid;
    FrameFOT: TFrameBaseSimpleGrid;
    cbAll: TCheckBox;
    plSelection: TPanel;
    dgSelected: TDBGridEh;
    btClear: TButton;
    procedure btOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dgSelectedKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FrameDepsTreeMemDataAfterScroll(DataSet: TDataSet);
    procedure FrameWorkersMemDataAfterScroll(DataSet: TDataSet);
    procedure cbAllClick(Sender: TObject);
    procedure FrameWorkersCRUDDispBeforeGetList(Sender: TObject);
    procedure FrameGroundsCRUDDispBeforeGetList(Sender: TObject);
    procedure FrameFOTCRUDDispBeforeGetList(Sender: TObject);
    procedure FrameWorkersdgGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btClearClick(Sender: TObject);
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormWorkers: TFormWorkers;

implementation

{$R *.dfm}

{ TFormWorkers }

procedure TFormWorkers.btClearClick(Sender: TObject);
begin
  inherited;
  MainDataSet.EmptyTable;
end;

procedure TFormWorkers.btOkClick(Sender: TObject);
var i: integer;
begin
  inherited;
  if not MainDataSet.Active then
      ShowMessage('�� �������� �� ����� ������.');
end;

procedure TFormWorkers.cbAllClick(Sender: TObject);
begin
  inherited;
  FrameDepsTreeMemDataAfterScroll(FrameDepsTree.MemData);
  if not FrameWorkers.CRUDDisp.GetList(nil,errormessage) then
  begin
    MessageDlg(errormessage, mtError, [mbOK], 0);
    exit;
  end;
end;

procedure TFormWorkers.dgSelectedKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key=VK_DELETE then
  MainDataSet.Delete;
end;

procedure TFormWorkers.FormShow(Sender: TObject);
var i: integer;
begin
  inherited;
  if (fsModal in FormState) then
  begin
     plSelection.Show;
     for i := 0 to FrameWorkers.dgGrid.Columns.Count-1 do
     begin
      FrameWorkers.dgGrid.Columns[i].TextEditing := false;
     end;
  end else
  begin
     plSelection.Hide;
  end;
end;

procedure TFormWorkers.FrameDepsTreeMemDataAfterScroll(DataSet: TDataSet);
begin
  FrameWorkers.acRefresh.Execute;
end;

procedure TFormWorkers.FrameFOTCRUDDispBeforeGetList(Sender: TObject);
begin
  FrameFOT.CRUDDisp.ApplyMasterDetailParams(FrameWorkers.CRUDDisp, 'worker_id', 'id');
end;

procedure TFormWorkers.FrameGroundsCRUDDispBeforeGetList(Sender: TObject);
begin
  FrameGrounds.CRUDDisp.ApplyMasterDetailParams(FrameWorkers.CRUDDisp, 'worker_id', 'id');
end;

procedure TFormWorkers.FrameWorkersCRUDDispBeforeGetList(Sender: TObject);
begin
  if cbAll.Checked then
    FrameWorkers.CRUDDisp.CrudAgent.ParamTransform.ResetContext
  else
    FrameWorkers.CRUDDisp.ApplyMasterDetailParams(FrameDepsTree.CRUDDisp, 'department_id', 'id');
end;

procedure TFormWorkers.FrameWorkersdgGridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key=32 then
  begin
    if (not MainDataSet.Active) or (MainDataSet.RecordCount=0) then
      MainDataSet.LoadFromDataSet(FrameWorkers.MemData,1,lmCopy,false)
    else
    begin
      if not MainDataSet.Locate('id',FrameWorkers.MemData.FieldByName('id').Value,[]) then
      MainDataSet.LoadFromDataSet(FrameWorkers.MemData,1,lmAppend,false);
    end;
    Key := 0;
  end;
end;

procedure TFormWorkers.FrameWorkersMemDataAfterScroll(DataSet: TDataSet);
begin
  FrameGrounds.acRefresh.Execute;
  FrameFot.acRefresh.Execute;
end;

procedure TFormWorkers.Init;
begin
  if self.IsInited then exit;
  with FrameDepsTree do
  begin
    start_id := null;
    key_field_name := 'id';
    parent_field_name := 'parent_id';
    display_field_name := 'code';
    Init;
  end;
  FrameWorkers.CRUDDisp.CrudAgent.ParamTransform.ResetContext;
  if not FrameWorkers.CRUDDisp.GetList(nil,errormessage) then
  begin
    MessageDlg(errormessage, mtError, [mbOK], 0);
  end;
  inherited;
end;

end.
