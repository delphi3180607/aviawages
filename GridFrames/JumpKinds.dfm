inherited FormJumpKinds: TFormJumpKinds
  Caption = #1058#1080#1087#1099' '#1087#1088#1099#1078#1082#1086#1074'/'#1089#1087#1091#1089#1082#1086#1074
  ClientWidth = 636
  ExplicitWidth = 652
  TextHeight = 15
  inherited pl1: TPanel
    Width = 636
    TabOrder = 1
    ExplicitWidth = 636
    inherited btOk: TButton
      Left = 433
      ExplicitLeft = 433
    end
    inherited btCancel: TButton
      Left = 536
      ExplicitLeft = 536
    end
  end
  inline FrameGrid: TFrameBaseGrid [1]
    Left = 0
    Top = 0
    Width = 636
    Height = 406
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 636
    ExplicitHeight = 406
    inherited dgGrid: TDBGridEh
      Width = 630
      Height = 357
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'code'
          Footers = <>
          Title.Caption = #1050#1086#1076
          Width = 202
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'name'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          Width = 286
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'payment_code'
          Footers = <>
          Title.Caption = #1050#1086#1076' '#1074#1099#1087#1083#1072#1090#1099
          Width = 195
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DisplayFormat = '### ### ##0.00'
          DynProps = <>
          EditButtons = <>
          FieldName = 'salary'
          Footers = <>
          Title.Caption = #1054#1082#1083#1072#1076
          Width = 90
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DisplayFormat = '##0.00'
          DynProps = <>
          EditButtons = <>
          FieldName = 'percent'
          Footers = <>
          Title.Caption = #1055#1088#1086#1094#1077#1085#1090
          Width = 57
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DisplayFormat = '### ##0.00'
          DynProps = <>
          EditButtons = <>
          FieldName = 'tarif'
          Footers = <>
          Title.Caption = #1058#1072#1088#1080#1092
          Width = 88
        end>
    end
    inherited tbTools: TToolBar
      Width = 630
      ExplicitWidth = 630
      inherited ToolButton13: TToolButton
        Visible = False
      end
    end
    inherited plTitleDetails: TPanel
      Width = 630
      ExplicitWidth = 630
    end
    inherited IL: TPngImageList
      Bitmap = {}
    end
    inherited CRUDDisp: TFMACRUDDisp
      Fields = <
        item
          Name = 'id'
          Precision = 0
          FieldNo = 1
          Size = 0
          DataType = ftInteger
          Attributes = [faHiddenCol]
        end
        item
          Name = 'payment_id'
          Precision = 0
          FieldNo = 2
          Size = 39
          DataType = ftString
          Attributes = [faHiddenCol]
        end
        item
          Name = 'payment_code'
          Precision = 0
          FieldNo = 3
          Size = 102
          DataType = ftWideString
          Attributes = []
          LongCaption = #1050#1086#1076' '#1074#1099#1087#1083#1072#1090#1099
          ShortCaption = #1050#1086#1076' '#1074#1099#1087#1083#1072#1090#1099
          KeyFieldName = 'payment_id'
        end
        item
          Name = 'payment_name'
          Precision = 0
          FieldNo = 4
          Size = 502
          DataType = ftWideString
          Attributes = [faHiddenCol]
          LongCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1074#1099#1087#1083#1072#1090#1099
          ShortCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1074#1099#1087#1083#1072#1090#1099
          KeyFieldName = 'payment_id'
        end
        item
          Name = 'code'
          Precision = 0
          FieldNo = 5
          Size = 102
          DataType = ftWideString
          Attributes = [faRequired]
          LongCaption = #1050#1086#1076
          ShortCaption = #1050#1086#1076
        end
        item
          Name = 'name'
          Precision = 0
          FieldNo = 6
          Size = 502
          DataType = ftWideString
          Attributes = []
          LongCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          ShortCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
        end
        item
          Name = 'salary'
          Precision = 12
          FieldNo = 9
          Size = 2
          DataType = ftBCD
          Attributes = []
          LongCaption = #1054#1082#1083#1072#1076
          ShortCaption = #1054#1082#1083#1072#1076
        end
        item
          Name = 'tarif'
          Precision = 12
          FieldNo = 7
          Size = 2
          DataType = ftBCD
          Attributes = [faRequired]
          LongCaption = #1058#1072#1088#1080#1092
          ShortCaption = #1058#1072#1088#1080#1092
        end
        item
          Name = 'percent'
          Precision = 5
          FieldNo = 8
          Size = 2
          DataType = ftBCD
          Attributes = [faRequired]
          LongCaption = #1055#1088#1086#1094#1077#1085#1090
          ShortCaption = #1055#1088#1086#1094#1077#1085#1090
        end>
      CrudAgent = dmjumpkinds.CrudAgent
      EditFormDataSource = FormEditJumpKind.dsLocal
    end
    inherited MemData: TMemTableEh
      FieldDefs = <
        item
          Name = 'id'
          DataType = ftInteger
        end
        item
          Name = 'payment_id'
          DataType = ftString
          Size = 39
        end
        item
          Name = 'payment_code'
          DataType = ftWideString
          Size = 102
        end
        item
          Name = 'payment_name'
          DataType = ftWideString
          Size = 502
        end
        item
          Name = 'code'
          DataType = ftWideString
          Size = 102
        end
        item
          Name = 'name'
          DataType = ftWideString
          Size = 502
        end
        item
          Name = 'tarif'
          DataType = ftBCD
          Precision = 12
          Size = 2
        end
        item
          Name = 'percent'
          DataType = ftBCD
          Precision = 5
          Size = 2
        end
        item
          Name = 'salary'
          DataType = ftBCD
          Precision = 12
          Size = 2
        end
        item
          Name = 'created_at'
          DataType = ftTimeStamp
        end
        item
          Name = 'updated_at'
          DataType = ftTimeStamp
        end>
      StoreDefs = True
    end
  end
end
