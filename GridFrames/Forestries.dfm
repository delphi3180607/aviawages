inherited FormForestries: TFormForestries
  Caption = #1051#1077#1089#1085#1080#1095#1077#1089#1090#1074#1072
  ClientWidth = 636
  ExplicitWidth = 652
  TextHeight = 15
  inherited pl1: TPanel
    Width = 636
    TabOrder = 1
    ExplicitWidth = 636
    inherited btOk: TButton
      Left = 433
      ExplicitLeft = 433
    end
    inherited btCancel: TButton
      Left = 536
      ExplicitLeft = 536
    end
  end
  inline FrameGrid: TFrameBaseGrid [1]
    Left = 0
    Top = 0
    Width = 636
    Height = 406
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 636
    ExplicitHeight = 406
    inherited dgGrid: TDBGridEh
      Width = 630
      Height = 357
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'name'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          Width = 383
        end>
    end
    inherited tbTools: TToolBar
      Width = 630
      ExplicitWidth = 630
      inherited ToolButton13: TToolButton
        Visible = False
      end
    end
    inherited plTitleDetails: TPanel
      Width = 630
      ExplicitWidth = 630
    end
    inherited IL: TPngImageList
      Bitmap = {}
    end
    inherited CRUDDisp: TFMACRUDDisp
      Fields = <
        item
          Name = 'id'
          Precision = 0
          FieldNo = 1
          Size = 0
          DataType = ftInteger
          Attributes = [faHiddenCol]
        end
        item
          Name = 'name'
          Precision = 0
          FieldNo = 2
          Size = 502
          DataType = ftWideString
          Attributes = []
          LongCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          ShortCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
        end>
      CrudAgent = dmforestries.CrudAgent
      EditFormDataSource = FormEditForestry.dsLocal
    end
    inherited MemData: TMemTableEh
      FieldDefs = <
        item
          Name = 'id'
          DataType = ftInteger
        end
        item
          Name = 'name'
          DataType = ftWideString
          Size = 502
        end>
      StoreDefs = True
    end
  end
end
