inherited FormDedScales: TFormDedScales
  Caption = #1064#1082#1072#1083#1072' '#1085#1072#1083#1086#1075#1086#1074' '#1087#1086' '#1087#1077#1088#1080#1086#1076#1072#1084
  TextHeight = 15
  inline FrameGrid: TFrameBaseGrid [1]
    Left = 0
    Top = 0
    Width = 626
    Height = 406
    Align = alClient
    TabOrder = 1
    ExplicitWidth = 626
    ExplicitHeight = 406
    inherited dgGrid: TDBGridEh
      Width = 620
      Height = 357
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'start_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
          Width = 150
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'esv'
          Footers = <>
          Title.Caption = #1045#1057#1042', %'
          Width = 150
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'pfr'
          Footers = <>
          Title.Caption = #1055#1060#1056' ('#1076#1086#1087'.'#1090#1072#1088#1080#1092'), %'
          Width = 150
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'fss'
          Footers = <>
          Title.Caption = #1060#1057#1057', %'
          Width = 150
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'vacation_reserve_koeff'
          Footers = <>
          Title.Caption = #1050#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090' '#1088#1077#1079#1077#1088#1074#1072' '#1086#1090#1087#1091#1089#1082#1086#1074
          Width = 120
        end>
    end
    inherited tbTools: TToolBar
      Width = 620
      ExplicitWidth = 620
      inherited ToolButton13: TToolButton
        Visible = False
      end
    end
    inherited plTitleDetails: TPanel
      Width = 620
      ExplicitWidth = 620
    end
    inherited IL: TPngImageList
      Bitmap = {}
    end
    inherited CRUDDisp: TFMACRUDDisp
      Fields = <
        item
          Name = 'id'
          Precision = 15
          FieldNo = 1
          Size = 0
          DataType = ftInteger
          Attributes = [faHiddenCol]
        end
        item
          Name = 'start_date'
          Precision = 0
          FieldNo = 2
          Size = 0
          DataType = ftDate
          Attributes = [faRequired]
          LongCaption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
          ShortCaption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
        end
        item
          Name = 'esv'
          Precision = 9
          FieldNo = 3
          Size = 4
          DataType = ftBCD
          Attributes = []
          LongCaption = #1045#1057#1042', %'
          ShortCaption = #1045#1057#1042', %'
        end
        item
          Name = 'pfr'
          Precision = 9
          FieldNo = 4
          Size = 4
          DataType = ftBCD
          Attributes = []
          LongCaption = #1055#1060#1056' ('#1076#1086#1087'.'#1090#1072#1088#1080#1092'), %'
          ShortCaption = #1055#1060#1056' ('#1076#1086#1087'.'#1090#1072#1088#1080#1092'), %'
        end
        item
          Name = 'fss'
          Precision = 9
          FieldNo = 5
          Size = 4
          DataType = ftBCD
          Attributes = []
          LongCaption = #1060#1057#1057', %'
          ShortCaption = #1060#1057#1057', %'
        end>
      CrudAgent = dmDedScale.CrudAgent
      EditFormDataSource = FormEditDedScale.dsLocal
    end
    inherited MemData: TMemTableEh
      Active = True
      object MemTableData: TMemTableDataEh
        object DataStruct: TMTDataStructEh
          object id: TMTNumericDataFieldEh
            FieldName = 'id'
            NumericDataType = fdtIntegerEh
            AutoIncrement = False
            DisplayLabel = 'id'
            DisplayWidth = 10
            currency = False
            Precision = 15
          end
          object start_date: TMTDateTimeDataFieldEh
            FieldName = 'start_date'
            DateTimeDataType = fdtDateEh
            DisplayLabel = 'start_date'
            DisplayWidth = 10
            Required = True
          end
          object esv: TMTNumericDataFieldEh
            FieldName = 'esv'
            NumericDataType = fdtBCDEh
            AutoIncrement = False
            DisplayLabel = 'esv'
            DisplayWidth = 10
            currency = False
            Precision = 9
            Scale = 4
          end
          object pfr: TMTNumericDataFieldEh
            FieldName = 'pfr'
            NumericDataType = fdtBCDEh
            AutoIncrement = False
            DisplayLabel = 'pfr'
            DisplayWidth = 10
            currency = False
            Precision = 9
            Scale = 4
          end
          object fss: TMTNumericDataFieldEh
            FieldName = 'fss'
            NumericDataType = fdtBCDEh
            AutoIncrement = False
            DisplayLabel = 'fss'
            DisplayWidth = 10
            currency = False
            Precision = 9
            Scale = 4
          end
        end
        object RecordsList: TRecordsListEh
        end
      end
    end
  end
end
