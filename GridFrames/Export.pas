unit Export;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, InitStated, Vcl.ComCtrls, Vcl.ExtCtrls,
  Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  MemTableDataEh, MemTableEh, dmuutils, dmucurrentperiod;

type
  TFormExport = class(TFormInitStated)
    lbWait: TLabel;
    btStart: TButton;
    procedure btStartClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormExport: TFormExport;

implementation

{$R *.dfm}

procedure TFormExport.btStartClick(Sender: TObject);
begin
  if Application.MessageBox(PWideChar('��������� ������� ������� �� ������� ������?'), '��������', MB_YESNO) = IDYes then
  begin
    lbWait.Caption := '�����, ����������� �������...';
    Screen.Cursor := crHourGlass;
    Application.ProcessMessages;
    try
      dmutils.smExport.Params.ParamByName('ncalcperiodid').Value := context_data_module.CalcPeriodId;
      dmutils.smExport.Params.ParamByName('sworkerid').Value := '';
      dmutils.smExport.ExecuteMethod;
    finally
      lbWait.Caption := '';
      Screen.Cursor := crDefault;
    end;
  end;
end;

end.
