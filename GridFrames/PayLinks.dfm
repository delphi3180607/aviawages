inherited FormPayLinks: TFormPayLinks
  Caption = #1057#1086#1086#1090#1074#1077#1090#1089#1090#1074#1080#1077' '#1074#1099#1087#1083#1072#1090' '#1074#1080#1076#1072#1084' '#1076#1077#1103#1090#1077#1083#1100#1085#1086#1089#1090#1080
  ClientWidth = 628
  ExplicitWidth = 644
  TextHeight = 15
  inherited pl1: TPanel
    Width = 628
    ExplicitWidth = 630
  end
  inline FrameVertGrid: TFrameBaseVertGrid [1]
    Left = 0
    Top = 0
    Width = 628
    Height = 406
    Align = alClient
    TabOrder = 1
    ExplicitWidth = 628
    ExplicitHeight = 406
    inherited VertGrid: TDBVertGridEh
      Width = 628
      Height = 359
      DataSource = FrameVertGrid.DataSource
      Font.Height = -16
      ParentFont = False
      ReadOnly = True
      LabelColWidth = 235
      Rows = <
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'pay_fire_code'
          RowLabel.Caption = #1059#1095#1072#1089#1090#1080#1077' '#1074' '#1090#1091#1096#1077#1085#1080#1080' '#1087#1086#1078#1072#1088#1086#1074
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'pay_jumps_code'
          RowLabel.Caption = #1055#1088#1099#1078#1082#1080'/'#1089#1087#1091#1089#1082#1080
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'pay_flights_code'
          RowLabel.Caption = #1053#1072#1083#1077#1090#1099
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'pay_over_code'
          RowLabel.Caption = #1057#1074#1077#1088#1093#1091#1088#1086#1095#1085#1099#1077
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'pay_holiday_code'
          RowLabel.Caption = #1042#1099#1093#1086#1076#1085#1099#1077
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'firecalc'
          RowLabel.Caption = #1042#1080#1076' '#1088#1072#1089#1095#1077#1090#1072' '#1076#1083#1103' '#1080#1084#1087#1086#1088#1090#1072
        end>
    end
    inherited tbTools: TToolBar
      Width = 622
      ExplicitWidth = 622
    end
    inherited plTitleDetails: TPanel
      Width = 622
      Caption = #1057#1086#1086#1090#1074#1077#1090#1089#1090#1074#1080#1077' '#1074#1099#1087#1083#1072#1090' '#1074#1080#1076#1072#1084' '#1076#1077#1103#1090#1077#1083#1100#1085#1086#1089#1090#1080
      ExplicitWidth = 622
    end
    inherited IL: TPngImageList
      Bitmap = {}
    end
    inherited AL: TActionList
      inherited acAdd: TAction
        Enabled = False
        Visible = False
      end
      inherited acDelete: TAction
        Enabled = False
        Visible = False
      end
      inherited acFilter: TAction
        Enabled = False
      end
      inherited acClearFilter: TAction
        Enabled = False
      end
      inherited acReports: TAction
        Enabled = False
      end
      inherited acExcel: TAction
        Enabled = False
        Visible = False
      end
    end
    inherited MemData: TMemTableEh
      FieldDefs = <
        item
          Name = 'id'
          DataType = ftInteger
        end
        item
          Name = 'pay_fire_id'
          DataType = ftString
          Size = 39
        end
        item
          Name = 'pay_fire_code'
          DataType = ftWideString
          Size = 102
        end
        item
          Name = 'pay_jumps_id'
          DataType = ftString
          Size = 39
        end
        item
          Name = 'pay_jumps_code'
          DataType = ftWideString
          Size = 102
        end
        item
          Name = 'pay_flights_id'
          DataType = ftString
          Size = 39
        end
        item
          Name = 'pay_flights_code'
          DataType = ftWideString
          Size = 102
        end
        item
          Name = 'pay_over_id'
          DataType = ftString
          Size = 39
        end
        item
          Name = 'pay_over_code'
          DataType = ftWideString
          Size = 102
        end
        item
          Name = 'pay_holiday_id'
          DataType = ftString
          Size = 39
        end
        item
          Name = 'pay_holiday_code'
          DataType = ftWideString
          Size = 102
        end
        item
          Name = 'firecalc'
          DataType = ftWideString
          Size = 502
        end>
      StoreDefs = True
    end
    inherited CRUDDisp: TFMACRUDDisp
      Fields = <
        item
          Name = 'id'
          Precision = 0
          FieldNo = 1
          Size = 0
          DataType = ftInteger
          Attributes = [faHiddenCol]
        end
        item
          Name = 'pay_fire_id'
          Precision = 0
          FieldNo = 2
          Size = 39
          DataType = ftString
          Attributes = [faHiddenCol]
        end
        item
          Name = 'pay_fire_code'
          Precision = 0
          FieldNo = 3
          Size = 102
          DataType = ftWideString
          Attributes = []
          LongCaption = #1059#1095#1072#1089#1090#1080#1077' '#1074' '#1090#1091#1096#1077#1085#1080#1080' '#1087#1086#1078#1072#1088#1086#1074
          ShortCaption = #1059#1095#1072#1089#1090#1080#1077' '#1074' '#1090#1091#1096#1077#1085#1080#1080' '#1087#1086#1078#1072#1088#1086#1074
          KeyFieldName = 'pay_fire_id'
        end
        item
          Name = 'pay_jumps_id'
          Precision = 0
          FieldNo = 4
          Size = 39
          DataType = ftString
          Attributes = [faHiddenCol]
        end
        item
          Name = 'pay_jumps_code'
          Precision = 0
          FieldNo = 5
          Size = 102
          DataType = ftWideString
          Attributes = []
          LongCaption = #1055#1088#1099#1078#1082#1080'/'#1089#1087#1091#1089#1082#1080
          ShortCaption = #1055#1088#1099#1078#1082#1080'/'#1089#1087#1091#1089#1082#1080
          KeyFieldName = 'pay_jumps_id'
        end
        item
          Name = 'pay_flights_id'
          Precision = 0
          FieldNo = 6
          Size = 39
          DataType = ftString
          Attributes = [faHiddenCol]
        end
        item
          Name = 'pay_flights_code'
          Precision = 0
          FieldNo = 7
          Size = 102
          DataType = ftWideString
          Attributes = []
          LongCaption = #1053#1072#1083#1077#1090#1099
          ShortCaption = #1053#1072#1083#1077#1090#1099
          KeyFieldName = 'pay_flights_id'
        end
        item
          Name = 'pay_over_id'
          Precision = 0
          FieldNo = 8
          Size = 39
          DataType = ftString
          Attributes = [faHiddenCol]
        end
        item
          Name = 'pay_over_code'
          Precision = 0
          FieldNo = 9
          Size = 102
          DataType = ftWideString
          Attributes = []
          LongCaption = #1057#1074#1077#1088#1093#1091#1088#1086#1095#1085#1099#1077
          ShortCaption = #1057#1074#1077#1088#1093#1091#1088#1086#1095#1085#1099#1077
          KeyFieldName = 'pay_over_id'
        end
        item
          Name = 'pay_holiday_id'
          Precision = 0
          FieldNo = 10
          Size = 39
          DataType = ftString
          Attributes = [faHiddenCol]
        end
        item
          Name = 'pay_holiday_code'
          Precision = 0
          FieldNo = 11
          Size = 102
          DataType = ftWideString
          Attributes = []
          LongCaption = #1042#1099#1093#1086#1076#1085#1099#1077
          ShortCaption = #1042#1099#1093#1086#1076#1085#1099#1077
          KeyFieldName = 'pay_holiday_id'
        end
        item
          Name = 'firecalc'
          Precision = 0
          FieldNo = 12
          Size = 502
          DataType = ftWideString
          Attributes = []
          LongCaption = #1042#1080#1076' '#1088#1072#1089#1095#1077#1090#1072' '#1076#1083#1103' '#1080#1084#1087#1086#1088#1090#1072
          ShortCaption = #1042#1080#1076' '#1088#1072#1089#1095#1077#1090#1072' '#1076#1083#1103' '#1080#1084#1087#1086#1088#1090#1072
        end>
      CrudAgent = dmPayLinks.CrudAgent
      EditFormDataSource = FormEditPayLinks.dsLocal
    end
  end
end
