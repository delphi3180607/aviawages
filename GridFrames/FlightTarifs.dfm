inherited FormFlightTarifs: TFormFlightTarifs
  Caption = #1058#1072#1088#1080#1092#1099' '#1085#1072#1083#1077#1090#1086#1074
  ClientWidth = 632
  ExplicitWidth = 648
  TextHeight = 15
  inherited pl1: TPanel
    Width = 632
    TabOrder = 1
    ExplicitWidth = 632
    inherited btOk: TButton
      Left = 429
      ExplicitLeft = 429
    end
    inherited btCancel: TButton
      Left = 532
      ExplicitLeft = 532
    end
  end
  inline FrameGrid: TFrameBaseGrid [1]
    Left = 0
    Top = 0
    Width = 632
    Height = 406
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 632
    ExplicitHeight = 406
    inherited dgGrid: TDBGridEh
      Width = 626
      Height = 357
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'paygroup_code'
          Footers = <>
          Title.Caption = #1043#1088#1091#1087#1087#1072' '#1086#1087#1083#1072#1090#1099
          Width = 225
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'plane_code'
          Footers = <>
          Title.Caption = #1042#1080#1076' '#1042#1057
          Width = 336
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DisplayFormat = '### ##0.00'
          DynProps = <>
          EditButtons = <>
          FieldName = 'head_salary'
          Footers = <>
          Title.Caption = #1054#1082#1083#1072#1076' '#1082#1086#1084#1072#1085#1076#1080#1088#1072
          Width = 110
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DisplayFormat = '##0.00'
          DynProps = <>
          EditButtons = <>
          FieldName = 'percent'
          Footers = <>
          Title.Caption = #1055#1088#1086#1094#1077#1085#1090
          Width = 59
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DisplayFormat = '### ##0.00'
          DynProps = <>
          EditButtons = <>
          FieldName = 'tarif'
          Footers = <>
          Title.Caption = #1063#1072#1089#1086#1074#1086#1081' '#1090#1072#1088#1080#1092
          Width = 93
        end>
    end
    inherited tbTools: TToolBar
      Width = 626
      ExplicitWidth = 626
      inherited ToolButton13: TToolButton
        Visible = False
      end
    end
    inherited plTitleDetails: TPanel
      Width = 626
      ExplicitWidth = 626
    end
    inherited IL: TPngImageList
      Bitmap = {}
    end
    inherited CRUDDisp: TFMACRUDDisp
      Fields = <
        item
          Name = 'id'
          Precision = 0
          FieldNo = 1
          Size = 0
          DataType = ftInteger
          Attributes = [faHiddenCol]
        end
        item
          Name = 'plane_id'
          Precision = 0
          FieldNo = 2
          Size = 0
          DataType = ftInteger
          Attributes = [faHiddenCol, faRequired]
        end
        item
          Name = 'paygroup_id'
          Precision = 0
          FieldNo = 3
          Size = 0
          DataType = ftInteger
          Attributes = [faHiddenCol, faRequired]
        end
        item
          Name = 'paygroup_code'
          Precision = 0
          FieldNo = 4
          Size = 102
          DataType = ftWideString
          Attributes = []
          LongCaption = #1043#1088#1091#1087#1087#1072' '#1086#1087#1083#1072#1090#1099
          ShortCaption = #1043#1088#1091#1087#1087#1072' '#1086#1087#1083#1072#1090#1099
          KeyFieldName = 'paygroup_id'
        end
        item
          Name = 'plane_code'
          Precision = 0
          FieldNo = 5
          Size = 162
          DataType = ftWideString
          Attributes = []
          LongCaption = #1042#1080#1076' '#1042#1057
          ShortCaption = #1042#1080#1076' '#1042#1057
          KeyFieldName = 'plane_id'
        end
        item
          Name = 'head_salary'
          Precision = 12
          FieldNo = 6
          Size = 2
          DataType = ftBCD
          Attributes = [faRequired]
          LongCaption = #1054#1082#1083#1072#1076' '#1082#1086#1084#1072#1085#1076#1080#1088#1072
          ShortCaption = #1054#1082#1083#1072#1076' '#1082#1086#1084#1072#1085#1076#1080#1088#1072
        end
        item
          Name = 'percent'
          Precision = 5
          FieldNo = 7
          Size = 2
          DataType = ftBCD
          Attributes = [faRequired]
          LongCaption = #1055#1088#1086#1094#1077#1085#1090
          ShortCaption = #1055#1088#1086#1094#1077#1085#1090
        end
        item
          Name = 'tarif'
          Precision = 12
          FieldNo = 8
          Size = 2
          DataType = ftBCD
          Attributes = []
          LongCaption = #1063#1072#1089#1086#1074#1086#1081' '#1090#1072#1088#1080#1092
          ShortCaption = #1063#1072#1089#1086#1074#1086#1081' '#1090#1072#1088#1080#1092
        end>
      CrudAgent = dmflighttarifs.CrudAgent
      EditFormDataSource = FormEditFlightTarif.dsLocal
    end
    inherited MemData: TMemTableEh
      FieldDefs = <
        item
          Name = 'id'
          DataType = ftInteger
        end
        item
          Name = 'plane_id'
          DataType = ftInteger
        end
        item
          Name = 'paygroup_id'
          DataType = ftInteger
        end
        item
          Name = 'paygroup_code'
          DataType = ftWideString
          Size = 102
        end
        item
          Name = 'plane_code'
          DataType = ftWideString
          Size = 162
        end
        item
          Name = 'head_salary'
          DataType = ftBCD
          Precision = 12
          Size = 2
        end
        item
          Name = 'percent'
          DataType = ftBCD
          Precision = 5
          Size = 2
        end
        item
          Name = 'tarif'
          DataType = ftBCD
          Precision = 12
          Size = 2
        end
        item
          Name = 'created_at'
          DataType = ftTimeStamp
        end
        item
          Name = 'updated_at'
          DataType = ftTimeStamp
        end>
      StoreDefs = True
    end
  end
end
