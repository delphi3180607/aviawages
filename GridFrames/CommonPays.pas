unit CommonPays;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, DB, InitStated, BaseGrid, dmucommonpays, EditCommonPay,
  Vcl.StdCtrls, Vcl.ExtCtrls, Workers, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client, MemTableDataEh,
  MemTableEh;

type
  TFormCommonPays = class(TFormInitStated)
    FrameGrid: TFrameBaseGrid;
    procedure FrameGridCRUDDispBeforeGetList(Sender: TObject);
    procedure FrameGridCRUDDispBeforeInsert(Sender: TObject);
    procedure FrameGridacMultyExecute(Sender: TObject);
  private
    { Private declarations }
  public
    {}
  end;

var
  FormCommonPays: TFormCommonPays;

implementation

uses dmucurrentperiod;

{$R *.dfm}

procedure TFormCommonPays.FrameGridacMultyExecute(Sender: TObject);
begin
  FrameGrid.DoMutipleAdd('worker_id','id',FormWorkers.FrameWorkers.CRUDDisp);
end;

procedure TFormCommonPays.FrameGridCRUDDispBeforeGetList(Sender: TObject);
begin
  with FrameGrid.CRUDDisp.CRUDAgent do
  begin
    ParamTransform.ResetContext;
    ParamTransform.AddPatternContext('/*wherefilter*/','calc_period_id = :calc_period_id');
    ParamTransform.AddParameterContext('calc_period_id', ftInteger, context_data_module.CalcPeriodId);
  end;
end;

procedure TFormCommonPays.FrameGridCRUDDispBeforeInsert(Sender: TObject);
begin
  inherited;
  FrameGrid.MemData.FieldByName('calc_period_id').Value := context_data_module.CalcPeriodId;
end;

end.
