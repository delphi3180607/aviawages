unit FireCases;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseGrid, InitStated, FMACrudDisp, JSON, dmuBaseDataSnap, DB,
  Vcl.Menus;

type
  TFormFireCases = class(TFormInitStated)
    FrameGrid: TFrameBaseGrid;
    procedure FrameGridCRUDDispBeforeGetList(Sender: TObject);
  private
    {}
  public
    procedure Init; override;
  end;

var
  FormFireCases: TFormFireCases;

implementation

uses dmufirecases, EditFireCase;

{$R *.dfm}

{ TFormFireCases }

procedure TFormFireCases.FrameGridCRUDDispBeforeGetList(Sender: TObject);
begin
  with FrameGrid.CRUDDisp.CRUDAgent do
  begin
    ParamTransform.ResetContext;
    ParamTransform.AddPatternContext('/*wherefilter*/','(datefrom <= :date_end) and (dateto >= :date_begin or dateto is null) ');
    ParamTransform.AddParameterContext('date_begin', ftDate, context_data_module.FilterStartDate);
    ParamTransform.AddParameterContext('date_end', ftDate, context_data_module.FilterEndDate);
  end;
end;

procedure TFormFireCases.Init;
begin
  inherited;
end;

end.
