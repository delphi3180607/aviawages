unit FlightTarifs;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, InitStated, BaseGrid, dmuflighttarifs, EditFlightTarif,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.StdCtrls, Vcl.ExtCtrls,
  MemTableDataEh, MemTableEh;

type
  TFormFlightTarifs = class(TFormInitStated)
    FrameGrid: TFrameBaseGrid;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormFlightTarifs: TFormFlightTarifs;

implementation

{$R *.dfm}

end.
