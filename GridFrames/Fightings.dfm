inherited FormFightings: TFormFightings
  Caption = #1059#1095#1072#1089#1090#1080#1077' '#1074' '#1087#1086#1078#1072#1088#1072#1093
  ClientHeight = 761
  ClientWidth = 1069
  ExplicitWidth = 1085
  ExplicitHeight = 800
  TextHeight = 15
  inherited splFireEng: TSplitter
    Left = 441
    Width = 4
    Height = 692
    ExplicitLeft = 265
    ExplicitTop = 0
    ExplicitWidth = 4
    ExplicitHeight = 560
  end
  inherited FrameFireLinked: TFrameBaseGrid
    Width = 441
    Height = 692
    ExplicitWidth = 441
    ExplicitHeight = 692
    inherited dgGrid: TDBGridEh
      Width = 435
      Height = 643
      SelectionDrawParams.SelectionStyle = gsdsGridThemedEh
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'name'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088' '#1087#1086#1078#1072#1088#1072
          Width = 67
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'department_name'
          Footers = <>
          Title.Caption = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
          Visible = False
          Width = 132
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'forestry_name'
          Footers = <>
          Title.Caption = #1051#1077#1089#1085#1080#1095#1077#1089#1090#1074#1086
          Width = 153
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'datefrom'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
          Width = 77
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'dateto'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
          Width = 91
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'square'
          Footers = <>
          Title.Caption = #1050#1074#1072#1088#1090#1072#1083
          Width = 69
        end>
    end
    inherited tbTools: TToolBar
      Width = 435
      ExplicitWidth = 435
      inherited ToolButton13: TToolButton
        Visible = False
      end
    end
    inherited plTitleDetails: TPanel
      Width = 435
      Caption = #1055#1086#1078#1072#1088#1099' '#1079#1072' '#1087#1077#1088#1080#1086#1076
      Color = clBtnText
      ExplicitWidth = 435
    end
    inherited IL: TPngImageList
      Bitmap = {}
    end
    inherited CRUDDisp: TFMACRUDDisp
      Fields = <
        item
          Name = 'id'
          Precision = 0
          FieldNo = 1
          Size = 0
          DataType = ftInteger
          Attributes = [faHiddenCol]
        end
        item
          Name = 'name'
          Precision = 0
          FieldNo = 2
          Size = 502
          DataType = ftWideString
          Attributes = [faRequired]
        end
        item
          Name = 'forestry_id'
          Precision = 0
          FieldNo = 3
          Size = 0
          DataType = ftInteger
          Attributes = [faHiddenCol, faRequired]
        end
        item
          Name = 'forestry_name'
          Precision = 0
          FieldNo = 4
          Size = 502
          DataType = ftWideString
          Attributes = []
        end
        item
          Name = 'square'
          Precision = 0
          FieldNo = 5
          Size = 0
          DataType = ftSmallint
          Attributes = []
        end
        item
          Name = 'department_id'
          Precision = 0
          FieldNo = 6
          Size = 39
          DataType = ftString
          Attributes = [faHiddenCol, faRequired]
        end
        item
          Name = 'department_name'
          Precision = 0
          FieldNo = 7
          Size = 512
          DataType = ftWideString
          Attributes = []
        end
        item
          Name = 'datefrom'
          Precision = 0
          FieldNo = 8
          Size = 0
          DataType = ftDate
          Attributes = [faRequired]
        end
        item
          Name = 'dateto'
          Precision = 0
          FieldNo = 9
          Size = 0
          DataType = ftDate
          Attributes = []
        end>
    end
    inherited MemData: TMemTableEh
      AfterScroll = FrameFireLinkedMemDataAfterScroll
    end
    inherited pmGridMenu: TPopupMenu
      Left = 96
      Top = 240
    end
  end
  inherited pl1: TPanel
    Top = 720
    Width = 1069
    TabOrder = 2
    ExplicitTop = 720
    ExplicitWidth = 1069
    inherited btOk: TButton
      Left = 866
      ExplicitLeft = 866
    end
    inherited btCancel: TButton
      Left = 969
      ExplicitLeft = 969
    end
  end
  object plAll: TPanel [3]
    Left = 445
    Top = 28
    Width = 624
    Height = 692
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Splitter1: TSplitter
      Left = 0
      Top = 505
      Width = 624
      Height = 4
      Cursor = crVSplit
      Align = alTop
      ExplicitTop = 297
      ExplicitWidth = 560
    end
    inline FrameFightings: TFrameBaseGrid
      Left = 0
      Top = 0
      Width = 624
      Height = 505
      Align = alTop
      TabOrder = 0
      ExplicitWidth = 624
      ExplicitHeight = 505
      inherited dgGrid: TDBGridEh
        Width = 618
        Height = 456
        DrawMemoText = False
        OnDrawColumnCell = FrameFightingsdgGridDrawColumnCell
        Columns = <
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'fire_name'
            Footers = <>
            Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1086#1078#1072#1088#1072
            Width = 265
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'department_code'
            Footers = <>
            Title.Caption = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
            Width = 159
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'worker_code'
            Footers = <>
            Title.Caption = #1057#1086#1090#1088#1091#1076#1085#1080#1082
            Width = 454
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            Checkboxes = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'isstorno_num'
            Footers = <>
            Title.Caption = #1057#1090#1086#1088#1085#1086
            Width = 65
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'date_from'
            Footers = <>
            Title.Caption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
            Width = 84
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'date_to'
            Footers = <>
            Title.Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
            Width = 99
          end>
      end
      inherited tbTools: TToolBar
        Width = 618
        ExplicitWidth = 618
        inherited ToolButton13: TToolButton
          Action = acStorno1
        end
      end
      inherited plTitleDetails: TPanel
        Width = 618
        Caption = #1059#1095#1072#1089#1090#1080#1077' '#1074' '#1087#1086#1078#1072#1088#1072#1093' '#1074' '#1090#1077#1082#1091#1097#1077#1084' '#1088#1072#1089#1095#1077#1090#1077
        ExplicitWidth = 618
      end
      inherited IL: TPngImageList
        Left = 32
        Bitmap = {}
      end
      inherited AL: TActionList
        Left = 89
        inherited acMulty: TAction
          Visible = True
          OnExecute = FrameFightingsacMultyExecute
        end
      end
      inherited DataSource: TDataSource
        Left = 232
      end
      inherited CRUDDisp: TFMACRUDDisp
        Fields = <
          item
            Name = 'id'
            Precision = 0
            FieldNo = 1
            Size = 0
            DataType = ftInteger
            Attributes = [faHiddenCol]
          end
          item
            Name = 'fire_case_id'
            Precision = 0
            FieldNo = 2
            Size = 0
            DataType = ftInteger
            Attributes = [faHiddenCol, faRequired]
          end
          item
            Name = 'worker_id'
            Precision = 0
            FieldNo = 3
            Size = 39
            DataType = ftString
            Attributes = [faHiddenCol, faRequired]
          end
          item
            Name = 'calc_period_id'
            Precision = 0
            FieldNo = 4
            Size = 0
            DataType = ftInteger
            Attributes = [faHiddenCol, faRequired]
          end
          item
            Name = 'date_from'
            Precision = 0
            FieldNo = 5
            Size = 0
            DataType = ftDate
            Attributes = [faHiddenCol, faRequired]
            LongCaption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
            ShortCaption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
          end
          item
            Name = 'date_to'
            Precision = 0
            FieldNo = 6
            Size = 0
            DataType = ftDate
            Attributes = []
            LongCaption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
            ShortCaption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
          end
          item
            Name = 'worker_code'
            Precision = 0
            FieldNo = 9
            Size = 102
            DataType = ftWideString
            Attributes = []
            LongCaption = #1057#1086#1090#1088#1091#1076#1085#1080#1082
            ShortCaption = #1057#1086#1090#1088#1091#1076#1085#1080#1082
            KeyFieldName = 'worker_id'
          end
          item
            Name = 'fire_name'
            Precision = 0
            FieldNo = 10
            Size = 502
            DataType = ftWideString
            Attributes = []
            LongCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1086#1078#1072#1088#1072
            ShortCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1086#1078#1072#1088#1072
            KeyFieldName = 'fire_id'
          end>
        CrudAgent = dmFightings.CrudAgent
        EditFormDataSource = FormEditFighting.dsLocal
        OnBeforeGetList = FrameFightingsCRUDDispBeforeGetList
        OnBeforeInsert = FrameFightingsCRUDDispBeforeInsert
        OnAfterInsert = FrameFightingsCRUDDispAfterInsert
        OnBeforeMultipleInsert = FrameFightingsCRUDDispBeforeMultipleInsert
        OnMultipleInsert = FrameFightingsCRUDDispMultipleInsert
        Left = 312
      end
      inherited MemData: TMemTableEh
        StoreDefs = True
        AfterScroll = FrameFightingsMemDataAfterScroll
        Left = 152
      end
      inherited pmGridMenu: TPopupMenu
        Top = 176
      end
    end
    object plBottom: TPanel
      Left = 0
      Top = 509
      Width = 624
      Height = 183
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      inline FrameDetails: TFrameBaseGrid
        Left = 0
        Top = 0
        Width = 624
        Height = 183
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 624
        ExplicitHeight = 183
        inherited dgGrid: TDBGridEh
          Width = 618
          Height = 134
          Columns = <
            item
              AutoFitColWidth = False
              CellButtons = <>
              DynProps = <>
              EditButtons = <>
              FieldName = 'day'
              Footers = <>
              Title.Caption = #1044#1072#1090#1072
              Width = 127
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              DisplayFormat = 'hh:nn'
              DynProps = <>
              EditButtons = <>
              FieldName = 'work_hours'
              Footers = <>
              Title.Caption = #1056#1072#1073#1086#1095#1080#1093' '#1095#1072#1089#1086#1074
              Width = 139
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              DisplayFormat = 'hh:nn'
              DynProps = <>
              EditButtons = <>
              FieldName = 'overtime_hours'
              Footers = <>
              Title.Caption = #1057#1074#1077#1088#1093#1091#1088#1086#1095#1085#1099#1093' '#1095#1072#1089#1086#1074
              Width = 129
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              DisplayFormat = 'hh:nn'
              DynProps = <>
              EditButtons = <>
              FieldName = 'holiday_hours'
              Footers = <>
              Title.Caption = #1055#1088#1072#1079#1076#1085#1080#1095#1085#1099#1093' ('#1074#1099#1093#1086#1076#1085#1099#1093') '#1095#1072#1089#1086#1074
              Width = 150
            end>
        end
        inherited tbTools: TToolBar
          Width = 618
          ExplicitWidth = 618
          inherited ToolButton13: TToolButton
            Visible = False
          end
        end
        inherited plTitleDetails: TPanel
          Width = 618
          Caption = #1044#1077#1090#1072#1083#1080#1079#1072#1094#1080#1103' '#1087#1086' '#1076#1085#1103#1084
          ExplicitWidth = 618
        end
        inherited IL: TPngImageList
          Left = 40
          Top = 104
          Bitmap = {}
        end
        inherited AL: TActionList
          Left = 94
          Top = 104
        end
        inherited DataSource: TDataSource
          Left = 241
          Top = 104
        end
        inherited CRUDDisp: TFMACRUDDisp
          Fields = <
            item
              Name = 'id'
              Precision = 0
              FieldNo = 1
              Size = 0
              DataType = ftInteger
              Attributes = [faHiddenCol]
            end
            item
              Name = 'fighting_id'
              Precision = 0
              FieldNo = 2
              Size = 0
              DataType = ftInteger
              Attributes = [faHiddenCol, faRequired]
            end
            item
              Name = 'day'
              Precision = 0
              FieldNo = 3
              Size = 0
              DataType = ftDate
              Attributes = [faHiddenCol, faRequired]
              LongCaption = #1044#1072#1090#1072
              ShortCaption = #1044#1072#1090#1072
            end
            item
              Name = 'work_hours'
              Precision = 0
              FieldNo = 4
              Size = 0
              DataType = ftInteger
              Attributes = [faHiddenCol, faRequired]
              LongCaption = #1056#1072#1073#1086#1095#1080#1093' '#1095#1072#1089#1086#1074
              ShortCaption = #1056#1072#1073#1086#1095#1080#1093' '#1095#1072#1089#1086#1074
            end
            item
              Name = 'overtime_hours'
              Precision = 0
              FieldNo = 5
              Size = 0
              DataType = ftInteger
              Attributes = []
              LongCaption = #1057#1074#1077#1088#1093#1091#1088#1086#1095#1085#1099#1093' '#1095#1072#1089#1086#1074
              ShortCaption = #1057#1074#1077#1088#1093#1091#1088#1086#1095#1085#1099#1093' '#1095#1072#1089#1086#1074
            end
            item
              Name = 'holiday_hours'
              Precision = 0
              FieldNo = 6
              Size = 0
              DataType = ftInteger
              Attributes = []
              LongCaption = #1055#1088#1072#1079#1076#1085#1080#1095#1085#1099#1093'('#1074#1099#1093#1086#1076#1085#1099#1093') '#1095#1072#1089#1086#1074
              ShortCaption = #1055#1088#1072#1079#1076#1085#1080#1095#1085#1099#1093'('#1074#1099#1093#1086#1076#1085#1099#1093') '#1095#1072#1089#1086#1074
            end>
          CrudAgent = dmFightings.CrudAgentDetail
          EditFormDataSource = FormEditFightingDetail.dsLocal
          OnBeforeGetList = FrameDetailsCRUDDispBeforeGetList
          OnBeforeInsert = FrameDetailsCRUDDispBeforeInsert
          Left = 321
          Top = 104
        end
        inherited MemData: TMemTableEh
          FieldDefs = <
            item
              Name = 'id'
              DataType = ftInteger
            end
            item
              Name = 'fighting_id'
              DataType = ftInteger
            end
            item
              Name = 'day'
              DataType = ftDate
            end
            item
              Name = 'work_hours'
              DataType = ftInteger
            end
            item
              Name = 'overtime_hours'
              DataType = ftInteger
            end
            item
              Name = 'holiday_hours'
              DataType = ftInteger
            end
            item
              Name = 'created_at'
              DataType = ftTimeStamp
            end
            item
              Name = 'updated_at'
              DataType = ftTimeStamp
            end>
          StoreDefs = True
          Left = 161
          Top = 104
        end
        inherited pmGridMenu: TPopupMenu
          Left = 400
          Top = 104
        end
      end
    end
  end
  inherited cbAll: TCheckBox
    Width = 1063
    TabOrder = 3
    OnClick = cbAllClick
    ExplicitWidth = 1063
  end
  inherited MainActionList: TActionList
    Images = FrameFireLinked.IL
  end
end
