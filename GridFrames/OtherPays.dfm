inherited FormOtherPays: TFormOtherPays
  Caption = #1055#1088#1086#1095#1080#1077' '#1074#1099#1087#1083#1072#1090#1099
  ClientHeight = 517
  ClientWidth = 891
  ExplicitWidth = 907
  ExplicitHeight = 556
  TextHeight = 15
  inherited splFireEng: TSplitter
    Left = 405
    Top = 21
    Height = 455
    ExplicitLeft = 385
    ExplicitTop = 21
    ExplicitHeight = 476
  end
  inherited FrameFireLinked: TFrameBaseGrid
    Top = 21
    Width = 405
    Height = 455
    ExplicitTop = 21
    ExplicitWidth = 405
    ExplicitHeight = 455
    inherited dgGrid: TDBGridEh
      Width = 399
      Height = 406
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'name'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088' '#1087#1086#1078#1072#1088#1072
          Width = 79
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'department_name'
          Footers = <>
          Title.Caption = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
          Visible = False
          Width = 191
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'forestry_name'
          Footers = <>
          Title.Caption = #1051#1077#1089#1085#1080#1095#1077#1089#1090#1074#1086
          Width = 178
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'datefrom'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
          Width = 79
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'dateto'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
          Width = 96
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'square'
          Footers = <>
          Title.Caption = #1050#1074#1072#1088#1090#1072#1083
          Width = 84
        end>
    end
    inherited tbTools: TToolBar
      Width = 399
      ExplicitWidth = 399
      inherited ToolButton13: TToolButton
        Visible = False
      end
    end
    inherited plTitleDetails: TPanel
      Width = 399
      Caption = #1055#1086#1078#1072#1088#1099' '#1079#1072' '#1087#1077#1088#1080#1086#1076
      ExplicitWidth = 399
    end
    inherited IL: TPngImageList
      Bitmap = {}
    end
    inherited CRUDDisp: TFMACRUDDisp
      Fields = <
        item
          Precision = 0
          FieldNo = 0
          Size = 0
          DataType = ftUnknown
          Attributes = []
        end>
    end
    inherited MemData: TMemTableEh
      AfterScroll = FrameFireLinkedMemDataAfterScroll
    end
    inherited pmGridMenu: TPopupMenu
      Left = 248
      Top = 176
    end
  end
  inherited pl1: TPanel
    Top = 476
    Width = 891
    ExplicitTop = 476
    ExplicitWidth = 891
    inherited btOk: TButton
      Left = 688
      ExplicitLeft = 688
    end
    inherited btCancel: TButton
      Left = 791
      ExplicitLeft = 791
    end
  end
  object plAll: TPanel [3]
    Left = 408
    Top = 21
    Width = 483
    Height = 455
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    inline FrameGrid: TFrameBaseGrid
      Left = 0
      Top = 0
      Width = 483
      Height = 455
      Align = alClient
      TabOrder = 0
      ExplicitWidth = 483
      ExplicitHeight = 455
      inherited dgGrid: TDBGridEh
        Width = 477
        Height = 406
        TitleParams.Font.Height = -13
        OnDrawColumnCell = FrameGriddgGridDrawColumnCell
        Columns = <
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'fire_case_name'
            Footers = <>
            Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1086#1078#1072#1088#1072
            Width = 234
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'department_code'
            Footers = <>
            Title.Caption = #1054#1090#1076#1077#1083#1077#1085#1080#1077
            Width = 150
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'worker_code'
            Footers = <>
            Title.Caption = #1057#1086#1090#1088#1091#1076#1085#1080#1082
            Width = 333
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'payment_code'
            Footers = <>
            Title.Caption = #1042#1099#1087#1083#1072#1090#1072
            Width = 157
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'hours'
            Footers = <>
            Title.Caption = #1063#1072#1089#1086#1074
            Width = 48
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DisplayFormat = '##0.00'
            DynProps = <>
            EditButtons = <>
            FieldName = 'percent'
            Footers = <>
            Title.Caption = #1055#1088#1086#1094#1077#1085#1090' '#1086#1087#1083#1072#1090#1099
            Width = 63
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            Checkboxes = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'isholiday'
            Footers = <>
            Title.Caption = #1042#1099#1093#1086#1076#1085#1086#1081'/ '#1087#1088#1072#1079#1076#1085#1080#1095#1085#1099#1081
            Width = 85
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            Checkboxes = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'isstorno_num'
            Footers = <>
            Title.Caption = #1057#1090#1086#1088#1085#1086
            Width = 65
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DisplayFormat = '### ### ##0.00'
            DynProps = <>
            EditButtons = <>
            FieldName = 'summa'
            Footers = <>
            Title.Caption = #1057#1091#1084#1084#1072
            Width = 87
          end>
      end
      inherited tbTools: TToolBar
        Width = 477
        ExplicitWidth = 477
        inherited ToolButton13: TToolButton
          Action = acStorno1
        end
      end
      inherited plTitleDetails: TPanel
        Width = 477
        Caption = #1055#1088#1086#1095#1080#1077' '#1074#1099#1087#1083#1072#1090#1099' '#1074' '#1090#1077#1082#1091#1097#1077#1084' '#1088#1072#1089#1095#1077#1090#1077
        ExplicitWidth = 477
      end
      inherited IL: TPngImageList
        Left = 40
        Top = 120
        Bitmap = {}
      end
      inherited AL: TActionList
        Left = 88
        Top = 120
        inherited acMulty: TAction
          Visible = True
          OnExecute = FrameGridacMultyExecute
        end
      end
      inherited DataSource: TDataSource
        Left = 224
        Top = 120
      end
      inherited CRUDDisp: TFMACRUDDisp
        Fields = <
          item
            Name = 'id'
            Precision = 0
            FieldNo = 1
            Size = 0
            DataType = ftInteger
            Attributes = [faHiddenCol]
          end
          item
            Name = 'fire_case_id'
            Precision = 0
            FieldNo = 2
            Size = 0
            DataType = ftInteger
            Attributes = [faHiddenCol, faRequired]
          end
          item
            Name = 'payment_id'
            Precision = 0
            FieldNo = 3
            Size = 38
            DataType = ftGuid
            Attributes = [faHiddenCol, faRequired]
          end
          item
            Name = 'worker_id'
            Precision = 0
            FieldNo = 4
            Size = 38
            DataType = ftGuid
            Attributes = [faHiddenCol, faRequired]
          end
          item
            Name = 'calc_period_id'
            Precision = 0
            FieldNo = 5
            Size = 0
            DataType = ftInteger
            Attributes = [faHiddenCol, faRequired]
          end
          item
            Name = 'hours'
            Precision = 5
            FieldNo = 6
            Size = 2
            DataType = ftBCD
            Attributes = []
            LongCaption = #1063#1072#1089#1086#1074
            ShortCaption = #1063#1072#1089#1086#1074
          end
          item
            Name = 'percent'
            Precision = 5
            FieldNo = 7
            Size = 2
            DataType = ftBCD
            Attributes = [faRequired]
            LongCaption = #1055#1088#1086#1094#1077#1085#1090' '#1086#1087#1083#1072#1090#1099
            ShortCaption = #1055#1088#1086#1094#1077#1085#1090' '#1086#1087#1083#1072#1090#1099
          end
          item
            Name = 'isholiday'
            Precision = 0
            FieldNo = 8
            Size = 5
            DataType = ftString
            Attributes = []
            LongCaption = #1055#1088#1080#1079#1085#1072#1082' '#1074#1099#1093#1086#1076#1085#1086#1075#1086'/'#1087#1088#1072#1079#1076#1085#1080#1095#1085#1086#1075#1086' '#1076#1085#1103
            ShortCaption = #1042#1099#1093#1086#1076#1085#1086#1081'/'#1087#1088#1072#1079#1076#1085#1080#1095#1085#1099#1081
          end
          item
            Name = 'summa'
            Precision = 12
            FieldNo = 9
            Size = 2
            DataType = ftBCD
            Attributes = []
            LongCaption = #1057#1091#1084#1084#1072
            ShortCaption = #1057#1091#1084#1084#1072
          end
          item
            Name = 'fire_case_name'
            Precision = 0
            FieldNo = 12
            Size = 250
            DataType = ftWideString
            Attributes = []
            LongCaption = #1055#1086#1078#1072#1088
            ShortCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1086#1078#1072#1088#1072
            KeyFieldName = 'fire_case_id'
          end
          item
            Name = 'payment_code'
            Precision = 0
            FieldNo = 13
            Size = 50
            DataType = ftWideString
            Attributes = []
            LongCaption = #1042#1099#1087#1083#1072#1090#1072
            ShortCaption = #1042#1099#1087#1083#1072#1090#1072
            KeyFieldName = 'payment_id'
          end
          item
            Name = 'worker_code'
            Precision = 0
            FieldNo = 14
            Size = 50
            DataType = ftWideString
            Attributes = []
            LongCaption = #1057#1086#1090#1088#1091#1076#1085#1080#1082
            ShortCaption = #1057#1086#1090#1088#1091#1076#1085#1080#1082
            KeyFieldName = 'worker_id'
          end>
        CrudAgent = dmOtherPays.CrudAgent
        EditFormDataSource = FormEditOtherPay.dsLocal
        OnBeforeGetList = FrameGridCRUDDispBeforeGetList
        OnBeforeInsert = FrameGridCRUDDispBeforeInsert
        Left = 296
        Top = 120
      end
      inherited MemData: TMemTableEh
        Left = 144
        Top = 120
      end
      inherited pmGridMenu: TPopupMenu
        Left = 152
        Top = 240
      end
    end
  end
  inherited cbAll: TCheckBox
    Width = 885
    Height = 18
    OnClick = cbAllClick
    ExplicitWidth = 885
    ExplicitHeight = 18
  end
  inherited MainActionList: TActionList
    Images = FrameFireLinked.IL
  end
end
