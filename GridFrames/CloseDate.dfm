inherited FormCloseDate: TFormCloseDate
  Caption = #1044#1072#1090#1072' '#1079#1072#1082#1088#1099#1090#1080#1103' '#1087#1077#1088#1080#1086#1076#1072
  Font.Height = -15
  ExplicitWidth = 650
  TextHeight = 20
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 345
    Height = 185
  end
  object dtCloseDate: TDBDateTimeEditEh
    Left = 32
    Top = 48
    Width = 119
    Height = 28
    ControlLabel.Width = 165
    ControlLabel.Height = 20
    ControlLabel.Caption = #1044#1072#1090#1072' '#1079#1072#1082#1088#1099#1090#1080#1103' '#1087#1077#1088#1080#1086#1076#1072
    ControlLabel.Visible = True
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 0
    Visible = True
    EditFormat = 'MM/YYYY'
  end
  object Button1: TButton
    Left = 32
    Top = 88
    Width = 121
    Height = 31
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    TabOrder = 1
  end
  object dsLocal: TDataSource
    Left = 232
    Top = 104
  end
end
