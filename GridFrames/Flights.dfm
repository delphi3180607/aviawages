inherited FormFlights: TFormFlights
  Caption = #1053#1072#1083#1077#1090#1099
  ClientHeight = 623
  ClientWidth = 930
  ExplicitWidth = 946
  ExplicitHeight = 662
  TextHeight = 15
  inherited splFireEng: TSplitter
    Left = 417
    Height = 553
    ExplicitLeft = 417
    ExplicitHeight = 581
  end
  inherited FrameFireLinked: TFrameBaseGrid
    Width = 417
    Height = 553
    ExplicitWidth = 417
    ExplicitHeight = 553
    inherited dgGrid: TDBGridEh
      Width = 411
      Height = 504
    end
    inherited tbTools: TToolBar
      Width = 411
      ExplicitWidth = 411
      inherited ToolButton13: TToolButton
        Visible = False
      end
    end
    inherited plTitleDetails: TPanel
      Width = 411
      Caption = #1055#1086#1078#1072#1088#1099' '#1079#1072' '#1087#1077#1088#1080#1086#1076
      ExplicitWidth = 411
    end
    inherited IL: TPngImageList
      Bitmap = {}
    end
    inherited CRUDDisp: TFMACRUDDisp
      Fields = <
        item
          Precision = 0
          FieldNo = 0
          Size = 0
          DataType = ftUnknown
          Attributes = []
        end>
    end
    inherited MemData: TMemTableEh
      AfterScroll = FrameFireLinkedMemDataAfterScroll
    end
  end
  inherited pl1: TPanel
    Top = 581
    Width = 930
    Height = 42
    ExplicitTop = 581
    ExplicitWidth = 930
    ExplicitHeight = 42
    inherited btOk: TButton
      Left = 727
      Height = 31
      ExplicitLeft = 727
      ExplicitHeight = 31
    end
    inherited btCancel: TButton
      Left = 830
      Height = 31
      ExplicitLeft = 830
      ExplicitHeight = 31
    end
  end
  object plAll: TPanel [3]
    Left = 420
    Top = 28
    Width = 510
    Height = 553
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    inline FrameGrid: TFrameBaseGrid
      Left = 0
      Top = 0
      Width = 510
      Height = 553
      Align = alClient
      TabOrder = 0
      ExplicitWidth = 510
      ExplicitHeight = 553
      inherited dgGrid: TDBGridEh
        Width = 504
        Height = 504
        OnDrawColumnCell = FrameGriddgGridDrawColumnCell
        Columns = <
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'fire_case_name'
            Footers = <>
            Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1086#1078#1072#1088#1072
            Width = 208
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'worker_code'
            Footers = <>
            Title.Caption = #1057#1086#1090#1088#1091#1076#1085#1080#1082
            Width = 418
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'plane_code'
            Footers = <>
            Title.Caption = #1042#1086#1079#1076#1091#1096#1085#1086#1077' '#1089#1091#1076#1085#1086
            Width = 135
          end
          item
            Alignment = taCenter
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'paygroup_code'
            Footers = <>
            Title.Caption = #1043#1088#1091#1087#1087#1072' '#1086#1087#1083#1072#1090#1099
            Width = 82
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'date_from'
            Footers = <>
            Title.Caption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
            Width = 77
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'date_to'
            Footers = <>
            Title.Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
            Width = 74
          end
          item
            Alignment = taCenter
            AutoFitColWidth = False
            CellButtons = <>
            DisplayFormat = 'HH:NN'
            DynProps = <>
            EditButtons = <>
            FieldName = 'hours'
            Footers = <>
            Title.Caption = #1063#1072#1089#1086#1074
            Width = 69
          end
          item
            CellButtons = <>
            Checkboxes = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'isstorno_num'
            Footers = <>
            Title.Caption = #1057#1090#1086#1088#1085#1086
            Width = 48
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DisplayFormat = '### ### ##0.00'
            DynProps = <>
            EditButtons = <>
            FieldName = 'summa'
            Footers = <>
            Title.Caption = #1057#1091#1084#1084#1072
            Width = 92
          end>
      end
      inherited tbTools: TToolBar
        Width = 504
        ExplicitWidth = 504
        inherited ToolButton13: TToolButton
          Action = acStorno1
        end
      end
      inherited plTitleDetails: TPanel
        Width = 504
        Caption = #1053#1072#1083#1077#1090#1099' '#1074' '#1090#1077#1082#1091#1097#1077#1084' '#1088#1072#1089#1095#1077#1090#1077
        ExplicitWidth = 504
      end
      inherited IL: TPngImageList
        Bitmap = {}
      end
      inherited AL: TActionList
        inherited acMulty: TAction
          Visible = True
          OnExecute = FrameGridacMultyExecute
        end
      end
      inherited CRUDDisp: TFMACRUDDisp
        Fields = <
          item
            Name = 'id'
            Precision = 0
            FieldNo = 1
            Size = 0
            DataType = ftInteger
            Attributes = [faHiddenCol]
          end
          item
            Name = 'fire_case_id'
            Precision = 0
            FieldNo = 2
            Size = 0
            DataType = ftInteger
            Attributes = [faHiddenCol, faRequired]
          end
          item
            Name = 'worker_id'
            Precision = 0
            FieldNo = 3
            Size = 38
            DataType = ftGuid
            Attributes = [faHiddenCol, faRequired]
          end
          item
            Name = 'plane_id'
            Precision = 0
            FieldNo = 4
            Size = 0
            DataType = ftInteger
            Attributes = [faHiddenCol, faRequired]
          end
          item
            Name = 'pay_group_id'
            Precision = 0
            FieldNo = 5
            Size = 0
            DataType = ftInteger
            Attributes = [faHiddenCol, faRequired]
          end
          item
            Name = 'calcperiod_id'
            Precision = 0
            FieldNo = 6
            Size = 0
            DataType = ftInteger
            Attributes = [faHiddenCol, faRequired]
          end
          item
            Name = 'date_from'
            Precision = 0
            FieldNo = 7
            Size = 0
            DataType = ftDate
            Attributes = [faRequired]
            LongCaption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
            ShortCaption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
          end
          item
            Name = 'date_to'
            Precision = 0
            FieldNo = 8
            Size = 0
            DataType = ftDate
            Attributes = []
            LongCaption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
            ShortCaption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
          end
          item
            Name = 'hours'
            Precision = 5
            FieldNo = 9
            Size = 2
            DataType = ftBCD
            Attributes = [faRequired]
            LongCaption = #1063#1072#1089#1086#1074
            ShortCaption = #1063#1072#1089#1086#1074
          end
          item
            Name = 'summa'
            Precision = 12
            FieldNo = 10
            Size = 2
            DataType = ftBCD
            Attributes = []
            LongCaption = #1057#1091#1084#1084#1072
            ShortCaption = #1057#1091#1084#1084#1072
          end
          item
            Name = 'worker_code'
            Precision = 0
            FieldNo = 13
            Size = 50
            DataType = ftWideString
            Attributes = []
            LongCaption = #1057#1086#1090#1088#1091#1076#1085#1080#1082
            ShortCaption = #1057#1086#1090#1088#1091#1076#1085#1080#1082
            KeyFieldName = 'worker_id'
          end
          item
            Name = 'fire_case_name'
            Precision = 0
            FieldNo = 14
            Size = 250
            DataType = ftWideString
            Attributes = []
            LongCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1086#1078#1072#1088#1072
            ShortCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1086#1078#1072#1088#1072
            KeyFieldName = 'fire_case_id'
          end
          item
            Name = 'plane_code'
            Precision = 0
            FieldNo = 15
            Size = 80
            DataType = ftWideString
            Attributes = []
            LongCaption = #1042#1086#1079#1076#1091#1096#1085#1086#1077' '#1089#1091#1076#1085#1086
            ShortCaption = #1042#1086#1079#1076#1091#1096#1085#1086#1077' '#1089#1091#1076#1085#1086
            KeyFieldName = 'plane_id'
          end
          item
            Name = 'paygroup_code'
            Precision = 0
            FieldNo = 16
            Size = 50
            DataType = ftWideString
            Attributes = []
            LongCaption = #1043#1088#1091#1087#1087#1072' '#1086#1087#1083#1072#1090#1099
            ShortCaption = #1043#1088#1091#1087#1087#1072' '#1086#1087#1083#1072#1090#1099
            KeyFieldName = 'pay_group_id'
          end>
        CrudAgent = dmFlights.CrudAgent
        EditFormDataSource = FormEditFlight.dsLocal
        OnBeforeGetList = FrameGridCRUDDispBeforeGetList
        OnBeforeInsert = FrameGridCRUDDispBeforeInsert
      end
    end
  end
  inherited cbAll: TCheckBox
    Width = 924
    OnClick = cbAllClick
    ExplicitWidth = 924
  end
  inherited MainActionList: TActionList
    Images = FrameFireLinked.IL
  end
end
