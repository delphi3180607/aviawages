unit Import;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, InitStated, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.StdCtrls, Vcl.ExtCtrls, MemTableDataEh, MemTableEh;

type
  TFormImport = class(TFormInitStated)
    btStart: TButton;
    lbWait: TLabel;
    procedure btStartClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormImport: TFormImport;

implementation

uses dmuutils;

{$R *.dfm}

procedure TFormImport.btStartClick(Sender: TObject);
begin
  lbWait.Caption := '�����, ����������� ������...';
  Screen.Cursor := crHourGlass;
  Application.ProcessMessages;
  try
    dmutils.smImport.ExecuteMethod;
  finally
    lbWait.Caption := '';
    Screen.Cursor := crDefault;
  end;
end;

end.
