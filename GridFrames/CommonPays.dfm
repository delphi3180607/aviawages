inherited FormCommonPays: TFormCommonPays
  Caption = #1054#1073#1097#1080#1077' '#1074#1099#1087#1083#1072#1090#1099
  ClientWidth = 632
  ExplicitWidth = 648
  TextHeight = 15
  inherited pl1: TPanel
    Width = 632
    TabOrder = 1
    ExplicitWidth = 632
    inherited btOk: TButton
      Left = 429
      ExplicitLeft = 429
    end
    inherited btCancel: TButton
      Left = 532
      ExplicitLeft = 532
    end
  end
  inline FrameGrid: TFrameBaseGrid [1]
    Left = 0
    Top = 0
    Width = 632
    Height = 406
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 632
    ExplicitHeight = 406
    inherited dgGrid: TDBGridEh
      Width = 626
      Height = 357
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'worker_code'
          Footers = <>
          Title.Caption = #1057#1086#1090#1088#1091#1076#1085#1080#1082
          Width = 488
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'payment_code'
          Footers = <>
          Title.Caption = #1042#1099#1087#1083#1072#1090#1072
          Width = 136
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DisplayFormat = '##0.000'
          DynProps = <>
          EditButtons = <>
          FieldName = 'percent'
          Footers = <>
          Title.Caption = #1055#1088#1086#1094#1077#1085#1090' '#1086#1087#1083#1072#1090#1099
          Width = 94
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DisplayFormat = '### ### ##0.00'
          DynProps = <>
          EditButtons = <>
          FieldName = 'summa'
          Footers = <>
          Title.Caption = #1057#1091#1084#1084#1072
          Width = 114
        end>
    end
    inherited tbTools: TToolBar
      Width = 626
      ExplicitWidth = 626
      inherited ToolButton13: TToolButton
        Visible = False
      end
    end
    inherited plTitleDetails: TPanel
      Width = 626
      Caption = #1054#1073#1097#1080#1077' '#1074#1099#1087#1083#1072#1090#1099' '#1074' '#1090#1077#1082#1091#1097#1077#1084' '#1088#1072#1089#1095#1077#1090#1077
      ExplicitWidth = 626
    end
    inherited IL: TPngImageList
      Bitmap = {}
    end
    inherited AL: TActionList
      inherited acMulty: TAction
        Visible = True
        OnExecute = FrameGridacMultyExecute
      end
    end
    inherited CRUDDisp: TFMACRUDDisp
      Fields = <
        item
          Name = 'id'
          Precision = 0
          FieldNo = 1
          Size = 0
          DataType = ftInteger
          Attributes = [faHiddenCol]
        end
        item
          Name = 'worker_id'
          Precision = 0
          FieldNo = 2
          Size = 38
          DataType = ftGuid
          Attributes = [faHiddenCol, faRequired]
        end
        item
          Name = 'payment_id'
          Precision = 0
          FieldNo = 3
          Size = 38
          DataType = ftGuid
          Attributes = [faHiddenCol, faRequired]
        end
        item
          Name = 'calcperiod_id'
          Precision = 0
          FieldNo = 4
          Size = 0
          DataType = ftInteger
          Attributes = [faHiddenCol, faRequired]
        end
        item
          Name = 'percent'
          Precision = 5
          FieldNo = 5
          Size = 2
          DataType = ftBCD
          Attributes = [faRequired]
          LongCaption = #1055#1088#1086#1094#1077#1085#1090' '#1086#1087#1083#1072#1090#1099
          ShortCaption = #1055#1088#1086#1094#1077#1085#1090' '#1086#1087#1083#1072#1090#1099
        end
        item
          Name = 'summa'
          Precision = 12
          FieldNo = 6
          Size = 2
          DataType = ftBCD
          Attributes = []
          LongCaption = #1057#1091#1084#1084#1072
          ShortCaption = #1057#1091#1084#1084#1072
        end
        item
          Name = 'worker_code'
          Precision = 0
          FieldNo = 9
          Size = 50
          DataType = ftWideString
          Attributes = []
          LongCaption = #1057#1086#1090#1088#1091#1076#1085#1080#1082
          ShortCaption = #1057#1086#1090#1088#1091#1076#1085#1080#1082
          KeyFieldName = 'worker_id'
        end
        item
          Name = 'payment_code'
          Precision = 0
          FieldNo = 10
          Size = 50
          DataType = ftWideString
          Attributes = []
          LongCaption = #1042#1099#1087#1083#1072#1090#1072
          ShortCaption = #1042#1099#1087#1083#1072#1090#1072
          KeyFieldName = 'payment_id'
        end>
      CrudAgent = dmCommonPays.CrudAgent
      EditFormDataSource = FormEditCommonPay.dsLocal
      OnBeforeGetList = FrameGridCRUDDispBeforeGetList
      OnBeforeInsert = FrameGridCRUDDispBeforeInsert
    end
  end
end
