inherited FormCalcPeriods: TFormCalcPeriods
  Caption = #1056#1072#1089#1095#1077#1090#1085#1099#1077' '#1087#1077#1088#1080#1086#1076#1099
  ClientWidth = 636
  ExplicitWidth = 652
  TextHeight = 15
  inherited pl1: TPanel
    Width = 636
    TabOrder = 1
    ExplicitWidth = 636
    inherited btOk: TButton
      Left = 433
      ExplicitLeft = 433
    end
    inherited btCancel: TButton
      Left = 536
      ExplicitLeft = 536
    end
  end
  inline FrameGrid: TFrameBaseGrid [1]
    Left = 0
    Top = 0
    Width = 636
    Height = 406
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 636
    ExplicitHeight = 406
    inherited dgGrid: TDBGridEh
      Width = 630
      Height = 357
      Font.Height = -16
      TitleParams.Font.Height = -16
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DisplayFormat = 'MM/YYYY'
          DynProps = <>
          EditButtons = <>
          FieldName = 'period_begin'
          Footers = <>
          Title.Caption = #1056#1072#1089#1095#1077#1090#1085#1099#1081' '#1087#1077#1088#1080#1086#1076
          Width = 150
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_closed'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1079#1072#1082#1088#1099#1090#1080#1103
          Width = 150
        end>
    end
    inherited tbTools: TToolBar
      Width = 630
      ExplicitWidth = 630
      inherited ToolButton13: TToolButton
        Visible = False
      end
    end
    inherited plTitleDetails: TPanel
      Width = 630
      ExplicitWidth = 630
    end
    inherited IL: TPngImageList
      Bitmap = {}
    end
    inherited CRUDDisp: TFMACRUDDisp
      Fields = <
        item
          Name = 'id'
          Precision = 0
          FieldNo = 1
          Size = 0
          DataType = ftInteger
          Attributes = [faHiddenCol]
        end
        item
          Name = 'period_begin'
          Precision = 0
          FieldNo = 2
          Size = 0
          DataType = ftDate
          Attributes = [faRequired]
          LongCaption = #1056#1072#1089#1095#1077#1090#1085#1099#1081' '#1087#1077#1088#1080#1086#1076
          ShortCaption = #1056#1072#1089#1095#1077#1090#1085#1099#1081' '#1087#1077#1088#1080#1086#1076
        end
        item
          Name = 'date_closed'
          Precision = 0
          FieldNo = 3
          Size = 0
          DataType = ftDate
          Attributes = []
          LongCaption = #1044#1072#1090#1072' '#1079#1072#1082#1088#1099#1090#1080#1103
          ShortCaption = #1044#1072#1090#1072' '#1079#1072#1082#1088#1099#1090#1080#1103
        end>
      CrudAgent = dmCalcPeriods.CrudAgent
      OnCheckState = FrameGridCRUDDispCheckState
      EditFormDataSource = FormEditCalcPeriod.dsLocal
    end
    inherited MemData: TMemTableEh
      FieldDefs = <
        item
          Name = 'id'
          DataType = ftInteger
        end
        item
          Name = 'period_begin'
          DataType = ftDate
        end
        item
          Name = 'date_closed'
          DataType = ftDate
        end
        item
          Name = 'created_at'
          DataType = ftDate
        end
        item
          Name = 'updated_at'
          DataType = ftDate
        end>
      StoreDefs = True
    end
  end
end
