unit StronoContainer;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, InitStated, MemTableDataEh, Data.DB,
  MemTableEh, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Mask, DBCtrlsEh, BaseGrid;

type
  TFormStornoContainer = class(TFormInitStated)
    plWorkSpace: TPanel;
    plOperation: TPanel;
    lbOperation: TLabel;
    Label1: TLabel;
    plTop: TPanel;
    Shape1: TShape;
    plCalcPeriod: TPanel;
    Label5: TLabel;
    dtCalcPeriod: TDBDateTimeEditEh;
    plPeriod: TPanel;
    Label3: TLabel;
    Label2: TLabel;
    dtBegin: TDBDateTimeEditEh;
    dtEnd: TDBDateTimeEditEh;
    procedure dtCalcPeriodChange(Sender: TObject);
  private
    FOperationCaption: string;
    FOperation: integer;
    FForm: TFormInitStated;
    procedure SetOperation(value: integer);
  public
    procedure InitGrid(grid: TFormInitStated);
    property Operation: integer read FOperation write SetOperation;
    property OperationCaption: string read FOperationCaption;
    property ImbeddedForm: TFormInitStated read FForm;
  end;

var
  FormStornoContainer: TFormStornoContainer;

implementation

uses dmucurrentperiod;

{$R *.dfm}

{ TFormStornoContainer }

procedure TFormStornoContainer.dtCalcPeriodChange(Sender: TObject);
var dtCalcPeriodDate, dtBeginDate, dtEndDate: TDateTime;
begin
  if not Assigned(FForm) then exit;
  if not Assigned(FForm.context_data_module) then exit;
  FForm.context_data_module.ChangePeriod(dtCalcPeriod.Value, false);
  ActiveControl := nil;
  FForm.context_data_module.SetCalcPeriod(dtCalcPeriodDate, dtBeginDate, dtEndDate);
  FForm.Reset;
  dtCalcPeriod.Value := dtCalcPeriodDate;
  dtBegin.Value := dtBeginDate;
  dtEnd.Value := dtEndDate;
end;

procedure TFormStornoContainer.InitGrid(grid: TFormInitStated);
var period_begin: TDateTime;
begin
  Application.CreateForm(TComponentClass(grid.ClassType), FForm);
  period_begin := dmcurrentperiod.CalcPeriodDate;
  period_begin := IncMonth(period_begin, -1);
  dtCalcPeriod.Value := period_begin;
  FForm.context_data_module := TDMCurrentPeriod.Create(self);
  FForm.StandAlone := true;
  dtCalcPeriodChange(dtCalcPeriod);
  FForm.Init;
  FForm.Parent := plWorkSpace;
  FForm.BorderStyle := bsNone;
  FForm.Align := alClient;
  Caption := '�������� ������ ������ �� �������� �������';
  FForm.Show;
end;


procedure TFormStornoContainer.SetOperation(value: integer);
begin
  FOperationCaption := '';
  case value of
    0: FOperationCaption := '������������� (������������� ������������� �������� � ���������� �����������)';
    1: FOperationCaption := '������������� ������������� ���������';
    2: FOperationCaption := '���������� ����������� ���������';
  end;
  FOperation := value;
  lbOperation.Caption := ' '+LowerCase(FOperationCaption);
end;

end.
