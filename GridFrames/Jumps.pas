unit Jumps;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, DB, InitStated, BaseGrid, dmujumps, editjump,
  Vcl.StdCtrls, Vcl.ExtCtrls, workers, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client, BaseFireLinked,
  MemTableDataEh, MemTableEh, System.Actions, Vcl.ActnList, GridsEh, DBGridEh;

type
  TFormJumps = class(TFormBaseFireLinked)
    plAll: TPanel;
    FrameGrid: TFrameBaseGrid;
    procedure FrameGridCRUDDispBeforeGetList(Sender: TObject);
    procedure FrameGridCRUDDispBeforeInsert(Sender: TObject);
    procedure FrameGridacMultyExecute(Sender: TObject);
    procedure FrameFireLinkedMemDataAfterScroll(DataSet: TDataSet);
    procedure cbAllClick(Sender: TObject);
    procedure FrameGriddgGridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
  private
    { Private declarations }
  protected
    procedure InsertStronoDocument; override;
  public
  end;

var
  FormJumps: TFormJumps;

implementation

{$R *.dfm}

uses dmucurrentperiod;

{ TFormJumps }

procedure TFormJumps.cbAllClick(Sender: TObject);
begin
  FrameFireLinkedMemDataAfterScroll(FrameFireLinked.MemData);
  if not FrameGrid.CRUDDisp.GetList(nil,errormessage) then
  begin
    MessageDlg(errormessage, mtError, [mbOK], 0);
  end;
end;

procedure TFormJumps.FrameFireLinkedMemDataAfterScroll(DataSet: TDataSet);
begin
  FrameGrid.acRefresh.Execute;
end;

procedure TFormJumps.FrameGridacMultyExecute(Sender: TObject);
begin
  FrameGrid.DoMutipleAdd('worker_id','id',FormWorkers.FrameWorkers.CRUDDisp);
end;

procedure TFormJumps.FrameGridCRUDDispBeforeGetList(Sender: TObject);
begin
  if cbAll.Checked then
    FrameGrid.CRUDDisp.CrudAgent.ParamTransform.ResetContext
  else
    FrameGrid.CRUDDisp.ApplyMasterDetailParams(FrameFireLinked.CRUDDisp, 'fire_case_id', 'id');

  with FrameGrid.CRUDDisp.CRUDAgent do
  begin
    ParamTransform.AddPatternContext('/*wherefilter*/','calc_period_id = :calc_period_id');
    ParamTransform.AddParameterContext('calc_period_id', ftInteger, context_data_module.CalcPeriodId);
  end;
end;

procedure TFormJumps.FrameGridCRUDDispBeforeInsert(Sender: TObject);
begin
  inherited;
  FrameGrid.MemData.FieldByName('calc_period_id').Value := context_data_module.CalcPeriodId;
  if StornoFireCaseId>0 then
    FrameGrid.MemData.FieldByName('fire_case_id').Value := StornoFireCaseId
  else
    FrameGrid.MemData.FieldByName('fire_case_id').Value := FrameFireLinked.MemData.FieldByName('id').Value;
end;

procedure TFormJumps.FrameGriddgGridDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
    if (not (gdSelected in State)) or (not FrameGrid.dgGrid.Focused)
    then
    begin
      if FrameGrid.MemData.FieldByName('isstorno_num').AsInteger>0 then
        FrameGrid.dgGrid.Canvas.Brush.Color := $00D6D1FC;
       FrameGrid.dgGrid.DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end;
end;

procedure TFormJumps.InsertStronoDocument;
begin
  inherited;
  FrameGrid.CRUDDisp.Insert;
end;

end.
