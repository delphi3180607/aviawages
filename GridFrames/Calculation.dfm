inherited FormCalculation: TFormCalculation
  Caption = #1056#1072#1089#1095#1077#1090
  ClientHeight = 642
  ClientWidth = 876
  ExplicitWidth = 892
  ExplicitHeight = 681
  TextHeight = 15
  object Splitter2: TSplitter [0]
    Left = 241
    Top = 0
    Height = 601
    ExplicitLeft = 247
    ExplicitTop = 8
    ExplicitHeight = 447
  end
  inherited pl1: TPanel
    Top = 601
    Width = 876
    TabOrder = 1
    ExplicitTop = 601
    ExplicitWidth = 876
    inherited btOk: TButton
      Left = 673
      ExplicitLeft = 673
    end
    inherited btCancel: TButton
      Left = 776
      ExplicitLeft = 776
    end
  end
  object plAll: TPanel [2]
    Left = 244
    Top = 0
    Width = 632
    Height = 601
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 201
      Width = 632
      Height = 3
      Cursor = crVSplit
      Align = alBottom
      ExplicitTop = 200
      ExplicitWidth = 47
    end
    inline FrameWorkers: TFrameBaseSimpleGrid
      Left = 0
      Top = 34
      Width = 632
      Height = 167
      Align = alClient
      TabOrder = 0
      ExplicitTop = 34
      ExplicitWidth = 632
      ExplicitHeight = 167
      inherited dgGrid: TDBGridEh
        Width = 626
        Height = 146
        Font.Height = -12
        IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh, gioShowRowselCheckboxesEh]
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        PopupMenu = FrameCalculation.pmGridMenu
        OnDrawColumnCell = FrameWorkersdgGridDrawColumnCell
        Columns = <
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'department_code'
            Footers = <>
            Title.Caption = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
            Width = 151
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'code'
            Footers = <>
            Title.Caption = #1060'.'#1048'.'#1054'.'
            Width = 198
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'post'
            Footers = <>
            Title.Caption = #1044#1086#1083#1078#1085#1086#1089#1090#1100
            Width = 247
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'kind'
            Footers = <>
            Title.Caption = #1042#1080#1076' '#1080#1089#1087#1086#1083#1085#1077#1085#1080#1103
            Width = 174
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'schedule_code'
            Footers = <>
            Title.Caption = #1043#1088#1072#1092#1080#1082' '#1088#1072#1073#1086#1090#1099
            Width = 116
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DisplayFormat = '##0.000'
            DynProps = <>
            EditButtons = <>
            FieldName = 'ratevol'
            Footers = <>
            Title.Caption = #1057#1090#1072#1074#1086#1082
            Width = 66
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'date_begin'
            Footers = <>
            Title.Caption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
            Width = 96
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'date_end'
            Footers = <>
            Title.Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
            Width = 108
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DisplayFormat = '### ### ##0.##'
            DynProps = <>
            EditButtons = <>
            FieldName = 'summ_calc'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Segoe UI'
            Font.Style = []
            Footers = <>
            Title.Caption = #1053#1072#1095#1080#1089#1083#1077#1085#1085#1086
            Width = 82
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            Checkboxes = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'isdeleted'
            Footers = <>
            Title.Caption = #1059#1076#1072#1083#1077#1085'?'
            Width = 61
          end>
      end
      inherited plTitleDetails: TPanel
        Width = 626
        Caption = #1056#1072#1089#1095#1077#1090' '#1074' '#1090#1077#1082#1091#1097#1077#1084' '#1087#1077#1088#1080#1086#1076#1077
        ExplicitWidth = 626
      end
      inherited MemData: TMemTableEh
        AfterScroll = FrameWorkersMemDataAfterScroll
        Left = 80
        Top = 72
      end
      inherited DataSource: TDataSource
        Left = 184
        Top = 72
      end
      inherited CRUDDisp: TFMACRUDDisp
        Fields = <
          item
            Name = 'id'
            Precision = 0
            FieldNo = 1
            Size = 39
            DataType = ftString
            Attributes = [faHiddenCol]
          end
          item
            Name = 'department_id'
            Precision = 0
            FieldNo = 2
            Size = 39
            DataType = ftString
            Attributes = [faHiddenCol]
          end
          item
            Name = 'department_code'
            Precision = 0
            FieldNo = 3
            Size = 162
            DataType = ftWideString
            Attributes = []
            LongCaption = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
            ShortCaption = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
          end
          item
            Name = 'department_name'
            Precision = 0
            FieldNo = 4
            Size = 502
            DataType = ftWideString
            Attributes = [faHiddenCol]
          end
          item
            Name = 'code'
            Precision = 0
            FieldNo = 5
            Size = 102
            DataType = ftWideString
            Attributes = []
            LongCaption = #1060'.'#1048'.'#1054'.'
            ShortCaption = #1060'.'#1048'.'#1054'.'
          end
          item
            Name = 'full_code'
            Precision = 0
            FieldNo = 6
            Size = 502
            DataType = ftWideString
            Attributes = [faHiddenCol]
          end
          item
            Name = 'name'
            Precision = 0
            FieldNo = 7
            Size = 502
            DataType = ftWideString
            Attributes = [faHiddenCol]
          end
          item
            Name = 'post'
            Precision = 0
            FieldNo = 8
            Size = 502
            DataType = ftWideString
            Attributes = []
            LongCaption = #1044#1086#1083#1078#1085#1086#1089#1090#1100
            ShortCaption = #1044#1086#1083#1078#1085#1086#1089#1090#1100
          end
          item
            Name = 'kind'
            Precision = 0
            FieldNo = 9
            Size = 242
            DataType = ftWideString
            Attributes = []
            LongCaption = #1042#1080#1076' '#1080#1089#1087#1086#1083#1085#1077#1085#1080#1103
            ShortCaption = #1042#1080#1076' '#1080#1089#1087#1086#1083#1085#1077#1085#1080#1103
          end
          item
            Name = 'ratevol'
            Precision = 5
            FieldNo = 10
            Size = 3
            DataType = ftBCD
            Attributes = []
            LongCaption = #1057#1090#1072#1074#1086#1082
            ShortCaption = #1057#1090#1072#1074#1086#1082
          end
          item
            Name = 'schedule_id'
            Precision = 0
            FieldNo = 11
            Size = 39
            DataType = ftString
            Attributes = [faHiddenCol]
          end
          item
            Name = 'date_begin'
            Precision = 0
            FieldNo = 12
            Size = 0
            DataType = ftDate
            Attributes = []
            LongCaption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
            ShortCaption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
          end
          item
            Name = 'date_end'
            Precision = 0
            FieldNo = 13
            Size = 0
            DataType = ftDate
            Attributes = []
            LongCaption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
            ShortCaption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
          end>
        CrudAgent = dmWorkers.CrudAgentExtended
        OnCheckState = FrameWorkersCRUDDispCheckState
        OnBeforeGetList = FrameWorkersCRUDDispBeforeGetList
        Left = 280
        Top = 72
      end
      inherited AL: TActionList
        Left = 448
        Top = 72
      end
      inherited pmGridMenu: TPopupMenu
        Left = 384
        Top = 72
      end
    end
    object plCalc: TPanel
      Left = 0
      Top = 0
      Width = 632
      Height = 34
      Align = alTop
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      object btCalc: TButton
        AlignWithMargins = True
        Left = 3
        Top = 0
        Width = 278
        Height = 31
        Margins.Top = 0
        Align = alLeft
        Caption = #1055#1088#1086#1080#1079#1074#1077#1089#1090#1080' '#1088#1072#1089#1095#1077#1090' '#1074' '#1090#1077#1082#1091#1097#1077#1084' '#1087#1077#1088#1080#1086#1076#1077
        TabOrder = 0
        OnClick = btCalcClick
      end
      object btExport: TButton
        AlignWithMargins = True
        Left = 287
        Top = 0
        Width = 194
        Height = 31
        Margins.Top = 0
        Align = alLeft
        Caption = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1086#1090#1084#1077#1095#1077#1085#1085#1099#1093
        TabOrder = 1
        OnClick = btExportClick
      end
    end
    object plBottom: TPanel
      Left = 0
      Top = 204
      Width = 632
      Height = 397
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      object Splitter3: TSplitter
        Left = 0
        Top = 354
        Width = 632
        Height = 3
        Cursor = crVSplit
        Align = alBottom
        ExplicitTop = 302
        ExplicitWidth = 635
      end
      inline FrameCalculation: TFrameBaseSimpleGrid
        Left = 0
        Top = 0
        Width = 632
        Height = 354
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 632
        ExplicitHeight = 354
        inherited dgGrid: TDBGridEh
          Top = 17
          Width = 626
          Height = 337
          Margins.Bottom = 0
          Font.Height = -12
          FooterRowCount = 1
          FooterParams.FillStyle = cfstThemedEh
          IndicatorOptions = [gioShowRowIndicatorEh]
          OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove]
          SortLocal = False
          SumList.Active = True
          SumList.VirtualRecords = True
          Columns = <
            item
              AutoFitColWidth = False
              CellButtons = <>
              DynProps = <>
              EditButtons = <>
              FieldName = 'fire_case_name'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Segoe UI'
              Font.Style = [fsBold]
              Footers = <>
              HideDuplicates = True
              Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1086#1078#1072#1088#1072
              Width = 292
              WordWrap = True
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              DynProps = <>
              EditButtons = <>
              FieldName = 'payment_code'
              Footers = <>
              Title.Caption = #1042#1099#1087#1083#1072#1090#1072
              Width = 150
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              DisplayFormat = '### ### ##0.00'
              DynProps = <>
              EditButtons = <>
              FieldName = 'base_tarif'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Segoe UI'
              Font.Style = [fsItalic]
              Footers = <>
              Title.Caption = #1041#1072#1079#1086#1074#1099#1081' '#1090#1072#1088#1080#1092
              Width = 82
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              DynProps = <>
              EditButtons = <>
              FieldName = 'norma'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Segoe UI'
              Font.Style = [fsItalic]
              Footers = <>
              Title.Caption = #1053#1086#1088#1084#1072
              Width = 66
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              DynProps = <>
              EditButtons = <>
              FieldName = 'fact'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Segoe UI'
              Font.Style = [fsItalic]
              Footers = <>
              Title.Caption = #1060#1072#1082#1090
              Width = 54
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              DynProps = <>
              EditButtons = <>
              FieldName = 'percent'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Segoe UI'
              Font.Style = [fsItalic]
              Footers = <>
              Title.Caption = #1055#1088#1086#1094#1077#1085#1090
              Width = 68
            end
            item
              CellButtons = <>
              DisplayFormat = '### ### ##0.00'
              DynProps = <>
              EditButtons = <>
              FieldName = 'summa'
              Footer.DisplayFormat = '########.00'
              Footer.FieldName = 'summa'
              Footer.Font.Charset = DEFAULT_CHARSET
              Footer.Font.Color = clWindowText
              Footer.Font.Height = -12
              Footer.Font.Name = 'Segoe UI'
              Footer.Font.Style = [fsBold]
              Footer.ValueType = fvtSum
              Footers = <>
              Title.Caption = #1057#1091#1084#1084#1072
              Width = 86
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              Checkboxes = True
              DynProps = <>
              EditButtons = <>
              FieldName = 'isdistr'
              Footers = <>
              Title.Caption = #1055#1088#1080#1079#1085#1072#1082' '#1088#1072#1089#1087#1088#1077#1076#1077#1083#1077#1085#1080#1103
              Width = 117
            end>
        end
        inherited plTitleDetails: TPanel
          Width = 626
          Height = 17
          Caption = #1056#1072#1089#1095#1077#1090#1085#1099#1077' '#1074#1099#1087#1083#1072#1090#1099
          ExplicitWidth = 626
          ExplicitHeight = 17
        end
        inherited MemData: TMemTableEh
          Left = 80
          Top = 88
        end
        inherited DataSource: TDataSource
          Left = 192
          Top = 88
        end
        inherited CRUDDisp: TFMACRUDDisp
          Fields = <
            item
              Name = 'id'
              Precision = 0
              FieldNo = 1
              Size = 0
              DataType = ftInteger
              Attributes = [faHiddenCol]
            end
            item
              Name = 'calc_period_id'
              Precision = 0
              FieldNo = 2
              Size = 0
              DataType = ftInteger
              Attributes = [faHiddenCol]
            end
            item
              Name = 'fire_case_id'
              Precision = 0
              FieldNo = 3
              Size = 0
              DataType = ftInteger
              Attributes = [faHiddenCol]
            end
            item
              Name = 'fire_case_name'
              Precision = 0
              FieldNo = 4
              Size = 502
              DataType = ftWideString
              Attributes = []
              LongCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1086#1078#1072#1088#1072
              ShortCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1086#1078#1072#1088#1072
            end
            item
              Name = 'fact'
              Precision = 6
              FieldNo = 5
              Size = 2
              DataType = ftBCD
              Attributes = []
              LongCaption = #1060#1072#1082#1090
              ShortCaption = #1060#1072#1082#1090
            end
            item
              Name = 'norma'
              Precision = 6
              FieldNo = 6
              Size = 2
              DataType = ftBCD
              Attributes = []
              LongCaption = #1053#1086#1088#1084#1072
              ShortCaption = #1053#1086#1088#1084#1072
            end
            item
              Name = 'hours'
              Precision = 6
              FieldNo = 7
              Size = 2
              DataType = ftBCD
              Attributes = []
              LongCaption = #1063#1072#1089#1099
              ShortCaption = #1063#1072#1089#1099
            end
            item
              Name = 'percent'
              Precision = 6
              FieldNo = 8
              Size = 2
              DataType = ftBCD
              Attributes = []
              LongCaption = #1055#1088#1086#1094#1077#1085#1090
              ShortCaption = #1055#1088#1086#1094#1077#1085#1090
            end
            item
              Name = 'worker_id'
              Precision = 0
              FieldNo = 9
              Size = 39
              DataType = ftString
              Attributes = [faHiddenCol]
            end
            item
              Name = 'schedule_id'
              Precision = 0
              FieldNo = 10
              Size = 39
              DataType = ftString
              Attributes = [faHiddenCol]
            end
            item
              Name = 'payment_id'
              Precision = 0
              FieldNo = 11
              Size = 39
              DataType = ftString
              Attributes = [faHiddenCol]
            end
            item
              Name = 'payment_code'
              Precision = 0
              FieldNo = 12
              Size = 102
              DataType = ftWideString
              Attributes = []
              LongCaption = #1042#1099#1087#1083#1072#1090#1072
              ShortCaption = #1042#1099#1087#1083#1072#1090#1072
            end
            item
              Name = 'payment_name'
              Precision = 0
              FieldNo = 13
              Size = 502
              DataType = ftWideString
              Attributes = [faHiddenCol]
            end
            item
              Name = 'summa'
              Precision = 12
              FieldNo = 14
              Size = 2
              DataType = ftBCD
              Attributes = []
              LongCaption = #1057#1091#1084#1084#1072
              ShortCaption = #1057#1091#1084#1084#1072
            end
            item
              Name = 'isdistributed'
              Precision = 0
              FieldNo = 15
              Size = 6
              DataType = ftString
              Attributes = []
              LongCaption = #1055#1088#1080#1079#1085#1072#1082' '#1088#1072#1089#1087#1088#1077#1076#1077#1083#1077#1085#1080#1103
              ShortCaption = #1055#1088#1080#1079#1085#1072#1082' '#1088#1072#1089#1087#1088#1077#1076#1077#1083#1077#1085#1080#1103
            end
            item
              Name = 'distribution_calc_period_id'
              Precision = 0
              FieldNo = 16
              Size = 0
              DataType = ftInteger
              Attributes = [faHiddenCol]
            end
            item
              Name = 'created_at'
              Precision = 0
              FieldNo = 17
              Size = 0
              DataType = ftTimeStamp
              Attributes = [faHiddenCol]
            end
            item
              Name = 'updated_at'
              Precision = 0
              FieldNo = 18
              Size = 0
              DataType = ftTimeStamp
              Attributes = [faHiddenCol]
            end>
          CrudAgent = dmAccurals.CrudAgent
          OnBeforeGetList = FrameCalculationCRUDDispBeforeGetList
          OnAfterGetList = FrameCalculationCRUDDispAfterGetList
          Left = 288
          Top = 88
        end
        inherited AL: TActionList
          Left = 456
          Top = 96
        end
        inherited pmGridMenu: TPopupMenu
          Left = 376
          Top = 88
        end
      end
      object tsFireCases: TTabControl
        AlignWithMargins = True
        Left = 0
        Top = 357
        Width = 632
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alBottom
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        MultiLine = True
        ParentFont = False
        TabOrder = 1
        OnChange = tsFireCasesChange
      end
    end
  end
  object plLeft: TPanel [3]
    Left = 0
    Top = 0
    Width = 241
    Height = 601
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 2
    inline FrameDepsTree: TFrameBaseTree
      Left = 0
      Top = 22
      Width = 241
      Height = 579
      Align = alClient
      TabOrder = 0
      ExplicitTop = 22
      ExplicitWidth = 241
      ExplicitHeight = 579
      inherited plTitleDetails: TPanel
        Width = 241
        Caption = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1103
        Color = 15920878
        Font.Style = []
        ExplicitWidth = 241
      end
      inherited tvData: TVirtualStringTree
        Width = 235
        Height = 553
        ParentFont = False
        ExplicitWidth = 235
        ExplicitHeight = 553
      end
      inherited MemData: TMemTableEh
        AfterScroll = FrameDepsTreeMemDataAfterScroll
        Left = 48
        Top = 104
      end
      inherited CRUDDisp: TFMACRUDDisp
        CrudAgent = dmdepartments.CrudAgentExtended
        OnBeforeGetList = FrameDepsTreeCRUDDispBeforeGetList
        Left = 48
        Top = 224
      end
      inherited IL: TPngImageList
        PngImages = <
          item
            Background = clWindow
            Name = 'folder'
            PngImage.Data = {
              89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
              610000000473424954080808087C086488000000097048597300000076000000
              76014E7B26080000001974455874536F667477617265007777772E696E6B7363
              6170652E6F72679BEE3C1A000000CF4944415478DA6364A010302A282808FCFF
              CF6CC8CCFC9F1924F0F72FE35F7676E6CBB76EDD7A439401E1BE06173243C5F5
              98981818A162FF771FFFF890F3EFA7FE97AF7F093F7DFF5F0A9BC6FFFF19FEBF
              FDCCBA8CB12ED7E27E5D9A8C02BA82FAA90F9FE7587F1497E0FBCF84CD80BFFF
              18187217FCBB83D3809FBFFE31F42D7AFA84E5EFEF5FD80C606763F8F7ECED9F
              4F380D200634CD7AF260D480E16140B8AFFE850C6052666662642445F3DF7FFF
              FF4F5FF5F222A39292123F300319C13213D10600331D50CF39926CC506005D5A
              8096F6275E470000000049454E44AE426082}
          end
          item
            Background = clWindow
            Name = 'wallet'
            PngImage.Data = {
              89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
              610000000473424954080808087C08648800000009704859730000006F000000
              6F01F1A2DC430000001974455874536F667477617265007777772E696E6B7363
              6170652E6F72679BEE3C1A000000F24944415478DA63FCFFFF3F032580119701
              6D27DBC4FFFCFF570A94F6B9FAF6EABEEFBF7FDAB1323038AD0B5CF70AAF0120
              8DBFFFFF2F636460C804723941622FBEBE6478FCF93188D9B739607D318A014A
              D3BE14FD67F8DFC0C9FA8D5747EC0C8386C8250666A6BF2886FEFBFF8FE1D2EB
              CB0C3FFF3232DC7ED5C7F0E79FC0837B99DC8A600314A77DFE546B7384F7F9F7
              8B40893F38FDFA12E88A474057F8287A33741E8B60B897C9C3083380CC50FCFF
              F05E26AF02050630805D415F03FE7CFAC8F0E7C30760A8FE83093D20C9809F8F
              1F31FCFF8B1A43F47501CD02F11390CD4BB601B0A44CBA218CE0E4CC48697606
              00A18094CA1283DBD70000000049454E44AE426082}
          end>
        Left = 48
        Top = 40
        Bitmap = {}
      end
      inherited pmTree: TPopupMenu
        Left = 48
        Top = 160
      end
    end
    object cbAll: TCheckBox
      AlignWithMargins = True
      Left = 3
      Top = 0
      Width = 235
      Height = 19
      Margins.Top = 0
      Align = alTop
      Caption = #1054#1090#1086#1073#1088#1072#1078#1072#1090#1100' '#1074#1089#1077#1093' '#1089#1086#1090#1088#1091#1076#1085#1080#1082#1086#1074
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = cbAllClick
    end
  end
end
