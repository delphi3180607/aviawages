unit GlobalExceptionHandlerUnit;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes, Vcl.Forms, Dialogs;

procedure GlobalExceptionHandler(Sender: TObject; E: Exception);

implementation

procedure GlobalExceptionHandler(Sender: TObject; E: Exception);
begin
  ShowMessage(E.Message);
  TApplication(Sender).Terminate;
end;


end.
