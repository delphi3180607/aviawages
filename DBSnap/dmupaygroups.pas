unit dmupaygroups;

interface

uses
  System.SysUtils, System.Classes, dmuBaseDataSnap, Data.FMTBcd, Data.DB,
  Data.SqlExpr, FMACrudAgent, FMAParamTransform;

type
  Tdmpaygroups = class(TdmBaseDataSnap)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmpaygroups: Tdmpaygroups;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.
