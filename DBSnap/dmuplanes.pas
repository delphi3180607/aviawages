unit dmuplanes;

interface

uses
  System.SysUtils, System.Classes, dmuBaseDataSnap, Data.FMTBcd, Data.DB,
  Data.SqlExpr, FMACrudAgent, FMAParamTransform;

type
  Tdmplanes = class(TdmBaseDataSnap)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmplanes: Tdmplanes;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.
