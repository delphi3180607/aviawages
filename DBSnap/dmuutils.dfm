object dmUtils: TdmUtils
  Height = 281
  Width = 393
  object smImport: TSqlServerMethod
    Params = <
      item
        DataType = ftBoolean
        Precision = 4
        Name = 'ReturnParameter'
        ParamType = ptResult
        Size = 4
      end>
    SQLConnection = DM.DataSnapConnection
    ServerMethodName = 'TsmUtils.ImportZPData'
    Left = 48
    Top = 32
  end
  object smCalculate: TSqlServerMethod
    Params = <
      item
        DataType = ftInteger
        Precision = 4
        Name = 'calc_period_id'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Precision = 4
        Name = 'ReturnParameter'
        ParamType = ptResult
        Size = 4
      end>
    SQLConnection = DM.DataSnapConnection
    ServerMethodName = 'TsmUtils.Calculate'
    Left = 144
    Top = 32
  end
  object smCustomQuery: TSqlServerMethod
    Params = <
      item
        DataType = ftObject
        Precision = 8000
        Name = 'filter'
        ParamType = ptInput
      end
      item
        DataType = ftDataSet
        Name = 'ReturnParameter'
        ParamType = ptResult
      end>
    SQLConnection = DM.DataSnapConnection
    ServerMethodName = 'TsmUtils.OpenCustomQuery'
    Left = 240
    Top = 32
  end
  object CustomQueryAgent: TFMACrudAgent
    ServerMethodGetList = smCustomQuery
    Active = False
    Left = 240
    Top = 112
  end
  object smExport: TSqlServerMethod
    Params = <
      item
        DataType = ftInteger
        Precision = 4
        Name = 'ncalcperiodid'
        ParamType = ptInput
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'sworkerid'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Precision = 4
        Name = 'ReturnParameter'
        ParamType = ptResult
        Size = 4
      end>
    SQLConnection = DM.DataSnapConnection
    ServerMethodName = 'TsmUtils.ExportZPData'
    Left = 48
    Top = 104
  end
end
