unit dmucommonpays;

interface

uses
  System.SysUtils, System.Classes, dmuBaseDataSnap, Data.FMTBcd, Data.DB,
  Data.SqlExpr, FMACrudAgent, FMAParamTransform;

type
  TdmCommonPays = class(TdmBaseDataSnap)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmCommonPays: TdmCommonPays;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.
