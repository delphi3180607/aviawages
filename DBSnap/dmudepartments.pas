unit dmudepartments;

interface

uses
  System.SysUtils, System.Classes, dmuBaseDataSnap, Data.FMTBcd, Data.DB,
  Data.SqlExpr, FMACrudAgent;

type
  Tdmdepartments = class(TdmBaseDataSnap)
    CrudAgentExtended: TFMACrudAgent;
    smGetListExtended: TSqlServerMethod;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmdepartments: Tdmdepartments;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.
