inherited dmFightings: TdmFightings
  Height = 448
  inherited smGetList: TSqlServerMethod
    Params = <
      item
        DataType = ftObject
        Precision = 8000
        Name = 'filter'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Precision = 4
        Name = 'result_state'
        ParamType = ptOutput
        Size = 4
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errcode'
        ParamType = ptOutput
        Size = 2000
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errmessage'
        ParamType = ptOutput
        Size = 2000
      end
      item
        DataType = ftDataSet
        Name = 'ReturnParameter'
        ParamType = ptResult
      end>
    ServerMethodName = 'TsmFightings.GetList'
  end
  inherited smInsert: TSqlServerMethod
    Params = <
      item
        DataType = ftObject
        Precision = 8000
        Name = 'data'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Precision = 4
        Name = 'result_state'
        ParamType = ptOutput
        Size = 4
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errcode'
        ParamType = ptOutput
        Size = 2000
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errmessage'
        ParamType = ptOutput
        Size = 2000
      end
      item
        DataType = ftDataSet
        Name = 'ReturnParameter'
        ParamType = ptResult
      end>
    ServerMethodName = 'TsmFightings.InsertWithReturn'
  end
  inherited smDelete: TSqlServerMethod
    Params = <
      item
        DataType = ftObject
        Precision = 8000
        Name = 'data'
        ParamType = ptInput
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errcode'
        ParamType = ptOutput
        Size = 2000
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errmessage'
        ParamType = ptOutput
        Size = 2000
      end
      item
        DataType = ftBoolean
        Precision = 4
        Name = 'ReturnParameter'
        ParamType = ptResult
        Size = 4
      end>
    ServerMethodName = 'TsmFightings.Delete'
  end
  inherited smUpdate: TSqlServerMethod
    Params = <
      item
        DataType = ftObject
        Precision = 8000
        Name = 'data'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Precision = 4
        Name = 'result_state'
        ParamType = ptOutput
        Size = 4
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errcode'
        ParamType = ptOutput
        Size = 2000
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errmessage'
        ParamType = ptOutput
        Size = 2000
      end
      item
        DataType = ftDataSet
        Name = 'ReturnParameter'
        ParamType = ptResult
      end>
    ServerMethodName = 'TsmFightings.UpdateWithReturn'
  end
  object CrudAgentDetail: TFMACrudAgent
    InsertExecutor = CrudAgentDetailInsertExecutor
    EditExecutor = CrudAgentDetailEditExecutor
    DeleteExecutor = CrudAgentDetailDeleteExecutor
    ServerMethodGetList = smDetailGetList
    Active = False
    Left = 48
    Top = 208
  end
  object smDetailGetList: TSqlServerMethod
    Params = <
      item
        DataType = ftObject
        Precision = 8000
        Name = 'filter'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Precision = 4
        Name = 'result_state'
        ParamType = ptOutput
        Size = 4
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errcode'
        ParamType = ptOutput
        Size = 2000
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errmessage'
        ParamType = ptOutput
        Size = 2000
      end
      item
        DataType = ftDataSet
        Name = 'ReturnParameter'
        ParamType = ptResult
      end>
    SQLConnection = DM.DataSnapConnection
    ServerMethodName = 'TsmFightingsDetail.GetList'
    Left = 48
    Top = 296
  end
  object smDetailInsert: TSqlServerMethod
    Params = <
      item
        DataType = ftObject
        Precision = 8000
        Name = 'data'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Precision = 4
        Name = 'result_state'
        ParamType = ptOutput
        Size = 4
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errcode'
        ParamType = ptOutput
        Size = 2000
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errmessage'
        ParamType = ptOutput
        Size = 2000
      end
      item
        DataType = ftDataSet
        Name = 'ReturnParameter'
        ParamType = ptResult
      end>
    SQLConnection = DM.DataSnapConnection
    ServerMethodName = 'TsmFightingsDetail.InsertWithReturn'
    Left = 120
    Top = 296
  end
  object smDetailDelete: TSqlServerMethod
    Params = <
      item
        DataType = ftObject
        Precision = 8000
        Name = 'data'
        ParamType = ptInput
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errcode'
        ParamType = ptOutput
        Size = 2000
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errmessage'
        ParamType = ptOutput
        Size = 2000
      end
      item
        DataType = ftBoolean
        Precision = 4
        Name = 'ReturnParameter'
        ParamType = ptResult
        Size = 4
      end>
    SQLConnection = DM.DataSnapConnection
    ServerMethodName = 'TsmFightingsDetail.Delete'
    Left = 184
    Top = 296
  end
  object smDetailUpdate: TSqlServerMethod
    Params = <
      item
        DataType = ftObject
        Precision = 8000
        Name = 'data'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Precision = 4
        Name = 'result_state'
        ParamType = ptOutput
        Size = 4
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errcode'
        ParamType = ptOutput
        Size = 2000
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errmessage'
        ParamType = ptOutput
        Size = 2000
      end
      item
        DataType = ftDataSet
        Name = 'ReturnParameter'
        ParamType = ptResult
      end>
    SQLConnection = DM.DataSnapConnection
    ServerMethodName = 'TsmFightingsDetail.UpdateWithReturn'
    Left = 248
    Top = 296
  end
end
