unit dmufirecases;

interface

uses
  System.SysUtils, System.Classes, dmuBaseDataSnap, Data.FMTBcd, Data.DB,
  Data.SqlExpr, FMACrudAgent, FMAParamTransform;

type
  TdmFireCases = class(TdmBaseDataSnap)
    CrudAgentEx: TFMACrudAgent;
    smGetListEx: TSqlServerMethod;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmFireCases: TdmFireCases;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses dmucurrentperiod;

{$R *.dfm}

end.
