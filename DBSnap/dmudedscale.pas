unit dmudedscale;

interface

uses
  System.SysUtils, System.Classes, dmuBaseDataSnap, Data.FMTBcd, Data.DB,
  Data.SqlExpr, FMACrudAgent;

type
  TdmDedScale = class(TdmBaseDataSnap)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmDedScale: TdmDedScale;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.
