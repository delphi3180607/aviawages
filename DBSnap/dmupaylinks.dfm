inherited dmPayLinks: TdmPayLinks
  inherited smGetList: TSqlServerMethod
    Params = <
      item
        DataType = ftObject
        Precision = 8000
        Name = 'filter'
        ParamType = ptInput
      end
      item
        DataType = ftDataSet
        Name = 'ds'
        ParamType = ptInputOutput
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errmessage'
        ParamType = ptInputOutput
        Size = 2000
      end
      item
        DataType = ftBoolean
        Precision = 4
        Name = 'ReturnParameter'
        ParamType = ptResult
        Size = 4
      end>
    ServerMethodName = 'TsmPayLinks.GetList'
  end
  inherited smUpdate: TSqlServerMethod
    Params = <
      item
        DataType = ftObject
        Precision = 8000
        Name = 'data'
        ParamType = ptInput
      end
      item
        DataType = ftDataSet
        Name = 'ds'
        ParamType = ptInputOutput
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errmessage'
        ParamType = ptInputOutput
        Size = 2000
      end
      item
        DataType = ftBoolean
        Precision = 4
        Name = 'ReturnParameter'
        ParamType = ptResult
        Size = 4
      end>
    ServerMethodName = 'TsmPayLinks.UpdateWithReturn'
  end
end
