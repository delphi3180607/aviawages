inherited dmCalcPeriods: TdmCalcPeriods
  inherited smGetList: TSqlServerMethod
    Params = <
      item
        DataType = ftObject
        Precision = 8000
        Name = 'filter'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Precision = 4
        Name = 'result_state'
        ParamType = ptOutput
        Size = 4
        Value = True
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errcode'
        ParamType = ptOutput
        Size = 2000
        Value = ''
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errmessage'
        ParamType = ptOutput
        Size = 2000
        Value = ''
      end
      item
        DataType = ftDataSet
        Name = 'ReturnParameter'
        ParamType = ptResult
        Value = 'TDataSet'
      end>
    ServerMethodName = 'TsmCalcPeriod.GetList'
  end
  inherited smInsert: TSqlServerMethod
    Params = <
      item
        DataType = ftObject
        Precision = 8000
        Name = 'data'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Precision = 4
        Name = 'result_state'
        ParamType = ptOutput
        Size = 4
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errcode'
        ParamType = ptOutput
        Size = 2000
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errmessage'
        ParamType = ptOutput
        Size = 2000
      end
      item
        DataType = ftDataSet
        Name = 'ReturnParameter'
        ParamType = ptResult
      end>
    ServerMethodName = 'TsmCalcPeriod.InsertWithReturn'
  end
  inherited smDelete: TSqlServerMethod
    Params = <
      item
        DataType = ftObject
        Precision = 8000
        Name = 'data'
        ParamType = ptInput
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errcode'
        ParamType = ptOutput
        Size = 2000
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errmessage'
        ParamType = ptOutput
        Size = 2000
      end
      item
        DataType = ftBoolean
        Precision = 4
        Name = 'ReturnParameter'
        ParamType = ptResult
        Size = 4
      end>
    ServerMethodName = 'TsmCalcPeriod.Delete'
  end
  inherited smUpdate: TSqlServerMethod
    Params = <
      item
        DataType = ftObject
        Precision = 8000
        Name = 'data'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Precision = 4
        Name = 'result_state'
        ParamType = ptOutput
        Size = 4
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errcode'
        ParamType = ptOutput
        Size = 2000
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errmessage'
        ParamType = ptOutput
        Size = 2000
      end
      item
        DataType = ftDataSet
        Name = 'ReturnParameter'
        ParamType = ptResult
      end>
    ServerMethodName = 'TsmCalcPeriod.UpdateWithReturn'
  end
end
