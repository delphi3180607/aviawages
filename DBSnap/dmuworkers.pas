unit dmuworkers;

interface

uses
  System.SysUtils, System.Classes, dmuBaseDataSnap, Data.FMTBcd, Data.DB,
  Data.SqlExpr, FMACrudAgent;

type
  TdmWorkers = class(TdmBaseDataSnap)
    CrudAgentExtended: TFMACrudAgent;
    smGetListExtended: TSqlServerMethod;
    procedure CrudAgentBeforeGetList(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmWorkers: TdmWorkers;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses dmucurrentperiod;

{$R *.dfm}

procedure TdmWorkers.CrudAgentBeforeGetList(Sender: TObject);
begin
  inherited;
  with CrudAgent do
  begin
    ParamTransform.AddPatternContext('/*wherefilter*/','(date_begin <= :date_end) and (date_end >= :date_begin or date_end is null) ');
    ParamTransform.AddParameterContext('date_begin', ftDate, dmcurrentperiod.FilterStartDate);
    ParamTransform.AddParameterContext('date_end', ftDate, dmcurrentperiod.FilterEndDate);
  end;
end;

end.
