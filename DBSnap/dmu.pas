unit dmu;

interface

uses
  System.SysUtils, System.Classes, Data.DBXDataSnap, Data.DBXCommon,
  IPPeerClient, Data.FMTBcd, Data.DB, Data.SqlExpr, JSON,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.VCLUI.Wait, FireDAC.Comp.Client, FireDAC.Phys.ODBC,
  FireDAC.Phys.ODBCDef, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet, VCL.Dialogs, FireDAC.Phys.SQLite,
  FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs,
  FireDAC.Phys.SQLiteWrapper.Stat, Vcl.Forms, IniFiles, StrUtils,
  Data.DbxHTTPLayer, Winapi.Windows;

type
  TDM = class(TDataModule)
    DataSnapConnection: TSQLConnection;
    connSettings: TFDConnection;
  private
    hostname: string;
    port: string;
    protocol: string;
    function IndexOfParam(param_name: string): integer;
    procedure ReadConfig;
    procedure SetConnectionParam(param_name, param_value: string);
  public
    function Connect: boolean;
  end;

var
  DM: TDM;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TDM }

function TDM.Connect: boolean;
begin
   result := true;
   ReadConfig;
   try
    DataSnapConnection.Connected := true;
   except
    if not DataSnapConnection.Connected then
    begin
      Application.MessageBox(PWideChar('�� ������� ������������ � ������� ����������.'+#13#10+DataSnapConnection.Params.Text), '������', MB_OK);
      result := false;
    end;
   end;
end;

function TDM.IndexOfParam(param_name: string): integer;
var i: integer;
begin
  result := -1;
  for i := 0 to DataSnapConnection.Params.Count-1 do
  begin
    if pos(param_name,DataSnapConnection.Params[i])=1 then
    begin
      result := i;
      exit;
    end;
  end;
end;

procedure TDM.ReadConfig;
var AppDir: string; infconn: TIniFile;
begin
  try
    AppDir := ExtractFileDir(Application.ExeName);
    connSettings.Params.Database := AppDir+'\settings.db';
    connSettings.Open;
    infconn := TIniFile.Create(AppDir+'/config.ini');
    hostname := infconn.ReadString('main','hostname','');
    port := infconn.ReadString('main','port','');
    protocol := infconn.ReadString('main','protocol','');
    SetConnectionParam('HostName', hostname);
    SetConnectionParam('Port', port);
    SetConnectionParam('CommunicationProtocol', protocol);
  except
    on E:Exception do
    begin
      Application.MessageBox(PWideChar(E.Message), '������', MB_OK);
      Application.Terminate;
    end;
  end;
end;


procedure TDM.SetConnectionParam(param_name, param_value: string);
var index: integer;
begin
  with DataSnapConnection do
  begin
    index := IndexOfParam(param_name);
    if index >-1 then
    begin
      Params[index] := param_name+'='+param_value;
    end else
    begin
      index := Params.Add(param_name);
      Params[index] := param_name+'='+param_value;
    end;
  end;
end;

end.
