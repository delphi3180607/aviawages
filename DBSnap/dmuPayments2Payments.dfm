inherited dmPayments2Payments: TdmPayments2Payments
  inherited smGetList: TSqlServerMethod
    Params = <
      item
        DataType = ftObject
        Precision = 8000
        Name = 'filter'
        ParamType = ptInput
      end
      item
        DataType = ftDataSet
        Name = 'ds'
        ParamType = ptInputOutput
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errmessage'
        ParamType = ptInputOutput
        Size = 2000
      end
      item
        DataType = ftBoolean
        Precision = 4
        Name = 'ReturnParameter'
        ParamType = ptResult
        Size = 4
      end>
    ServerMethodName = 'TsmPayments2Payments.GetList'
  end
  inherited smInsert: TSqlServerMethod
    Params = <
      item
        DataType = ftObject
        Precision = 8000
        Name = 'data'
        ParamType = ptInput
      end
      item
        DataType = ftDataSet
        Name = 'ds'
        ParamType = ptInputOutput
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errmessage'
        ParamType = ptInputOutput
        Size = 2000
      end
      item
        DataType = ftBoolean
        Precision = 4
        Name = 'ReturnParameter'
        ParamType = ptResult
        Size = 4
      end>
    ServerMethodName = 'TsmPayments2Payments.InsertWithReturn'
  end
  inherited smDelete: TSqlServerMethod
    Params = <
      item
        DataType = ftObject
        Precision = 8000
        Name = 'data'
        ParamType = ptInput
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errmessage'
        ParamType = ptInputOutput
        Size = 2000
      end
      item
        DataType = ftBoolean
        Precision = 4
        Name = 'ReturnParameter'
        ParamType = ptResult
        Size = 4
      end>
    ServerMethodName = 'TsmPayments2Payments.Delete'
  end
  inherited smUpdate: TSqlServerMethod
    Params = <
      item
        DataType = ftObject
        Precision = 8000
        Name = 'data'
        ParamType = ptInput
      end
      item
        DataType = ftDataSet
        Name = 'ds'
        ParamType = ptInputOutput
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errmessage'
        ParamType = ptInputOutput
        Size = 2000
      end
      item
        DataType = ftBoolean
        Precision = 4
        Name = 'ReturnParameter'
        ParamType = ptResult
        Size = 4
      end>
    ServerMethodName = 'TsmPayments2Payments.UpdateWithReturn'
  end
end
