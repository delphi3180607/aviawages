inherited dmdepartments: Tdmdepartments
  inherited smGetList: TSqlServerMethod
    Params = <
      item
        DataType = ftObject
        Precision = 8000
        Name = 'filter'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Precision = 4
        Name = 'result_state'
        ParamType = ptOutput
        Size = 4
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errcode'
        ParamType = ptOutput
        Size = 2000
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errmessage'
        ParamType = ptOutput
        Size = 2000
      end
      item
        DataType = ftDataSet
        Name = 'ReturnParameter'
        ParamType = ptResult
      end>
    ServerMethodName = 'TsmDepartments.GetList'
  end
  object CrudAgentExtended: TFMACrudAgent
    InsertExecutor = CrudAgentInsertExecutor
    EditExecutor = CrudAgentEditExecutor
    DeleteExecutor = CrudAgentDeleteExecutor
    ServerMethodGetList = smGetListExtended
    Active = False
    Left = 144
    Top = 16
  end
  object smGetListExtended: TSqlServerMethod
    Params = <
      item
        DataType = ftObject
        Precision = 8000
        Name = 'filter'
        ParamType = ptInput
      end
      item
        DataType = ftBoolean
        Precision = 4
        Name = 'result_state'
        ParamType = ptOutput
        Size = 4
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errcode'
        ParamType = ptOutput
        Size = 2000
      end
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'errmessage'
        ParamType = ptOutput
        Size = 2000
      end
      item
        DataType = ftDataSet
        Name = 'ReturnParameter'
        ParamType = ptResult
      end>
    SQLConnection = DM.DataSnapConnection
    ServerMethodName = 'TsmDepartments.GetListExtended'
    Left = 256
    Top = 16
  end
end
