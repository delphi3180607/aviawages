unit dmuutils;

interface

uses
  System.SysUtils, System.Classes, Data.FMTBcd, Data.DB, Data.SqlExpr, dmu,
  FMACrudAgent;

type
  TdmUtils = class(TDataModule)
    smImport: TSqlServerMethod;
    smCalculate: TSqlServerMethod;
    smCustomQuery: TSqlServerMethod;
    CustomQueryAgent: TFMACrudAgent;
    smExport: TSqlServerMethod;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmUtils: TdmUtils;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.
