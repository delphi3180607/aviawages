unit dmufightings;

interface

uses
  System.SysUtils, System.Classes, dmuBaseDataSnap, Data.FMTBcd, Data.DB,
  Data.SqlExpr, FMACrudAgent, JSON, FMAParamTransform;

type
  TdmFightings = class(TdmBaseDataSnap)
    CrudAgentDetail: TFMACrudAgent;
    smDetailGetList: TSqlServerMethod;
    smDetailInsert: TSqlServerMethod;
    smDetailDelete: TSqlServerMethod;
    smDetailUpdate: TSqlServerMethod;
    procedure CrudAgentDetailGetListExecutor(out DataSet: TDataSet; context: TJSONObject; out ErrorMessage: string);
    procedure CrudAgentDetailInsertExecutor(var DataSet: TDataSet;
      out result_state: Boolean; out ErrorMessage: string);
    procedure CrudAgentDetailEditExecutor(var DataSet: TDataSet;
      out result_state: Boolean; out ErrorMessage: string);
    procedure CrudAgentDetailDeleteExecutor(var DataSet: TDataSet;
      out result_state: Boolean; out ErrorMessage: string);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmFightings: TdmFightings;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TdmFightings.CrudAgentDetailDeleteExecutor(var DataSet: TDataSet; out result_state: Boolean; out ErrorMessage: string);
var o: TJSONObject;
begin
  o := CrudAgentDetail.ParamTransform.DataSetToJSONParams(DataSet);
  smDetailDelete.ParamByName('data').SetObjectValue(o,ftObject, false);
  smDetailDelete.ExecuteMethod;
  result_state := smDetailDelete.ParamByName('ReturnParameter').AsBoolean;
  if not result_state then
  begin
    DataSet.Cancel;
    ErrorMessage := smDelete.ParamByName('errmessage').AsString;
  end;
end;

procedure TdmFightings.CrudAgentDetailEditExecutor(var DataSet: TDataSet; out result_state: Boolean; out ErrorMessage: string);
var o: TJSONObject;
begin
  smDetailUpdate.Close;
  o := CrudAgentDetail.ParamTransform.DataSetToJSONParams(DataSet);
  smDetailUpdate.ParamByName('data').SetObjectValue(o,ftObject, false);
  smDetailUpdate.Open;
  result_state := smDetailUpdate.ParamByName('result_state').AsBoolean;
  if not result_state then
  begin
    DataSet.Cancel;
    ErrorMessage := smDetailUpdate.ParamByName('errmessage').AsString;
  end;
end;

procedure TdmFightings.CrudAgentDetailGetListExecutor(out DataSet: TDataSet; context: TJSONObject; out ErrorMessage: string);
begin
  smDetailGetList.ParamByName('context').SetObjectValue(context,ftObject, false);
  smDetailGetList.Open;
  DataSet := smGetList;
end;

procedure TdmFightings.CrudAgentDetailInsertExecutor(var DataSet: TDataSet; out result_state: Boolean; out ErrorMessage: string);
var o: TJSONObject;
begin
  smDetailInsert.Close;
  o := CrudAgentDetail.ParamTransform.DataSetToJSONParams(DataSet);
  smDetailInsert.ParamByName('data').SetObjectValue(o,ftObject, false);
  smDetailInsert.Open;
  result_state := smDetailInsert.ParamByName('result_state').AsBoolean;
  if not result_state then
  begin
    DataSet.Cancel;
    ErrorMessage := smDetailInsert.ParamByName('errmessage').AsString;
  end;
end;

end.

