unit dmupaylinks;

interface

uses
  System.SysUtils, System.Classes, dmuBaseDataSnap, Data.FMTBcd, Data.DB,
  Data.SqlExpr, FMACrudAgent;

type
  TdmPayLinks = class(TdmBaseDataSnap)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmPayLinks: TdmPayLinks;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.
