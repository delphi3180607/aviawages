unit EditCommonPay;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FMAEdit, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls, FMALookUp, Vcl.Mask, DBCtrlsEh, dmuworkers, dmupayments;

type
  TFormEditCommonPay = class(TFMAFormEdit)
    edPERCENT: TDBNumberEditEh;
    edSUMMA: TDBNumberEditEh;
    edWORKER_CODE: TFMALookUp;
    edPAYMENT_CODE: TFMALookUp;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditCommonPay: TFormEditCommonPay;

implementation

{$R *.dfm}

end.
