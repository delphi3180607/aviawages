inherited FormEditJump: TFormEditJump
  Caption = #1055#1088#1099#1078#1086#1082'/'#1089#1087#1091#1089#1082
  ClientHeight = 358
  ClientWidth = 451
  ExplicitWidth = 467
  ExplicitHeight = 397
  TextHeight = 17
  inherited plBottom: TPanel
    Top = 315
    Width = 451
    TabOrder = 7
    ExplicitTop = 315
    ExplicitWidth = 451
    inherited btOk: TButton
      Left = 248
      ExplicitLeft = 248
    end
    inherited btCancel: TButton
      Left = 351
      ExplicitLeft = 351
    end
  end
  object edAMOUNT: TDBNumberEditEh [1]
    Left = 14
    Top = 183
    Width = 121
    Height = 25
    ControlLabel.Width = 70
    ControlLabel.Height = 17
    ControlLabel.Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
    ControlLabel.Visible = True
    DataField = 'amount'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 3
    Visible = True
  end
  object edDATE_BEGIN: TDBDateTimeEditEh [2]
    Left = 14
    Top = 236
    Width = 121
    Height = 25
    ControlLabel.Width = 74
    ControlLabel.Height = 17
    ControlLabel.Caption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
    ControlLabel.Visible = True
    DataField = 'date_begin'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Kind = dtkDateEh
    TabOrder = 4
    Visible = True
  end
  object edDATE_END: TDBDateTimeEditEh [3]
    Left = 158
    Top = 236
    Width = 121
    Height = 25
    ControlLabel.Width = 96
    ControlLabel.Height = 17
    ControlLabel.Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
    ControlLabel.Visible = True
    DataField = 'date_end'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Kind = dtkDateEh
    TabOrder = 5
    Visible = True
  end
  object edFIRE_CASE_NAME: TFMALookUp [4]
    Left = 14
    Top = 28
    Width = 419
    Height = 25
    ControlLabel.Width = 139
    ControlLabel.Height = 17
    ControlLabel.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1086#1078#1072#1088#1072
    ControlLabel.Visible = True
    DataField = 'fire_case_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 0
    Visible = True
    CrudAgent = dmFireCases.CrudAgent
    KeyFieldName = 'id'
    DisplayFieldName = 'full_name'
    DisplayRowsCount = 0
    SearchFromStart = False
  end
  object edWORKER_CODE: TFMALookUp [5]
    Left = 14
    Top = 79
    Width = 419
    Height = 25
    ControlLabel.Width = 62
    ControlLabel.Height = 17
    ControlLabel.Caption = #1057#1086#1090#1088#1091#1076#1085#1080#1082
    ControlLabel.Visible = True
    DataField = 'worker_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 1
    Visible = True
    CrudAgent = dmWorkers.CrudAgent
    KeyFieldName = 'id'
    DisplayFieldName = 'full_code'
    DisplayRowsCount = 0
    SearchFromStart = True
  end
  object edJUMP_KIND_NAME: TFMALookUp [6]
    Left = 14
    Top = 131
    Width = 235
    Height = 25
    ControlLabel.Width = 114
    ControlLabel.Height = 17
    ControlLabel.Caption = #1042#1080#1076' '#1087#1088#1099#1078#1082#1072'/'#1089#1087#1091#1089#1082#1072
    ControlLabel.Visible = True
    DataField = 'jump_kind_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 2
    Visible = True
    CrudAgent = dmjumpkinds.CrudAgent
    KeyFieldName = 'id'
    DisplayFieldName = 'name'
    DisplayRowsCount = 0
    SearchFromStart = False
  end
  object cbStorno: TDBCheckBoxEh [7]
    Left = 14
    Top = 279
    Width = 103
    Height = 17
    Caption = #1057#1090#1086#1088#1085#1086
    DataField = 'isstorno'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 6
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
end
