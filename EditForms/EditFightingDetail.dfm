inherited FormEditFightingDetail: TFormEditFightingDetail
  Caption = #1059#1095#1072#1089#1090#1080#1077' '#1074' '#1090#1091#1096#1077#1085#1080#1080' '#1087#1086#1078#1072#1088#1072' - '#1076#1085#1080'/'#1095#1072#1089#1099
  ClientHeight = 272
  ClientWidth = 233
  ExplicitWidth = 247
  ExplicitHeight = 311
  TextHeight = 17
  inherited plBottom: TPanel
    Top = 229
    Width = 233
    TabOrder = 4
    ExplicitTop = 229
    ExplicitWidth = 231
    inherited btOk: TButton
      Left = 30
      ExplicitLeft = 28
    end
    inherited btCancel: TButton
      Left = 133
      ExplicitLeft = 131
    end
  end
  object edDAY: TDBDateTimeEditEh [1]
    Left = 10
    Top = 25
    Width = 121
    Height = 25
    ControlLabel.Width = 28
    ControlLabel.Height = 17
    ControlLabel.Caption = #1044#1072#1090#1072
    ControlLabel.Visible = True
    DataField = 'day'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Kind = dtkDateEh
    TabOrder = 0
    Visible = True
  end
  object edWORK_HOURS: TDBDateTimeEditEh [2]
    Left = 10
    Top = 76
    Width = 115
    Height = 25
    ControlLabel.Width = 89
    ControlLabel.Height = 17
    ControlLabel.Caption = #1056#1072#1073#1086#1095#1080#1093' '#1095#1072#1089#1086#1074
    ControlLabel.Visible = True
    DataField = 'work_hours'
    DataSource = dsLocal
    DynProps = <>
    EditButton.Visible = False
    EditButtons = <>
    TabOrder = 1
    Visible = True
    EditFormat = 'hh:nn'
  end
  object edOVERTIME_HOURS: TDBDateTimeEditEh [3]
    Left = 10
    Top = 127
    Width = 115
    Height = 25
    ControlLabel.Width = 126
    ControlLabel.Height = 17
    ControlLabel.Caption = #1057#1074#1077#1088#1093#1091#1088#1086#1095#1085#1099#1093' '#1095#1072#1089#1086#1074
    ControlLabel.Visible = True
    DataField = 'overtime_hours'
    DataSource = dsLocal
    DynProps = <>
    EditButton.Visible = False
    EditButtons = <>
    TabOrder = 2
    Visible = True
    EditFormat = 'hh:nn'
  end
  object edHOLIDAY_HOURS: TDBDateTimeEditEh [4]
    Left = 10
    Top = 179
    Width = 115
    Height = 25
    ControlLabel.Width = 98
    ControlLabel.Height = 17
    ControlLabel.Caption = #1042#1099#1093#1086#1076#1085#1099#1093' '#1095#1072#1089#1086#1074
    ControlLabel.Visible = True
    DataField = 'holiday_hours'
    DataSource = dsLocal
    DynProps = <>
    EditButton.Visible = False
    EditButtons = <>
    TabOrder = 3
    Visible = True
    EditFormat = 'hh:nn'
  end
end
