unit EditFireCase;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FMAEdit, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls, FireCases, BaseGrid, DBCtrlsEh, Vcl.Mask,
  FMALookUp, dmuforestries, dmudepartments;

type
  TFormEditFireCase = class(TFMAFormEdit)
    edNAME: TDBEditEh;
    edFORESTRY_NAME: TFMALookUp;
    edSQUARE: TDBNumberEditEh;
    edDEPARTMENT_NAME: TFMALookUp;
    edDATEFROM: TDBDateTimeEditEh;
    edDATETO: TDBDateTimeEditEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditFireCase: TFormEditFireCase;

implementation

{$R *.dfm}

end.
