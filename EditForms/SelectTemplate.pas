unit SelectTemplate;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FMAEdit, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Data.DB, Vcl.StdCtrls, Vcl.ExtCtrls, MemTableDataEh, MemTableEh, IOUtils;

type
  TFormSelectTemplate = class(TFMAFormEdit)
    Grid: TDBGridEh;
    meFiles: TMemTableEh;
  private
    { Private declarations }
  public
    function Init: boolean;
  end;

var
  FormSelectTemplate: TFormSelectTemplate;

implementation

{$R *.dfm}

function TFormSelectTemplate.Init: boolean;
var filename, filenameshort, path: string;
begin
   result := true;
   path := ExtractFilePath(Application.ExeName)+'templates';
   meFiles.Active := true;
   meFiles.EmptyTable;
   try
     for filename in TDirectory.GetFiles(path)  do
     begin
       filenameshort := ExtractFileName(filename);
       meFiles.Append;
       meFiles.FieldByName('template_name').Value := filenameshort;
       meFiles.Post;
     end;
   except
    on E: Exception do
    begin
      ShowMessage(E.Message);
      result := false;
    end;
   end;
   meFiles.First;
end;


end.
