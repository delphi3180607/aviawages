inherited FormSelectTemplate: TFormSelectTemplate
  Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1096#1072#1073#1083#1086#1085
  TextHeight = 17
  inherited plBottom: TPanel
    TabOrder = 1
  end
  object Grid: TDBGridEh [1]
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 596
    Height = 385
    Align = alClient
    DataSource = dsLocal
    DynProps = <>
    GridLineParams.VertEmptySpaceStyle = dessNonEh
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove]
    TabOrder = 0
    Columns = <
      item
        AutoFitColWidth = False
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'template_name'
        Footers = <>
        Title.Caption = #1048#1084#1103' '#1096#1072#1073#1083#1086#1085#1072
        Width = 550
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited dsLocal: TDataSource
    DataSet = meFiles
  end
  object meFiles: TMemTableEh
    Params = <>
    Options = [mtoPersistentStructEh]
    Left = 296
    Top = 88
    object MemTableData: TMemTableDataEh
      object DataStruct: TMTDataStructEh
        object template_name: TMTStringDataFieldEh
          FieldName = 'template_name'
          StringDataType = fdtStringEh
          DisplayWidth = 200
          Size = 400
        end
      end
      object RecordsList: TRecordsListEh
      end
    end
  end
end
