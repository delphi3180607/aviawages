inherited FormEditFlight: TFormEditFlight
  Caption = #1053#1072#1083#1077#1090
  ClientHeight = 373
  ClientWidth = 424
  ExplicitWidth = 440
  ExplicitHeight = 412
  TextHeight = 17
  inherited plBottom: TPanel
    Top = 330
    Width = 424
    TabOrder = 8
    ExplicitTop = 330
    ExplicitWidth = 424
    inherited btOk: TButton
      Left = 221
      ExplicitLeft = 221
    end
    inherited btCancel: TButton
      Left = 324
      ExplicitLeft = 324
    end
  end
  object edDATE_FROM: TDBDateTimeEditEh [1]
    Left = 10
    Top = 228
    Width = 121
    Height = 25
    ControlLabel.Width = 74
    ControlLabel.Height = 17
    ControlLabel.Caption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
    ControlLabel.Visible = True
    DataField = 'date_from'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Kind = dtkDateEh
    TabOrder = 4
    Visible = True
  end
  object edDATE_TO: TDBDateTimeEditEh [2]
    Left = 154
    Top = 228
    Width = 121
    Height = 25
    ControlLabel.Width = 96
    ControlLabel.Height = 17
    ControlLabel.Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
    ControlLabel.Visible = True
    DataField = 'date_to'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Kind = dtkDateEh
    TabOrder = 5
    Visible = True
  end
  object edWORKER_CODE: TFMALookUp [3]
    Left = 10
    Top = 78
    Width = 396
    Height = 25
    ControlLabel.Width = 62
    ControlLabel.Height = 17
    ControlLabel.Caption = #1057#1086#1090#1088#1091#1076#1085#1080#1082
    ControlLabel.Visible = True
    DataField = 'worker_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 1
    Visible = True
    CrudAgent = dmWorkers.CrudAgent
    KeyFieldName = 'id'
    DisplayFieldName = 'full_code'
    DisplayRowsCount = 0
    SearchFromStart = True
  end
  object edFIRE_CASE_NAME: TFMALookUp [4]
    Left = 10
    Top = 28
    Width = 396
    Height = 25
    ControlLabel.Width = 139
    ControlLabel.Height = 17
    ControlLabel.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1086#1078#1072#1088#1072
    ControlLabel.Visible = True
    DataField = 'fire_case_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 0
    Visible = True
    CrudAgent = dmFireCases.CrudAgent
    KeyFieldName = 'id'
    DisplayFieldName = 'full_name'
    DisplayRowsCount = 0
    SearchFromStart = False
  end
  object edPLANE_CODE: TFMALookUp [5]
    Left = 10
    Top = 128
    Width = 396
    Height = 25
    ControlLabel.Width = 105
    ControlLabel.Height = 17
    ControlLabel.Caption = #1042#1086#1079#1076#1091#1096#1085#1086#1077' '#1089#1091#1076#1085#1086
    ControlLabel.Visible = True
    DataField = 'plane_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 2
    Visible = True
    CrudAgent = dmplanes.CrudAgent
    KeyFieldName = 'id'
    DisplayFieldName = 'name'
    DisplayRowsCount = 0
    SearchFromStart = False
  end
  object edPAYGROUP_CODE: TFMALookUp [6]
    Left = 10
    Top = 178
    Width = 300
    Height = 25
    ControlLabel.Width = 88
    ControlLabel.Height = 17
    ControlLabel.Caption = #1043#1088#1091#1087#1087#1072' '#1086#1087#1083#1072#1090#1099
    ControlLabel.Visible = True
    DataField = 'pay_group_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 3
    Visible = True
    CrudAgent = dmpaygroups.CrudAgent
    KeyFieldName = 'id'
    DisplayFieldName = 'code'
    DisplayRowsCount = 0
    SearchFromStart = False
  end
  object edHOURS: TDBDateTimeEditEh [7]
    Left = 10
    Top = 283
    Width = 119
    Height = 25
    ControlLabel.Width = 37
    ControlLabel.Height = 17
    ControlLabel.Caption = #1063#1072#1089#1086#1074
    ControlLabel.Visible = True
    DataField = 'hours'
    DataSource = dsLocal
    DynProps = <>
    EditButton.Visible = False
    EditButtons = <>
    TabOrder = 6
    Visible = True
    EditFormat = 'hh:nn'
  end
  object cbStorno: TDBCheckBoxEh [8]
    Left = 154
    Top = 287
    Width = 103
    Height = 17
    Caption = #1057#1090#1086#1088#1085#1086
    DataField = 'isstorno'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 7
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  inherited dsLocal: TDataSource
    Left = 224
    Top = 528
  end
end
