unit EditReport;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FMAEdit, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls, Vcl.Mask, DBCtrlsEh;

type
  TFormEditReport = class(TFMAFormEdit)
    edNAME: TDBEditEh;
    edTEMPLATE: TDBEditEh;
    edPREPARE_COMMAND_SQL: TDBEditEh;
    edQUERY_SQL: TDBEditEh;
    edGroupFieldLevel1: TDBEditEh;
    edGroupFieldLevel2: TDBEditEh;
    procedure edPREPARE_COMMAND_SQLEditButtons0Click(Sender: TObject;
      var Handled: Boolean);
    procedure edQUERY_SQLEditButtons0Click(Sender: TObject;
      var Handled: Boolean);
    procedure edTEMPLATEEditButtons0Click(Sender: TObject;
      var Handled: Boolean);
  private
    { Private declarations }
  public
    procedure EditText(dbeditcontrol: TDBEditEh);
  end;

var
  FormEditReport: TFormEditReport;

implementation

{$R *.dfm}

uses EditSQL, SelectTemplate;

procedure TFormEditReport.EditText(dbeditcontrol: TDBEditEh);
begin
  FormEditSQL.meSQL.Lines.Clear;
  FormEditSQL.meSQL.Lines.Text := dbeditcontrol.Text;
  FormEditSQL.ShowModal;
  if FormEditSQL.ModalResult = mrOk then
  begin
    dbeditcontrol.Text := FormEditSQL.meSQL.Lines.Text;
  end;
end;

procedure TFormEditReport.edPREPARE_COMMAND_SQLEditButtons0Click(Sender: TObject; var Handled: Boolean);
begin
  EditText(edPREPARE_COMMAND_SQL);
end;

procedure TFormEditReport.edQUERY_SQLEditButtons0Click(Sender: TObject; var Handled: Boolean);
begin
  EditText(edQUERY_SQL);
end;

procedure TFormEditReport.edTEMPLATEEditButtons0Click(Sender: TObject; var Handled: Boolean);
begin
  if not FormSelectTemplate.Init then exit;
  FormSelectTemplate.ShowModal;
  if FormSelectTemplate.ModalResult = mrOk then
  begin
    edTemplate.Text :=  FormSelectTemplate.meFiles.FieldByName('template_name').AsString;
  end;
end;

end.
