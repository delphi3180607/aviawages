inherited FormEditCalcPeriod: TFormEditCalcPeriod
  Caption = #1056#1072#1089#1095#1077#1090#1085#1099#1081' '#1087#1077#1088#1080#1086#1076
  ClientHeight = 192
  ClientWidth = 326
  ExplicitWidth = 340
  ExplicitHeight = 231
  TextHeight = 17
  inherited plBottom: TPanel
    Top = 149
    Width = 326
    ExplicitTop = 149
    ExplicitWidth = 324
    inherited btOk: TButton
      Left = 123
      ExplicitLeft = 121
    end
    inherited btCancel: TButton
      Left = 226
      ExplicitLeft = 224
    end
  end
  object edDATE_CLOSED: TDBDateTimeEditEh [1]
    Left = 10
    Top = 75
    Width = 129
    Height = 25
    ControlLabel.Width = 87
    ControlLabel.Height = 17
    ControlLabel.Caption = #1044#1072#1090#1072' '#1079#1072#1082#1088#1099#1090#1080#1103
    ControlLabel.Visible = True
    DataField = 'date_closed'
    DataSource = dsLocal
    DynProps = <>
    Enabled = False
    EditButtons = <>
    Kind = dtkDateEh
    TabOrder = 1
    Visible = True
  end
  object edPERIOD_BEGIN: TDBDateTimeEditEh [2]
    Left = 10
    Top = 25
    Width = 127
    Height = 25
    ControlLabel.Width = 110
    ControlLabel.Height = 17
    ControlLabel.Caption = #1056#1072#1089#1095#1077#1090#1085#1099#1081' '#1087#1077#1088#1080#1086#1076
    ControlLabel.Visible = True
    DataField = 'period_begin'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 2
    Visible = True
    EditFormat = 'MM/YYYY'
  end
  object btClose: TButton [3]
    Left = 145
    Top = 75
    Width = 90
    Height = 26
    Caption = #1047#1072#1082#1088#1099#1090#1100
    TabOrder = 3
    OnClick = btCloseClick
  end
  inherited dsLocal: TDataSource
    Left = 32
    Top = 144
  end
end
