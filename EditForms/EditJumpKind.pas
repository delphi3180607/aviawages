unit EditJumpKind;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FMAEdit, Data.DB, Vcl.StdCtrls, Math,
  Vcl.ExtCtrls, Vcl.Mask, DBCtrlsEh, FMALookUp, dmupayments, FMAFunctions;

type
  TFormEditJumpKind = class(TFMAFormEdit)
    edPAYMENT_CODE: TFMALookUp;
    edCODE: TDBEditEh;
    edNAME: TDBEditEh;
    edTARIF: TDBNumberEditEh;
    edPERCENT: TDBNumberEditEh;
    edSALARY: TDBNumberEditEh;
    procedure edSALARYChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditJumpKind: TFormEditJumpKind;

implementation

{$R *.dfm}

procedure TFormEditJumpKind.edSALARYChange(Sender: TObject);
begin
  if Assigned(dsLocal.DataSet) then
  edTARIF.Value :=  Math.RoundTo(VarToRealDef(edSALARY.Value)*VarToRealDef(edPERCENT.Value)/100,-2);
end;

end.
