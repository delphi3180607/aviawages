inherited FormEditPayment2Payment: TFormEditPayment2Payment
  Caption = #1057#1074#1103#1079#1072#1085#1085#1072#1103' '#1074#1099#1087#1083#1072#1090#1072
  ClientHeight = 134
  ClientWidth = 361
  ExplicitWidth = 377
  ExplicitHeight = 173
  TextHeight = 17
  inherited plBottom: TPanel
    Top = 91
    Width = 361
    ExplicitTop = 91
    ExplicitWidth = 361
    inherited btOk: TButton
      Left = 158
      ExplicitLeft = 158
    end
    inherited btCancel: TButton
      Left = 261
      ExplicitLeft = 261
    end
  end
  object edPAYMENT_CODE: TFMALookUp [1]
    Left = 10
    Top = 28
    Width = 319
    Height = 25
    ControlLabel.Width = 49
    ControlLabel.Height = 17
    ControlLabel.Caption = #1042#1099#1087#1083#1072#1090#1072
    ControlLabel.Visible = True
    DataField = 'payment_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 1
    Visible = True
    CrudAgent = dmPayments.CrudAgent
    KeyFieldName = 'id'
    DisplayFieldName = 'code'
    DisplayRowsCount = 0
    SearchFromStart = False
  end
  inherited dsLocal: TDataSource
    Left = 232
    Top = 56
  end
end
