inherited FormSelectStorno: TFormSelectStorno
  Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1074#1072#1088#1080#1072#1085#1090' '#1082#1086#1088#1088#1077#1082#1090#1080#1088#1086#1074#1082#1080
  ClientHeight = 165
  ClientWidth = 504
  ExplicitWidth = 520
  ExplicitHeight = 204
  TextHeight = 17
  inherited plBottom: TPanel
    Top = 122
    Width = 504
    TabOrder = 3
    ExplicitTop = 122
    ExplicitWidth = 504
    inherited btOk: TButton
      Left = 301
      Caption = #1042#1099#1073#1088#1072#1090#1100
      ExplicitLeft = 301
    end
    inherited btCancel: TButton
      Left = 404
      ExplicitLeft = 404
    end
  end
  object rb0: TRadioButton [1]
    Left = 21
    Top = 15
    Width = 429
    Height = 17
    Caption = #1050#1086#1088#1088#1077#1082#1090#1080#1088#1086#1074#1072#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090' '#1080#1079' '#1087#1088#1086#1096#1083#1086#1075#1086' '#1087#1077#1088#1080#1086#1076#1072' (+ '#1080' - )'
    Checked = True
    TabOrder = 0
    TabStop = True
  end
  object rb1: TRadioButton [2]
    Left = 21
    Top = 47
    Width = 429
    Height = 17
    Caption = #1057#1090#1086#1088#1085#1080#1088#1086#1074#1072#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090' '#1080#1079' '#1087#1088#1086#1096#1083#1086#1075#1086' '#1087#1077#1088#1080#1086#1076#1072' ('#1090#1086#1083#1100#1082#1086' - )'
    TabOrder = 1
  end
  object rb2: TRadioButton [3]
    Left = 21
    Top = 79
    Width = 453
    Height = 17
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090', '#1086#1090#1085#1086#1089#1103#1097#1080#1081#1089#1103' '#1082' '#1087#1088#1086#1096#1083#1086#1084#1091' '#1087#1077#1088#1080#1086#1076#1091' ('#1090#1086#1083#1100#1082#1086' + )'
    TabOrder = 2
  end
  inherited dsLocal: TDataSource
    Left = 193
    Top = 76
  end
end
