unit EditPlane;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FMAEdit, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls, Vcl.Mask, DBCtrlsEh;

type
  TFormEditPlane = class(TFMAFormEdit)
    edCODE: TDBEditEh;
    edNAME: TDBEditEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditPlane: TFormEditPlane;

implementation

{$R *.dfm}

end.
