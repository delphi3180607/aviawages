unit EditSQL;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FMAEdit, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls, SynEditHighlighter, SynHighlighterSQL, SynEdit;

type
  TFormEditSQL = class(TFMAFormEdit)
    meSQL: TSynEdit;
    SynSQLSyn1: TSynSQLSyn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditSQL: TFormEditSQL;

implementation

{$R *.dfm}

end.
