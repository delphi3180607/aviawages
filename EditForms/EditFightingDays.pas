unit EditFightingDays;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FMAEdit, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  FireDAC.Stan.StorageBin;

type
  TFormEditFightingDays = class(TFMAFormEdit)
    MemData: TFDMemTable;
    Grid: TDBGridEh;
    DataSource: TDataSource;
    procedure GridColumns1UpdateData(Sender: TObject; var Text: string;
      var Value: Variant; var UseText, Handled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditFightingDays: TFormEditFightingDays;

implementation

{$R *.dfm}

procedure TFormEditFightingDays.GridColumns1UpdateData(Sender: TObject;
  var Text: string; var Value: Variant; var UseText, Handled: Boolean);
begin
  if Text<>'' then
  begin
    Text := StringReplace(Text,' ','0',[rfReplaceAll]);
    try
      Value := StrToTime(Text);
    except
      begin
        MessageDlg('�������� �������� ������� '+Text,mtInformation,[mbOK],0);
        Text := '';
        exit;
      end;
    end;
  end;
end;

end.
