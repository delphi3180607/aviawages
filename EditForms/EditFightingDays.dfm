inherited FormEditFightingDays: TFormEditFightingDays
  Caption = #1044#1085#1080' '#1080' '#1095#1072#1089#1099' '#1079#1072' '#1087#1077#1088#1080#1086#1076
  ClientWidth = 610
  ExplicitWidth = 622
  TextHeight = 17
  inherited plBottom: TPanel
    Width = 610
    ExplicitTop = 390
    ExplicitWidth = 606
  end
  object Grid: TDBGridEh [1]
    Left = 0
    Top = 0
    Width = 610
    Height = 391
    Align = alClient
    AllowedOperations = [alopUpdateEh]
    DataSource = DataSource
    DynProps = <>
    GridLineParams.VertEmptySpaceStyle = dessNonEh
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove]
    TabOrder = 1
    TitleParams.FillStyle = cfstThemedEh
    TitleParams.Font.Charset = DEFAULT_CHARSET
    TitleParams.Font.Color = clWindowText
    TitleParams.Font.Height = -16
    TitleParams.Font.Name = 'Segoe UI'
    TitleParams.Font.Style = []
    TitleParams.MultiTitle = True
    TitleParams.ParentFont = False
    Columns = <
      item
        AutoFitColWidth = False
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'Day'
        Footers = <>
        Title.Caption = #1044#1072#1090#1072
        Width = 102
      end
      item
        AutoFitColWidth = False
        CellButtons = <>
        DisplayFormat = 'hh:nn'
        DynProps = <>
        EditButtons = <>
        EditMask = '00:00;1;0'
        FieldName = 'work_hours'
        Footers = <>
        Title.Caption = #1056#1072#1073#1086#1095#1080#1077' '#1095#1072#1089#1099
        Width = 109
        OnUpdateData = GridColumns1UpdateData
      end
      item
        AutoFitColWidth = False
        CellButtons = <>
        DisplayFormat = 'hh:nn'
        DynProps = <>
        EditButtons = <>
        EditMask = '00:00;1;0'
        FieldName = 'overtime_hours'
        Footers = <>
        Title.Caption = #1057#1074#1077#1088#1093#1091#1088#1086#1095#1085#1099#1077' '#1095#1072#1089#1099
        Width = 157
        OnUpdateData = GridColumns1UpdateData
      end
      item
        AutoFitColWidth = False
        CellButtons = <>
        DisplayFormat = 'hh:nn'
        DynProps = <>
        EditButtons = <>
        EditMask = '00:00;1;0'
        FieldName = 'holiday_hours'
        Footers = <>
        Title.Caption = #1042#1099#1093#1086#1076#1085#1099#1077' '#1095#1072#1089#1099
        Width = 138
        OnUpdateData = GridColumns1UpdateData
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object MemData: TFDMemTable
    FieldDefs = <
      item
        Name = 'day'
        DataType = ftDateTime
      end
      item
        Name = 'work_hours'
        DataType = ftTime
      end
      item
        Name = 'overtime_hours'
        DataType = ftTime
      end
      item
        Name = 'holiday_hours'
        DataType = ftTime
      end
      item
        Name = 'fighting_id'
        DataType = ftLargeint
      end>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvPersistent, rvSilentMode]
    ResourceOptions.Persistent = True
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 280
    Top = 88
  end
  object DataSource: TDataSource
    DataSet = MemData
    Left = 392
    Top = 88
  end
end
