unit EditReportParams;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FMAEdit, Data.DB, Vcl.StdCtrls, Vcl.ExtCtrls, MemTableDataEh,
  MemTableEh, System.Generics.Collections, FMALookUp, FMACRUDAgentInterface, FMAFilter;

type

  TMasterFilterSet = record
    MasterParamId: integer;
    FilterExpression: string;
  end;

  TFormEditReportParams = class(TFMAFormEdit)
    ParamBuffer: TMemTableEh;
    procedure btOkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
  public
    ControlList: TDictionary<integer, Pointer>;
    MasterFilterList: TDictionary<TFMALookUp, TMasterFilterSet>;
    function MasterFilterSet(v1: integer; v2: string): TMasterFilterSet;
    procedure OnBeforeGetList(Sender: TObject);
  end;

var
  FormEditReportParams: TFormEditReportParams;

implementation

{$R *.dfm}

procedure TFormEditReportParams.btOkClick(Sender: TObject);
begin
  ParamBuffer.Post;
end;

function TFormEditReportParams.MasterFilterSet(v1: integer; v2: string): TMasterFilterSet;
begin
  result.MasterParamId := v1;
  result.FilterExpression := v2;
end;

procedure TFormEditReportParams.FormCreate(Sender: TObject);
begin
  ControlList := TDictionary<integer, Pointer>.Create;
  MasterFilterList := TDictionary<TFMALookUp, TMasterFilterSet>.Create;
end;

procedure TFormEditReportParams.OnBeforeGetList(Sender: TObject);
var mfs: TMasterFilterSet; lu: TFMALookUp; c: Pointer; ca: IFMACrudAgent; ds: TDataSet; field: TField;
n: string;
begin
  lu := TFMALookUp(Sender);
  if MasterFilterList.TryGetValue(lu, mfs) then
  begin
    if ControlList.TryGetValue(mfs.MasterParamId, c) then
    begin
      ca := TFMALookUp(c).CrudAgent;
      ds := TDataSet(ca.GetServerMethodGetList);
      lu.CrudAgent.OneOffFilter := TFMAFilter.Create;
      with lu.CrudAgent.OneOffFilter do
      begin
        FilterExpression := mfs.FilterExpression;
        for field in ds.Fields do
        begin
          // ID ������� �� ������ ������ �� ���������
          if field.FieldName = 'id' then continue;
          FilterParameters.Add(field.FieldName, FilterValue(field.DataType,field.Value));
        end;
      end;
    end;
  end;
end;


end.

