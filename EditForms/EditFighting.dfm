inherited FormEditFighting: TFormEditFighting
  Caption = #1059#1095#1072#1089#1090#1080#1077' '#1074' '#1087#1086#1078#1072#1088#1077
  ClientHeight = 269
  ClientWidth = 429
  ExplicitWidth = 445
  ExplicitHeight = 308
  TextHeight = 17
  inherited plBottom: TPanel
    Top = 226
    Width = 429
    TabOrder = 5
    ExplicitTop = 226
    ExplicitWidth = 429
    inherited btOk: TButton
      Left = 226
      ExplicitLeft = 226
    end
    inherited btCancel: TButton
      Left = 329
      ExplicitLeft = 329
    end
  end
  object edDATE_FROM: TDBDateTimeEditEh [1]
    Left = 9
    Top = 137
    Width = 121
    Height = 25
    ControlLabel.Width = 74
    ControlLabel.Height = 17
    ControlLabel.Caption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
    ControlLabel.Visible = True
    DataField = 'date_from'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Kind = dtkDateEh
    TabOrder = 2
    Visible = True
  end
  object edDATE_TO: TDBDateTimeEditEh [2]
    Left = 145
    Top = 137
    Width = 121
    Height = 25
    ControlLabel.Width = 96
    ControlLabel.Height = 17
    ControlLabel.Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
    ControlLabel.Visible = True
    DataField = 'date_to'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Kind = dtkDateEh
    TabOrder = 3
    Visible = True
  end
  object edWORKER_CODE: TFMALookUp [3]
    Left = 9
    Top = 76
    Width = 392
    Height = 25
    ControlLabel.Width = 62
    ControlLabel.Height = 17
    ControlLabel.Caption = #1057#1086#1090#1088#1091#1076#1085#1080#1082
    ControlLabel.Visible = True
    DataField = 'worker_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 1
    Visible = True
    CrudAgent = dmWorkers.CrudAgent
    KeyFieldName = 'id'
    DisplayFieldName = 'full_code'
    DisplayRowsCount = 0
    SearchFromStart = True
  end
  object edFIRE_NAME: TFMALookUp [4]
    Left = 9
    Top = 26
    Width = 392
    Height = 25
    ControlLabel.Width = 139
    ControlLabel.Height = 17
    ControlLabel.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1086#1078#1072#1088#1072
    ControlLabel.Visible = True
    DataField = 'fire_case_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 0
    Visible = True
    CrudAgent = dmFireCases.CrudAgent
    KeyFieldName = 'id'
    DisplayFieldName = 'full_name'
    DisplayRowsCount = 0
    SearchFromStart = False
    OnBeforeGetList = edFIRE_NAMEBeforeGetList
  end
  object cbStorno: TDBCheckBoxEh [5]
    Left = 9
    Top = 187
    Width = 191
    Height = 17
    Caption = #1057#1090#1086#1088#1085#1086
    DataField = 'isstorno'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 4
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  inherited dsLocal: TDataSource
    Left = 304
    Top = 76
  end
end
