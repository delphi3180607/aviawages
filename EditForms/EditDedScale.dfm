inherited FormEditDedScale: TFormEditDedScale
  Caption = #1047#1085#1072#1095#1077#1085#1080#1103
  ClientHeight = 348
  ClientWidth = 371
  ExplicitWidth = 387
  ExplicitHeight = 387
  TextHeight = 17
  inherited plBottom: TPanel
    Top = 305
    Width = 371
    TabOrder = 5
    ExplicitTop = 305
    ExplicitWidth = 371
    inherited btOk: TButton
      Left = 168
      ExplicitLeft = 168
    end
    inherited btCancel: TButton
      Left = 271
      ExplicitLeft = 271
    end
  end
  object edSTART_DATE: TDBDateTimeEditEh [1]
    Left = 181
    Top = 25
    Width = 122
    Height = 25
    ControlLabel.Width = 74
    ControlLabel.Height = 17
    ControlLabel.Caption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
    ControlLabel.Visible = True
    ControlLabelLocation.Position = lpLeftTextBaselineEh
    DataField = 'start_date'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Kind = dtkDateEh
    TabOrder = 0
    Visible = True
  end
  object edESV: TDBNumberEditEh [2]
    Left = 181
    Top = 75
    Width = 122
    Height = 25
    ControlLabel.Width = 40
    ControlLabel.Height = 17
    ControlLabel.Caption = #1045#1057#1042', %'
    ControlLabel.Visible = True
    ControlLabelLocation.Position = lpLeftTextBaselineEh
    DataField = 'esv'
    DataSource = dsLocal
    DecimalPlaces = 4
    DynProps = <>
    EditButtons = <>
    TabOrder = 1
    Visible = True
  end
  object edPFR: TDBNumberEditEh [3]
    Left = 181
    Top = 125
    Width = 122
    Height = 25
    ControlLabel.Width = 116
    ControlLabel.Height = 17
    ControlLabel.Caption = #1055#1060#1056' ('#1076#1086#1087'.'#1090#1072#1088#1080#1092'), %'
    ControlLabel.Visible = True
    ControlLabelLocation.Position = lpLeftTextBaselineEh
    DataField = 'pfr'
    DataSource = dsLocal
    DecimalPlaces = 4
    DynProps = <>
    EditButtons = <>
    TabOrder = 2
    Visible = True
  end
  object edFSS: TDBNumberEditEh [4]
    Left = 181
    Top = 175
    Width = 122
    Height = 25
    ControlLabel.Width = 43
    ControlLabel.Height = 17
    ControlLabel.Caption = #1060#1057#1057', %'
    ControlLabel.Visible = True
    ControlLabelLocation.Position = lpLeftTextBaselineEh
    DataField = 'fss'
    DataSource = dsLocal
    DecimalPlaces = 4
    DynProps = <>
    EditButtons = <>
    TabOrder = 3
    Visible = True
  end
  object edVacationReserveKoeff: TDBNumberEditEh [5]
    Left = 181
    Top = 250
    Width = 122
    Height = 25
    ControlLabel.Width = 115
    ControlLabel.Height = 38
    ControlLabel.Caption = #1050#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090' '#1088#1077#1079#1077#1088#1074#1072' '#1086#1090#1087#1091#1089#1082#1086#1074
    ControlLabel.Visible = True
    ControlLabel.WordWrap = True
    ControlLabelLocation.Spacing = 0
    ControlLabelLocation.Position = lpLeftBottomEh
    DataField = 'vacation_reserve_koeff'
    DataSource = dsLocal
    DecimalPlaces = 4
    DynProps = <>
    EditButtons = <>
    TabOrder = 4
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 317
  end
end
