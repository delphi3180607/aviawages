inherited FormEditSQL: TFormEditSQL
  Caption = #1047#1072#1087#1088#1086#1089
  ClientHeight = 652
  ClientWidth = 896
  ExplicitWidth = 912
  ExplicitHeight = 691
  TextHeight = 17
  inherited plBottom: TPanel
    Top = 609
    Width = 896
    ExplicitTop = 609
    ExplicitWidth = 896
    inherited btOk: TButton
      Left = 693
      ExplicitLeft = 693
    end
    inherited btCancel: TButton
      Left = 796
      ExplicitLeft = 796
    end
  end
  object meSQL: TSynEdit [1]
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 890
    Height = 603
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Consolas'
    Font.Style = []
    Font.Quality = fqClearTypeNatural
    TabOrder = 1
    UseCodeFolding = False
    Gutter.Font.Charset = DEFAULT_CHARSET
    Gutter.Font.Color = clWindowText
    Gutter.Font.Height = -11
    Gutter.Font.Name = 'Consolas'
    Gutter.Font.Style = []
    Gutter.Bands = <
      item
        Kind = gbkMarks
        Width = 13
      end
      item
        Kind = gbkLineNumbers
      end
      item
        Kind = gbkFold
      end
      item
        Kind = gbkTrackChanges
      end
      item
        Kind = gbkMargin
        Width = 3
      end>
    Highlighter = SynSQLSyn1
    SelectedColor.Alpha = 0.400000005960464500
    ExplicitLeft = 296
    ExplicitTop = 184
    ExplicitWidth = 393
    ExplicitHeight = 265
  end
  object SynSQLSyn1: TSynSQLSyn
    CommentAttri.Foreground = 4227072
    DataTypeAttri.Foreground = clTeal
    FunctionAttri.Foreground = clMaroon
    SymbolAttri.Foreground = clBlue
    ProcNameAttri.Foreground = clMaroon
    SQLDialect = sqlPostgres
    Left = 184
    Top = 256
  end
end
