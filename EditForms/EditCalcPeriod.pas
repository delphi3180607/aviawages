unit EditCalcPeriod;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FMAEdit, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls, DBCtrlsEh, Vcl.Mask;

type
  TFormEditCalcPeriod = class(TFMAFormEdit)
    edDATE_CLOSED: TDBDateTimeEditEh;
    edPERIOD_BEGIN: TDBDateTimeEditEh;
    btClose: TButton;
    procedure FormShow(Sender: TObject);
    procedure btCloseClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditCalcPeriod: TFormEditCalcPeriod;

implementation

{$R *.dfm}

procedure TFormEditCalcPeriod.btCloseClick(Sender: TObject);
begin
  if edDATE_CLOSED.Value = null then
    edDATE_CLOSED.Value := now()
  else
    edDATE_CLOSED.Value := null;
end;

procedure TFormEditCalcPeriod.FormShow(Sender: TObject);
begin
  inherited;
  if edDATE_CLOSED.Value = null then
    btClose.Caption := '�������'
  else
    btClose.Caption := '�������';
end;

end.
