unit EditJump;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FMAEdit, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls, DBCtrlsEh, Vcl.Mask, FMALookUp, dmujumpkinds, dmuworkers, dmufirecases;

type
  TFormEditJump = class(TFMAFormEdit)
    edAMOUNT: TDBNumberEditEh;
    edDATE_BEGIN: TDBDateTimeEditEh;
    edDATE_END: TDBDateTimeEditEh;
    edFIRE_CASE_NAME: TFMALookUp;
    edWORKER_CODE: TFMALookUp;
    edJUMP_KIND_NAME: TFMALookUp;
    cbStorno: TDBCheckBoxEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditJump: TFormEditJump;

implementation

{$R *.dfm}

end.
