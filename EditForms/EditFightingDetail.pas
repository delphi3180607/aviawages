unit EditFightingDetail;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FMAEdit, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls, DBCtrlsEh, Vcl.Mask;

type
  TFormEditFightingDetail = class(TFMAFormEdit)
    edDAY: TDBDateTimeEditEh;
    edWORK_HOURS: TDBDateTimeEditEh;
    edOVERTIME_HOURS: TDBDateTimeEditEh;
    edHOLIDAY_HOURS: TDBDateTimeEditEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditFightingDetail: TFormEditFightingDetail;

implementation

{$R *.dfm}

end.
