unit EditReportColumn;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FMAEdit, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls, DBCtrlsEh, Vcl.Mask;

type
  TFormEditReportColumn = class(TFMAFormEdit)
    edFIELD_NAME: TDBEditEh;
    edTITLE: TDBEditEh;
    edDISPLAY_WIDTH: TDBNumberEditEh;
    edDISPLAY_ALIGN: TDBComboBoxEh;
    edORDER_NUM: TDBNumberEditEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditReportColumn: TFormEditReportColumn;

implementation

{$R *.dfm}

end.
