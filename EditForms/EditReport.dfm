inherited FormEditReport: TFormEditReport
  Caption = #1054#1090#1095#1077#1090
  ClientHeight = 532
  ClientWidth = 578
  ExplicitWidth = 594
  ExplicitHeight = 571
  TextHeight = 17
  inherited plBottom: TPanel
    Top = 489
    Width = 578
    TabOrder = 6
    ExplicitTop = 511
    ExplicitWidth = 578
    inherited btOk: TButton
      Left = 375
      ExplicitLeft = 375
    end
    inherited btCancel: TButton
      Left = 478
      ExplicitLeft = 478
    end
  end
  object edNAME: TDBEditEh [1]
    Left = 10
    Top = 25
    Width = 551
    Height = 25
    ControlLabel.Width = 89
    ControlLabel.Height = 17
    ControlLabel.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
    ControlLabel.Visible = True
    DataField = 'name'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 0
    Visible = True
  end
  object edTEMPLATE: TDBEditEh [2]
    Left = 10
    Top = 75
    Width = 551
    Height = 25
    ControlLabel.Width = 50
    ControlLabel.Height = 17
    ControlLabel.Caption = #1064#1072#1073#1083#1086#1085
    ControlLabel.Visible = True
    DataField = 'template'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
        Style = ebsEllipsisEh
        OnClick = edTEMPLATEEditButtons0Click
      end>
    TabOrder = 1
    Visible = True
  end
  object edPREPARE_COMMAND_SQL: TDBEditEh [3]
    Left = 10
    Top = 130
    Width = 239
    Height = 239
    AutoSize = False
    ControlLabel.Width = 125
    ControlLabel.Height = 17
    ControlLabel.Caption = #1050#1086#1084#1072#1085#1076#1072' '#1087#1086#1076#1075#1086#1090#1086#1074#1082#1080
    ControlLabel.Visible = True
    DataField = 'prepare_command_sql'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
        Style = ebsEllipsisEh
        OnClick = edPREPARE_COMMAND_SQLEditButtons0Click
      end>
    TabOrder = 2
    Visible = True
    WordWrap = True
  end
  object edQUERY_SQL: TDBEditEh [4]
    Left = 264
    Top = 130
    Width = 297
    Height = 239
    AutoSize = False
    ControlLabel.Width = 178
    ControlLabel.Height = 17
    ControlLabel.Caption = #1047#1072#1087#1088#1086#1089' '#1076#1083#1103' '#1087#1086#1083#1091#1095#1077#1085#1080#1103' '#1086#1090#1095#1077#1090#1072
    ControlLabel.Visible = True
    DataField = 'query_sql'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
        Style = ebsEllipsisEh
        OnClick = edQUERY_SQLEditButtons0Click
      end>
    TabOrder = 3
    Visible = True
    WordWrap = True
  end
  object edGroupFieldLevel1: TDBEditEh [5]
    Left = 10
    Top = 397
    Width = 335
    Height = 25
    ControlLabel.Width = 234
    ControlLabel.Height = 17
    ControlLabel.Caption = #1043#1088#1091#1087#1087#1080#1088#1086#1074#1082#1072' '#1087#1086' '#1087#1086#1083#1102' '#1087#1077#1088#1074#1086#1075#1086' '#1091#1088#1086#1074#1085#1103
    ControlLabel.Visible = True
    DataField = 'group_field_level1'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 4
    Visible = True
  end
  object edGroupFieldLevel2: TDBEditEh [6]
    Left = 10
    Top = 448
    Width = 335
    Height = 25
    ControlLabel.Width = 233
    ControlLabel.Height = 17
    ControlLabel.Caption = #1043#1088#1091#1087#1087#1080#1088#1086#1074#1082#1072' '#1087#1086' '#1087#1086#1083#1102' '#1074#1090#1086#1088#1086#1075#1086' '#1091#1088#1086#1074#1085#1103
    ControlLabel.Visible = True
    DataField = 'group_field_level2'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 5
    Visible = True
  end
  inherited dsLocal: TDataSource
    Top = 93
  end
end
