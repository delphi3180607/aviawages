unit EditReportParam;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FMAEdit, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls, DBCtrlsEh, Vcl.Mask, FMALookUp, FMACrudAgent, FMAFilter;

type
  TFormEditReportParam = class(TFMAFormEdit)
    edCODE: TDBEditEh;
    edCAPTION: TDBEditEh;
    edPARAM_TYPE: TDBComboBoxEh;
    edPARAM_LENGTH: TDBNumberEditEh;
    edPARAM_DECIMALS: TDBNumberEditEh;
    edGETLIST_METHOD: TDBComboBoxEh;
    edDISPLAY_FIELD_NAME: TDBEditEh;
    edKEY_FIELD_NAME: TDBEditEh;
    edORDER_NUM: TDBNumberEditEh;
    edMASTER_PARAMETER: TFMALookUp;
    edExpression: TDBEditEh;
    procedure edMASTER_PARAMETERBeforeGetList(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditReportParam: TFormEditReportParam;

implementation

uses dmureportparams, dmuBaseDataSnap;

{$R *.dfm}

procedure TFormEditReportParam.edMASTER_PARAMETERBeforeGetList(Sender: TObject);
begin
  edMASTER_PARAMETER.CrudAgent.OneOffFilter := TFMAFilter.Create;
  with edMASTER_PARAMETER.CrudAgent.OneOffFilter do
  begin
    FilterExpression := 'report_id = :report_id';
    FilterParameters.Add('report_id', FilterValue( dsLocal.DataSet.FieldByName('report_id').DataType, dsLocal.DataSet.FieldByName('report_id').Value));
  end;
end;

end.
