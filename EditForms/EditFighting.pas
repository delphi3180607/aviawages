unit EditFighting;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FMAEdit, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls, DBCtrlsEh, Vcl.Mask, FMALookUp, dmuworkers;

type
  TFormEditFighting = class(TFMAFormEdit)
    edDATE_FROM: TDBDateTimeEditEh;
    edDATE_TO: TDBDateTimeEditEh;
    edWORKER_CODE: TFMALookUp;
    edFIRE_NAME: TFMALookUp;
    cbStorno: TDBCheckBoxEh;
    procedure edFIRE_NAMEBeforeGetList(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditFighting: TFormEditFighting;

implementation

uses dmucurrentperiod;

{$R *.dfm}

procedure TFormEditFighting.edFIRE_NAMEBeforeGetList(Sender: TObject);
begin
  with edFIRE_NAME.CRUDAgent do
  begin
    ParamTransform.ResetContext;
    ParamTransform.AddPatternContext('/*wherefilter*/','(datefrom <= :date_end) and (dateto >= :date_begin or dateto is null) ');
    ParamTransform.AddParameterContext('date_begin', ftDate, dmcurrentperiod.FilterStartDate);
    ParamTransform.AddParameterContext('date_end', ftDate, dmcurrentperiod.FilterEndDate);
  end;
end;

end.
