unit EditFlightTarif;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FMAEdit, Data.DB, Vcl.StdCtrls, FMAFunctions, Math,
  Vcl.ExtCtrls, Vcl.Mask, DBCtrlsEh, FMALookUp, dmuplanes, dmupaygroups;

type
  TFormEditFlightTarif = class(TFMAFormEdit)
    edPAYGROUP_CODE: TFMALookUp;
    edPLANE_CODE: TFMALookUp;
    edHEAD_SALARY: TDBNumberEditEh;
    edPERCENT: TDBNumberEditEh;
    edTARIF: TDBNumberEditEh;
    procedure edPERCENTChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditFlightTarif: TFormEditFlightTarif;

implementation

{$R *.dfm}

procedure TFormEditFlightTarif.edPERCENTChange(Sender: TObject);
begin
  if Assigned(dsLocal.DataSet) then
  edTARIF.Value :=  Math.RoundTo(VarToRealDef(edHEAD_SALARY.Value)*VarToRealDef(edPERCENT.Value)/100,-2);
end;

end.
