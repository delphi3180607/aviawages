inherited FormEditFlightTarif: TFormEditFlightTarif
  Caption = #1058#1072#1088#1080#1092' '#1085#1072#1083#1077#1090#1072
  ClientHeight = 237
  ClientWidth = 424
  ExplicitWidth = 440
  ExplicitHeight = 276
  TextHeight = 17
  inherited plBottom: TPanel
    Top = 194
    Width = 424
    TabOrder = 5
    ExplicitTop = 194
    ExplicitWidth = 424
    inherited btOk: TButton
      Left = 221
      ExplicitLeft = 221
    end
    inherited btCancel: TButton
      Left = 324
      ExplicitLeft = 324
    end
  end
  object edPAYGROUP_CODE: TFMALookUp [1]
    Left = 12
    Top = 29
    Width = 388
    Height = 25
    ControlLabel.Width = 88
    ControlLabel.Height = 17
    ControlLabel.Caption = #1043#1088#1091#1087#1087#1072' '#1086#1087#1083#1072#1090#1099
    ControlLabel.Visible = True
    DataField = 'paygroup_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 0
    Visible = True
    CrudAgent = dmpaygroups.CrudAgent
    KeyFieldName = 'id'
    DisplayFieldName = 'name'
    DisplayRowsCount = 0
    SearchFromStart = False
  end
  object edPLANE_CODE: TFMALookUp [2]
    Left = 12
    Top = 81
    Width = 388
    Height = 25
    ControlLabel.Width = 40
    ControlLabel.Height = 17
    ControlLabel.Caption = #1042#1080#1076' '#1042#1057
    ControlLabel.Visible = True
    DataField = 'plane_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 1
    Visible = True
    CrudAgent = dmplanes.CrudAgent
    KeyFieldName = 'id'
    DisplayFieldName = 'name'
    DisplayRowsCount = 0
    SearchFromStart = False
  end
  object edHEAD_SALARY: TDBNumberEditEh [3]
    Left = 12
    Top = 136
    Width = 121
    Height = 25
    ControlLabel.Width = 107
    ControlLabel.Height = 17
    ControlLabel.Caption = #1054#1082#1083#1072#1076' '#1082#1086#1084#1072#1085#1076#1080#1088#1072
    ControlLabel.Visible = True
    DataField = 'head_salary'
    DataSource = dsLocal
    DisplayFormat = '### ### ##0.00'
    DynProps = <>
    EditButtons = <>
    TabOrder = 2
    Visible = True
    OnChange = edPERCENTChange
  end
  object edPERCENT: TDBNumberEditEh [4]
    Left = 146
    Top = 136
    Width = 121
    Height = 25
    ControlLabel.Width = 51
    ControlLabel.Height = 17
    ControlLabel.Caption = #1055#1088#1086#1094#1077#1085#1090
    ControlLabel.Visible = True
    DataField = 'percent'
    DataSource = dsLocal
    DisplayFormat = '### ### ##0.00'
    DynProps = <>
    EditButtons = <>
    TabOrder = 3
    Visible = True
    OnChange = edPERCENTChange
  end
  object edTARIF: TDBNumberEditEh [5]
    Left = 279
    Top = 136
    Width = 121
    Height = 25
    ControlLabel.Width = 92
    ControlLabel.Height = 17
    ControlLabel.Caption = #1063#1072#1089#1086#1074#1086#1081' '#1090#1072#1088#1080#1092
    ControlLabel.Visible = True
    DataField = 'tarif'
    DataSource = dsLocal
    DisplayFormat = '### ### ##0.00'
    DynProps = <>
    Enabled = False
    EditButtons = <>
    ReadOnly = True
    TabOrder = 4
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 322
    Top = 132
  end
end
