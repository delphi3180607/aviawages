unit EditDedScale;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FMAEdit, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls, DBCtrlsEh, Vcl.Mask;

type
  TFormEditDedScale = class(TFMAFormEdit)
    edSTART_DATE: TDBDateTimeEditEh;
    edESV: TDBNumberEditEh;
    edPFR: TDBNumberEditEh;
    edFSS: TDBNumberEditEh;
    edVacationReserveKoeff: TDBNumberEditEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditDedScale: TFormEditDedScale;

implementation

{$R *.dfm}

end.
