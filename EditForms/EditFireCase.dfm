inherited FormEditFireCase: TFormEditFireCase
  Caption = #1057#1074#1077#1076#1077#1085#1080#1103' '#1086' '#1087#1086#1078#1072#1088#1077
  ClientHeight = 275
  ClientWidth = 406
  ExplicitWidth = 422
  ExplicitHeight = 314
  TextHeight = 17
  inherited plBottom: TPanel
    Top = 232
    Width = 406
    ExplicitTop = 232
    ExplicitWidth = 406
    inherited btOk: TButton
      Left = 203
      ExplicitLeft = 203
    end
    inherited btCancel: TButton
      Left = 306
      ExplicitLeft = 306
    end
  end
  object edNAME: TDBEditEh [1]
    Left = 10
    Top = 25
    Width = 231
    Height = 25
    ControlLabel.Width = 89
    ControlLabel.Height = 17
    ControlLabel.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
    ControlLabel.Visible = True
    Ctl3D = True
    DataField = 'name'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    HighlightRequired = True
    ParentCtl3D = False
    TabOrder = 1
    Visible = True
  end
  object edFORESTRY_NAME: TFMALookUp [2]
    Left = 10
    Top = 75
    Width = 231
    Height = 25
    ControlLabel.Width = 76
    ControlLabel.Height = 17
    ControlLabel.Caption = #1051#1077#1089#1085#1080#1095#1077#1089#1090#1074#1086
    ControlLabel.Visible = True
    DataField = 'forestry_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 2
    Visible = True
    CrudAgent = dmforestries.CrudAgent
    KeyFieldName = 'id'
    DisplayFieldName = 'name'
    DisplayRowsCount = 0
    SearchFromStart = False
  end
  object edSQUARE: TDBNumberEditEh [3]
    Left = 258
    Top = 75
    Width = 121
    Height = 25
    ControlLabel.Width = 49
    ControlLabel.Height = 17
    ControlLabel.Caption = #1050#1074#1072#1088#1090#1072#1083
    ControlLabel.Visible = True
    DataField = 'square'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 3
    Visible = True
  end
  object edDEPARTMENT_NAME: TFMALookUp [4]
    Left = 10
    Top = 126
    Width = 369
    Height = 25
    ControlLabel.Width = 64
    ControlLabel.Height = 17
    ControlLabel.Caption = #1054#1090#1076#1077#1083#1077#1085#1080#1077
    ControlLabel.Visible = True
    DataField = 'department_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 4
    Visible = True
    CrudAgent = dmdepartments.CrudAgent
    KeyFieldName = 'id'
    DisplayFieldName = 'name'
    DisplayRowsCount = 0
    SearchFromStart = False
  end
  object edDATEFROM: TDBDateTimeEditEh [5]
    Left = 10
    Top = 186
    Width = 121
    Height = 25
    ControlLabel.Width = 74
    ControlLabel.Height = 17
    ControlLabel.Caption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
    ControlLabel.Visible = True
    DataField = 'datefrom'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Kind = dtkDateEh
    TabOrder = 5
    Visible = True
  end
  object edDATETO: TDBDateTimeEditEh [6]
    Left = 155
    Top = 186
    Width = 121
    Height = 25
    ControlLabel.Width = 96
    ControlLabel.Height = 17
    ControlLabel.Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
    ControlLabel.Visible = True
    DataField = 'dateto'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Kind = dtkDateEh
    TabOrder = 6
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 376
    Top = 312
  end
end
