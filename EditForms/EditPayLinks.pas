unit EditPayLinks;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FMAEdit, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls, Vcl.Mask, DBCtrlsEh, FMALookUp, dmupayments;

type
  TFormEditPayLinks = class(TFMAFormEdit)
    edPAY_FIRE_CODE: TFMALookUp;
    edPAY_JUMPS_CODE: TFMALookUp;
    edPAY_FLIGHTS_CODE: TFMALookUp;
    edPAY_OVER_CODE: TFMALookUp;
    edPAY_HOLIDAY_CODE: TFMALookUp;
    edFIRECALC: TDBEditEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditPayLinks: TFormEditPayLinks;

implementation

{$R *.dfm}

end.
