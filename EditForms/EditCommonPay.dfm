inherited FormEditCommonPay: TFormEditCommonPay
  Caption = #1054#1073#1097#1072#1103' '#1074#1099#1087#1083#1072#1090#1072
  ClientHeight = 232
  ClientWidth = 398
  ExplicitWidth = 414
  ExplicitHeight = 271
  TextHeight = 17
  inherited plBottom: TPanel
    Top = 189
    Width = 398
    TabOrder = 4
    ExplicitTop = 189
    ExplicitWidth = 398
    inherited btOk: TButton
      Left = 195
      ExplicitLeft = 195
    end
    inherited btCancel: TButton
      Left = 298
      ExplicitLeft = 298
    end
  end
  object edPERCENT: TDBNumberEditEh [1]
    Left = 15
    Top = 132
    Width = 121
    Height = 25
    ControlLabel.Width = 98
    ControlLabel.Height = 17
    ControlLabel.Caption = #1055#1088#1086#1094#1077#1085#1090' '#1086#1087#1083#1072#1090#1099
    ControlLabel.Visible = True
    DataField = 'percent'
    DataSource = dsLocal
    DecimalPlaces = 0
    DynProps = <>
    EditButtons = <>
    TabOrder = 2
    Visible = True
  end
  object edSUMMA: TDBNumberEditEh [2]
    Left = 175
    Top = 132
    Width = 121
    Height = 25
    ControlLabel.Width = 39
    ControlLabel.Height = 17
    ControlLabel.Caption = #1057#1091#1084#1084#1072
    ControlLabel.Visible = True
    DataField = 'summa'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 3
    Visible = True
  end
  object edWORKER_CODE: TFMALookUp [3]
    Left = 15
    Top = 28
    Width = 362
    Height = 25
    ControlLabel.Width = 62
    ControlLabel.Height = 17
    ControlLabel.Caption = #1057#1086#1090#1088#1091#1076#1085#1080#1082
    ControlLabel.Visible = True
    DataField = 'worker_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 0
    Visible = True
    CrudAgent = dmWorkers.CrudAgent
    KeyFieldName = 'id'
    DisplayFieldName = 'full_code'
    DisplayRowsCount = 0
    SearchFromStart = True
  end
  object edPAYMENT_CODE: TFMALookUp [4]
    Left = 15
    Top = 78
    Width = 362
    Height = 25
    ControlLabel.Width = 49
    ControlLabel.Height = 17
    ControlLabel.Caption = #1042#1099#1087#1083#1072#1090#1072
    ControlLabel.Visible = True
    DataField = 'payment_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 1
    Visible = True
    WordWrap = True
    CrudAgent = dmPayments.CrudAgent
    KeyFieldName = 'id'
    DisplayFieldName = 'code'
    DisplayRowsCount = 0
    SearchFromStart = False
  end
  inherited dsLocal: TDataSource
    Left = 328
    Top = 128
  end
end
