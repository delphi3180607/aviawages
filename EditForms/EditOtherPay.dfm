inherited FormEditOtherPay: TFormEditOtherPay
  Caption = #1055#1088#1086#1095#1072#1103' '#1074#1099#1087#1083#1072#1090#1072
  ClientHeight = 354
  ClientWidth = 436
  ExplicitWidth = 452
  ExplicitHeight = 393
  TextHeight = 17
  inherited plBottom: TPanel
    Top = 311
    Width = 436
    TabOrder = 8
    ExplicitTop = 311
    ExplicitWidth = 436
    inherited btOk: TButton
      Left = 233
      ExplicitLeft = 233
    end
    inherited btCancel: TButton
      Left = 336
      ExplicitLeft = 336
    end
  end
  object edPERCENT: TDBNumberEditEh [1]
    Left = 14
    Top = 231
    Width = 121
    Height = 25
    ControlLabel.Width = 98
    ControlLabel.Height = 17
    ControlLabel.Caption = #1055#1088#1086#1094#1077#1085#1090' '#1086#1087#1083#1072#1090#1099
    ControlLabel.Visible = True
    DataField = 'percent'
    DataSource = dsLocal
    DisplayFormat = '##0.00'
    DynProps = <>
    EditButtons = <>
    TabOrder = 5
    Visible = True
  end
  object edSUMMA: TDBNumberEditEh [2]
    Left = 166
    Top = 231
    Width = 121
    Height = 25
    ControlLabel.Width = 39
    ControlLabel.Height = 17
    ControlLabel.Caption = #1057#1091#1084#1084#1072
    ControlLabel.Visible = True
    DataField = 'summa'
    DataSource = dsLocal
    DisplayFormat = '### ### ##0.00'
    DynProps = <>
    EditButtons = <>
    TabOrder = 6
    Visible = True
  end
  object edFIRE_CASE_NAME: TFMALookUp [3]
    Left = 14
    Top = 29
    Width = 403
    Height = 25
    ControlLabel.Width = 139
    ControlLabel.Height = 17
    ControlLabel.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1086#1078#1072#1088#1072
    ControlLabel.Visible = True
    DataField = 'fire_case_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 0
    Visible = True
    CrudAgent = dmFireCases.CrudAgent
    KeyFieldName = 'id'
    DisplayFieldName = 'full_name'
    DisplayRowsCount = 0
    SearchFromStart = False
  end
  object edPAYMENT_CODE: TFMALookUp [4]
    Left = 14
    Top = 129
    Width = 403
    Height = 25
    ControlLabel.Width = 49
    ControlLabel.Height = 17
    ControlLabel.Caption = #1042#1099#1087#1083#1072#1090#1072
    ControlLabel.Visible = True
    DataField = 'payment_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 2
    Visible = True
    WordWrap = True
    CrudAgent = dmPayments.CrudAgent
    KeyFieldName = 'id'
    DisplayFieldName = 'code'
    DisplayRowsCount = 0
    SearchFromStart = False
  end
  object edWORKER_CODE: TFMALookUp [5]
    Left = 14
    Top = 79
    Width = 403
    Height = 25
    ControlLabel.Width = 62
    ControlLabel.Height = 17
    ControlLabel.Caption = #1057#1086#1090#1088#1091#1076#1085#1080#1082
    ControlLabel.Visible = True
    DataField = 'worker_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 1
    Visible = True
    CrudAgent = dmWorkers.CrudAgent
    KeyFieldName = 'id'
    DisplayFieldName = 'full_code'
    DisplayRowsCount = 0
    SearchFromStart = True
  end
  object edISHOLIDAY: TDBCheckBoxEh [6]
    Left = 166
    Top = 183
    Width = 201
    Height = 17
    Caption = #1042#1099#1093#1086#1076#1085#1086#1081'/'#1087#1088#1072#1079#1076#1085#1080#1095#1085#1099#1081
    DataField = 'isholiday'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 4
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object edHours: TDBNumberEditEh [7]
    Left = 14
    Top = 179
    Width = 121
    Height = 25
    ControlLabel.Width = 31
    ControlLabel.Height = 17
    ControlLabel.Caption = #1063#1072#1089#1099
    ControlLabel.Visible = True
    DataField = 'hours'
    DataSource = dsLocal
    DecimalPlaces = 0
    DynProps = <>
    EditButtons = <>
    TabOrder = 3
    Visible = True
  end
  object cbStorno: TDBCheckBoxEh [8]
    Left = 14
    Top = 276
    Width = 103
    Height = 17
    Caption = #1057#1090#1086#1088#1085#1086
    DataField = 'isstorno'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 7
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  inherited dsLocal: TDataSource
    Left = 352
    Top = 216
  end
end
