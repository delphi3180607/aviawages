inherited FormEditReportColumn: TFormEditReportColumn
  Caption = #1050#1086#1083#1086#1085#1082#1072' '#1086#1090#1095#1077#1090#1072
  ClientHeight = 310
  ClientWidth = 360
  ExplicitWidth = 374
  ExplicitHeight = 349
  TextHeight = 17
  inherited plBottom: TPanel
    Top = 267
    Width = 360
    ExplicitTop = 267
    ExplicitWidth = 358
    inherited btOk: TButton
      Left = 157
      ExplicitLeft = 155
    end
    inherited btCancel: TButton
      Left = 260
      ExplicitLeft = 258
    end
  end
  object edFIELD_NAME: TDBEditEh [1]
    Left = 10
    Top = 25
    Width = 343
    Height = 25
    ControlLabel.Width = 59
    ControlLabel.Height = 17
    ControlLabel.Caption = #1048#1084#1103' '#1087#1086#1083#1103
    ControlLabel.Visible = True
    DataField = 'field_name'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 1
    Visible = True
  end
  object edTITLE: TDBEditEh [2]
    Left = 10
    Top = 75
    Width = 343
    Height = 25
    ControlLabel.Width = 63
    ControlLabel.Height = 17
    ControlLabel.Caption = #1047#1072#1075#1086#1083#1086#1074#1086#1082
    ControlLabel.Visible = True
    DataField = 'title'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 2
    Visible = True
  end
  object edDISPLAY_WIDTH: TDBNumberEditEh [3]
    Left = 10
    Top = 125
    Width = 121
    Height = 25
    ControlLabel.Width = 49
    ControlLabel.Height = 17
    ControlLabel.Caption = #1064#1080#1088#1080#1085#1072
    ControlLabel.Visible = True
    DataField = 'display_width'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 3
    Visible = True
  end
  object edDISPLAY_ALIGN: TDBComboBoxEh [4]
    Left = 10
    Top = 175
    Width = 343
    Height = 25
    ControlLabel.Width = 87
    ControlLabel.Height = 17
    ControlLabel.Caption = #1042#1099#1088#1072#1074#1085#1080#1074#1072#1085#1080#1077
    ControlLabel.Visible = True
    DataField = 'display_align'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Items.Strings = (
      ''
      #1042#1087#1088#1072#1074#1086
      #1042#1083#1077#1074#1086
      #1055#1086' '#1094#1077#1085#1090#1088#1091)
    KeyItems.Strings = (
      ''
      'Right'
      'Left'
      'Center')
    TabOrder = 4
    Visible = True
  end
  object edORDER_NUM: TDBNumberEditEh [5]
    Left = 10
    Top = 225
    Width = 121
    Height = 25
    ControlLabel.Width = 53
    ControlLabel.Height = 17
    ControlLabel.Caption = #1055#1086#1088#1103#1076#1086#1082
    ControlLabel.Visible = True
    DataField = 'order_num'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 5
    Visible = True
  end
end
