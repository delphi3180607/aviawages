inherited FormEditPlane: TFormEditPlane
  Caption = #1042#1080#1076' '#1074#1086#1079#1076#1091#1096#1085#1086#1075#1086' '#1089#1091#1076#1085#1072
  ClientHeight = 163
  ClientWidth = 367
  ExplicitWidth = 383
  ExplicitHeight = 202
  TextHeight = 17
  inherited plBottom: TPanel
    Top = 120
    Width = 367
    ExplicitTop = 120
    ExplicitWidth = 367
    inherited btOk: TButton
      Left = 164
      ExplicitLeft = 164
    end
    inherited btCancel: TButton
      Left = 267
      ExplicitLeft = 267
    end
  end
  object edCODE: TDBEditEh [1]
    Left = 5
    Top = 24
    Width = 180
    Height = 25
    ControlLabel.Width = 23
    ControlLabel.Height = 17
    ControlLabel.Caption = #1050#1086#1076
    ControlLabel.Visible = True
    DataField = 'code'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 1
    Visible = True
  end
  object edNAME: TDBEditEh [2]
    Left = 5
    Top = 77
    Width = 351
    Height = 25
    ControlLabel.Width = 89
    ControlLabel.Height = 17
    ControlLabel.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
    ControlLabel.Visible = True
    DataField = 'name'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 2
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 448
    Top = 128
  end
end
