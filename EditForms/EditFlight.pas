unit EditFlight;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FMAEdit, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls, FMALookUp, DBCtrlsEh, Vcl.Mask, dmufirecases, dmuplanes, dmupaygroups, dmuworkers;

type
  TFormEditFlight = class(TFMAFormEdit)
    edDATE_FROM: TDBDateTimeEditEh;
    edDATE_TO: TDBDateTimeEditEh;
    edWORKER_CODE: TFMALookUp;
    edFIRE_CASE_NAME: TFMALookUp;
    edPLANE_CODE: TFMALookUp;
    edPAYGROUP_CODE: TFMALookUp;
    edHOURS: TDBDateTimeEditEh;
    cbStorno: TDBCheckBoxEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditFlight: TFormEditFlight;

implementation

{$R *.dfm}

end.
