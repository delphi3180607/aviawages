inherited FormEditPayLinks: TFormEditPayLinks
  Caption = #1057#1086#1086#1090#1074#1077#1090#1089#1090#1074#1080#1077' '#1074#1099#1087#1083#1072#1090
  ClientHeight = 366
  ClientWidth = 436
  ExplicitWidth = 452
  ExplicitHeight = 405
  TextHeight = 17
  inherited plBottom: TPanel
    Top = 323
    Width = 436
    ExplicitTop = 323
    ExplicitWidth = 434
    inherited btOk: TButton
      Left = 233
      ExplicitLeft = 231
    end
    inherited btCancel: TButton
      Left = 336
      ExplicitLeft = 334
    end
  end
  object edPAY_FIRE_CODE: TFMALookUp [1]
    Left = 10
    Top = 25
    Width = 400
    Height = 25
    ControlLabel.Width = 169
    ControlLabel.Height = 17
    ControlLabel.Caption = #1059#1095#1072#1089#1090#1080#1077' '#1074' '#1090#1091#1096#1077#1085#1080#1080' '#1087#1086#1078#1072#1088#1086#1074
    ControlLabel.Visible = True
    DataField = 'pay_fire_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 1
    Visible = True
    CrudAgent = dmPayments.CrudAgent
    KeyFieldName = 'id'
    DisplayFieldName = 'code'
    DisplayRowsCount = 0
    SearchFromStart = False
  end
  object edPAY_JUMPS_CODE: TFMALookUp [2]
    Left = 10
    Top = 75
    Width = 400
    Height = 25
    ControlLabel.Width = 91
    ControlLabel.Height = 17
    ControlLabel.Caption = #1055#1088#1099#1078#1082#1080'/'#1089#1087#1091#1089#1082#1080
    ControlLabel.Visible = True
    DataField = 'pay_jumps_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 2
    Visible = True
    CrudAgent = dmPayments.CrudAgent
    KeyFieldName = 'id'
    DisplayFieldName = 'code'
    DisplayRowsCount = 0
    SearchFromStart = False
  end
  object edPAY_FLIGHTS_CODE: TFMALookUp [3]
    Left = 10
    Top = 125
    Width = 400
    Height = 25
    ControlLabel.Width = 44
    ControlLabel.Height = 17
    ControlLabel.Caption = #1053#1072#1083#1077#1090#1099
    ControlLabel.Visible = True
    DataField = 'pay_flights_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 3
    Visible = True
    CrudAgent = dmPayments.CrudAgent
    KeyFieldName = 'id'
    DisplayFieldName = 'code'
    DisplayRowsCount = 0
    SearchFromStart = False
  end
  object edPAY_OVER_CODE: TFMALookUp [4]
    Left = 10
    Top = 175
    Width = 400
    Height = 25
    ControlLabel.Width = 88
    ControlLabel.Height = 17
    ControlLabel.Caption = #1057#1074#1077#1088#1093#1091#1088#1086#1095#1085#1099#1077
    ControlLabel.Visible = True
    DataField = 'pay_over_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 4
    Visible = True
    CrudAgent = dmPayments.CrudAgent
    KeyFieldName = 'id'
    DisplayFieldName = 'code'
    DisplayRowsCount = 0
    SearchFromStart = False
  end
  object edPAY_HOLIDAY_CODE: TFMALookUp [5]
    Left = 10
    Top = 225
    Width = 400
    Height = 25
    ControlLabel.Width = 60
    ControlLabel.Height = 17
    ControlLabel.Caption = #1042#1099#1093#1086#1076#1085#1099#1077
    ControlLabel.Visible = True
    DataField = 'pay_holiday_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 5
    Visible = True
    CrudAgent = dmPayments.CrudAgent
    KeyFieldName = 'id'
    DisplayFieldName = 'code'
    DisplayRowsCount = 0
    SearchFromStart = False
  end
  object edFIRECALC: TDBEditEh [6]
    Left = 10
    Top = 275
    Width = 400
    Height = 25
    ControlLabel.Width = 152
    ControlLabel.Height = 17
    ControlLabel.Caption = #1042#1080#1076' '#1088#1072#1089#1095#1077#1090#1072' '#1076#1083#1103' '#1080#1084#1087#1086#1088#1090#1072
    ControlLabel.Visible = True
    DataField = 'firecalc'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 6
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 120
    Top = 304
  end
end
