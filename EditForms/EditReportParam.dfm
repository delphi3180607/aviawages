inherited FormEditReportParam: TFormEditReportParam
  Caption = #1055#1072#1088#1072#1084#1077#1090#1088' '#1086#1090#1095#1077#1090#1072
  ClientHeight = 494
  ClientWidth = 597
  ExplicitWidth = 613
  ExplicitHeight = 533
  TextHeight = 17
  inherited plBottom: TPanel
    Top = 451
    Width = 597
    TabOrder = 11
    ExplicitTop = 451
    ExplicitWidth = 597
    inherited btOk: TButton
      Left = 394
      ExplicitLeft = 394
    end
    inherited btCancel: TButton
      Left = 497
      ExplicitLeft = 497
    end
  end
  object edCODE: TDBEditEh [1]
    Left = 10
    Top = 25
    Width = 215
    Height = 25
    ControlLabel.Width = 92
    ControlLabel.Height = 17
    ControlLabel.Caption = #1050#1086#1076' '#1087#1072#1088#1072#1084#1077#1090#1088#1072
    ControlLabel.Visible = True
    DataField = 'code'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 0
    Visible = True
  end
  object edCAPTION: TDBEditEh [2]
    Left = 10
    Top = 79
    Width = 584
    Height = 25
    ControlLabel.Width = 63
    ControlLabel.Height = 17
    ControlLabel.Caption = #1047#1072#1075#1086#1083#1086#1074#1086#1082
    ControlLabel.Visible = True
    DataField = 'caption'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 1
    Visible = True
  end
  object edPARAM_TYPE: TDBComboBoxEh [3]
    Left = 242
    Top = 25
    Width = 352
    Height = 25
    ControlLabel.Width = 90
    ControlLabel.Height = 17
    ControlLabel.Caption = #1058#1080#1087' '#1087#1072#1088#1072#1084#1077#1090#1088#1072
    ControlLabel.Visible = True
    DataField = 'param_type'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Items.Strings = (
      #1057#1090#1088#1086#1082#1072
      #1063#1080#1089#1083#1086
      #1044#1072#1090#1072
      #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082)
    KeyItems.Strings = (
      'String'
      'Number'
      'Date'
      'List')
    TabOrder = 2
    Visible = True
  end
  object edPARAM_LENGTH: TDBNumberEditEh [4]
    Left = 10
    Top = 133
    Width = 121
    Height = 25
    ControlLabel.Width = 37
    ControlLabel.Height = 17
    ControlLabel.Caption = #1044#1083#1080#1085#1072
    ControlLabel.Visible = True
    DataField = 'param_length'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 3
    Visible = True
  end
  object edPARAM_DECIMALS: TDBNumberEditEh [5]
    Left = 146
    Top = 133
    Width = 123
    Height = 25
    ControlLabel.Width = 115
    ControlLabel.Height = 17
    ControlLabel.Caption = #1044#1077#1089#1103#1090#1080#1095#1085#1099#1093' '#1079#1085#1072#1082#1086#1074
    ControlLabel.Visible = True
    DataField = 'param_decimals'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 4
    Visible = True
  end
  object edGETLIST_METHOD: TDBComboBoxEh [6]
    Left = 10
    Top = 188
    Width = 259
    Height = 25
    ControlLabel.Width = 72
    ControlLabel.Height = 17
    ControlLabel.Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082
    ControlLabel.Visible = True
    DataField = 'getlist_method'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Items.Strings = (
      #1056#1072#1089#1095#1077#1090#1085#1099#1077' '#1087#1077#1088#1080#1086#1076#1099
      #1057#1083#1091#1095#1072#1080' '#1087#1086#1078#1072#1088#1086#1074
      #1054#1090#1076#1077#1083#1077#1085#1080#1103
      #1057#1086#1090#1088#1091#1076#1085#1080#1082#1080
      #1042#1099#1087#1083#1072#1090#1099)
    KeyItems.Strings = (
      'calc_periods'
      'fire_cases'
      'departments'
      'workers'
      'payments')
    TabOrder = 5
    Visible = True
  end
  object edDISPLAY_FIELD_NAME: TDBEditEh [7]
    Left = 10
    Top = 242
    Width = 259
    Height = 25
    ControlLabel.Width = 169
    ControlLabel.Height = 17
    ControlLabel.Caption = #1048#1084#1103' '#1087#1086#1083#1103' '#1076#1083#1103' '#1086#1090#1086#1073#1088#1072#1078#1077#1085#1080#1103
    ControlLabel.Visible = True
    DataField = 'display_field_name'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 6
    Visible = True
  end
  object edKEY_FIELD_NAME: TDBEditEh [8]
    Left = 282
    Top = 242
    Width = 312
    Height = 25
    ControlLabel.Width = 160
    ControlLabel.Height = 17
    ControlLabel.Caption = #1048#1084#1103' '#1087#1086#1083#1103' '#1080#1076#1077#1085#1090#1080#1092#1080#1082#1072#1090#1086#1088#1072
    ControlLabel.Visible = True
    DataField = 'key_field_name'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 7
    Visible = True
  end
  object edORDER_NUM: TDBNumberEditEh [9]
    Left = 10
    Top = 413
    Width = 145
    Height = 25
    ControlLabel.Width = 53
    ControlLabel.Height = 17
    ControlLabel.Caption = #1055#1086#1088#1103#1076#1086#1082
    ControlLabel.Visible = True
    DataField = 'order_num'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 10
    Visible = True
  end
  object edMASTER_PARAMETER: TFMALookUp [10]
    Left = 10
    Top = 302
    Width = 259
    Height = 25
    ControlLabel.Width = 108
    ControlLabel.Height = 17
    ControlLabel.Caption = #1052#1072#1089#1090#1077#1088'-'#1087#1072#1088#1072#1084#1077#1090#1088
    ControlLabel.Visible = True
    DataField = 'master_param_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 8
    Visible = True
    CrudAgent = dmReportParams.CrudAgent
    KeyFieldName = 'id'
    DisplayFieldName = 'code'
    DisplayRowsCount = 0
    SearchFromStart = False
    OnBeforeGetList = edMASTER_PARAMETERBeforeGetList
  end
  object edExpression: TDBEditEh [11]
    Left = 10
    Top = 358
    Width = 584
    Height = 25
    ControlLabel.Width = 147
    ControlLabel.Height = 17
    ControlLabel.Caption = #1042#1099#1088#1072#1078#1077#1085#1080#1077' '#1076#1083#1103' '#1092#1080#1083#1100#1090#1088#1072
    ControlLabel.Visible = True
    DataField = 'filter_expression'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 9
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 344
    Top = 153
  end
end
