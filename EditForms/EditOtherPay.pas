unit EditOtherPay;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FMAEdit, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls, FMALookUp, Vcl.Mask, DBCtrlsEh, dmufirecases, dmuworkers, dmupayments;

type
  TFormEditOtherPay = class(TFMAFormEdit)
    edPERCENT: TDBNumberEditEh;
    edSUMMA: TDBNumberEditEh;
    edFIRE_CASE_NAME: TFMALookUp;
    edPAYMENT_CODE: TFMALookUp;
    edWORKER_CODE: TFMALookUp;
    edISHOLIDAY: TDBCheckBoxEh;
    edHours: TDBNumberEditEh;
    cbStorno: TDBCheckBoxEh;
    procedure edHOURSKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditOtherPay: TFormEditOtherPay;

implementation

{$R *.dfm}

procedure TFormEditOtherPay.edHOURSKeyPress(Sender: TObject; var Key: Char);
begin
  if not (Key in ['0'..'9']) and not (Key = ':') then
  begin
    Key := #0;
    exit;
  end;
end;

end.
