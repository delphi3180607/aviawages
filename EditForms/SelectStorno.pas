unit SelectStorno;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FMAEdit, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls;

type
  TFormSelectStorno = class(TFMAFormEdit)
    rb0: TRadioButton;
    rb1: TRadioButton;
    rb2: TRadioButton;
    procedure btOkClick(Sender: TObject);
  private
    FOperation: integer;
  public
    property Operation: integer read FOperation;
  end;

var
  FormSelectStorno: TFormSelectStorno;

implementation

{$R *.dfm}

procedure TFormSelectStorno.btOkClick(Sender: TObject);
begin
  if rb0.Checked then FOperation := 0
  else if rb1.Checked then FOperation := 1
  else if rb2.Checked then FOperation := 2
  else
  begin
    ShowMessage('�� ������ ������� �������������.');
    ModalResult := mrNone;
  end;
end;

end.
