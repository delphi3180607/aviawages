inherited FormEditJumpKind: TFormEditJumpKind
  Caption = #1042#1080#1076' '#1087#1088#1099#1078#1082#1072'/'#1089#1087#1091#1089#1082#1072
  ClientHeight = 281
  ClientWidth = 428
  ExplicitWidth = 444
  ExplicitHeight = 320
  TextHeight = 17
  inherited plBottom: TPanel
    Top = 238
    Width = 428
    TabOrder = 6
    ExplicitTop = 238
    ExplicitWidth = 426
    inherited btOk: TButton
      Left = 225
      ExplicitLeft = 223
    end
    inherited btCancel: TButton
      Left = 328
      ExplicitLeft = 326
    end
  end
  object edPAYMENT_CODE: TFMALookUp [1]
    Left = 9
    Top = 193
    Width = 398
    Height = 25
    ControlLabel.Width = 78
    ControlLabel.Height = 17
    ControlLabel.Caption = #1050#1086#1076' '#1074#1099#1087#1083#1072#1090#1099
    ControlLabel.Visible = True
    DataField = 'payment_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 5
    Visible = True
    WordWrap = True
    CrudAgent = dmPayments.CrudAgent
    KeyFieldName = 'id'
    DisplayFieldName = 'code'
    DisplayRowsCount = 0
    SearchFromStart = False
  end
  object edCODE: TDBEditEh [2]
    Left = 9
    Top = 27
    Width = 244
    Height = 25
    ControlLabel.Width = 23
    ControlLabel.Height = 17
    ControlLabel.Caption = #1050#1086#1076
    ControlLabel.Visible = True
    DataField = 'code'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 0
    Visible = True
  end
  object edNAME: TDBEditEh [3]
    Left = 9
    Top = 80
    Width = 398
    Height = 25
    ControlLabel.Width = 89
    ControlLabel.Height = 17
    ControlLabel.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
    ControlLabel.Visible = True
    DataField = 'name'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 1
    Visible = True
  end
  object edTARIF: TDBNumberEditEh [4]
    Left = 286
    Top = 133
    Width = 121
    Height = 25
    ControlLabel.Width = 38
    ControlLabel.Height = 17
    ControlLabel.Caption = #1058#1072#1088#1080#1092
    ControlLabel.Visible = True
    DataField = 'tarif'
    DataSource = dsLocal
    DisplayFormat = '### ### ##0.00'
    DynProps = <>
    Enabled = False
    EditButtons = <>
    ReadOnly = True
    TabOrder = 4
    Visible = True
  end
  object edPERCENT: TDBNumberEditEh [5]
    Left = 147
    Top = 133
    Width = 121
    Height = 25
    ControlLabel.Width = 51
    ControlLabel.Height = 17
    ControlLabel.Caption = #1055#1088#1086#1094#1077#1085#1090
    ControlLabel.Visible = True
    DataField = 'percent'
    DataSource = dsLocal
    DisplayFormat = '### ### ##0.00'
    DynProps = <>
    EditButtons = <>
    TabOrder = 3
    Visible = True
    OnChange = edSALARYChange
  end
  object edSALARY: TDBNumberEditEh [6]
    Left = 9
    Top = 133
    Width = 121
    Height = 25
    ControlLabel.Width = 37
    ControlLabel.Height = 17
    ControlLabel.Caption = #1054#1082#1083#1072#1076
    ControlLabel.Visible = True
    DataField = 'salary'
    DataSource = dsLocal
    DisplayFormat = '### ### ##0.00'
    DynProps = <>
    EditButtons = <>
    TabOrder = 2
    Visible = True
    OnChange = edSALARYChange
  end
  inherited dsLocal: TDataSource
    Left = 300
    Top = 26
  end
end
