object FormMain: TFormMain
  Left = 0
  Top = 0
  Caption = #1056#1072#1089#1095#1077#1090' '#1079#1072#1088#1072#1073#1086#1090#1085#1086#1081' '#1087#1083#1072#1090#1099' '#1074' '#1088#1072#1079#1088#1077#1079#1077' '#1089#1083#1091#1095#1072#1077#1074' '#1090#1091#1096#1077#1085#1080#1103' '#1087#1086#1078#1072#1088#1086#1074
  ClientHeight = 553
  ClientWidth = 838
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  WindowState = wsMaximized
  StyleElements = [seFont, seClient]
  OnClose = FormClose
  OnCreate = FormCreate
  TextHeight = 15
  object plTop: TPanel
    Left = 0
    Top = 0
    Width = 838
    Height = 45
    Margins.Left = 5
    Align = alTop
    BevelOuter = bvNone
    Color = 16246229
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    StyleElements = [seFont, seBorder]
    object plCalcPeriod: TPanel
      AlignWithMargins = True
      Left = 3
      Top = 7
      Width = 225
      Height = 32
      Margins.Top = 7
      Margins.Bottom = 6
      Align = alLeft
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object Label5: TLabel
        AlignWithMargins = True
        Left = 3
        Top = 6
        Width = 110
        Height = 26
        Margins.Top = 6
        Margins.Bottom = 0
        Align = alLeft
        Caption = #1056#1072#1089#1095#1077#1090#1085#1099#1081' '#1087#1077#1088#1080#1086#1076
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        ExplicitHeight = 17
      end
      object dtCalcPeriod: TDBDateTimeEditEh
        AlignWithMargins = True
        Left = 119
        Top = 3
        Width = 103
        Height = 24
        Margins.Bottom = 5
        ControlLabel.Font.Charset = DEFAULT_CHARSET
        ControlLabel.Font.Color = clWindowText
        ControlLabel.Font.Height = -12
        ControlLabel.Font.Name = 'Segoe UI'
        ControlLabel.Font.Style = []
        ControlLabel.ParentFont = False
        Align = alClient
        BevelInner = bvNone
        BevelOuter = bvNone
        Ctl3D = True
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        Visible = True
        OnChange = dtCalcPeriodChange
        ExplicitHeight = 28
        EditFormat = 'MM/YYYY'
      end
    end
    object plPeriod: TPanel
      AlignWithMargins = True
      Left = 234
      Top = 7
      Width = 391
      Height = 32
      Margins.Top = 7
      Margins.Bottom = 6
      Align = alLeft
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 1
      object Label3: TLabel
        AlignWithMargins = True
        Left = 3
        Top = 6
        Width = 158
        Height = 26
        Margins.Top = 6
        Margins.Bottom = 0
        Align = alLeft
        Caption = #1055#1077#1088#1080#1086#1076' '#1086#1090#1073#1086#1088#1072' '#1089#1086#1073#1099#1090#1080#1081' '#1089
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        ExplicitHeight = 17
      end
      object Label1: TLabel
        AlignWithMargins = True
        Left = 263
        Top = 6
        Width = 15
        Height = 26
        Margins.Top = 6
        Margins.Bottom = 0
        Align = alLeft
        Caption = #1087#1086
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        ExplicitHeight = 17
      end
      object dtBegin: TDBDateTimeEditEh
        AlignWithMargins = True
        Left = 167
        Top = 3
        Width = 90
        Height = 24
        Margins.Bottom = 5
        Align = alLeft
        BevelInner = bvNone
        BevelOuter = bvNone
        Ctl3D = True
        DynProps = <>
        EditButton.Visible = False
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        Visible = True
        ExplicitHeight = 25
        EditFormat = 'DD/MM/YYYY'
      end
      object dtEnd: TDBDateTimeEditEh
        AlignWithMargins = True
        Left = 284
        Top = 3
        Width = 91
        Height = 24
        Margins.Bottom = 5
        Align = alLeft
        BevelInner = bvNone
        BevelOuter = bvNone
        Ctl3D = True
        DynProps = <>
        EditButton.Visible = False
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
        Visible = True
        ExplicitHeight = 25
        EditFormat = 'DD/MM/YYYY'
      end
    end
  end
  object sbBottom: TStatusBar
    Left = 0
    Top = 534
    Width = 838
    Height = 19
    BorderWidth = 1
    Panels = <
      item
        Alignment = taRightJustify
        Text = 
          #1055#1088#1086#1075#1088#1072#1084#1084#1085#1086#1077' '#1086#1073#1077#1089#1087#1077#1095#1077#1085#1080#1077' '#1088#1072#1079#1088#1072#1073#1086#1090#1072#1085#1086' '#1087#1086' '#1079#1072#1082#1072#1079#1091': '#1050#1043#1057#1040#1059' '#171#1044#1042' '#1072#1074#1080#1072#1073#1072#1079 +
          #1072#187' | '#1080#1089#1087#1086#1083#1085#1080#1090#1077#1083#1100' '#1048#1055' '#1057#1077#1088#1076#1072#1096#1077#1085#1082#1086' '#1040'.'#1054'. | 2024'#1075
        Width = 50
      end>
    StyleElements = [seClient, seBorder]
  end
  object vpWorkspace: TFMAVertPages
    AlignWithMargins = True
    Left = 0
    Top = 72
    Width = 838
    Height = 462
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    Padding.Top = 5
    ParentFont = False
    TabOrder = 2
    ShowButtons = False
  end
  object SectionTabs: TIceTabSet
    Left = 0
    Top = 45
    Width = 838
    Height = 27
    Margins.Left = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 5586227
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    SelectedFont.Charset = DEFAULT_CHARSET
    SelectedFont.Color = clBlack
    SelectedFont.Height = -13
    SelectedFont.Name = 'Segoe UI'
    SelectedFont.Style = []
    ModifiedFont.Charset = DEFAULT_CHARSET
    ModifiedFont.Color = 11777023
    ModifiedFont.Height = -12
    ModifiedFont.Name = 'Segoe UI'
    ModifiedFont.Style = []
    Tabs = <
      item
        Caption = #1055#1077#1088#1074#1080#1095#1085#1099#1077' '#1076#1072#1085#1085#1099#1077
        Selected = True
        Modified = False
      end
      item
        Caption = #1056#1072#1089#1095#1077#1090
        Selected = False
        Modified = False
      end
      item
        Caption = #1054#1090#1095#1077#1090#1099
        Selected = False
        Modified = False
      end
      item
        Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
        Selected = False
        Modified = False
      end
      item
        Caption = #1042#1085#1077#1096#1085#1080#1077' '#1076#1072#1085#1085#1099#1077
        Selected = False
        Modified = False
      end
      item
        Caption = #1054#1073#1089#1083#1091#1078#1080#1074#1072#1085#1080#1077
        Selected = False
        Modified = False
      end>
    TabIndex = 0
    TabBorderColor = 9914950
    MaintainMenu = False
    TabHeight = 26
    CloseTab = False
    AutoTabWidth = False
    TabStartColor = 16644338
    TabStopColor = 14729641
    SelectedTabStartColor = 15846833
    SelectedTabStopColor = 15846833
    ModifiedTabStartColor = 10588280
    ModifiedTabStopColor = 10588280
    BackgroundStartColor = 16246229
    BackgroundStopColor = 16246229
    CanDragTabs = False
    OnTabSelected = SectionTabsTabSelected
  end
  object IL: TPngImageList
    Height = 24
    Width = 24
    PngImages = <
      item
        Background = clWindow
        Name = 'wildfire'
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F80000000970485973000000B1000000B101C62D498D0000001974455874536F
          667477617265007777772E696E6B73636170652E6F72679BEE3C1A0000022349
          44415478DACDD54F48554114C7F1634A1404D5224224C28AA85DABC285BD2290
          92A845269658519B48A9C0CCA810256A617F16950446828A9045A51115151A05
          2EC2366D444217854158687F202AEAF53D9CF360105FF7A94F70E083BCF1DEFB
          9B3B67666E864C73CB98690199284129065191CE803CD4E31172F00CF7D215B0
          1B7BB1071F3D44FB46D211B00127B00DBFBCAF1B05F83DD580B9E8C2567C0EFA
          AFE30ADE4C36609ED81CDFC24F348CF9BF8EBE1047271B701A9BB04EACA089B9
          2EF2BF77C4EA50830FFE7B1FFA520DE8F169D16928F5BE057812BCC17CDCC549
          1CF081ACC79FA88055A8F29BC27659ACB87A4F0C4750EED75DC352BCC2FDA880
          43F886D6A06F359E8B6D326D37C556D70086C496EC28CA70382A4047DA8CD741
          DF42B4A1D37F6FF7A9D3DA9CC325B15DDE10D42969C043ECC297A06F313A908F
          385E60A75881132D0B4FB1312AE0A5172B1EF4DDF0E225CE9EABFEC0FDC135B3
          BC46B1A880761CC3FBA04F477B112BFD9E7E54E27670CD12BFA6382A408BF41D
          4D41DF19BC458BFFD67369B9D83E48345D4DB3C556D47F0316899D92319F161D
          996EA42DC1B4E97DBAD176F89B667A5DB4F89FA202B455FB1C9FC5669F8E39C8
          153BF086F1D5A7E4B1BFC90F9C1FFBA064015A305D96BD624B5047BE02A7FCE1
          BAA27AFCDAE35823B66CE3A90688BF761DD6E202DEF99B69C003B193B6CA836A
          F177BC87A4F23D58868362EB3B5BEC1BA01B4CCFA546B14F67D236E33EFA136E
          FF0062BE74199C05CE960000000049454E44AE426082}
      end>
    Left = 416
    Top = 128
    Bitmap = {}
  end
  object AL: TActionList
    Left = 376
    Top = 128
    object ActionConnection: TAction
      Caption = #1053#1072#1089#1090#1088#1086#1080#1090#1100' '#1089#1086#1077#1076#1080#1085#1077#1085#1080#1077
      ShortCut = 24641
    end
  end
end
