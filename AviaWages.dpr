program AviaWages;

uses
  Vcl.Forms,
  System.SysUtils,
  Vcl.Dialogs,
  dmu in 'DBSnap\dmu.pas' {DM: TDataModule},
  FireCases in 'GridFrames\FireCases.pas' {FormFireCases},
  main in 'main.pas' {FormMain},
  Splash in 'Splash.pas' {FormSplash},
  FormsGroup in 'FormGroups\FormsGroup.pas' {FormFormsGroup},
  PeriodGroup in 'FormGroups\PeriodGroup.pas' {FormPeriodGroup},
  CalculationGroup in 'FormGroups\CalculationGroup.pas' {FormCalcGroup},
  InputGroup in 'FormGroups\InputGroup.pas' {FormInputGroup},
  MasterDataGroup in 'FormGroups\MasterDataGroup.pas' {FormMasterDataGroup},
  ReportsGroup in 'FormGroups\ReportsGroup.pas' {FormReportsGroup},
  ServiceGroup in 'FormGroups\ServiceGroup.pas' {FormServiceGroup},
  Vcl.Themes,
  Vcl.Styles,
  InitStated in 'BaseClasses\InitStated.pas' {FormInitStated},
  EditFireCase in 'EditForms\EditFireCase.pas' {FormEditFireCase},
  Fightings in 'GridFrames\Fightings.pas' {FormFightings},
  EditFighting in 'EditForms\EditFighting.pas' {FormEditFighting},
  Jumps in 'GridFrames\Jumps.pas' {FormJumps},
  Flights in 'GridFrames\Flights.pas' {FormFlights},
  OtherPays in 'GridFrames\OtherPays.pas' {FormOtherPays},
  CommonPays in 'GridFrames\CommonPays.pas' {FormCommonPays},
  EditJump in 'EditForms\EditJump.pas' {FormEditJump},
  EditFlight in 'EditForms\EditFlight.pas' {FormEditFlight},
  EditCommonPay in 'EditForms\EditCommonPay.pas' {FormEditCommonPay},
  EditOtherPay in 'EditForms\EditOtherPay.pas' {FormEditOtherPay},
  CalcPeriods in 'GridFrames\CalcPeriods.pas' {FormCalcPeriods},
  dmucalcperiods in 'DBSnap\dmucalcperiods.pas' {dmCalcPeriods: TDataModule},
  FMAEdit in 'C:\RAD11\_COMPONENTS\FMACrudDisp\FMAEdit.pas' {FMAFormEdit},
  EditCalcPeriod in 'EditForms\EditCalcPeriod.pas' {FormEditCalcPeriod},
  dmuBaseDataSnap in 'BaseClasses\dmuBaseDataSnap.pas' {dmBaseDataSnap: TDataModule},
  Forestries in 'GridFrames\Forestries.pas' {FormForestries},
  JumpKinds in 'GridFrames\JumpKinds.pas' {FormJumpKinds},
  Planes in 'GridFrames\Planes.pas' {FormPlanes},
  PayGroups in 'GridFrames\PayGroups.pas' {FormPayGroups},
  FlightTarifs in 'GridFrames\FlightTarifs.pas' {FormFlightTarifs},
  Calculation in 'GridFrames\Calculation.pas' {FormCalculation},
  BaseSimpleGrid in 'BaseClasses\BaseSimpleGrid.pas' {FrameBaseSimpleGrid: TFrame},
  Export in 'GridFrames\Export.pas' {FormExport},
  dmucommonpays in 'DBSnap\dmucommonpays.pas' {dmCommonPays: TDataModule},
  dmufightings in 'DBSnap\dmufightings.pas' {dmFightings: TDataModule},
  dmufirecases in 'DBSnap\dmufirecases.pas' {dmFireCases: TDataModule},
  dmuflights in 'DBSnap\dmuflights.pas' {dmFlights: TDataModule},
  dmujumps in 'DBSnap\dmujumps.pas' {dmJumps: TDataModule},
  dmuotherpays in 'DBSnap\dmuotherpays.pas' {dmOtherPays: TDataModule},
  BaseFireLinked in 'BaseClasses\BaseFireLinked.pas' {FormBaseFireLinked},
  dmuforestries in 'DBSnap\dmuforestries.pas' {dmforestries: TDataModule},
  dmujumpkinds in 'DBSnap\dmujumpkinds.pas' {dmjumpkinds: TDataModule},
  dmuplanes in 'DBSnap\dmuplanes.pas' {dmplanes: TDataModule},
  dmupaygroups in 'DBSnap\dmupaygroups.pas' {dmpaygroups: TDataModule},
  dmuflighttarifs in 'DBSnap\dmuflighttarifs.pas' {dmflighttarifs: TDataModule},
  EditForestry in 'EditForms\EditForestry.pas' {FormEditForestry},
  EditPayGroup in 'EditForms\EditPayGroup.pas' {FormEditPayGroup},
  EditPlane in 'EditForms\EditPlane.pas' {FormEditPlane},
  EditFlightTarif in 'EditForms\EditFlightTarif.pas' {FormEditFlightTarif},
  EditJumpKind in 'EditForms\EditJumpKind.pas' {FormEditJumpKind},
  EditFightingDetail in 'EditForms\EditFightingDetail.pas' {FormEditFightingDetail},
  dmudepartments in 'DBSnap\dmudepartments.pas' {dmdepartments: TDataModule},
  dmuworkers in 'DBSnap\dmuworkers.pas' {dmWorkers: TDataModule},
  Workers in 'GridFrames\Workers.pas' {FormWorkers},
  ExternalGroup in 'FormGroups\ExternalGroup.pas' {FormExternalGroup},
  Departments in 'GridFrames\Departments.pas' {FormDepartments},
  BaseTree in 'BaseClasses\BaseTree.pas' {FrameBaseTree: TFrame},
  dmucatalogs in 'DBSnap\dmucatalogs.pas' {dmCatalogs: TDataModule},
  dmupayments in 'DBSnap\dmupayments.pas' {dmPayments: TDataModule},
  Payments in 'GridFrames\Payments.pas' {FormPayments},
  dmucurrentperiod in 'Repository\dmucurrentperiod.pas' {dmcurrentperiod: TDataModule},
  GlobalExceptionHandlerUnit in 'GlobalExceptionHandlerUnit.pas',
  dmugrounds in 'DBSnap\dmugrounds.pas' {dmGrounds: TDataModule},
  BaseVertGrid in 'BaseClasses\BaseVertGrid.pas' {FrameBaseVertGrid: TFrame},
  PayLinks in 'GridFrames\PayLinks.pas' {FormPayLinks},
  EditPayLinks in 'EditForms\EditPayLinks.pas' {FormEditPayLinks},
  dmupaylinks in 'DBSnap\dmupaylinks.pas' {dmPayLinks: TDataModule},
  dmuPayments2Payments in 'DBSnap\dmuPayments2Payments.pas' {dmPayments2Payments: TDataModule},
  EditPayment2Payment in 'EditForms\EditPayment2Payment.pas' {FormEditPayment2Payment},
  EditFightingDays in 'EditForms\EditFightingDays.pas' {FormEditFightingDays},
  Import in 'GridFrames\Import.pas' {FormImport},
  dmuutils in 'DBSnap\dmuutils.pas' {dmUtils: TDataModule},
  dmuaccurals in 'DBSnap\dmuaccurals.pas' {dmAccurals: TDataModule},
  dmufot in 'DBSnap\dmufot.pas' {dmFOT: TDataModule},
  Reports in 'GridFrames\Reports.pas' {FormReports},
  dmureports in 'DBSnap\dmureports.pas' {dmReports: TDataModule},
  dmureportcolumns in 'DBSnap\dmureportcolumns.pas' {dmReportColumns: TDataModule},
  dmureportparams in 'DBSnap\dmureportparams.pas' {dmReportParams: TDataModule},
  EditReport in 'EditForms\EditReport.pas' {FormEditReport},
  EditReportColumn in 'EditForms\EditReportColumn.pas' {FormEditReportColumn},
  EditReportParam in 'EditForms\EditReportParam.pas' {FormEditReportParam},
  UserReports in 'FormGroups\UserReports.pas' {FormUserReports},
  EditReportParams in 'EditForms\EditReportParams.pas' {FormEditReportParams},
  EditSQL in 'EditForms\EditSQL.pas' {FormEditSQL},
  SelectTemplate in 'EditForms\SelectTemplate.pas' {FormSelectTemplate},
  BaseGrid in 'BaseClasses\BaseGrid.pas' {FrameBaseGrid: TFrame},
  dmuformcollection in 'Repository\dmuformcollection.pas' {dmFormCollection: TDataModule},
  Functions in 'Functions.pas',
  dmudedscale in 'DBSnap\dmudedscale.pas' {dmDedScale: TDataModule},
  DedScales in 'GridFrames\DedScales.pas' {FormDedScales},
  EditDedScale in 'EditForms\EditDedScale.pas' {FormEditDedScale},
  StronoContainer in 'GridFrames\StronoContainer.pas' {FormStornoContainer},
  SelectStorno in 'EditForms\SelectStorno.pas' {FormSelectStorno};

{$R *.res}

var myYear, myMonth, myDay: Word;

begin
  Application.Initialize;
  Application.OnException := TFormMain.GlobalExceptionHandler;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Waikawa Light 2');
  Application.CreateForm(TFormMain, FormMain);
  DecodeDate(now(), myYear, myMonth, myDay);
  Application.CreateForm(TFormSplash, FormSplash);
  FormSplash.Show;
  Application.ProcessMessages;
  Application.CreateForm(TDM, DM);
  if not DM.Connect then
  begin
    Application.Terminate;
    Application.Run;
    exit;
  end;
  PrepareForm(Tdmcurrentperiod, dmcurrentperiod);
  PrepareForm(TdmDedScale, dmDedScale);
  PrepareForm(TFormDedScales, FormDedScales);
  PrepareForm(TFormEditDedScale, FormEditDedScale);
  PrepareForm(TdmCatalogs, dmCatalogs);
  PrepareForm(TFormStornoContainer, FormStornoContainer);
  PrepareForm(TFormSelectStorno, FormSelectStorno);
  PrepareForm(TdmGrounds, dmGrounds);
  PrepareForm(TdmPayments, dmPayments);
  PrepareForm(TdmPayLinks, dmPayLinks);
  PrepareForm(TdmPayments2Payments, dmPayments2Payments);
  PrepareForm(TdmCalcPeriods, dmCalcPeriods);
  PrepareForm(TdmBaseDataSnap, dmBaseDataSnap);
  PrepareForm(TdmCommonPays, dmCommonPays);
  PrepareForm(TdmFightings, dmFightings);
  PrepareForm(TdmFireCases, dmFireCases);
  PrepareForm(TdmFlights, dmFlights);
  PrepareForm(TdmJumps, dmJumps);
  PrepareForm(TdmOtherPays, dmOtherPays);
  PrepareForm(Tdmforestries, dmforestries);
  PrepareForm(Tdmjumpkinds, dmjumpkinds);
  PrepareForm(Tdmplanes, dmplanes);
  PrepareForm(Tdmpaygroups, dmpaygroups);
  PrepareForm(Tdmflighttarifs, dmflighttarifs);
  PrepareForm(Tdmdepartments, dmdepartments);
  PrepareForm(TdmWorkers, dmWorkers);
  PrepareForm(Tdmfirecases, dmfirecases);
  PrepareForm(Tdmfightings, dmfightings);
  PrepareForm(Tdmjumps, dmjumps);
  PrepareForm(Tdmflights, dmflights);
  PrepareForm(Tdmotherpays, dmotherpays);
  PrepareForm(Tdmcommonpays, dmcommonpays);
  PrepareForm(TdmUtils, dmUtils);
  PrepareForm(TdmAccurals, dmAccurals);
  PrepareForm(TdmFOT, dmFOT);
  PrepareForm(TdmReports, dmReports);
  PrepareForm(TdmReportColumns, dmReportColumns);
  PrepareForm(TdmReportParams, dmReportParams);
  PrepareForm(TdmFormCollection, dmFormCollection);

  PrepareForm(TFormPayments, FormPayments);
  PrepareForm(TFormPayLinks, FormPayLinks);
  PrepareForm(TFormEditPayLinks, FormEditPayLinks);
  PrepareForm(TFormEditPayment2Payment, FormEditPayment2Payment);
  PrepareForm(TFormEditFightingDays, FormEditFightingDays);
  PrepareForm(TFormCalcPeriods, FormCalcPeriods);
  PrepareForm(TFormEditCalcPeriod, FormEditCalcPeriod);
  PrepareForm(TFormForestries, FormForestries);
  PrepareForm(TFormJumpKinds, FormJumpKinds);
  PrepareForm(TFormPlanes, FormPlanes);
  PrepareForm(TFormFlightTarifs, FormFlightTarifs);
  PrepareForm(TFormCalculation, FormCalculation);
  PrepareForm(TFormExport, FormExport);
  PrepareForm(TFormBaseFireLinked, FormBaseFireLinked);
  PrepareForm(TFormEditForestry, FormEditForestry);
  PrepareForm(TFormEditPlane, FormEditPlane);
  PrepareForm(TFormEditFlightTarif, FormEditFlightTarif);
  PrepareForm(TFormEditJumpKind, FormEditJumpKind);
  PrepareForm(TFormEditFightingDetail, FormEditFightingDetail);
  PrepareForm(TFormDepartments, FormDepartments);
  PrepareForm(TFormWorkers, FormWorkers);
  PrepareForm(TFormFightings, FormFightings);
  PrepareForm(TFormEditFighting, FormEditFighting);
  PrepareForm(TFormJumps, FormJumps);
  PrepareForm(TFormFlights, FormFlights);
  PrepareForm(TFormOtherPays, FormOtherPays);
  PrepareForm(TFormCommonPays, FormCommonPays);
  PrepareForm(TFormEditJump, FormEditJump);
  PrepareForm(TFormEditFlight, FormEditFlight);
  PrepareForm(TFormEditCommonPay, FormEditCommonPay);
  PrepareForm(TFormEditOtherPay, FormEditOtherPay);
  PrepareForm(TFormEditFireCase, FormEditFireCase);
  PrepareForm(TFormFireCases, FormFireCases);
  PrepareForm(TFormImport, FormImport);
  PrepareForm(TFormReports, FormReports);
  PrepareForm(TFormEditReport, FormEditReport);
  PrepareForm(TFormEditReportColumn, FormEditReportColumn);
  PrepareForm(TFormEditReportParam, FormEditReportParam);
  PrepareForm(TFormUserReports, FormUserReports);
  PrepareForm(TFormEditReportParams, FormEditReportParams);
  PrepareForm(TFormEditSQL, FormEditSQL);
  PrepareForm(TFormSelectTemplate, FormSelectTemplate);

  PrepareForm(TFormPayGroups, FormPayGroups);
  PrepareForm(TFormReportsGroup, FormReportsGroup);
  PrepareForm(TFormServiceGroup, FormServiceGroup);
  PrepareForm(TFormEditPayGroup, FormEditPayGroup);
  PrepareForm(TFormExternalGroup, FormExternalGroup);
  PrepareForm(TFormFormsGroup, FormFormsGroup);
  PrepareForm(TFormPeriodGroup, FormPeriodGroup);
  PrepareForm(TFormCalcGroup, FormCalcGroup);
  PrepareForm(TFormInputGroup, FormInputGroup);
  PrepareForm(TFormMasterDataGroup, FormMasterDataGroup);

  FormSplash.Hide;
  FormMain.Init;
  Application.Run;
end.
