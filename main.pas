unit main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Datasnap.DBClient, Data.FMTBcd,
  Data.DBXDataSnap, Data.DBXCommon, Data.SqlExpr, Vcl.Tabs,
  Vcl.Mask, DBCtrlsEh, Vcl.StdCtrls, System.ImageList, Vcl.ImgList,
  PngImageList, FMAVertPages, Vcl.ExtCtrls, FormsGroup, Vcl.Buttons, JSON,
  PngBitBtn, Vcl.ComCtrls, System.Actions, Vcl.ActnList, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FMACrudDisp, dmucalcperiods, dmu, DBGridEhGrouping,
  ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh,
  DBGridEh, IceTabSet, Vcl.ButtonGroup;

type
  TFormMain = class(TForm)
    IL: TPngImageList;
    plTop: TPanel;
    sbBottom: TStatusBar;
    vpWorkspace: TFMAVertPages;
    AL: TActionList;
    ActionConnection: TAction;
    plCalcPeriod: TPanel;
    Label5: TLabel;
    dtCalcPeriod: TDBDateTimeEditEh;
    plPeriod: TPanel;
    Label3: TLabel;
    dtBegin: TDBDateTimeEditEh;
    dtEnd: TDBDateTimeEditEh;
    SectionTabs: TIceTabSet;
    Label1: TLabel;
    procedure btSetupClick(Sender: TObject);
    procedure dtCalcPeriodChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SectionTabsTabSelected(Sender: TObject; ATab: TIceTab; ASelected: Boolean);
  private
    FInited: boolean;
    FUpdate: boolean;
    procedure SetControlPanelColor(IsPeriodClosed: boolean);
    procedure PlaceAndInitGroup(formGroup: TFormFormsGroup);
    procedure PlaceForm(form: TForm);
    procedure ChangeAppSection(num: integer);
  public
    procedure Init;
    class procedure GlobalExceptionHandler(Sender: TObject; E: Exception);
  end;

var
  FormMain: TFormMain;

implementation

{$R *.dfm}

uses FireCases, InputGroup, CalculationGroup, ReportsGroup, MasterDataGroup, UserReports,
ServiceGroup, dmuBaseDataSnap, ExternalGroup, dmucurrentperiod, Splash;

procedure TFormMain.btSetupClick(Sender: TObject);
begin
  MessageDlg('������ ���������� � �������� ���������� ', mtError, [mbOK], 0);
end;

procedure TFormMain.dtCalcPeriodChange(Sender: TObject);
var dtCalcPeriodDate, dtBeginDate, dtEndDate: TDateTime;
begin
  if not Assigned(dmcurrentperiod) then exit;
  if FUpdate then exit;
  dmcurrentperiod.ChangePeriod(dtCalcPeriod.Value, true);
  ActiveControl := nil;
  dmcurrentperiod.SetCalcPeriod(dtCalcPeriodDate, dtBeginDate, dtEndDate);
  dtCalcPeriod.Value := dtCalcPeriodDate;
  dtBegin.Value := dtBeginDate;
  dtEnd.Value := dtEndDate;
  SetControlPanelColor(dmcurrentperiod.IsPeriodClosed);
  dmcurrentperiod.ReinitializeForms;
end;

procedure TFormMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  vpWorkspace.RemoveForms();
  dm.DataSnapConnection.Close;
end;

procedure TFormMain.FormCreate(Sender: TObject);
var myYear, myMonth, myDay: Word;
begin
 FInited := false;
 DecodeDate(now(), myYear, myMonth, myDay);
end;

class procedure TFormMain.GlobalExceptionHandler(Sender: TObject; E: Exception);
begin
  ShowMessage(E.Message);
  Application.Terminate;
end;

procedure TFormMain.PlaceAndInitGroup(formGroup: TFormFormsGroup);
begin
  vpWorkspace.AddForm(formGroup);
end;

procedure TFormMain.PlaceForm(form: TForm);
begin
  vpWorkspace.AddForm(form);
end;

procedure TFormMain.SetControlPanelColor(IsPeriodClosed: boolean);
begin
  if IsPeriodClosed then
  begin
    plTop.Color := $00A59DFF;
  end else
  begin
    plTop.Color := $00F7E5D5; //$00EEE6DD;
  end;
end;

procedure TFormMain.ChangeAppSection(num: integer);
begin
  case num of
    0: PlaceAndInitGroup(FormInputGroup);
    1: PlaceAndInitGroup(FormCalcGroup);
    2: PlaceForm(FormUserReports);
    3: PlaceAndInitGroup(FormMasterDataGroup);
    4: PlaceAndInitGroup(FormExternalGroup);
    5: PlaceAndInitGroup(FormServiceGroup);
  end;
end;

procedure TFormMain.SectionTabsTabSelected(Sender: TObject; ATab: TIceTab; ASelected: Boolean);
begin
  ChangeAppSection(SectionTabs.TabIndex);
end;

procedure TFormMain.Init;
var dtCalcPeriodDate, dtBeginDate, dtEndDate: TDateTime;
begin
  if FInited then exit;
  ChangeAppSection(0);
  FUpdate := true;
  dmcurrentperiod.SetDefaultPeriod;
  dmcurrentperiod.SetCalcPeriod(dtCalcPeriodDate, dtBeginDate, dtEndDate);
  dtCalcPeriod.Value := dtCalcPeriodDate;
  dtBegin.Value := dtBeginDate;
  dtEnd.Value := dtEndDate;
  SetControlPanelColor(dmcurrentperiod.IsPeriodClosed);
  FUpdate := false;
  FInited := true;
end;


end.
