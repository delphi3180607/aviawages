unit BaseVertGrid;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, EhLibVCL, GridsEh,
  DBAxisGridsEh, DBVertGridsEh, MemTableDataEh, MemTableEh, Vcl.ExtCtrls, 
  FMACrudDisp, Data.DB, System.Actions, Vcl.ActnList, Vcl.ComCtrls, Vcl.ToolWin,
  System.ImageList, Vcl.ImgList, PngImageList;

type
  TFrameBaseVertGrid = class(TFrame)
    VertGrid: TDBVertGridEh;
    IL: TPngImageList;
    tbTools: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton2: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    ToolButton12: TToolButton;
    AL: TActionList;
    acAdd: TAction;
    acDelete: TAction;
    acEdit: TAction;
    acFilter: TAction;
    acClearFilter: TAction;
    acReports: TAction;
    acExcel: TAction;
    acRefresh: TAction;
    MemData: TMemTableEh;
    DataSource: TDataSource;
    CRUDDisp: TFMACRUDDisp;
    plTitleDetails: TPanel;
    procedure acAddExecute(Sender: TObject);
    procedure acDeleteExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acRefreshExecute(Sender: TObject);
  private
    { Private declarations }
  protected
    errormessage: string;
  public
    procedure Init; virtual;
  end;

implementation

{$R *.dfm}

procedure TFrameBaseVertGrid.acAddExecute(Sender: TObject);
begin
 CRUDDisp.Insert;
end;

procedure TFrameBaseVertGrid.acDeleteExecute(Sender: TObject);
begin
 CRUDDisp.Delete;
end;

procedure TFrameBaseVertGrid.acEditExecute(Sender: TObject);
begin
 CRUDDisp.Edit;
end;

procedure TFrameBaseVertGrid.acRefreshExecute(Sender: TObject);
begin
  CRUDDisp.CrudAgent.ParamTransform.ResetContext;
  if not CRUDDisp.GetList(nil,errormessage) then
  begin
    MessageDlg(errormessage, mtError, [mbOK], 0);
  end;
end;

procedure TFrameBaseVertGrid.Init;
begin
  acRefresh.Execute;
end;

end.
