object FrameBaseTree: TFrameBaseTree
  Left = 0
  Top = 0
  Width = 640
  Height = 480
  TabOrder = 0
  object plTitleDetails: TPanel
    Left = 0
    Top = 0
    Width = 640
    Height = 20
    Align = alTop
    BevelOuter = bvNone
    Color = 15848399
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    StyleElements = [seFont, seBorder]
  end
  object tvData: TVirtualStringTree
    AlignWithMargins = True
    Left = 3
    Top = 23
    Width = 634
    Height = 454
    Align = alClient
    DefaultNodeHeight = 19
    Header.AutoSizeIndex = 0
    Header.Height = 15
    Header.MainColumn = -1
    Images = IL
    PopupMenu = pmTree
    TabOrder = 1
    TreeOptions.MiscOptions = [toAcceptOLEDrop, toCheckSupport, toFullRepaintOnResize, toInitOnSave, toToggleOnDblClick, toWheelPanning, toEditOnClick]
    TreeOptions.SelectionOptions = [toMultiSelect, toSelectNextNodeOnRemoval]
    OnAddToSelection = tvDataAddToSelection
    OnGetText = tvDataGetText
    OnGetImageIndex = tvDataGetImageIndex
    Touch.InteractiveGestures = [igPan, igPressAndTap]
    Touch.InteractiveGestureOptions = [igoPanSingleFingerHorizontal, igoPanSingleFingerVertical, igoPanInertia, igoPanGutter, igoParentPassthrough]
    Columns = <>
  end
  object MemData: TMemTableEh
    Params = <>
    Left = 56
    Top = 96
  end
  object CRUDDisp: TFMACRUDDisp
    Fields = <>
    MemData = MemData
    ReadOnly = False
    Left = 120
    Top = 96
  end
  object IL: TPngImageList
    PngImages = <>
    Left = 224
    Top = 104
  end
  object pmTree: TPopupMenu
    Left = 328
    Top = 104
    object N1: TMenuItem
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      OnClick = N1Click
    end
  end
end
