unit InitStated;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseGrid, Vcl.ExtCtrls, Vcl.StdCtrls, DB,
  MemTableDataEh, MemTableEh, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client, dmucurrentperiod;

type
  TFormInitStated = class(TForm)
    pl1: TPanel;
    btOk: TButton;
    btCancel: TButton;
    MainDataSet: TMemTableEh;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    IsInited: boolean;
    errormessage: string;
  public
    StandAlone: boolean;
    context_data_module: Tdmcurrentperiod;
    constructor Create(AOwner: TComponent); override;
    procedure Init; virtual;
    procedure Reset;
  end;

var
  FormInitStated: TFormInitStated;

implementation

{$R *.dfm}

{ TFormInitStated }

constructor TFormInitStated.Create(AOwner: TComponent);
begin
  inherited;
  IsInited := false;
  StandAlone := false;
end;

procedure TFormInitStated.FormShow(Sender: TObject);
begin
  if (fsModal in FormState) then
  begin
       pl1.Show;
  end else
  begin
       pl1.Hide;
  end;
end;

procedure TFormInitStated.Init;
var i: integer;
begin
   if IsInited then exit;
   Screen.Cursor := crHourGlass;
   if not StandAlone then context_data_module := dmcurrentperiod;
   for i := 0 to self.ComponentCount-1 do
   begin
      if self.Components[i] is TFrameBaseGrid then
      begin
        try
          if not IsInited then TFrameBaseGrid(self.Components[i]).Init;
          IsInited := true;
          exit;
        except
        on E : Exception do
          begin
           Screen.Cursor := crDefault;
           ShowMessage('������ ������������� ������ '+TFrameBaseGrid(self.Components[i]).Name);
           exit;
          end;
        end;
      end;
   end;
   Screen.Cursor := crDefault;
end;

procedure TFormInitStated.Reset;
begin
  IsInited := false;
  Init;
end;

end.
