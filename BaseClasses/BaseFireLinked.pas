unit BaseFireLinked;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, InitStated, BaseSimpleGrid, Vcl.ExtCtrls,
  BaseGrid, EditFireCase, Vcl.StdCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  MemTableDataEh, MemTableEh, System.Actions, Vcl.ActnList, GridsEh, DBGridEh;

type
  TFormBaseFireLinked = class(TFormInitStated)
    splFireEng: TSplitter;
    FrameFireLinked: TFrameBaseGrid;
    cbAll: TCheckBox;
    MainActionList: TActionList;
    acStorno1: TAction;
    procedure FrameFireLinkedCRUDDispBeforeGetList(Sender: TObject);
    procedure acStorno1Execute(Sender: TObject);
    procedure FrameFireLinkedCRUDDispCheckState(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FrameFireLinkeddgGridDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumnEh;
      State: TGridDrawState);
  private
    { Private declarations }
  protected
    StornoFireCaseId: integer;
    procedure InsertStronoDocument; virtual;
  public
    {}
  end;

var
  FormBaseFireLinked: TFormBaseFireLinked;

implementation

uses dmu, dmucurrentperiod, dmuformcollection, SelectStorno, StronoContainer, firecases;

{$R *.dfm}

procedure TFormBaseFireLinked.acStorno1Execute(Sender: TObject);
begin
  {
  FormSelectStorno.ShowModal;
  if FormSelectStorno.ModalResult = mrOk then
  begin
    if Owner is TFormInitStated then
    begin
      with FormStornoContainer do
      begin
        InitGrid(TFormInitStated(self.Owner));
        Position := poMainFormCenter;
        Width := Screen.Width*6 div 7;
        Height := Screen.Height*6 div 7;
        Operation := FormSelectStorno.Operation;
        ShowModal;
      end;
      if FormStornoContainer.ModalResult = mrOk then
      begin
       //
      end;
    end;
  end;
  }
  try
    with FormStornoContainer do
    begin
      InitGrid(TFormInitStated(FormFireCases));
      Position := poMainFormCenter;
      Width := Screen.Width*6 div 7;
      Height := Screen.Height*6 div 7;
      Operation := 1;
      ShowModal;
    end;
    if FormStornoContainer.ModalResult = mrOk then
    begin
       StornoFireCaseId := TFormFireCases(FormStornoContainer.ImbeddedForm).FrameGrid.MemData.FieldByName('id').AsInteger;
       InsertStronoDocument;
    end;
  finally
    StornoFireCaseId := 0;
  end;

end;

procedure TFormBaseFireLinked.FormCreate(Sender: TObject);
begin
  inherited;
  StornoFireCaseId := 0;
end;

procedure TFormBaseFireLinked.FrameFireLinkedCRUDDispBeforeGetList(Sender: TObject);
begin
  with FrameFireLinked.CRUDDisp.CRUDAgent do
  begin
    ParamTransform.ResetContext;
    ParamTransform.AddParameterContext('date_begin', ftDate, context_data_module.FilterStartDate);
    ParamTransform.AddParameterContext('date_end', ftDate, context_data_module.FilterEndDate);
    ParamTransform.AddParameterContext('calc_period_id', ftInteger, context_data_module.CalcPeriodId);
  end;
end;

procedure TFormBaseFireLinked.FrameFireLinkedCRUDDispCheckState(Sender: TObject);
var container: TFormInitStated;
begin
  inherited;

  FrameFireLinked.CRUDDispCheckState(Sender);

  container := TFormInitStated(Owner);
  if container.StandAlone then
  begin
      acStorno1.Enabled := not container.StandAlone;
  end else
  begin
    acStorno1.Enabled := not dmcurrentperiod.IsPeriodClosed;
  end;

end;

procedure TFormBaseFireLinked.FrameFireLinkeddgGridDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
    if (not (gdSelected in State)) or (not FrameFireLinked.dgGrid.Focused)
    then
    begin
      if FrameFireLinked.MemData.FieldByName('storno_mark').AsInteger>0 then
        FrameFireLinked.dgGrid.Canvas.Brush.Color := $00D6D1FC;
       FrameFireLinked.dgGrid.DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end;
end;

procedure TFormBaseFireLinked.InsertStronoDocument;
begin
 //--
end;

end.
