object dmBaseDataSnap: TdmBaseDataSnap
  Height = 258
  Width = 401
  object CrudAgent: TFMACrudAgent
    InsertExecutor = CrudAgentInsertExecutor
    EditExecutor = CrudAgentEditExecutor
    DeleteExecutor = CrudAgentDeleteExecutor
    ServerMethodGetList = smGetList
    Active = False
    Left = 48
    Top = 16
  end
  object smGetList: TSqlServerMethod
    Params = <>
    SQLConnection = DM.DataSnapConnection
    Left = 48
    Top = 96
  end
  object smInsert: TSqlServerMethod
    Params = <>
    SQLConnection = DM.DataSnapConnection
    Left = 120
    Top = 96
  end
  object smDelete: TSqlServerMethod
    Params = <>
    SQLConnection = DM.DataSnapConnection
    Left = 184
    Top = 96
  end
  object smUpdate: TSqlServerMethod
    Params = <>
    SQLConnection = DM.DataSnapConnection
    Left = 248
    Top = 96
  end
end
