unit BaseGrid;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  System.ImageList, Vcl.ImgList, PngImageList, System.Actions,
  Vcl.ActnList, system.JSON, Data.FMTBcd, Data.SqlExpr, ComObj,
  Data.DB, MemTableEh, EhLibMTE, Vcl.ComCtrls,
  Vcl.ToolWin, DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh,
  FMACrudDisp, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh, Vcl.StdCtrls,
  Vcl.Menus, FMAParamTransform, FMAFunctions, Vcl.ExtCtrls, dmucalcperiods,
  functions, MemTableDataEh;

type
  TFrameBaseGrid = class(TFrame)
    CRUDDisp: TFMACRUDDisp;
    IL: TPngImageList;
    AL: TActionList;
    acAdd: TAction;
    acDelete: TAction;
    acEdit: TAction;
    acFilter: TAction;
    acClearFilter: TAction;
    acReports: TAction;
    acRefresh: TAction;
    acExcel: TAction;
    DataSource: TDataSource;
    dgGrid: TDBGridEh;
    MemData: TMemTableEh;
    tbTools: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    ToolButton11: TToolButton;
    acMulty: TAction;
    pmGridMenu: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    ToolButton10: TToolButton;
    ToolButton12: TToolButton;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    Excel1: TMenuItem;
    plTitleDetails: TPanel;
    ToolButton13: TToolButton;
    procedure acAddExecute(Sender: TObject);
    procedure acDeleteExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acRefreshExecute(Sender: TObject);
    procedure dgGridDblClick(Sender: TObject);
    procedure CRUDDispCheckState(Sender: TObject);
    procedure acExcelExecute(Sender: TObject);
  private
    { Private declarations }
  protected
    errormessage: string;
    procedure ExecuteStornoOperation(docid: integer); virtual;
  public
    procedure Init; virtual;
    procedure DoMutipleAdd(multiple_field_name, source_field_name: string; source_crud_disp: TFMACRUDDisp);
  end;


implementation

uses dmu, dmucurrentperiod, dmuformcollection, InitStated;

{$R *.dfm}

{ TFrameBaseGrid }

procedure TFrameBaseGrid.acAddExecute(Sender: TObject);
begin
  CRUDDisp.Insert;
end;

procedure TFrameBaseGrid.acDeleteExecute(Sender: TObject);
var i: integer;
begin

  if dgGrid.Selection.SelectionType = gstNon then
  begin
    CRUDDisp.Delete;
  end;

  if dgGrid.Selection.SelectionType = gstRecordBookmarks then
  begin
    if Application.MessageBox(PWideChar('������� ���������� ������ ?'), '��������', MB_YESNO) = IDYes then
    begin
      for i := 0 to dgGrid.Selection.Rows.Count-1 do
      begin
        MemData.Bookmark := dgGrid.Selection.Rows[i];
        CRUDDisp.InnerDelete;
      end;
      CRUDDisp.GetList(nil, errormessage);
    end;
  end;

  if dgGrid.Selection.SelectionType = gstAll then
  begin
    if Application.MessageBox(PWideChar('������� ��� ������ ?'), '��������', MB_YESNO) = IDYes then
    begin
      MemData.First;
      while not MemData.Eof do
      begin
        CRUDDisp.InnerDelete;
        MemData.Next;
      end;
    end;
    CRUDDisp.GetList(nil, errormessage);
  end;

end;

procedure TFrameBaseGrid.acEditExecute(Sender: TObject);
begin
  if MemData.RecordCount>0 then
  CRUDDisp.Edit;
end;

procedure TFrameBaseGrid.acExcelExecute(Sender: TObject);
begin
  ExportExcel(dgGrid, plTitleDetails.Caption);
end;

procedure TFrameBaseGrid.acRefreshExecute(Sender: TObject);
begin
  if not Assigned(CRUDDisp.CrudAgent) then exit;

  CRUDDisp.CrudAgent.ParamTransform.ResetContext;
  if not CRUDDisp.GetList(nil,errormessage) then
  begin
    MessageDlg(errormessage, mtError, [mbOK], 0);
    exit;
  end;

end;

procedure TFrameBaseGrid.CRUDDispCheckState(Sender: TObject);
var container: TFormInitStated;
begin
  if Owner is TFormInitStated then container := TFormInitStated(Owner);
  if container.StandAlone then
  begin
      CRUDDisp.ReadOnly := container.StandAlone;
      acAdd.Enabled := not container.StandAlone;
      acDelete.Enabled := not container.StandAlone;
      acEdit.Enabled := not container.StandAlone;
      acMulty.Enabled := not container.StandAlone;
      tbTools.Repaint;
  end else
  begin
    CRUDDisp.ReadOnly := dmcurrentperiod.IsPeriodClosed;
    acAdd.Enabled := not dmcurrentperiod.IsPeriodClosed;
    acDelete.Enabled := not dmcurrentperiod.IsPeriodClosed;
    acEdit.Enabled := not dmcurrentperiod.IsPeriodClosed;
    acMulty.Enabled := not dmcurrentperiod.IsPeriodClosed;
  end;
end;

procedure TFrameBaseGrid.dgGridDblClick(Sender: TObject);
begin
  acEdit.Execute;
end;

procedure TFrameBaseGrid.DoMutipleAdd(multiple_field_name, source_field_name: string; source_crud_disp: TFMACRUDDisp);
var form: TForm; formnew: TFormInitStated;
begin

  form := GetOwnerForm(source_crud_disp);
  if not dmformcollection.SelectFormCollection.TryGetValue(form.ClassName, formnew) then
  begin
    Application.CreateForm(TComponentClass(form.ClassType), formnew);
    dmformcollection.SelectFormCollection.Add(form.ClassName, formnew);
  end;

  formnew.Init;
  formnew.Parent := nil;
  formnew.Align := alNone;
  formnew.Position := poMainFormCenter;
  formnew.Width := Screen.Width*2 div 3;
  formnew.Height := Screen.Height*2 div 3;
  formnew.ShowModal;
  if formnew.ModalResult = mrOk then
  begin
    CRUDDisp.MultipleInsert(multiple_field_name, source_field_name, formnew.MainDataSet);
  end;
end;

procedure TFrameBaseGrid.ExecuteStornoOperation(docid: integer);
begin
end;

procedure TFrameBaseGrid.Init;
begin
  acRefresh.Execute;
end;


end.
