unit BaseTree;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, MemTableDataEh, MemTableEh, 
  Vcl.ComCtrls, FMACrudDisp, Data.DB, Vcl.ExtCtrls, System.ImageList, Vcl.ImgList, 
  PngImageList, Vcl.Menus, VirtualTrees.BaseAncestorVCL, VirtualTrees.BaseTree,
  VirtualTrees.AncestorVCL, VirtualTrees, VirtualTrees.Types;

type
  TFrameBaseTree = class(TFrame)
    MemData: TMemTableEh;
    CRUDDisp: TFMACRUDDisp;
    plTitleDetails: TPanel;
    IL: TPngImageList;
    pmTree: TPopupMenu;
    N1: TMenuItem;
    tvData: TVirtualStringTree;
    procedure N1Click(Sender: TObject);
    procedure tvDataGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
    procedure tvDataGetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Kind: TVTImageKind; Column: TColumnIndex; var Ghosted: Boolean;
      var ImageIndex: TImageIndex);
    procedure tvDataAddToSelection(Sender: TBaseVirtualTree;
      Node: PVirtualNode);
  private
    FIsUpdating: boolean;
    procedure RollUp(parent_id: Variant; parent_sibling: PVirtualNode);
  protected
    errormessage: string;
  public
    key_field_name: string;
    parent_field_name: string;
    display_field_name: string;
    start_id: Variant;
    procedure Init; virtual;
    property IsUpdating: boolean read FIsUpdating;
  end;

  TTreeData = class
  public
    DisplayValue: string;
    IsHighlighted: boolean;
    KeyValue: Variant;
  end;

implementation

{$R *.dfm}

{ TFrameBaseTree }

procedure TFrameBaseTree.Init;
var oldAfterScroll: TDataSetNotifyEvent;
begin
  if not Assigned(CRUDDisp.CrudAgent) then exit;
  CRUDDisp.CrudAgent.ParamTransform.ResetContext;
  CRUDDisp.CrudAgent.ParamTransform.AddPatternContext('/*wherefilter*/', '(parent_id = :parent_id) or (parent_id is null and :parent_id is null)');
  FIsUpdating := true;
  tvData.TreeOptions.MiscOptions := tvData.TreeOptions.MiscOptions - [toReadOnly];
  tvData.Clear;
  oldAfterScroll := MemData.AfterScroll;
  MemData.AfterScroll := nil;
  RollUp(start_id, nil);
  CRUDDisp.CrudAgent.ParamTransform.ResetContext;
  if not CRUDDisp.GetList(nil,errormessage) then
  begin
    MessageDlg(errormessage, mtError, [mbOK], 0);
    exit;
  end;
  FIsUpdating := false;
  tvData.TreeOptions.MiscOptions := tvData.TreeOptions.MiscOptions + [toReadOnly];
  MemData.AfterScroll := oldAfterScroll;
end;

procedure TFrameBaseTree.N1Click(Sender: TObject);
begin
  tvData.Refresh;
end;

procedure TFrameBaseTree.RollUp(parent_id: Variant; parent_sibling: PVirtualNode);
var current_node: PVirtualNode; key_field_index, display_field_index: integer;
image_index: integer; localData: TMemTableEh; data: TTreeData;
begin
  CRUDDisp.CrudAgent.ParamTransform.AddParameterContext('parent_id', ftGuid, parent_id);
  localData := TMemTableEh.Create(self);
  if not CRUDDisp.GetList(localData, errormessage) then
  begin
    MessageDlg(errormessage, mtError, [mbOK], 0);
    exit;
  end;
  localData.First;
  key_field_index := localData.FieldByName(key_field_name).Index;
  display_field_index := localData.FieldByName(display_field_name).Index;

  if parent_sibling=nil then
    image_index := 0
  else
    image_index := 1;

  while not localData.Eof do
  begin

    data := TTreeData.Create;
    data.KeyValue := localData.Fields[key_field_index].Value;
    data.DisplayValue := localData.Fields[display_field_index].AsString;
    data.IsHighlighted := (pos('(+)', localData.Fields[display_field_index].AsString)>0);

    current_node := tvData.AddChild(parent_sibling, data);
    if data.IsHighlighted then
    begin
      current_node.CheckType := ctCheckBox;
      current_node.CheckState := csMixedNormal;
      if Assigned(current_node.Parent) then
      begin
        current_node.Parent.CheckType := ctCheckBox;
        current_node.Parent.CheckState := csMixedNormal;
      end;
    end;
    RollUp(localData.Fields[key_field_index].Value, current_node);
    localData.Next;
  end;
  localData.Close;
  localData.Free;
end;

procedure TFrameBaseTree.tvDataAddToSelection(Sender: TBaseVirtualTree; Node: PVirtualNode);
var keyValue: Variant;
begin
  if FIsUpdating then exit;
  keyValue := Node.GetData<TTreeData>.KeyValue;
  MemData.Locate(key_field_name, KeyValue, []);
end;

procedure TFrameBaseTree.tvDataGetImageIndex(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex;
  var Ghosted: Boolean; var ImageIndex: TImageIndex);
begin
  if Kind in [ikNormal, ikSelected] then
  if Node.CheckState <> csMixedNormal then
    ImageIndex := 0
  else
    ImageIndex := 1;
end;

procedure TFrameBaseTree.tvDataGetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: string);
begin
  CellText := Node.GetData<TTreeData>.DisplayValue;
end;

end.
