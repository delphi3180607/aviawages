unit BaseSimpleGrid;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, MemTableDataEh, MemTableEh,
  DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, EhLibVCL, GridsEh,
  DBAxisGridsEh, DBGridEh, EhLibMTE, FMACrudDisp, Data.DB, Vcl.ExtCtrls,
  Vcl.Menus, System.Actions, Vcl.ActnList, functions;

type
  TFrameBaseSimpleGrid = class(TFrame)
    MemData: TMemTableEh;
    DataSource: TDataSource;
    CRUDDisp: TFMACRUDDisp;
    dgGrid: TDBGridEh;
    plTitleDetails: TPanel;
    AL: TActionList;
    acExcel: TAction;
    acRefresh: TAction;
    pmGridMenu: TPopupMenu;
    N5: TMenuItem;
    Excel1: TMenuItem;
    procedure acRefreshExecute(Sender: TObject);
    procedure acExcelExecute(Sender: TObject);
  private
    { Private declarations }
  protected
    errormessage: string;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TFrameBaseSimpleGrid.acExcelExecute(Sender: TObject);
begin
  ExportExcel(self.dgGrid, plTitleDetails.Caption);
end;

procedure TFrameBaseSimpleGrid.acRefreshExecute(Sender: TObject);
begin
  if not CRUDDisp.GetList(nil,errormessage) then
  begin
    MessageDlg(errormessage, mtError, [mbOK], 0);
  end;
end;

end.
