unit dmuBaseDataSnap;

interface

uses
  System.SysUtils, System.Classes, DB, JSON, Generics.Collections, Data.FMTBcd,
  Data.SqlExpr, FMACrudAgent, Variants, FMAFunctions, Dialogs, FMAParamTransform,
  System.Generics.Collections;

type

  TdmBaseDataSnap = class(TDataModule)
    CrudAgent: TFMACrudAgent;
    smGetList: TSqlServerMethod;
    smInsert: TSqlServerMethod;
    smDelete: TSqlServerMethod;
    smUpdate: TSqlServerMethod;
    procedure CrudAgentDeleteExecutor(var DataSet: TDataSet; out result_state: Boolean; out ErrorMessage: string);
    procedure CrudAgentEditExecutor(var DataSet: TDataSet; out result_state: Boolean; out ErrorMessage: string);
    procedure CrudAgentInsertExecutor(var DataSet: TDataSet; out result_state: Boolean; out ErrorMessage: string);
  private
  public
  end;

var
  dmBaseDataSnap: TdmBaseDataSnap;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TdmBaseDataSnap }


procedure TdmBaseDataSnap.CrudAgentDeleteExecutor(var DataSet: TDataSet; out result_state: Boolean; out ErrorMessage: string);
var o: TJSONObject; errmessage: string;
begin
  o := CrudAgent.ParamTransform.DataSetToJSONParams(DataSet);
  smDelete.ParamByName('data').SetObjectValue(o,ftObject, false);
  smDelete.ExecuteMethod;
  result_state := smDelete.ParamByName('ReturnParameter').AsBoolean;
  if not result_state then
  begin
    DataSet.Cancel;
    ErrorMessage := smDelete.ParamByName('errmessage').AsString;
  end;
end;

procedure TdmBaseDataSnap.CrudAgentEditExecutor(var DataSet: TDataSet; out result_state: Boolean; out ErrorMessage: string);
var o: TJSONObject; i: integer;
begin
  smUpdate.Close;
  o := CrudAgent.ParamTransform.DataSetToJSONParams(DataSet);
  smUpdate.ParamByName('data').SetObjectValue(o,ftObject, false);
  smUpdate.Open;
  result_state := smUpdate.ParamByName('result_state').AsBoolean;
  if result_state then
  begin
    for i := 0 to DataSet.FieldCount-1 do
    begin
      DataSet.Fields[i].Value := smUpdate.Fields[i].Value;
    end;
  end else
  begin
    DataSet.Cancel;
    ErrorMessage := smUpdate.ParamByName('errmessage').AsString;
  end;
end;

procedure TdmBaseDataSnap.CrudAgentInsertExecutor(var DataSet: TDataSet; out result_state: Boolean; out ErrorMessage: string);
var o: TJSONObject; field_value: Variant; field_type: TFieldType; i: integer;
begin
  smInsert.Close;
  o := CrudAgent.ParamTransform.DataSetToJSONParams(DataSet);
  smInsert.ParamByName('data').SetObjectValue(o,ftObject,false);
  smInsert.Open;
  result_state := smInsert.ParamByName('result_state').AsBoolean;
  if result_state then
  begin
    for i := 0 to DataSet.FieldCount-1 do
    begin
      DataSet.Fields[i].Value := smInsert.Fields[i].Value;
    end;
  end else
  begin
    DataSet.Cancel;
    ErrorMessage := smInsert.ParamByName('errmessage').AsString;
  end;
end;

end.
