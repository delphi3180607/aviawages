inherited FormBaseFireLinked: TFormBaseFireLinked
  Caption = 'FormBaseFireLinked'
  ClientWidth = 622
  OnCreate = FormCreate
  ExplicitWidth = 638
  TextHeight = 15
  object splFireEng: TSplitter [0]
    Left = 377
    Top = 28
    Height = 378
    ExplicitLeft = 216
    ExplicitTop = 120
    ExplicitHeight = 100
  end
  inline FrameFireLinked: TFrameBaseGrid [1]
    Left = 0
    Top = 28
    Width = 377
    Height = 378
    Align = alLeft
    TabOrder = 0
    ExplicitTop = 28
    ExplicitWidth = 377
    ExplicitHeight = 378
    inherited dgGrid: TDBGridEh
      Width = 371
      Height = 329
      DataGrouping.Active = True
      DataGrouping.DefaultStateExpanded = True
      DataGrouping.GroupLevels = <
        item
          ColumnName = 'Column_1_department_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end>
      FrozenCols = 5
      ParentShowHint = False
      ShowHint = True
      OnDrawColumnCell = FrameFireLinkeddgGridDrawColumnCell
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'name'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088' '#1087#1086#1078#1072#1088#1072
          Width = 67
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'department_name'
          Footers = <>
          Title.Caption = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
          Visible = False
          Width = 191
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'forestry_name'
          Footers = <>
          Title.Caption = #1051#1077#1089#1085#1080#1095#1077#1089#1090#1074#1086
          Width = 189
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'datefrom'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
          Width = 79
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'dateto'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
          Width = 96
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'square'
          Footers = <>
          Title.Caption = #1050#1074#1072#1088#1090#1072#1083
          Width = 84
        end>
    end
    inherited tbTools: TToolBar
      Width = 371
      ExplicitWidth = 371
    end
    inherited plTitleDetails: TPanel
      Width = 371
      ExplicitWidth = 371
    end
    inherited IL: TPngImageList
      Left = 32
      Bitmap = {}
    end
    inherited AL: TActionList
      Left = 72
    end
    inherited DataSource: TDataSource
      Left = 104
      Top = 176
    end
    inherited CRUDDisp: TFMACRUDDisp
      Fields = <
        item
          Name = 'id'
          Precision = 0
          FieldNo = 1
          Size = 0
          DataType = ftInteger
          Attributes = [faHiddenCol]
        end
        item
          Name = 'name'
          Precision = 0
          FieldNo = 2
          Size = 502
          DataType = ftWideString
          Attributes = []
          LongCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1086#1078#1072#1088#1072
          ShortCaption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1086#1078#1072#1088#1072
        end
        item
          Name = 'forestry_id'
          Precision = 0
          FieldNo = 3
          Size = 0
          DataType = ftInteger
          Attributes = [faHiddenCol]
        end
        item
          Name = 'forestry_name'
          Precision = 0
          FieldNo = 4
          Size = 502
          DataType = ftWideString
          Attributes = []
          LongCaption = #1051#1077#1089#1085#1080#1095#1077#1089#1090#1074#1086
          ShortCaption = #1051#1077#1089#1085#1080#1095#1077#1089#1090#1074#1086
          KeyFieldName = 'forestry_id'
        end
        item
          Name = 'square'
          Precision = 0
          FieldNo = 5
          Size = 0
          DataType = ftSmallint
          Attributes = []
          LongCaption = #1050#1074#1072#1088#1090#1072#1083
          ShortCaption = #1050#1074#1072#1088#1090#1072#1083
        end
        item
          Name = 'department_id'
          Precision = 0
          FieldNo = 6
          Size = 39
          DataType = ftString
          Attributes = [faHiddenCol]
        end
        item
          Name = 'department_name'
          Precision = 0
          FieldNo = 7
          Size = 512
          DataType = ftWideString
          Attributes = []
          LongCaption = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
          ShortCaption = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
          KeyFieldName = 'department_id'
        end
        item
          Name = 'datefrom'
          Precision = 0
          FieldNo = 8
          Size = 0
          DataType = ftDate
          Attributes = []
          LongCaption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
          ShortCaption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
        end
        item
          Name = 'dateto'
          Precision = 0
          FieldNo = 9
          Size = 0
          DataType = ftDate
          Attributes = []
          LongCaption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
          ShortCaption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
        end>
      CrudAgent = dmFireCases.CrudAgentEx
      OnCheckState = FrameFireLinkedCRUDDispCheckState
      EditFormDataSource = FormEditFireCase.dsLocal
      OnBeforeGetList = FrameFireLinkedCRUDDispBeforeGetList
      Left = 176
      Top = 176
    end
    inherited MemData: TMemTableEh
      FieldDefs = <
        item
          Name = 'id'
          DataType = ftInteger
        end
        item
          Name = 'name'
          DataType = ftWideString
          Size = 502
        end
        item
          Name = 'forestry_id'
          DataType = ftInteger
        end
        item
          Name = 'forestry_name'
          DataType = ftWideString
          Size = 502
        end
        item
          Name = 'square'
          DataType = ftSmallint
        end
        item
          Name = 'department_id'
          DataType = ftString
          Size = 39
        end
        item
          Name = 'department_name'
          DataType = ftWideString
          Size = 512
        end
        item
          Name = 'datefrom'
          DataType = ftDate
        end
        item
          Name = 'dateto'
          DataType = ftDate
        end>
      StoreDefs = True
      Left = 32
      Top = 176
    end
    inherited pmGridMenu: TPopupMenu
      Left = 128
    end
  end
  inherited pl1: TPanel [2]
    Width = 622
    TabOrder = 1
    ExplicitWidth = 622
    inherited btOk: TButton
      Left = 419
      ExplicitLeft = 419
    end
    inherited btCancel: TButton
      Left = 522
      ExplicitLeft = 522
    end
  end
  object cbAll: TCheckBox
    AlignWithMargins = True
    Left = 3
    Top = 0
    Width = 616
    Height = 25
    Margins.Top = 0
    Align = alTop
    Caption = #1054#1090#1086#1073#1088#1072#1078#1072#1090#1100' '#1074#1089#1077' '#1091#1095#1072#1089#1090#1080#1103' '#1074' '#1087#1086#1078#1072#1088#1072#1093
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
  object MainActionList: TActionList
    Images = FrameFireLinked.IL
    Left = 264
    Top = 392
    object acStorno1: TAction
      Hint = #1057#1086#1079#1076#1072#1090#1100' '#1089#1090#1086#1088#1080#1088#1091#1102#1097#1080#1081' '#1076#1086#1082#1091#1084#1077#1085#1090' '#1079#1072' '#1087#1088#1086#1096#1083#1099#1081' '#1087#1077#1088#1080#1086#1076
      ImageIndex = 9
      OnExecute = acStorno1Execute
    end
  end
end
