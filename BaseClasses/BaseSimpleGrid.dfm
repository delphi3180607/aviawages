object FrameBaseSimpleGrid: TFrameBaseSimpleGrid
  Left = 0
  Top = 0
  Width = 640
  Height = 480
  TabOrder = 0
  object dgGrid: TDBGridEh
    AlignWithMargins = True
    Left = 3
    Top = 18
    Width = 634
    Height = 459
    Margins.Top = 0
    Align = alClient
    AllowedOperations = []
    Border.Color = clBlack
    Border.EdgeBorders = []
    Border.ExtendedDraw = False
    ColumnDefValues.Title.TitleButton = True
    Ctl3D = False
    DataSource = DataSource
    DrawMemoText = True
    DynProps = <>
    EvenRowColor = 16774378
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    GridLineParams.ColorScheme = glcsClassicEh
    GridLineParams.DataHorzColor = 14337969
    GridLineParams.DataHorzLines = False
    GridLineParams.DataVertColor = 14337969
    GridLineParams.VertEmptySpaceStyle = dessNonEh
    IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
    IndicatorParams.FillStyle = cfstGradientEh
    OddRowColor = clWhite
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
    ParentCtl3D = False
    ParentFont = False
    PopupMenu = pmGridMenu
    SortLocal = True
    STFilter.HorzLineColor = 10329501
    STFilter.InstantApply = True
    STFilter.Local = True
    STFilter.VertLineColor = 10329501
    STFilter.Visible = True
    TabOrder = 0
    TitleParams.FillStyle = cfstGradientEh
    TitleParams.Font.Charset = DEFAULT_CHARSET
    TitleParams.Font.Color = clWindowText
    TitleParams.Font.Height = -13
    TitleParams.Font.Name = 'Segoe UI'
    TitleParams.Font.Style = []
    TitleParams.HorzLineColor = 7554359
    TitleParams.MultiTitle = True
    TitleParams.ParentFont = False
    TitleParams.VertLineColor = 7554359
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object plTitleDetails: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 0
    Width = 634
    Height = 18
    Margins.Top = 0
    Margins.Bottom = 0
    Align = alTop
    BevelOuter = bvLowered
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentColor = True
    ParentFont = False
    TabOrder = 1
  end
  object MemData: TMemTableEh
    FieldDefs = <>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 48
    Top = 104
  end
  object DataSource: TDataSource
    DataSet = MemData
    Left = 123
    Top = 104
  end
  object CRUDDisp: TFMACRUDDisp
    Fields = <>
    Grid = dgGrid
    MemData = MemData
    ReadOnly = False
    Left = 199
    Top = 104
  end
  object AL: TActionList
    Left = 272
    Top = 104
    object acExcel: TAction
      Caption = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1074' Excel'
      Hint = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1074' Excel'
      ImageIndex = 6
      ShortCut = 16469
      OnExecute = acExcelExecute
    end
    object acRefresh: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      Hint = #1054#1073#1085#1086#1074#1080#1090#1100
      ImageIndex = 8
      ShortCut = 116
      OnExecute = acRefreshExecute
    end
  end
  object pmGridMenu: TPopupMenu
    Left = 336
    Top = 104
    object N5: TMenuItem
      Action = acRefresh
    end
    object Excel1: TMenuItem
      Action = acExcel
    end
  end
end
