--
-- PostgreSQL database dump
--

-- Dumped from database version 15.5
-- Dumped by pg_dump version 15.5

-- Started on 2024-06-30 18:36:24

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3 (class 3079 OID 618970)
-- Name: dblink; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS dblink WITH SCHEMA public;


--
-- TOC entry 3854 (class 0 OID 0)
-- Dependencies: 3
-- Name: EXTENSION dblink; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION dblink IS 'connect to other PostgreSQL databases from within a database';


--
-- TOC entry 2 (class 3079 OID 618949)
-- Name: postgres_fdw; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgres_fdw WITH SCHEMA public;


--
-- TOC entry 3855 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION postgres_fdw; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgres_fdw IS 'foreign-data wrapper for remote PostgreSQL servers';


--
-- TOC entry 318 (class 1255 OID 580591)
-- Name: f_bi_calc_periods(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.f_bi_calc_periods() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
	new.period_begin := date_trunc('month', new.period_begin);	
	return new;
END;
$$;


ALTER FUNCTION public.f_bi_calc_periods() OWNER TO postgres;

--
-- TOC entry 379 (class 1255 OID 580592)
-- Name: f_bui_flights(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.f_bui_flights() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare 
nSumma numeric(12,2);
nHours numeric;
begin
	select max(ft.tarif) into nSumma 
	from public.flight_tarifs ft
	where plane_id = new.plane_id
	and paygroup_id = new.pay_group_id;

	nHours := EXTRACT(EPOCH FROM new.hours)/3600;

	if not coalesce(new.isstorno,false) then
		new.summa := coalesce(nSumma*nHours,0);
	else
		new.summa := coalesce(-nSumma*nHours,0);
	end if;

	return new;

END;
$$;


ALTER FUNCTION public.f_bui_flights() OWNER TO postgres;

--
-- TOC entry 387 (class 1255 OID 580593)
-- Name: f_bui_jumps(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.f_bui_jumps() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare 
nTarif numeric(12,2);
nSumma numeric(12,2);
begin
	select max(jk.tarif) into nTarif from jump_kinds jk where jk.id = new.jump_kind_id;
	if not coalesce(new.isstorno,false) then
		new.summa := coalesce(nTarif*new.amount,0);
	else
		new.summa := coalesce(-nTarif*new.amount,0);
	end if;
	return new;
END;
$$;


ALTER FUNCTION public.f_bui_jumps() OWNER TO postgres;

--
-- TOC entry 388 (class 1255 OID 580594)
-- Name: f_bui_other_pays(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.f_bui_other_pays() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare 
nSalary numeric(12,2);
nSumma numeric(12,2);
nHours numeric(8,4);
nHoursNorma numeric(8,4);
nKoeff numeric(2);
begin
	nSalary := public.f_getsalary(new.worker_id, new.calc_period_id);
	nHoursNorma := public.f_getnormhours(new.worker_id, new.calc_period_id);
	if new.hours is null then
		nHours := nHoursNorma;
	else
		nHours := new.hours;
	end if;
	nKoeff := 1;
	if coalesce(new.isholiday,1)=1 then
		nKoeff := 2;
	end if;
	if not coalesce(new.isstorno,false) then
		new.summa := coalesce((nSalary*nHours*new.percent*nKoeff)/100/nHoursNorma,0);
	else
		new.summa := coalesce((-nSalary*nHours*new.percent*nKoeff)/100/nHoursNorma,0);
	end if;
	return new;
END;
$$;


ALTER FUNCTION public.f_bui_other_pays() OWNER TO postgres;

--
-- TOC entry 322 (class 1255 OID 580595)
-- Name: f_dblink_exists(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.f_dblink_exists(text) RETURNS boolean
    LANGUAGE sql
    AS $_$
   SELECT COALESCE($1 = ANY (dblink_get_connections()), false)
$_$;


ALTER FUNCTION public.f_dblink_exists(text) OWNER TO postgres;

--
-- TOC entry 323 (class 1255 OID 580596)
-- Name: f_decimalhours(time without time zone); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.f_decimalhours(thours time without time zone) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
begin
	return EXTRACT(EPOCH FROM tHours)/3600;
END;
$$;


ALTER FUNCTION public.f_decimalhours(thours time without time zone) OWNER TO postgres;

--
-- TOC entry 341 (class 1255 OID 580597)
-- Name: f_getbasesum(uuid, integer, uuid, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.f_getbasesum(uworkerid uuid, ncalcperiodid integer, upaymentid uuid, nfirecaseid integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
declare 
dPeriodBegin date;
dPeriodEnd date;
nSumma numeric(12,2);
nKoeff numeric(9,2);
begin
	
	select f_getsalary(uworkerid, ncalcperiodid) into nSumma;

	-- складываем коэффициенты зависимых выплат
	select sum(a."percent") into nKoeff  
	from accurals a
	inner join payments2payments pp on (a.payment_id = pp.payment_id)
	where pp.parent_payment_id = upaymentid
	and a.calc_period_id = ncalcperiodid
	and a.fire_case_id = nfirecaseid 
	and a.worker_id = uworkerid;
	
	return nSumma*(1+(coalesce(nKoeff,0)/100));

END;
$$;


ALTER FUNCTION public.f_getbasesum(uworkerid uuid, ncalcperiodid integer, upaymentid uuid, nfirecaseid integer) OWNER TO postgres;

--
-- TOC entry 348 (class 1255 OID 580598)
-- Name: f_getnormhours(uuid, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.f_getnormhours(uworkerid uuid, ncalcperiodid integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
declare 
dPeriodBegin date;
dPeriodEnd date;
nHours numeric(12,2);
begin
	select 
	date_trunc('month',c.period_begin), 
	(date_trunc('month',c.period_begin) + INTERVAL '1 month')-INTERVAL '1 day'
	into dPeriodBegin, dPeriodEnd
	from public.calc_periods c
	where c.id = nCalcPeriodId;
	
	select sum(sd.hours) into nHours  
	from schedules sh
	join schedule_days sd on (sd.schedule_id = sh.id) 
	join workers w on (w.schedule_id = sh.id)
	where w.id = uWorkerId
	and sd.day>=dPeriodBegin
	and sd.day<=dPeriodEnd;

	return nHours;
END;
$$;


ALTER FUNCTION public.f_getnormhours(uworkerid uuid, ncalcperiodid integer) OWNER TO postgres;

--
-- TOC entry 381 (class 1255 OID 580599)
-- Name: f_getsalary(uuid, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.f_getsalary(uworkerid uuid, ncalcperiodid integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
declare 
dPeriodBegin date;
dPeriodEnd date;
nSumma numeric(12,2);
begin
	select 
	date_trunc('month',c.period_begin), 
	(date_trunc('month',c.period_begin) + INTERVAL '1 month')-INTERVAL '1 day'
	into dPeriodBegin, dPeriodEnd
	from public.calc_periods c
	where c.id = nCalcPeriodId;
	
    -- пытаемся получить сумму из ФОТ
	select max(f.Summa) into nSumma  
	from fot f
	where f.worker_id = uWorkerId
	and f.start_date <=dPeriodEnd
	and (f.end_date>=dPeriodBegin or f.end_date is null)
	and f.payment_id = (select p.pay_fire_id from paylinks p);

	-- если нет, то берем из Основания как есть
	if coalesce(nSumma,0) = 0 then
		select max(g.Summa) into nSumma  
		from grounds g
		where g.worker_id = uWorkerId
		and g.date_from <=dPeriodEnd
		and (g.date_to>=dPeriodBegin or g.date_to is null)
		and g.payment_id = (select p.pay_fire_id from paylinks p);
	end if;

	return nSumma;
END;
$$;


ALTER FUNCTION public.f_getsalary(uworkerid uuid, ncalcperiodid integer) OWNER TO postgres;

--
-- TOC entry 382 (class 1255 OID 580600)
-- Name: f_getxmlparam(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.f_getxmlparam(sxml character varying) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
declare nPos integer;
BEGIN

	if sXML is null then
		return null;
	end if;
	
	nPos := strpos(sXML, 'PRC=');
	if nPos = 0 then
		return null;
	end if;
	sXML := SUBSTRING(sXML,nPos+4,255);
	nPos := strpos(sXML, ' ');
	sXML := SUBSTRING(sXML,0,nPos-1);
	sXML := trim(REPLACE(sXML,'"',''));
	
	return coalesce(sXML::numeric,0);

END;
$$;


ALTER FUNCTION public.f_getxmlparam(sxml character varying) OWNER TO postgres;

--
-- TOC entry 386 (class 1255 OID 580601)
-- Name: p_calculate(integer); Type: PROCEDURE; Schema: public; Owner: postgres
--

CREATE PROCEDURE public.p_calculate(IN ncalcperiodid integer)
    LANGUAGE plpgsql
    AS $$
declare
nPayment uuid;
dPeriodBegin date;
dPeriodEnd date;
uPayFireId uuid;
uPayOverId uuid;
uPayHolidayId uuid;
begin

	select 
	date_trunc('month',c.period_begin), 
	(date_trunc('month',c.period_begin) + INTERVAL '1 month')-INTERVAL '1 day'
	into dPeriodBegin, dPeriodEnd
	from public.calc_periods c
	where c.id = nCalcPeriodId;
	
	-- чистим расчет за расчетный период
	delete from accurals where calc_period_id = nCalcPeriodId;

	select l.pay_fire_id, l.pay_over_id, l.pay_holiday_id	
	into uPayFireId, uPayOverId, uPayHolidayId
	from paylinks l;

	--добавляем участие в пожарах
	select pl.pay_fire_id into nPayment from paylinks pl;

	insert into public.accurals (
		calc_period_id, fire_case_id, isfirepayment, base_tarif, fact, norma, 
		worker_id, payment_id, schedule_id, ratevol
	) select
		nCalcPeriodId, fire_case_id, 1, max(f_getsalary(worker_id, calc_period_id)), sum(f_decimalhours(fd.work_hours)), max(f_getnormhours(worker_id, calc_period_id)),
		ft.worker_id, nPayment, w.schedule_id, w.ratevol  
	from fightings ft
	inner join workers w on (w.id = ft.worker_id)
	join fighting_details fd on (fd.fighting_id = ft.id)
	where ft.calc_period_id = nCalcPeriodId
	group by nCalcPeriodId, fire_case_id, ft.worker_id, w.schedule_id, w.ratevol, nPayment;

	-- расчет участия в пожарах
	update public.accurals
	set summa = (base_tarif*ratevol*fact)/norma 
	where calc_period_id = nCalcPeriodId
	and payment_id = nPayment;

	--////////////////////////////////

	-- добавляем прыжки/спуски
	insert into public.accurals (
		calc_period_id, fire_case_id, isfirepayment, base_tarif, fact, norma, worker_id, payment_id, summa
	) select
		nCalcPeriodId, j.fire_case_id, 0, null, null, null, j.worker_id, jk.payment_id, j.summa
	from jumps j
	join jump_kinds jk on (jk.id = j.jump_kind_id)
	where j.calc_period_id = nCalcPeriodId;

	select pl.pay_flights_id into nPayment from paylinks pl;

	-- добавляем налеты
	insert into public.accurals (
		calc_period_id, fire_case_id, isfirepayment, base_tarif, fact, norma, worker_id, payment_id, summa
	) select
		nCalcPeriodId, fl.fire_case_id, 0, null, f_decimalhours(fl.hours), null, fl.worker_id, nPayment, fl.summa
	from flights fl
	where fl.calc_period_id = nCalcPeriodId;

	-- добавляем разовые выплаты
	insert into public.accurals (
		calc_period_id, fire_case_id, isfirepayment, base_tarif, fact, norma, worker_id, payment_id, summa
	) select
		nCalcPeriodId, op.fire_case_id, 0, null, op.hours, null, op.worker_id, op.payment_id, op.summa
	from other_pays op
	where op.calc_period_id = nCalcPeriodId;

	--/////////////////////////////////////

	-- добавление и расчет зависимых выплат
	insert into accurals (
		calc_period_id, fire_case_id, worker_id, payment_id, isfirepayment , "percent", summa, fact, norma		
	)
	select 
		a.calc_period_id, a.fire_case_id, a.worker_id, g.payment_id, 0, g."percent", sum(a.summa*g."percent")/100 as summa, max(a.fact), max(a.norma)
	from accurals a 
	inner join payments2payments pp on (pp.payment_id = a.payment_id)
	inner join grounds g on (g.worker_id = a.worker_id and g.payment_id = pp.parent_payment_id)
	where (g.date_from <= dPeriodEnd) and (g.date_to>=dPeriodBegin or g.date_to is null)
	and a.calc_period_id = ncalcperiodid 
	and not g.payment_id in ( 
   		uPayFireId, uPayOverId, uPayHolidayId
   	)
	group by a.calc_period_id, a.fire_case_id, a.worker_id, g.payment_id, g."percent";

	--/////////////////////////////////////
	
	--добавляем и рассчитываем сверхурочные
	select pl.pay_over_id into nPayment from paylinks pl;

	drop table if exists accurals_temp;

	create temp table accurals_temp as
	select 
	nCalcPeriodId as calc_period_id, fire_case_id, 1 as isfirepayment, ft.worker_id, nPayment as payment_id, w.schedule_id, w.ratevol,
	f_getbasesum(worker_id, calc_period_id, nPayment, ft.fire_case_id) as base_tarif, 
	f_decimalhours(fd.overtime_hours) as fact, 
	f_getnormhours(worker_id, calc_period_id) as norma
	from fightings ft
	inner join workers w on (w.id = ft.worker_id)
	join fighting_details fd on (fd.fighting_id = ft.id)
	where ft.calc_period_id = nCalcPeriodId;	

	--рассчитываем сверхурочные
	insert into public.accurals (
		calc_period_id, fire_case_id, isfirepayment, worker_id, payment_id, schedule_id, ratevol, percent,
		base_tarif, fact, norma, summa
	) 
	select 
	calc_period_id, fire_case_id, isfirepayment, worker_id, payment_id, schedule_id, ratevol, 150,  
	max(base_tarif), sum(case when fact>2 then 2 else fact end) as fact, max(norma), 
	sum(
		((case when fact>2 then 2 else fact end)*base_tarif*ratevol*1.5)/norma
	)
	from accurals_temp t
	group by calc_period_id, fire_case_id, isfirepayment, worker_id, payment_id, schedule_id, ratevol, base_tarif;


	insert into public.accurals (
		calc_period_id, fire_case_id, isfirepayment, worker_id, payment_id, schedule_id, ratevol, percent,
		base_tarif, fact, norma, summa
	) 
	select 
	calc_period_id, fire_case_id, isfirepayment, worker_id, payment_id, schedule_id, ratevol, 200,
	max(base_tarif), sum(case when fact>2 then fact-2 else 0 end) as fact, max(norma), 
	sum(
		((case when fact>2 then fact-2 else 0 end)*base_tarif*ratevol*2)/norma
	)
	from accurals_temp t
	group by calc_period_id, fire_case_id, isfirepayment, worker_id, payment_id, schedule_id, ratevol, base_tarif;

	drop table accurals_temp;

	--добавляем выходные
	select pl.pay_holiday_id into nPayment from paylinks pl;

	insert into public.accurals (
		calc_period_id, fire_case_id, isfirepayment, base_tarif, fact, norma, 
		worker_id, payment_id, schedule_id, ratevol, percent
	) select
		nCalcPeriodId, fire_case_id, 1, 
		max(f_getbasesum(worker_id, calc_period_id, nPayment, ft.fire_case_id)), 
		sum(f_decimalhours(fd.holiday_hours)), max(f_getnormhours(worker_id, calc_period_id)),
		ft.worker_id, nPayment, w.schedule_id, w.ratevol, 200  
	from fightings ft
	inner join workers w on (w.id = ft.worker_id)
	join fighting_details fd on (fd.fighting_id = ft.id)
	where ft.calc_period_id = nCalcPeriodId 
	group by nCalcPeriodId, fire_case_id, ft.worker_id, w.schedule_id, w.ratevol, nPayment;

	-- расчет выходных
	update public.accurals
	set summa = (base_tarif*ratevol*fact*2)/norma 
	where calc_period_id = nCalcPeriodId
	and payment_id = nPayment;

	--////////////////////////

   	-- удаление нулевых
	delete from accurals where calc_period_id = nCalcPeriodId and coalesce(summa,0)=0;

	-- повторный пересчет
	update accurals 
	set summa = s1.summa 
	from (
		select  
		a.calc_period_id, a.fire_case_id, a.worker_id, g.payment_id, 0, g."percent", sum(a.summa*g."percent")/100 as summa
		from accurals a 
		inner join payments2payments pp on (pp.payment_id = a.payment_id)
		inner join grounds g on (g.worker_id = a.worker_id and g.payment_id = pp.parent_payment_id)
		where (g.date_from <= dPeriodEnd) and (g.date_to>=dPeriodBegin or g.date_to is null)
		and a.calc_period_id = ncalcperiodid 
		group by a.calc_period_id, a.fire_case_id, a.worker_id, g.payment_id, g."percent"
    ) AS s1
    where accurals.calc_period_id = s1.calc_period_id 
    and accurals.fire_case_id = s1.fire_case_id
    and accurals.worker_id = s1.worker_id
    and accurals.payment_id = s1.payment_id
   	and not accurals.payment_id in (
   		uPayFireId, uPayOverId, uPayHolidayId
   	);
   
   --///////////////////////
   
   	-- повторное добавление и расчет зависимых выплат - без дублирования
	insert into accurals (
		calc_period_id, fire_case_id, worker_id, payment_id, isfirepayment , "percent", summa, fact, norma		
	)
	select 
		a.calc_period_id, a.fire_case_id, a.worker_id, g.payment_id, 0, g."percent", sum(a.summa*g."percent")/100 as summa, max(a.fact), max(a.norma)
	from accurals a 
	inner join payments2payments pp on (pp.payment_id = a.payment_id)
	inner join grounds g on (g.worker_id = a.worker_id and g.payment_id = pp.parent_payment_id)
	where (g.date_from <= dPeriodEnd) and (g.date_to>=dPeriodBegin or g.date_to is null)
	and a.calc_period_id = ncalcperiodid 
	and not g.payment_id in ( 
   		uPayFireId, uPayOverId, uPayHolidayId
   	)
   	and not exists (
   	   select 1 from accurals a1
   	   where a.calc_period_id = a1.calc_period_id
   	   and a.fire_case_id = a1.fire_case_id
   	   and a.worker_id = a1.worker_id
   	   and g.payment_id = a1.payment_id
   	)
	group by a.calc_period_id, a.fire_case_id, a.worker_id, g.payment_id, g."percent";

	--////////////////////////

   	-- удаление нулевых
	delete from accurals where calc_period_id = nCalcPeriodId and coalesce(summa,0)=0;
   
   -- распределение общих выплат
   
   	update accurals set isdistributed = false where calc_period_id = nCalcPeriodId;
   
   	insert into accurals (calc_period_id, worker_id, payment_id, fire_case_id, percent, summa, isdistributed, distribution_calc_period_id)
	select
	ncalcperiodid,
	cp.worker_id,
	cp.payment_id,
	s_fires.fire_case_id, 
	cp."percent",
	(cp.summa/s_total.summa)*s_fires.summa as summ_distrib,
	true,
	s_fires.calc_period_id
	from common_pays cp,
	(
		select 
		a.fire_case_id, 
		a.worker_id,
		a.calc_period_id,
		sum(a.summa) as summa 
		from accurals a,
		calc_periods cc,
		calc_periods cc1
		where not coalesce(a.isdistributed)
		and cc.id = ncalcperiodid 
		and a.calc_period_id = cc1.id
		and extract(year from cc1.period_begin) = extract(year from cc.period_begin)
		and extract(month from cc1.period_begin) <= extract(month from cc.period_begin)
		group by a.fire_case_id, a.worker_id, a.calc_period_id
	) s_fires,
	(
		select 
		a.worker_id,
		sum(a.summa) as summa 
		from accurals a,
		calc_periods cc,
		calc_periods cc1
		where not coalesce(a.isdistributed)
		and cc.id = ncalcperiodid 
		and a.calc_period_id = cc1.id
		and extract(year from cc1.period_begin) = extract(year from cc.period_begin)
		and extract(month from cc1.period_begin) <= extract(month from cc.period_begin)
		group by a.worker_id
	) s_total
	where s_fires.worker_id = s_total.worker_id
	and cp.worker_id = s_fires.worker_id
	and cp.calc_period_id = ncalcperiodid;
   
   
END;
$$;


ALTER PROCEDURE public.p_calculate(IN ncalcperiodid integer) OWNER TO postgres;

--
-- TOC entry 383 (class 1255 OID 580603)
-- Name: p_export_zp_data(integer, character varying); Type: PROCEDURE; Schema: public; Owner: postgres
--

CREATE PROCEDURE public.p_export_zp_data(IN ncalcperiodid integer, IN sworkerid character varying)
    LANGUAGE plpgsql
    AS $$
declare 
dPeriodBegin date;
uWorkerId uuid;
BEGIN
	select 
	date_trunc('month',c.period_begin) 
	into dPeriodBegin
	from public.calc_periods c
	where c.id = ncalcperiodid;
	
	-- Добавляем расширение postgres_fdw
	CREATE EXTENSION if not exists postgres_fdw;
	-- Создаем внешний сервер foreign_server с помощью postgres_fdw.
	CREATE SERVER if not exists tornado_fdw FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host 'localhost', dbname 'parus');
	
	CREATE USER MAPPING if not exists FOR CURRENT_USER SERVER tornado_fdw OPTIONS (user 'postgres', password '1');
	
	drop FOREIGN TABLE if exists foreign_temp_zp_data;
	CREATE FOREIGN TABLE foreign_temp_zp_data (
		worker_id uuid NULL,
		payment_id uuid NULL,
		"percent" numeric NULL,
		base_tarif numeric NULL,
		fact numeric NULL,
		norma numeric NULL,
		summa numeric NULL
	) SERVER tornado_fdw 
	OPTIONS (table_name 'temp_zp_data');
	
	truncate table foreign_temp_zp_data;

	if sWorkerId = '' then
		uWorkerId := null;
	else
		uWorkerId := sWorkerId::uuid;
	end if;
	
	insert into foreign_temp_zp_data (
	worker_id, 
	payment_id, 
	percent,
	base_tarif,
	fact,
	norma,
	summa 
	)
	select 
	a.worker_id, 
	a.payment_id, 
	a."percent" as percent,
	max(a.base_tarif) as base_tarif,
	sum(a.fact) as fact,
	max(a.norma) as norma,
	sum(a.summa) as summa 
	from accurals a 
	where a.calc_period_id = ncalcperiodid
	and (a.worker_id = uWorkerId or uWorkerId is null) 
	group by a.worker_id, a.payment_id, a."percent";

	commit;

	drop SERVER if exists tornado_fdw cascade; 

	call p_launchforeighimport(dPeriodBegin);

END;
$$;


ALTER PROCEDURE public.p_export_zp_data(IN ncalcperiodid integer, IN sworkerid character varying) OWNER TO postgres;

--
-- TOC entry 384 (class 1255 OID 580604)
-- Name: p_import_zp_data(); Type: PROCEDURE; Schema: public; Owner: postgres
--

CREATE PROCEDURE public.p_import_zp_data()
    LANGUAGE plpgsql
    AS $$
declare 
	n int;
	dLastUpdate timestamp;
	workers_deleting cursor for 
	select id from public.workers
	where not exists (
		select 1 from public.temp_workers td
		where td.id = workers.id
	);
	_workers_deleting record;
begin
	
	n := 1;

	CREATE EXTENSION if not exists dblink;

	if f_dblink_exists('tornado') then
		perform dblink_disconnect('tornado');
	end if;
	perform dblink_connect('tornado','host=localhost port=5432 dbname=parus user=postgres password=1 options=-csearch_path=');

	truncate table public.temp_catalogs;
	truncate table public.temp_workers;
	truncate table public.temp_departments;
	truncate table public.temp_payments;
	truncate table public.temp_schedules;
	truncate table public.temp_schedule_days;
	truncate table public.temp_fot;
	truncate table public.temp_grounds;
	truncate table public.temp_objects_updated;

	select max(date_time) into dLastUpdate from import_log;
	if dLastUpdate is null then
		dLastUpdate := to_date('01.01.1950','dd.mm.yyyy');
	end if;

	insert into public.temp_objects_updated (id)
	SELECT * FROM dblink('tornado', '
		select id from userdata.object o where o."_updDate">'''||to_char(dLastUpdate,'yyyy-mm-dd')||'''::timestamp
	')
	as t(
		id uuid
	);

	-- каталоги
	insert into public.temp_catalogs (id, parent_id, name)
	SELECT * FROM dblink('tornado', '
		select id, "HierarchyLink_id", "DisplayName" from userdata."ParusNetServerSystemCatalog"
	')
	as t(
		id uuid,
		parent_id uuid,
		"name" text
	);

	-- выплаты и удержания
	insert into public.temp_payments (id, catalog_id, num, code, "name")
	SELECT * FROM dblink('tornado', '
		select id, "Catalog_id", "Num", "Mnemo", "Name"
		from userdata."ParusBusinessSalaryGenericSnu" 
		where "Type"=''B774D32A-C4C5-4D09-8042-6FBF48818592'' and not coalesce("IsHide",false)
	')
	as t(
		id uuid,
		catalog_id uuid,
		num numeric(6),
		code varchar(50),
		"name" varchar(250)
	);

	-- подразделения
	insert into public.temp_departments (id, parent_id, Code, Name )
	SELECT * FROM dblink('tornado', '
		select id as external_id, "HigherSubdivision_id" as parent_id, "Mnemo" as Code, coalesce("Name","Mnemo") as Name 
		from userdata."ParusBusinessStructuralSubdivision"
	')
	as t(
		external_id uuid,
		parent_id uuid,
		code varchar(40),
		"name" text
	);

	-- сотрудники
	insert into public.temp_workers 
	(id, department_id, code, "name", post, kind, ratevol, schedule_id, date_begin, date_end)
	SELECT * FROM dblink('tornado', '
		select ea.id as external_id, 
		s.id as department, 
		p."Mnemo" as code,
	 	coalesce(p."Surnam",'''')||'' ''||coalesce(p."Name",'''')||'' ''||coalesce(p."SecondName",'''') as Name, 
		coalesce(posts."Name", posts."Mnemo") as post, 
		k."Name" as kind, 
		eah."RateVol" as ratevol, 
		eah."ScheduleOfWork_id",
		ea."DateFrom" as date_begin, 
		ea."DateTo" as date_end
		from 
		userdata."ParusBusinessPerson" p, 
		userdata."ParusYugBusinessGenericPersonnelWorker" w, 
		userdata."ParusYugBusinessGenericPersonnelEmploymentAccount" ea, 
		userdata."ParusYugBusinessGenericPersonnelEmploymentAccountHistory" eah,
		userdata."ParusYugBusinessGenericPersonnelPost" posts, 
		userdata."ParusBusinessStructuralSubdivision" s,
		userdata."ParusYugBusinessGenericPersonnelKindOfEmploymentAccount" k
		where p.id = w."Person_id" and ea."Worker_id" = w.id
		and ea."Post_id" = posts.id
		and ea."Subdivision_id" = s.id
		and eah."EmploymentAccount_id" = ea.id
		and eah."DateOfChange" = (
			select MAX(eah1."DateOfChange") 
			from userdata."ParusYugBusinessGenericPersonnelEmploymentAccountHistory" eah1 
			where eah1."EmploymentAccount_id" = ea.id
		)
		and ea."Kind_id" = k.id
		order by p."Mnemo"
	')
	AS t(
		external_id uuid,
		department uuid,
		code varchar(20),
		"name" text,
		post varchar(1000),
		kind varchar(1000),
		ratevol numeric(6, 3),
		"ScheduleOfWork_id" uuid,
		date_begin timestamp,
		date_end timestamp
	);

	-- календари
	insert into public.temp_schedules (id, code, name)
	SELECT * FROM dblink('tornado', '
		select id, "Mnemo", "Name" from
		userdata."ParusYugBusinessGenericPersonnelScheduleOfWork"
	')
	as t(
		id uuid, 
		code varchar(40), 
		name varchar(250)	
	);

	-- дни календарей
	insert into public.temp_schedule_days(id, schedule_id, "day", hours, day_type)
	SELECT * FROM dblink('tornado', '
		select c.id, c."ScheduleOfWork_id", c."Day", c."HoursNumbers", t."Mnemo" from
		userdata."ParusYugBusinessGenericPersonnelWorkCalendar" c, 
		userdata."ParusYugBusinessGenericPersonnelWorkHoursType" t
		where c."HoursType_id" = t.id
	')
	as t(
		id uuid, 
		schedule_id uuid, 
		"day" date, 
		hours numeric(5,2), 
		day_type varchar(50)
	);

	-- ОСНОВАНИЯ
	insert into public.temp_grounds (id, worker_id, payment_id, date_from, date_to, "percent", xmlstr, lnumber, rnumber, summa)
	SELECT * FROM dblink('tornado', '
		select o.id, o."EmploymentAccount_id", o."Snu_id", o."StartDate", o."EndDate", 
		p."NumberValue" as percent, o."StrXml", 0 as rnumber, 0 as lnumber, p1."NumberValue" as summa
		from userdata."ParusBusinessSalaryGenericSnu" snu
		inner join userdata."ParusBusinessSalaryCalculationOsn" o on (snu.id = o."Snu_id")
		left outer join userdata."ParusBusinessSalaryCalculationOsnParam" p on (o.id = p."Osn_id" and p."Code" = ''PRC'')
		left outer join userdata."ParusBusinessSalaryCalculationOsnParam" p1 on (o.id = p1."Osn_id" and p."Code" = ''RATEVALUE'')
		where not coalesce(snu."IsHide",false) and not o."EmploymentAccount_id" is null 
	')
	as t(
		id uuid, 
		worker_id uuid,
		payment_id uuid,
		date_from date,
		date_to date,
		"percent" numeric,
		xmlstr varchar,
		lnumber numeric,
		rnumber numeric,
		summa numeric
	);

	-- ФОТ
	insert into temp_fot (main_lpf_id, worker_id, payment_id, start_date, end_date, lpf_coefficient, koeff_kind, summa)
	SELECT * FROM dblink('tornado', '
		select distinct m.id, e."EmploymentAccount_id" as worker_id, s.id as payment_id, 
		m."StartDate", m."EndDate", m."LPFCoefficient", 
		(case when m."KindOfLPFCoefficient"=''ac73703b-4e7a-4a10-81d3-c7b7ad90f3a4'' then ''%'' else ''rate'' end) as koeff_kind,
		m."Sum"
		from userdata."ParusBusinessGenericPersonnelMainLPF" m
		inner join userdata."ParusYugBusinessGenericPersonnelEALPF" e on (m.id = e.id)
		inner join userdata."ParusYugBusinessGenericPersonnelLPFCategoryMembership" cm on (m."Category_id" = cm."LPFItem_id")
		inner join userdata."ParusBusinessSalaryGenericSnu" s on (s."LPFCategoryGroup_id" = cm."LPFCategoryGroup_id")
	')
	as t(
		main_lpf_id	uuid,
		worker_id uuid,
		payment_id uuid,
		start_date date,
		end_date date,
		lpf_coefficient numeric, 
		koeff_kind varchar,
		summa numeric
	);
	
-- разносим
	-- подразделения
	insert into public.departments 
	select * from public.temp_departments tw 
	where not exists (
		select 1 from public.departments  w
		where w.id = tw.id
	); 

	update public.departments
	set code = s1.code, name = s1.name
	from (
		select td.* 
		from public.temp_departments td
		inner join public.temp_objects_updated o on (o.id = td.id)
	) s1
	where s1.id = public.departments.id;

	-- сотрудники
	insert into public.workers
	select * from public.temp_workers tw 
	where not exists (
		select 1 from public.workers w
		where w.id = tw.id
	);

	update public.workers
	set code = s1.code, name = s1.name, 
	post = s1.post, kind = s1.kind, 
	ratevol = s1.ratevol, schedule_id = s1.schedule_id, 
	date_begin = s1.date_begin, date_end = s1.date_end	
	from (
		select td.* 
		from public.temp_workers td
		-- 14.04.2024
		--inner join public.temp_objects_updated o on (o.id = td.id)
	) s1
	where s1.id = public.workers.id;

	-- удаление лишних
	for _workers_deleting in workers_deleting loop
		begin
			delete from public.workers where id = _workers_deleting.id;
		exception
			when others then
			update public.workers set isdeleted=1 where id = _workers_deleting.id;  
		end;
	end loop;

	-- каталоги
	insert into public.catalogs
	select * from public.temp_catalogs tw 
	where not exists (
		select 1 from public.catalogs w
		where w.id = tw.id
	); 

	update public.catalogs
	set parent_id = s1.parent_id, name = s1.name 
	from (
		select td.* 
		from public.temp_catalogs td
		inner join public.temp_objects_updated o on (o.id = td.id)
	) s1
	where s1.id = public.catalogs.id;


	-- выплаты
	insert into public.payments 
	select * from public.temp_payments tw 
	where not exists (
		select 1 from public.payments w
		where w.id = tw.id
	);

	update public.payments
	set catalog_id = s1.catalog_id, num = s1.num, code = s1.code, name = s1.name 
	from (
		select td.* 
		from public.temp_payments td
		inner join public.temp_objects_updated o on (o.id = td.id)
	) s1
	where s1.id = public.payments.id;

	-- расписание
	insert into public.schedules 
	select * from public.temp_schedules tw 
	where not exists (
		select 1 from public.schedules w
		where w.id = tw.id
	); 

	update public.schedules
	set code = s1.code, name = s1.name 
	from (
		select td.* 
		from public.temp_schedules td
		inner join public.temp_objects_updated o on (o.id = td.id)
	) s1
	where s1.id = public.schedules.id;

	-- дни расписания
	insert into public.schedule_days 
	select * from public.temp_schedule_days tw 
	where not exists (
		select 1 from public.schedule_days w
		where w.id = tw.id
	);

	update public.schedule_days
	set schedule_id = s1.schedule_id, day = s1.day, day_type = s1.day_type, hours = s1.hours	
	from (
		select td.* 
		from public.temp_schedule_days td
		inner join public.temp_objects_updated o on (o.id = td.id)
	) s1
	where s1.id = public.schedule_days.id;

	-- основания
	insert into public.grounds (
	id, worker_id, payment_id, date_from, date_to, "percent", lnumber, rnumber, Summa
	) 
	select g.id, g.worker_id, g.payment_id, g.date_from, g.date_to, 
	max((case when coalesce(g.percent, 0)>coalesce(f_GetXmlParam(g.xmlstr),0) then g.percent else f_GetXmlParam(g.xmlstr) end)) as "percent",
	max(g.lnumber), max(g.rnumber),
	max(g.summa) as Summa
	from public.temp_grounds g
	inner join payments p on (p.id = g.payment_id) 
	where not exists (
		select 1 from public.grounds g1
		where g1.id = g.id
	)
	group by g.id, g.worker_id, g.payment_id, g.date_from, g.date_to;

	update public.grounds 
	set worker_id = s1.worker_id, payment_id = s1.payment_id, 
	date_from = s1.date_from, date_to = s1.date_to, 
	"percent" = (case when coalesce(s1.percent, 0)>coalesce(f_GetXmlParam(s1.xmlstr),0) then s1.percent else f_GetXmlParam(s1.xmlstr) end), 
	lnumber = s1.lnumber, rnumber = s1.rnumber, summa = s1.summa	
	from (
		select td.* 
		from public.temp_grounds td
		--полный апдейт (исправление ошибок)
		--inner join public.temp_objects_updated o on (o.id = td.id)
	) s1 
	where s1.id = public.grounds.id;

	-- удаление лишних
	delete from public.grounds
	where not exists (
		select 1 from public.temp_grounds td
		where td.id = grounds.id
	);

	-- ФОТ
	-- 14.04.2024
	truncate table public.fot;
	insert into public.fot (
		worker_id, main_lpf_id,	payment_id, start_date, end_date, lpf_coefficient, koeff_kind, summa
	)
	select g.worker_id, g.main_lpf_id, g.payment_id, g.start_date, g.end_date, g.lpf_coefficient, g.koeff_kind, g.summa
	from public.temp_fot g
	inner join payments p on (p.id = g.payment_id) 
	where not exists (
		select 1 from public.fot g1
		where g1.id = g.id
	);

	-- апдейт показателей основания
	--1)
	update grounds 
		set "percent" = coalesce(grounds."percent",s1.lpf_coefficient), 
		summa = coalesce(grounds.summa,s1.summa)
	from (
		select g.id, f.lpf_coefficient, f.summa 
		from fot f
		inner join grounds g on (
			g.worker_id=f.worker_id and
			g.payment_id=f.payment_id and
			(f.end_date is null and g.date_to is null)
		)
	) s1
	where grounds.id = s1.id;

	--2)
	update grounds 
		set "percent" = coalesce(grounds."percent",s1.lpf_coefficient), 
		summa = coalesce(grounds.summa,s1.summa)
	from (
		select g.id, f.lpf_coefficient, f.summa 
		from fot f
		inner join grounds g on (
			g.worker_id=f.worker_id and
			g.payment_id=f.payment_id and
			(f.end_date>g.date_from or f.end_date is null) and
			(g.date_to>f.start_date or g.date_to is null)
		)
	) s1
	where grounds.id = s1.id;

	insert into import_log (date_time, result)
	values (now(), '');

END;
$$;


ALTER PROCEDURE public.p_import_zp_data() OWNER TO postgres;

--
-- TOC entry 385 (class 1255 OID 580606)
-- Name: p_launchforeighimport(date); Type: PROCEDURE; Schema: public; Owner: postgres
--

CREATE PROCEDURE public.p_launchforeighimport(IN dperiodbegin date)
    LANGUAGE plpgsql
    AS $$
BEGIN
	
	if f_dblink_exists('tornado') then
		perform dblink_disconnect('tornado');
	end if;
	perform dblink_connect('tornado','host=localhost port=5432 dbname=parus user=postgres password=1 options=-csearch_path=');

	perform dblink_exec(
		'tornado', 
		'CALL public.P_ImportAviaWages(to_date('''||to_char(dPeriodBegin,'yyyy-mm-dd')||''',''yyyy-mm-dd''));'
	);

END;
$$;


ALTER PROCEDURE public.p_launchforeighimport(IN dperiodbegin date) OWNER TO postgres;

--
-- TOC entry 2400 (class 1417 OID 619022)
-- Name: tornado_fdw; Type: SERVER; Schema: -; Owner: postgres
--

CREATE SERVER tornado_fdw FOREIGN DATA WRAPPER postgres_fdw OPTIONS (
    dbname 'parus',
    host 'localhost'
);


ALTER SERVER tornado_fdw OWNER TO postgres;

--
-- TOC entry 3856 (class 0 OID 0)
-- Name: USER MAPPING postgres SERVER tornado_fdw; Type: USER MAPPING; Schema: -; Owner: postgres
--

CREATE USER MAPPING FOR postgres SERVER tornado_fdw OPTIONS (
    password '1',
    "user" 'postgres'
);


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 226 (class 1259 OID 580607)
-- Name: accurals; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.accurals (
    id integer NOT NULL,
    calc_period_id integer NOT NULL,
    fire_case_id integer,
    isfirepayment numeric(1,0) DEFAULT 0,
    ratevol numeric(5,3),
    base_tarif numeric(12,2),
    fact numeric(6,2),
    norma numeric(6,2),
    percent numeric(6,2),
    worker_id uuid,
    schedule_id uuid,
    payment_id uuid,
    summa numeric(12,2),
    isdistributed boolean DEFAULT false,
    distribution_calc_period_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.accurals OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 580612)
-- Name: accurals_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.accurals ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.accurals_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 228 (class 1259 OID 580613)
-- Name: calc_periods; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.calc_periods (
    id integer NOT NULL,
    period_begin date NOT NULL,
    date_closed date,
    created_at date,
    updated_at date
);


ALTER TABLE public.calc_periods OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 580616)
-- Name: calc_periods_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.calc_periods ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.calc_periods_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 230 (class 1259 OID 580617)
-- Name: catalogs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.catalogs (
    id uuid NOT NULL,
    parent_id uuid,
    name character varying(250)
);


ALTER TABLE public.catalogs OWNER TO postgres;

--
-- TOC entry 231 (class 1259 OID 580620)
-- Name: common_pays; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.common_pays (
    id integer NOT NULL,
    worker_id uuid NOT NULL,
    payment_id uuid NOT NULL,
    calc_period_id integer NOT NULL,
    percent numeric(5,2),
    summa numeric(12,2) NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.common_pays OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 580623)
-- Name: common_pays_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.common_pays ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.common_pays_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 309 (class 1259 OID 591335)
-- Name: deduction_scale; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.deduction_scale (
    id integer NOT NULL,
    start_date date NOT NULL,
    esv numeric(9,4),
    pfr numeric(9,4),
    fss numeric(9,4),
    vacation_reserve_koeff numeric(12,4)
);


ALTER TABLE public.deduction_scale OWNER TO postgres;

--
-- TOC entry 308 (class 1259 OID 591334)
-- Name: deduction_scale_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.deduction_scale ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.deduction_scale_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 233 (class 1259 OID 580624)
-- Name: departments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.departments (
    id uuid NOT NULL,
    parent_id uuid,
    code character varying(80),
    name character varying(250)
);


ALTER TABLE public.departments OWNER TO postgres;

--
-- TOC entry 234 (class 1259 OID 580627)
-- Name: fighting_details; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fighting_details (
    id integer NOT NULL,
    fighting_id integer NOT NULL,
    day date NOT NULL,
    work_hours time without time zone,
    overtime_hours time without time zone,
    holiday_hours time without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.fighting_details OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 580630)
-- Name: fighting_details_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.fighting_details ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.fighting_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 236 (class 1259 OID 580631)
-- Name: fightings; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fightings (
    id integer NOT NULL,
    fire_case_id integer NOT NULL,
    worker_id uuid,
    calc_period_id integer NOT NULL,
    date_from date NOT NULL,
    date_to date,
    isstorno boolean
);


ALTER TABLE public.fightings OWNER TO postgres;

--
-- TOC entry 237 (class 1259 OID 580634)
-- Name: fightings_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.fightings ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.fightings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 238 (class 1259 OID 580635)
-- Name: fire_cases; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fire_cases (
    id integer NOT NULL,
    name character varying(250) NOT NULL,
    forestry_id integer,
    square smallint,
    department_id uuid,
    datefrom date,
    dateto date,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.fire_cases OWNER TO postgres;

--
-- TOC entry 239 (class 1259 OID 580638)
-- Name: firecases_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.firecases_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.firecases_id_seq OWNER TO postgres;

--
-- TOC entry 3857 (class 0 OID 0)
-- Dependencies: 239
-- Name: firecases_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.firecases_id_seq OWNED BY public.fire_cases.id;


--
-- TOC entry 240 (class 1259 OID 580639)
-- Name: flight_tarifs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.flight_tarifs (
    id integer NOT NULL,
    plane_id integer,
    paygroup_id integer,
    head_salary numeric(12,2),
    percent numeric(5,2),
    tarif numeric(12,2),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.flight_tarifs OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 580642)
-- Name: flights; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.flights (
    id integer NOT NULL,
    fire_case_id integer NOT NULL,
    worker_id uuid NOT NULL,
    plane_id integer NOT NULL,
    pay_group_id integer NOT NULL,
    calc_period_id integer NOT NULL,
    date_from date,
    date_to date,
    hours time without time zone,
    summa numeric(14,4),
    isstorno boolean
);


ALTER TABLE public.flights OWNER TO postgres;

--
-- TOC entry 242 (class 1259 OID 580645)
-- Name: flights_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.flights ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.flights_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 311 (class 1259 OID 619031)
-- Name: foreign_temp_zp_data; Type: FOREIGN TABLE; Schema: public; Owner: postgres
--

CREATE FOREIGN TABLE public.foreign_temp_zp_data (
    worker_id uuid,
    payment_id uuid,
    percent numeric,
    base_tarif numeric,
    fact numeric,
    norma numeric,
    summa numeric
)
SERVER tornado_fdw
OPTIONS (
    table_name 'temp_zp_data'
);


ALTER FOREIGN TABLE public.foreign_temp_zp_data OWNER TO postgres;

--
-- TOC entry 243 (class 1259 OID 580646)
-- Name: forestries; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.forestries (
    id integer NOT NULL,
    name character varying(250) NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.forestries OWNER TO postgres;

--
-- TOC entry 244 (class 1259 OID 580649)
-- Name: forestries_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.forestries ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.forestries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 245 (class 1259 OID 580650)
-- Name: fot; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fot (
    id integer NOT NULL,
    main_lpf_id uuid NOT NULL,
    worker_id uuid NOT NULL,
    payment_id uuid NOT NULL,
    start_date date NOT NULL,
    end_date date,
    lpf_coefficient numeric(12,4),
    koeff_kind character varying(10) NOT NULL,
    summa numeric(12,2)
);


ALTER TABLE public.fot OWNER TO postgres;

--
-- TOC entry 246 (class 1259 OID 580653)
-- Name: fot_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.fot ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.fot_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 247 (class 1259 OID 580654)
-- Name: grounds; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.grounds (
    id uuid NOT NULL,
    worker_id uuid NOT NULL,
    payment_id uuid NOT NULL,
    date_from date,
    date_to date,
    percent numeric(12,2),
    lnumber integer,
    rnumber integer,
    summa numeric(12,2)
);


ALTER TABLE public.grounds OWNER TO postgres;

--
-- TOC entry 248 (class 1259 OID 580657)
-- Name: import_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.import_log (
    id integer NOT NULL,
    date_time timestamp without time zone NOT NULL,
    result character varying(250)
);


ALTER TABLE public.import_log OWNER TO postgres;

--
-- TOC entry 249 (class 1259 OID 580660)
-- Name: import_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.import_log ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.import_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 250 (class 1259 OID 580661)
-- Name: jump_kinds; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jump_kinds (
    id integer NOT NULL,
    payment_id uuid,
    code character varying(50),
    name character varying(250),
    tarif numeric(12,2),
    percent numeric(5,2),
    salary numeric(12,2),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.jump_kinds OWNER TO postgres;

--
-- TOC entry 251 (class 1259 OID 580664)
-- Name: jump_kind_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.jump_kinds ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.jump_kind_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 252 (class 1259 OID 580665)
-- Name: jumps; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jumps (
    id integer NOT NULL,
    fire_case_id integer NOT NULL,
    worker_id uuid NOT NULL,
    jump_kind_id integer NOT NULL,
    amount integer NOT NULL,
    calc_period_id integer NOT NULL,
    date_begin date NOT NULL,
    date_end date,
    summa numeric(12,2),
    isstorno boolean
);


ALTER TABLE public.jumps OWNER TO postgres;

--
-- TOC entry 253 (class 1259 OID 580668)
-- Name: jumps_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.jumps ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.jumps_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 254 (class 1259 OID 580669)
-- Name: mdays; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.mdays (
    id integer NOT NULL,
    day integer NOT NULL
);


ALTER TABLE public.mdays OWNER TO postgres;

--
-- TOC entry 255 (class 1259 OID 580672)
-- Name: mdays_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.mdays ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.mdays_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 256 (class 1259 OID 580673)
-- Name: other_pays; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.other_pays (
    id integer NOT NULL,
    fire_case_id integer NOT NULL,
    payment_id uuid NOT NULL,
    worker_id uuid NOT NULL,
    calc_period_id integer NOT NULL,
    hours numeric(8,4),
    percent numeric(5,2),
    isholiday numeric(1,0) DEFAULT 0,
    summa numeric(12,2),
    isstorno boolean
);


ALTER TABLE public.other_pays OWNER TO postgres;

--
-- TOC entry 257 (class 1259 OID 580677)
-- Name: other_pays_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.other_pays ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.other_pays_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 258 (class 1259 OID 580678)
-- Name: paygroups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.paygroups (
    id integer NOT NULL,
    code character varying(50),
    name character varying(250),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.paygroups OWNER TO postgres;

--
-- TOC entry 259 (class 1259 OID 580681)
-- Name: paygroup_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.paygroups ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.paygroup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 260 (class 1259 OID 580682)
-- Name: paylinks; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.paylinks (
    id integer NOT NULL,
    pay_fire_id uuid,
    pay_jumps_id uuid,
    pay_flights_id uuid,
    pay_over_id uuid,
    pay_holiday_id uuid,
    firecalc character varying(250)
);


ALTER TABLE public.paylinks OWNER TO postgres;

--
-- TOC entry 261 (class 1259 OID 580685)
-- Name: paylinks_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.paylinks ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.paylinks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 262 (class 1259 OID 580686)
-- Name: payments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.payments (
    id uuid NOT NULL,
    catalog_id uuid,
    num numeric(6,0),
    code character varying(50),
    name character varying(250)
);


ALTER TABLE public.payments OWNER TO postgres;

--
-- TOC entry 263 (class 1259 OID 580689)
-- Name: payments2payments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.payments2payments (
    id integer NOT NULL,
    parent_payment_id uuid NOT NULL,
    payment_id uuid NOT NULL
);


ALTER TABLE public.payments2payments OWNER TO postgres;

--
-- TOC entry 264 (class 1259 OID 580692)
-- Name: payments2paymens_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.payments2payments ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.payments2paymens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 265 (class 1259 OID 580693)
-- Name: planes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.planes (
    id integer NOT NULL,
    code character varying(80),
    name character varying(250),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.planes OWNER TO postgres;

--
-- TOC entry 266 (class 1259 OID 580696)
-- Name: planes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.planes ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.planes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 267 (class 1259 OID 580697)
-- Name: report_columns; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.report_columns (
    id integer NOT NULL,
    report_id integer NOT NULL,
    field_name character varying(80) NOT NULL,
    title character varying(80),
    display_width integer,
    display_align character varying(20),
    order_num integer
);


ALTER TABLE public.report_columns OWNER TO postgres;

--
-- TOC entry 268 (class 1259 OID 580700)
-- Name: report_columns_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.report_columns ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.report_columns_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 269 (class 1259 OID 580701)
-- Name: report_params; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.report_params (
    id integer NOT NULL,
    report_id integer NOT NULL,
    code character varying(50) NOT NULL,
    caption character varying(250),
    param_type character varying(20),
    param_length integer,
    param_decimals integer,
    getlist_method character varying(250),
    display_field_name character varying(80),
    key_field_name character varying(80),
    order_num integer,
    master_param_id integer,
    filter_expression character varying(250)
);


ALTER TABLE public.report_params OWNER TO postgres;

--
-- TOC entry 270 (class 1259 OID 580706)
-- Name: report_params_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.report_params ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.report_params_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 271 (class 1259 OID 580707)
-- Name: reports; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reports (
    id integer NOT NULL,
    name character varying(250),
    template character varying(250),
    prepare_command_sql text,
    query_sql text,
    group_field_level1 character varying(250),
    group_field_level2 character varying(250)
);


ALTER TABLE public.reports OWNER TO postgres;

--
-- TOC entry 272 (class 1259 OID 580712)
-- Name: reports_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.reports ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.reports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 273 (class 1259 OID 580713)
-- Name: schedule_days; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.schedule_days (
    id uuid NOT NULL,
    schedule_id uuid NOT NULL,
    day date NOT NULL,
    day_type character varying(50),
    hours numeric(5,2)
);


ALTER TABLE public.schedule_days OWNER TO postgres;

--
-- TOC entry 274 (class 1259 OID 580716)
-- Name: schedules; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.schedules (
    id uuid NOT NULL,
    code character varying(40),
    name character varying(250)
);


ALTER TABLE public.schedules OWNER TO postgres;

--
-- TOC entry 275 (class 1259 OID 580719)
-- Name: tarifs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.flight_tarifs ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.tarifs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 276 (class 1259 OID 580720)
-- Name: temp_catalogs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.temp_catalogs (
    id uuid NOT NULL,
    parent_id uuid,
    name character varying(250)
);


ALTER TABLE public.temp_catalogs OWNER TO postgres;

--
-- TOC entry 277 (class 1259 OID 580723)
-- Name: temp_departments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.temp_departments (
    id uuid NOT NULL,
    parent_id uuid,
    code character varying(80),
    name character varying(250)
);


ALTER TABLE public.temp_departments OWNER TO postgres;

--
-- TOC entry 278 (class 1259 OID 580726)
-- Name: temp_fot; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.temp_fot (
    id integer NOT NULL,
    main_lpf_id uuid NOT NULL,
    worker_id uuid NOT NULL,
    payment_id uuid NOT NULL,
    start_date date NOT NULL,
    end_date date,
    lpf_coefficient numeric(12,4),
    koeff_kind character varying(10) NOT NULL,
    summa numeric(12,2)
);


ALTER TABLE public.temp_fot OWNER TO postgres;

--
-- TOC entry 279 (class 1259 OID 580729)
-- Name: temp_fot_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.temp_fot ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.temp_fot_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 280 (class 1259 OID 580730)
-- Name: temp_fot_summ; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.temp_fot_summ (
    osn_id uuid NOT NULL,
    worker_id uuid,
    fot_summa numeric(12,2)
);


ALTER TABLE public.temp_fot_summ OWNER TO postgres;

--
-- TOC entry 281 (class 1259 OID 580733)
-- Name: temp_grounds; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.temp_grounds (
    id uuid NOT NULL,
    worker_id uuid NOT NULL,
    payment_id uuid NOT NULL,
    date_from date,
    date_to date,
    xmlstr character varying,
    percent numeric(12,2),
    lnumber integer,
    rnumber integer,
    summa numeric(12,2)
);


ALTER TABLE public.temp_grounds OWNER TO postgres;

--
-- TOC entry 282 (class 1259 OID 580738)
-- Name: temp_objects_updated; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.temp_objects_updated (
    id uuid NOT NULL
);


ALTER TABLE public.temp_objects_updated OWNER TO postgres;

--
-- TOC entry 283 (class 1259 OID 580741)
-- Name: temp_payments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.temp_payments (
    id uuid NOT NULL,
    catalog_id uuid,
    num numeric(6,0),
    code character varying(50),
    name character varying(250)
);


ALTER TABLE public.temp_payments OWNER TO postgres;

--
-- TOC entry 284 (class 1259 OID 580744)
-- Name: temp_schedule_days; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.temp_schedule_days (
    id uuid NOT NULL,
    schedule_id uuid NOT NULL,
    day date NOT NULL,
    day_type character varying(50),
    hours numeric(5,2)
);


ALTER TABLE public.temp_schedule_days OWNER TO postgres;

--
-- TOC entry 285 (class 1259 OID 580747)
-- Name: temp_schedules; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.temp_schedules (
    id uuid NOT NULL,
    code character varying(40),
    name character varying(250)
);


ALTER TABLE public.temp_schedules OWNER TO postgres;

--
-- TOC entry 286 (class 1259 OID 580750)
-- Name: temp_workers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.temp_workers (
    id uuid NOT NULL,
    department_id uuid NOT NULL,
    code character varying(50),
    name character varying(250),
    post character varying(250),
    kind character varying(120),
    ratevol numeric(5,3),
    schedule_id uuid,
    date_begin date,
    date_end date
);


ALTER TABLE public.temp_workers OWNER TO postgres;

--
-- TOC entry 287 (class 1259 OID 580755)
-- Name: temp_zp_data; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.temp_zp_data (
    worker_id uuid,
    payment_id uuid,
    fact numeric,
    norma numeric,
    percent numeric,
    summa numeric
);


ALTER TABLE public.temp_zp_data OWNER TO postgres;

--
-- TOC entry 288 (class 1259 OID 580760)
-- Name: v_fire_cases; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_fire_cases AS
 SELECT c.id,
    c.name,
    ("substring"(((((TRIM(BOTH FROM c.name) || ', '::text) || TRIM(BOTH FROM f.name)) || ', с '::text) || to_char((c.datefrom)::timestamp with time zone, 'dd.mm.yyyy'::text)), 1, 250))::character varying(250) AS full_name,
    c.forestry_id,
    f.name AS forestry_name,
    c.square,
    c.department_id,
    COALESCE(d.name, d.code) AS department_name,
    c.datefrom,
    c.dateto
   FROM ((public.fire_cases c
     LEFT JOIN public.forestries f ON ((f.id = c.forestry_id)))
     LEFT JOIN public.departments d ON ((d.id = c.department_id)))
  ORDER BY c.name;


ALTER TABLE public.v_fire_cases OWNER TO postgres;

--
-- TOC entry 307 (class 1259 OID 582311)
-- Name: v_accurals; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_accurals AS
 SELECT ac.id,
    ac.calc_period_id,
    ac.fire_case_id,
    fc.full_name AS fire_case_name,
    ac.isfirepayment,
    ac.base_tarif,
    ac.fact,
    ac.norma,
    ac.percent,
    ac.worker_id,
    ac.schedule_id,
    ac.payment_id,
    p.num AS payment_num,
    p.code AS payment_code,
    p.name AS payment_name,
    ac.summa,
    ac.isdistributed,
        CASE
            WHEN ac.isdistributed THEN 1
            ELSE 0
        END AS isdistr,
    ac.distribution_calc_period_id AS periodofdistribution,
    ac.created_at,
    ac.updated_at
   FROM ((public.accurals ac
     LEFT JOIN public.v_fire_cases fc ON ((fc.id = ac.fire_case_id)))
     LEFT JOIN public.payments p ON ((p.id = ac.payment_id)))
  ORDER BY fc.full_name, p.num;


ALTER TABLE public.v_accurals OWNER TO postgres;

--
-- TOC entry 312 (class 1259 OID 644255)
-- Name: v_calc_periods; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_calc_periods AS
 SELECT cp.id,
    date_trunc('month'::text, (cp.period_begin)::timestamp with time zone) AS period_begin,
    (substr(to_char(date_trunc('month'::text, (cp.period_begin)::timestamp with time zone), 'mm.yyyy'::text), 1, 20))::character varying(20) AS period_begin_str,
    cp.date_closed,
    ( SELECT max(date_trunc('month'::text, (cp1.period_begin)::timestamp with time zone)) AS max
           FROM public.calc_periods cp1
          WHERE (cp1.period_begin < cp.period_begin)) AS last_period_begin,
    (date_trunc('month'::text, (cp.period_begin)::timestamp with time zone) - '15 days'::interval) AS filter_date_begin,
    (date_trunc('month'::text, (cp.period_begin)::timestamp with time zone) + '35 days'::interval) AS filter_date_end,
    cp.created_at,
    cp.updated_at
   FROM public.calc_periods cp
  ORDER BY cp.period_begin;


ALTER TABLE public.v_calc_periods OWNER TO postgres;

--
-- TOC entry 289 (class 1259 OID 580775)
-- Name: v_catalogs; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_catalogs AS
 SELECT c.id,
    c.parent_id,
    c.name
   FROM public.catalogs c
  ORDER BY c.name;


ALTER TABLE public.v_catalogs OWNER TO postgres;

--
-- TOC entry 290 (class 1259 OID 580779)
-- Name: workers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.workers (
    id uuid NOT NULL,
    department_id uuid NOT NULL,
    code character varying(50),
    name character varying(250),
    post character varying(250),
    kind character varying(120),
    ratevol numeric(5,3),
    schedule_id uuid,
    date_begin date,
    date_end date,
    isdeleted numeric(1,0)
);


ALTER TABLE public.workers OWNER TO postgres;

--
-- TOC entry 291 (class 1259 OID 580784)
-- Name: v_workers; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_workers AS
 SELECT w.id,
    w.department_id,
    d.code AS department_code,
    d.name AS department_name,
    w.code,
    (substr(((((((((((((w.code)::text || ', '::text) || (d.code)::text) || ', '::text) || (w.kind)::text) || ', '::text) || '(с '::text) || to_char((w.date_begin)::timestamp with time zone, 'dd.mm.yyyy'::text)) || COALESCE((' по '::text || to_char((w.date_end)::timestamp with time zone, 'dd.mm.yyyy'::text)), ''::text)) || ')='::text) || TRIM(BOTH FROM to_char(w.ratevol, '990.999'::text))) || ' ст.'::text), 1, 250))::character varying(250) AS full_code,
    w.name,
    w.post,
    w.kind,
    w.ratevol,
    w.schedule_id,
    w.date_begin,
    w.date_end,
    s.code AS schedule_code,
    w.isdeleted
   FROM ((public.workers w
     LEFT JOIN public.departments d ON ((d.id = w.department_id)))
     LEFT JOIN public.schedules s ON ((s.id = w.schedule_id)))
  ORDER BY w.code;


ALTER TABLE public.v_workers OWNER TO postgres;

--
-- TOC entry 292 (class 1259 OID 580789)
-- Name: v_common_pays; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_common_pays AS
 SELECT cp.id,
    cp.worker_id,
    cp.payment_id,
    cp.calc_period_id,
    cp.percent,
    cp.summa,
    cp.created_at,
    cp.updated_at,
    w.full_code AS worker_code,
    p.code AS payment_code
   FROM ((public.common_pays cp
     LEFT JOIN public.v_workers w ON ((w.id = cp.worker_id)))
     LEFT JOIN public.payments p ON ((p.id = cp.payment_id)))
  ORDER BY w.code;


ALTER TABLE public.v_common_pays OWNER TO postgres;

--
-- TOC entry 293 (class 1259 OID 580794)
-- Name: v_departments; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_departments AS
 SELECT d.id,
    d.parent_id,
    d1.code AS parent_code,
    d1.name AS parent_name,
    d.code,
    d.name
   FROM (public.departments d
     LEFT JOIN public.departments d1 ON ((d1.id = d.parent_id)))
  ORDER BY d.code;


ALTER TABLE public.v_departments OWNER TO postgres;

--
-- TOC entry 294 (class 1259 OID 580798)
-- Name: v_fighting_details; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_fighting_details AS
 SELECT fd.id,
    fd.fighting_id,
    fd.day,
    fd.work_hours,
    fd.overtime_hours,
    fd.holiday_hours,
    fd.created_at,
    fd.updated_at
   FROM (public.fighting_details fd
     LEFT JOIN public.fightings f ON ((f.id = fd.fighting_id)))
  ORDER BY fd.day;


ALTER TABLE public.v_fighting_details OWNER TO postgres;

--
-- TOC entry 315 (class 1259 OID 645299)
-- Name: v_fightings; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_fightings AS
 SELECT f.id,
    f.fire_case_id,
    f.worker_id,
    f.calc_period_id,
    f.date_from,
    f.date_to,
    fc.full_name AS fire_name,
    w.full_code AS worker_code,
    d.code AS department_code,
    COALESCE(f.isstorno, false) AS isstorno,
        CASE
            WHEN COALESCE(f.isstorno, false) THEN 1
            ELSE 0
        END AS isstorno_num
   FROM (((public.fightings f
     JOIN public.v_fire_cases fc ON ((fc.id = f.fire_case_id)))
     JOIN public.v_workers w ON ((w.id = f.worker_id)))
     JOIN public.departments d ON ((d.id = w.department_id)))
  ORDER BY w.full_code;


ALTER TABLE public.v_fightings OWNER TO postgres;

--
-- TOC entry 295 (class 1259 OID 580807)
-- Name: v_flight_tarifs; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_flight_tarifs AS
 SELECT ft.id,
    ft.plane_id,
    ft.paygroup_id,
    pg.code AS paygroup_code,
    p.code AS plane_code,
    ft.head_salary,
    ft.percent,
    ft.tarif,
    ft.created_at,
    ft.updated_at
   FROM ((public.flight_tarifs ft
     LEFT JOIN public.paygroups pg ON ((pg.id = ft.paygroup_id)))
     LEFT JOIN public.planes p ON ((p.id = ft.plane_id)))
  ORDER BY pg.code, p.code;


ALTER TABLE public.v_flight_tarifs OWNER TO postgres;

--
-- TOC entry 314 (class 1259 OID 645287)
-- Name: v_flights; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_flights AS
 SELECT f.id,
    f.fire_case_id,
    f.worker_id,
    f.plane_id,
    f.pay_group_id,
    f.calc_period_id,
    f.date_from,
    f.date_to,
    f.hours,
    f.summa,
    w.full_code AS worker_code,
    fc.full_name AS fire_case_name,
    p.code AS plane_code,
    pg.code AS paygroup_code,
    COALESCE(f.isstorno, false) AS isstorno,
        CASE
            WHEN COALESCE(f.isstorno, false) THEN 1
            ELSE 0
        END AS isstorno_num
   FROM ((((public.flights f
     LEFT JOIN public.v_fire_cases fc ON ((fc.id = f.fire_case_id)))
     LEFT JOIN public.v_workers w ON ((w.id = f.worker_id)))
     LEFT JOIN public.planes p ON ((p.id = f.plane_id)))
     LEFT JOIN public.paygroups pg ON ((pg.id = f.pay_group_id)))
  ORDER BY f.date_from;


ALTER TABLE public.v_flights OWNER TO postgres;

--
-- TOC entry 296 (class 1259 OID 580817)
-- Name: v_forestries; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_forestries AS
 SELECT f.id,
    f.name
   FROM public.forestries f
  ORDER BY f.name;


ALTER TABLE public.v_forestries OWNER TO postgres;

--
-- TOC entry 297 (class 1259 OID 580821)
-- Name: v_fot; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_fot AS
 SELECT f.id,
    f.worker_id,
    f.payment_id,
    f.start_date,
    f.end_date,
    f.lpf_coefficient,
    f.koeff_kind,
    f.summa,
    p.code AS payment_code
   FROM (public.fot f
     LEFT JOIN public.payments p ON ((f.payment_id = p.id)))
  ORDER BY p.num;


ALTER TABLE public.v_fot OWNER TO postgres;

--
-- TOC entry 298 (class 1259 OID 580825)
-- Name: v_grounds; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_grounds AS
 SELECT g.id,
    g.worker_id,
    g.payment_id,
    p.code AS payment_code,
    p.name AS payment_name,
    g.date_from,
    g.date_to,
    g.percent,
    g.lnumber,
    g.rnumber,
    g.summa
   FROM (public.grounds g
     LEFT JOIN public.payments p ON ((p.id = g.payment_id)))
  ORDER BY p.num;


ALTER TABLE public.v_grounds OWNER TO postgres;

--
-- TOC entry 299 (class 1259 OID 580829)
-- Name: v_jump_kinds; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_jump_kinds AS
 SELECT jk.id,
    jk.payment_id,
    p.code AS payment_code,
    p.name AS payment_name,
    jk.code,
    jk.name,
    jk.tarif,
    jk.percent,
    jk.salary,
    jk.created_at,
    jk.updated_at
   FROM (public.jump_kinds jk
     LEFT JOIN public.payments p ON ((p.id = jk.payment_id)))
  ORDER BY jk.code;


ALTER TABLE public.v_jump_kinds OWNER TO postgres;

--
-- TOC entry 316 (class 1259 OID 645309)
-- Name: v_jumps; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_jumps AS
 SELECT j.id,
    j.fire_case_id,
    j.worker_id,
    j.jump_kind_id,
    j.amount,
    j.calc_period_id,
    j.date_begin,
    j.date_end,
    fc.full_name AS fire_case_name,
    w.full_code AS worker_code,
    jk.name AS jump_kind_name,
    j.summa,
    COALESCE(j.isstorno, false) AS isstorno,
        CASE
            WHEN COALESCE(j.isstorno, false) THEN 1
            ELSE 0
        END AS isstorno_num
   FROM (((public.jumps j
     LEFT JOIN public.v_fire_cases fc ON ((fc.id = j.fire_case_id)))
     LEFT JOIN public.v_workers w ON ((w.id = j.worker_id)))
     LEFT JOIN public.jump_kinds jk ON ((jk.id = j.jump_kind_id)))
  ORDER BY j.date_begin;


ALTER TABLE public.v_jumps OWNER TO postgres;

--
-- TOC entry 317 (class 1259 OID 645314)
-- Name: v_other_pays; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_other_pays AS
 SELECT op.id,
    op.fire_case_id,
    op.payment_id,
    op.worker_id,
    op.calc_period_id,
    op.hours,
    op.percent,
    op.isholiday,
    op.summa,
    fc.full_name AS fire_case_name,
    p.code AS payment_code,
    w.full_code AS worker_code,
    d.code AS department_code,
    COALESCE(op.isstorno, false) AS isstorno,
        CASE
            WHEN COALESCE(op.isstorno, false) THEN 1
            ELSE 0
        END AS isstorno_num
   FROM ((((public.other_pays op
     LEFT JOIN public.v_fire_cases fc ON ((fc.id = op.fire_case_id)))
     LEFT JOIN public.payments p ON ((p.id = op.payment_id)))
     LEFT JOIN public.v_workers w ON ((w.id = op.worker_id)))
     LEFT JOIN public.departments d ON ((d.id = w.department_id)))
  ORDER BY w.code;


ALTER TABLE public.v_other_pays OWNER TO postgres;

--
-- TOC entry 300 (class 1259 OID 580843)
-- Name: v_paygroups; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_paygroups AS
 SELECT pg.id,
    pg.code,
    pg.name,
    pg.created_at,
    pg.updated_at
   FROM public.paygroups pg
  ORDER BY pg.code;


ALTER TABLE public.v_paygroups OWNER TO postgres;

--
-- TOC entry 301 (class 1259 OID 580847)
-- Name: v_paylinks; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_paylinks AS
 SELECT p.id,
    p.pay_fire_id,
    p1.code AS pay_fire_code,
    p.pay_jumps_id,
    p2.code AS pay_jumps_code,
    p.pay_flights_id,
    p3.code AS pay_flights_code,
    p.pay_over_id,
    p4.code AS pay_over_code,
    p.pay_holiday_id,
    p5.code AS pay_holiday_code,
    p.firecalc
   FROM (((((public.paylinks p
     LEFT JOIN public.payments p1 ON ((p1.id = p.pay_fire_id)))
     LEFT JOIN public.payments p2 ON ((p2.id = p.pay_jumps_id)))
     LEFT JOIN public.payments p3 ON ((p3.id = p.pay_flights_id)))
     LEFT JOIN public.payments p4 ON ((p4.id = p.pay_over_id)))
     LEFT JOIN public.payments p5 ON ((p5.id = p.pay_holiday_id)));


ALTER TABLE public.v_paylinks OWNER TO postgres;

--
-- TOC entry 302 (class 1259 OID 580852)
-- Name: v_payments; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_payments AS
 SELECT p.id,
    p.catalog_id,
    p.num,
    p.code,
    p.name
   FROM public.payments p
  ORDER BY p.code;


ALTER TABLE public.v_payments OWNER TO postgres;

--
-- TOC entry 303 (class 1259 OID 580856)
-- Name: v_payments2payments; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_payments2payments AS
 SELECT pp.id,
    pp.parent_payment_id,
    pp.payment_id,
    p.code AS payment_code,
    p.name AS payment_name
   FROM (public.payments2payments pp
     LEFT JOIN public.payments p ON ((p.id = pp.payment_id)))
  ORDER BY p.num;


ALTER TABLE public.v_payments2payments OWNER TO postgres;

--
-- TOC entry 304 (class 1259 OID 580860)
-- Name: v_planes; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_planes AS
 SELECT p.id,
    p.code,
    p.name,
    p.created_at,
    p.updated_at
   FROM public.planes p
  ORDER BY p.code;


ALTER TABLE public.v_planes OWNER TO postgres;

--
-- TOC entry 305 (class 1259 OID 580864)
-- Name: v_report_columns; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_report_columns AS
 SELECT report_columns.id,
    report_columns.report_id,
    report_columns.field_name,
    report_columns.title,
    report_columns.display_width,
    report_columns.display_align,
    report_columns.order_num
   FROM public.report_columns
  ORDER BY report_columns.order_num;


ALTER TABLE public.v_report_columns OWNER TO postgres;

--
-- TOC entry 313 (class 1259 OID 644268)
-- Name: v_report_params; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_report_params AS
 SELECT report_params.id,
    report_params.report_id,
    report_params.code,
    report_params.caption,
    report_params.param_type,
    report_params.param_length,
    report_params.param_decimals,
    report_params.getlist_method,
    report_params.display_field_name,
    report_params.key_field_name,
    report_params.order_num,
    report_params.master_param_id,
    report_params.filter_expression
   FROM public.report_params
  ORDER BY report_params.order_num;


ALTER TABLE public.v_report_params OWNER TO postgres;

--
-- TOC entry 306 (class 1259 OID 580872)
-- Name: v_reports; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_reports AS
 SELECT reports.id,
    reports.name,
    reports.template,
    reports.prepare_command_sql,
    reports.query_sql,
    reports.group_field_level1,
    reports.group_field_level2
   FROM public.reports
  ORDER BY reports.name;


ALTER TABLE public.v_reports OWNER TO postgres;

--
-- TOC entry 3541 (class 2604 OID 580876)
-- Name: fire_cases id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fire_cases ALTER COLUMN id SET DEFAULT nextval('public.firecases_id_seq'::regclass);


--
-- TOC entry 3547 (class 2606 OID 580878)
-- Name: accurals accurals_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.accurals
    ADD CONSTRAINT accurals_pk PRIMARY KEY (id);


--
-- TOC entry 3550 (class 2606 OID 580880)
-- Name: calc_periods calc_periods_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.calc_periods
    ADD CONSTRAINT calc_periods_pk PRIMARY KEY (id);


--
-- TOC entry 3552 (class 2606 OID 580882)
-- Name: catalogs catalogs_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.catalogs
    ADD CONSTRAINT catalogs_pk PRIMARY KEY (id);


--
-- TOC entry 3554 (class 2606 OID 580884)
-- Name: common_pays common_pays_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.common_pays
    ADD CONSTRAINT common_pays_pk PRIMARY KEY (id);


--
-- TOC entry 3641 (class 2606 OID 591339)
-- Name: deduction_scale deduction_scale_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.deduction_scale
    ADD CONSTRAINT deduction_scale_pk PRIMARY KEY (id);


--
-- TOC entry 3556 (class 2606 OID 580886)
-- Name: departments department_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.departments
    ADD CONSTRAINT department_pk PRIMARY KEY (id);


--
-- TOC entry 3559 (class 2606 OID 580888)
-- Name: fighting_details fighting_details_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fighting_details
    ADD CONSTRAINT fighting_details_pk PRIMARY KEY (id);


--
-- TOC entry 3562 (class 2606 OID 580890)
-- Name: fightings fightings_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fightings
    ADD CONSTRAINT fightings_pk PRIMARY KEY (id);


--
-- TOC entry 3567 (class 2606 OID 580892)
-- Name: fire_cases firecases_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fire_cases
    ADD CONSTRAINT firecases_pk PRIMARY KEY (id);


--
-- TOC entry 3571 (class 2606 OID 580894)
-- Name: flights flights_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.flights
    ADD CONSTRAINT flights_pk PRIMARY KEY (id);


--
-- TOC entry 3574 (class 2606 OID 580896)
-- Name: forestries forestries_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.forestries
    ADD CONSTRAINT forestries_pk PRIMARY KEY (id);


--
-- TOC entry 3578 (class 2606 OID 580898)
-- Name: fot fot_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fot
    ADD CONSTRAINT fot_pk PRIMARY KEY (id);


--
-- TOC entry 3581 (class 2606 OID 580900)
-- Name: grounds grounds_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grounds
    ADD CONSTRAINT grounds_pk PRIMARY KEY (id);


--
-- TOC entry 3584 (class 2606 OID 580902)
-- Name: import_log import_log_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.import_log
    ADD CONSTRAINT import_log_pk PRIMARY KEY (id);


--
-- TOC entry 3586 (class 2606 OID 580904)
-- Name: jump_kinds jump_kind_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jump_kinds
    ADD CONSTRAINT jump_kind_pk PRIMARY KEY (id);


--
-- TOC entry 3588 (class 2606 OID 580906)
-- Name: jumps jumps_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jumps
    ADD CONSTRAINT jumps_pk PRIMARY KEY (id);


--
-- TOC entry 3590 (class 2606 OID 580908)
-- Name: mdays mdays_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mdays
    ADD CONSTRAINT mdays_pk PRIMARY KEY (id);


--
-- TOC entry 3592 (class 2606 OID 580910)
-- Name: other_pays otherpays_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.other_pays
    ADD CONSTRAINT otherpays_pk PRIMARY KEY (id);


--
-- TOC entry 3594 (class 2606 OID 580912)
-- Name: paygroups paygroup_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.paygroups
    ADD CONSTRAINT paygroup_pk PRIMARY KEY (id);


--
-- TOC entry 3596 (class 2606 OID 580914)
-- Name: paylinks paylinks_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.paylinks
    ADD CONSTRAINT paylinks_pk PRIMARY KEY (id);


--
-- TOC entry 3600 (class 2606 OID 580916)
-- Name: payments2payments payments2payments_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payments2payments
    ADD CONSTRAINT payments2payments_pk PRIMARY KEY (id);


--
-- TOC entry 3598 (class 2606 OID 580918)
-- Name: payments payments_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payments
    ADD CONSTRAINT payments_pk PRIMARY KEY (id);


--
-- TOC entry 3602 (class 2606 OID 580920)
-- Name: planes planes_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.planes
    ADD CONSTRAINT planes_pk PRIMARY KEY (id);


--
-- TOC entry 3604 (class 2606 OID 580922)
-- Name: report_columns report_columns_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.report_columns
    ADD CONSTRAINT report_columns_pk PRIMARY KEY (id);


--
-- TOC entry 3606 (class 2606 OID 580924)
-- Name: report_params report_params_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.report_params
    ADD CONSTRAINT report_params_pk PRIMARY KEY (id);


--
-- TOC entry 3608 (class 2606 OID 580926)
-- Name: reports reports_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reports
    ADD CONSTRAINT reports_pk PRIMARY KEY (id);


--
-- TOC entry 3611 (class 2606 OID 580928)
-- Name: schedule_days schedule_days_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.schedule_days
    ADD CONSTRAINT schedule_days_pk PRIMARY KEY (id);


--
-- TOC entry 3613 (class 2606 OID 580930)
-- Name: schedules schedule_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.schedules
    ADD CONSTRAINT schedule_pk PRIMARY KEY (id);


--
-- TOC entry 3569 (class 2606 OID 580932)
-- Name: flight_tarifs tarifs_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.flight_tarifs
    ADD CONSTRAINT tarifs_pk PRIMARY KEY (id);


--
-- TOC entry 3615 (class 2606 OID 580934)
-- Name: temp_catalogs temp_catalogs_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.temp_catalogs
    ADD CONSTRAINT temp_catalogs_pk PRIMARY KEY (id);


--
-- TOC entry 3617 (class 2606 OID 580936)
-- Name: temp_departments temp_department_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.temp_departments
    ADD CONSTRAINT temp_department_pk PRIMARY KEY (id);


--
-- TOC entry 3621 (class 2606 OID 580938)
-- Name: temp_fot temp_fot_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.temp_fot
    ADD CONSTRAINT temp_fot_pk PRIMARY KEY (id);


--
-- TOC entry 3624 (class 2606 OID 580940)
-- Name: temp_fot_summ temp_fot_summ_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.temp_fot_summ
    ADD CONSTRAINT temp_fot_summ_pk PRIMARY KEY (osn_id);


--
-- TOC entry 3627 (class 2606 OID 580942)
-- Name: temp_grounds temp_grounds_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.temp_grounds
    ADD CONSTRAINT temp_grounds_pk PRIMARY KEY (id);


--
-- TOC entry 3630 (class 2606 OID 580944)
-- Name: temp_payments temp_payments_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.temp_payments
    ADD CONSTRAINT temp_payments_pk PRIMARY KEY (id);


--
-- TOC entry 3632 (class 2606 OID 580946)
-- Name: temp_schedule_days temp_schedule_days_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.temp_schedule_days
    ADD CONSTRAINT temp_schedule_days_pk PRIMARY KEY (id);


--
-- TOC entry 3634 (class 2606 OID 580948)
-- Name: temp_schedules temp_schedule_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.temp_schedules
    ADD CONSTRAINT temp_schedule_pk PRIMARY KEY (id);


--
-- TOC entry 3636 (class 2606 OID 580950)
-- Name: temp_workers temp_worker_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.temp_workers
    ADD CONSTRAINT temp_worker_pk PRIMARY KEY (id);


--
-- TOC entry 3638 (class 2606 OID 580952)
-- Name: workers worker_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workers
    ADD CONSTRAINT worker_pk PRIMARY KEY (id);


--
-- TOC entry 3543 (class 1259 OID 580953)
-- Name: accurals_calc_period_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX accurals_calc_period_id_idx ON public.accurals USING btree (calc_period_id);


--
-- TOC entry 3544 (class 1259 OID 580954)
-- Name: accurals_calc_period_worker_payment_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX accurals_calc_period_worker_payment_idx ON public.accurals USING btree (calc_period_id, fire_case_id, worker_id, payment_id);


--
-- TOC entry 3545 (class 1259 OID 599883)
-- Name: accurals_distribution_calc_period_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX accurals_distribution_calc_period_id_idx ON public.accurals USING btree (distribution_calc_period_id);


--
-- TOC entry 3548 (class 1259 OID 580955)
-- Name: calc_periods_period_begin_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX calc_periods_period_begin_idx ON public.calc_periods USING btree (period_begin);


--
-- TOC entry 3557 (class 1259 OID 580956)
-- Name: departments_parent_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX departments_parent_id_idx ON public.departments USING btree (parent_id);


--
-- TOC entry 3560 (class 1259 OID 580957)
-- Name: fightings_calc_period_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fightings_calc_period_id_idx ON public.fightings USING btree (calc_period_id);


--
-- TOC entry 3563 (class 1259 OID 580958)
-- Name: fire_cases_datefrom_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fire_cases_datefrom_idx ON public.fire_cases USING btree (datefrom);


--
-- TOC entry 3564 (class 1259 OID 580959)
-- Name: fire_cases_dateto_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fire_cases_dateto_idx ON public.fire_cases USING btree (dateto);


--
-- TOC entry 3565 (class 1259 OID 580960)
-- Name: fire_cases_name_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX fire_cases_name_idx ON public.fire_cases USING btree (name, department_id, forestry_id, datefrom);


--
-- TOC entry 3572 (class 1259 OID 696404)
-- Name: forestries_name_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX forestries_name_idx ON public.forestries USING btree (name);


--
-- TOC entry 3575 (class 1259 OID 580961)
-- Name: fot_mainlpf_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fot_mainlpf_id_idx ON public.fot USING btree (main_lpf_id);


--
-- TOC entry 3576 (class 1259 OID 580962)
-- Name: fot_payment_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fot_payment_id_idx ON public.fot USING btree (payment_id);


--
-- TOC entry 3579 (class 1259 OID 580963)
-- Name: fot_worker_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fot_worker_id_idx ON public.fot USING btree (worker_id);


--
-- TOC entry 3582 (class 1259 OID 580964)
-- Name: grounds_worker_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX grounds_worker_id_idx ON public.grounds USING btree (worker_id, payment_id);


--
-- TOC entry 3609 (class 1259 OID 580965)
-- Name: schedule_days_day_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX schedule_days_day_idx ON public.schedule_days USING btree (day);


--
-- TOC entry 3618 (class 1259 OID 580966)
-- Name: temp_fot_mainlpf_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX temp_fot_mainlpf_id_idx ON public.temp_fot USING btree (main_lpf_id);


--
-- TOC entry 3619 (class 1259 OID 580967)
-- Name: temp_fot_payment_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX temp_fot_payment_id_idx ON public.temp_fot USING btree (payment_id);


--
-- TOC entry 3625 (class 1259 OID 580968)
-- Name: temp_fot_summ_worker_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX temp_fot_summ_worker_id_idx ON public.temp_fot_summ USING btree (worker_id);


--
-- TOC entry 3622 (class 1259 OID 580969)
-- Name: temp_fot_worker_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX temp_fot_worker_id_idx ON public.temp_fot USING btree (worker_id);


--
-- TOC entry 3628 (class 1259 OID 580970)
-- Name: temp_objects_updated_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX temp_objects_updated_id_idx ON public.temp_objects_updated USING btree (id);


--
-- TOC entry 3639 (class 1259 OID 580971)
-- Name: workers_department_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX workers_department_id_idx ON public.workers USING btree (department_id);


--
-- TOC entry 3679 (class 2620 OID 580972)
-- Name: flights bui_flights; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER bui_flights BEFORE INSERT OR UPDATE ON public.flights FOR EACH ROW EXECUTE FUNCTION public.f_bui_flights();


--
-- TOC entry 3680 (class 2620 OID 580973)
-- Name: jumps bui_jumps; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER bui_jumps BEFORE INSERT OR UPDATE ON public.jumps FOR EACH ROW EXECUTE FUNCTION public.f_bui_jumps();


--
-- TOC entry 3681 (class 2620 OID 580974)
-- Name: other_pays bui_other_pays; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER bui_other_pays BEFORE INSERT OR UPDATE ON public.other_pays FOR EACH ROW EXECUTE FUNCTION public.f_bui_other_pays();


--
-- TOC entry 3642 (class 2606 OID 580975)
-- Name: accurals accurals_calc_periods_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.accurals
    ADD CONSTRAINT accurals_calc_periods_fk FOREIGN KEY (calc_period_id) REFERENCES public.calc_periods(id);


--
-- TOC entry 3643 (class 2606 OID 580980)
-- Name: accurals accurals_fire_cases_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.accurals
    ADD CONSTRAINT accurals_fire_cases_fk FOREIGN KEY (fire_case_id) REFERENCES public.fire_cases(id);


--
-- TOC entry 3644 (class 2606 OID 580985)
-- Name: accurals accurals_payments_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.accurals
    ADD CONSTRAINT accurals_payments_fk FOREIGN KEY (payment_id) REFERENCES public.payments(id);


--
-- TOC entry 3645 (class 2606 OID 580990)
-- Name: accurals accurals_workers_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.accurals
    ADD CONSTRAINT accurals_workers_fk FOREIGN KEY (worker_id) REFERENCES public.workers(id);


--
-- TOC entry 3646 (class 2606 OID 580995)
-- Name: common_pays common_pays_calc_periods_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.common_pays
    ADD CONSTRAINT common_pays_calc_periods_fk FOREIGN KEY (calc_period_id) REFERENCES public.calc_periods(id);


--
-- TOC entry 3647 (class 2606 OID 581000)
-- Name: common_pays common_pays_payments_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.common_pays
    ADD CONSTRAINT common_pays_payments_fk FOREIGN KEY (payment_id) REFERENCES public.payments(id);


--
-- TOC entry 3648 (class 2606 OID 581005)
-- Name: common_pays common_pays_worker_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.common_pays
    ADD CONSTRAINT common_pays_worker_fk FOREIGN KEY (worker_id) REFERENCES public.workers(id);


--
-- TOC entry 3649 (class 2606 OID 581010)
-- Name: fighting_details fighting_details_fightings_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fighting_details
    ADD CONSTRAINT fighting_details_fightings_fk FOREIGN KEY (fighting_id) REFERENCES public.fightings(id) ON DELETE CASCADE;


--
-- TOC entry 3650 (class 2606 OID 581015)
-- Name: fightings fightings_calc_periods_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fightings
    ADD CONSTRAINT fightings_calc_periods_fk FOREIGN KEY (calc_period_id) REFERENCES public.calc_periods(id);


--
-- TOC entry 3651 (class 2606 OID 581020)
-- Name: fightings fightings_fire_cases_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fightings
    ADD CONSTRAINT fightings_fire_cases_fk FOREIGN KEY (fire_case_id) REFERENCES public.fire_cases(id);


--
-- TOC entry 3652 (class 2606 OID 581025)
-- Name: fightings fightings_worker_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fightings
    ADD CONSTRAINT fightings_worker_fk FOREIGN KEY (worker_id) REFERENCES public.workers(id);


--
-- TOC entry 3653 (class 2606 OID 581030)
-- Name: fire_cases fire_cases_forestries_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fire_cases
    ADD CONSTRAINT fire_cases_forestries_fk FOREIGN KEY (forestry_id) REFERENCES public.forestries(id);


--
-- TOC entry 3656 (class 2606 OID 581035)
-- Name: flights flights_calc_periods_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.flights
    ADD CONSTRAINT flights_calc_periods_fk FOREIGN KEY (calc_period_id) REFERENCES public.calc_periods(id);


--
-- TOC entry 3657 (class 2606 OID 581040)
-- Name: flights flights_fire_cases_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.flights
    ADD CONSTRAINT flights_fire_cases_fk FOREIGN KEY (fire_case_id) REFERENCES public.fire_cases(id);


--
-- TOC entry 3658 (class 2606 OID 581045)
-- Name: flights flights_planes_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.flights
    ADD CONSTRAINT flights_planes_fk FOREIGN KEY (plane_id) REFERENCES public.planes(id);


--
-- TOC entry 3659 (class 2606 OID 581050)
-- Name: flights flights_worker_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.flights
    ADD CONSTRAINT flights_worker_fk FOREIGN KEY (worker_id) REFERENCES public.workers(id);


--
-- TOC entry 3660 (class 2606 OID 581055)
-- Name: fot fot_workers_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fot
    ADD CONSTRAINT fot_workers_fk FOREIGN KEY (worker_id) REFERENCES public.workers(id) ON DELETE CASCADE;


--
-- TOC entry 3661 (class 2606 OID 581060)
-- Name: grounds grounds_payments_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grounds
    ADD CONSTRAINT grounds_payments_fk FOREIGN KEY (payment_id) REFERENCES public.payments(id);


--
-- TOC entry 3662 (class 2606 OID 581065)
-- Name: grounds grounds_workers_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grounds
    ADD CONSTRAINT grounds_workers_fk FOREIGN KEY (worker_id) REFERENCES public.workers(id) ON DELETE CASCADE;


--
-- TOC entry 3663 (class 2606 OID 581070)
-- Name: jumps jumps_calc_periods_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jumps
    ADD CONSTRAINT jumps_calc_periods_fk FOREIGN KEY (calc_period_id) REFERENCES public.calc_periods(id);


--
-- TOC entry 3664 (class 2606 OID 581075)
-- Name: jumps jumps_fire_cases_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jumps
    ADD CONSTRAINT jumps_fire_cases_fk FOREIGN KEY (fire_case_id) REFERENCES public.fire_cases(id);


--
-- TOC entry 3665 (class 2606 OID 581080)
-- Name: jumps jumps_jump_kind_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jumps
    ADD CONSTRAINT jumps_jump_kind_fk FOREIGN KEY (jump_kind_id) REFERENCES public.jump_kinds(id);


--
-- TOC entry 3666 (class 2606 OID 581085)
-- Name: jumps jumps_worker_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jumps
    ADD CONSTRAINT jumps_worker_fk FOREIGN KEY (worker_id) REFERENCES public.workers(id);


--
-- TOC entry 3667 (class 2606 OID 581090)
-- Name: other_pays other_pays_calc_periods_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.other_pays
    ADD CONSTRAINT other_pays_calc_periods_fk FOREIGN KEY (calc_period_id) REFERENCES public.calc_periods(id);


--
-- TOC entry 3668 (class 2606 OID 581095)
-- Name: other_pays other_pays_fire_cases_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.other_pays
    ADD CONSTRAINT other_pays_fire_cases_fk FOREIGN KEY (fire_case_id) REFERENCES public.fire_cases(id);


--
-- TOC entry 3669 (class 2606 OID 581100)
-- Name: other_pays other_pays_payments_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.other_pays
    ADD CONSTRAINT other_pays_payments_fk FOREIGN KEY (payment_id) REFERENCES public.payments(id);


--
-- TOC entry 3670 (class 2606 OID 581105)
-- Name: other_pays other_pays_worker_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.other_pays
    ADD CONSTRAINT other_pays_worker_fk FOREIGN KEY (worker_id) REFERENCES public.workers(id);


--
-- TOC entry 3672 (class 2606 OID 581110)
-- Name: payments2payments payments2paymens_payments_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payments2payments
    ADD CONSTRAINT payments2paymens_payments_fk FOREIGN KEY (parent_payment_id) REFERENCES public.payments(id);


--
-- TOC entry 3673 (class 2606 OID 581115)
-- Name: payments2payments payments2paymens_payments_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payments2payments
    ADD CONSTRAINT payments2paymens_payments_fk_1 FOREIGN KEY (payment_id) REFERENCES public.payments(id);


--
-- TOC entry 3671 (class 2606 OID 581120)
-- Name: payments payments_catalogs_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payments
    ADD CONSTRAINT payments_catalogs_fk FOREIGN KEY (catalog_id) REFERENCES public.catalogs(id);


--
-- TOC entry 3674 (class 2606 OID 581125)
-- Name: report_columns report_columns_reports_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.report_columns
    ADD CONSTRAINT report_columns_reports_fk FOREIGN KEY (report_id) REFERENCES public.reports(id) ON DELETE CASCADE;


--
-- TOC entry 3675 (class 2606 OID 644245)
-- Name: report_params report_params_report_params_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.report_params
    ADD CONSTRAINT report_params_report_params_fk FOREIGN KEY (master_param_id) REFERENCES public.report_params(id);


--
-- TOC entry 3676 (class 2606 OID 581130)
-- Name: report_params report_params_reports_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.report_params
    ADD CONSTRAINT report_params_reports_fk FOREIGN KEY (report_id) REFERENCES public.reports(id) ON DELETE CASCADE;


--
-- TOC entry 3677 (class 2606 OID 581135)
-- Name: schedule_days schedule_days_schedule_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.schedule_days
    ADD CONSTRAINT schedule_days_schedule_fk FOREIGN KEY (schedule_id) REFERENCES public.schedules(id);


--
-- TOC entry 3654 (class 2606 OID 581140)
-- Name: flight_tarifs tarifs_paygroup_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.flight_tarifs
    ADD CONSTRAINT tarifs_paygroup_fk FOREIGN KEY (paygroup_id) REFERENCES public.paygroups(id);


--
-- TOC entry 3655 (class 2606 OID 581145)
-- Name: flight_tarifs tarifs_planes_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.flight_tarifs
    ADD CONSTRAINT tarifs_planes_fk FOREIGN KEY (plane_id) REFERENCES public.planes(id);


--
-- TOC entry 3678 (class 2606 OID 581150)
-- Name: workers worker_department_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workers
    ADD CONSTRAINT worker_department_fk FOREIGN KEY (department_id) REFERENCES public.departments(id);


-- Completed on 2024-06-30 18:36:26

--
-- PostgreSQL database dump complete
--

