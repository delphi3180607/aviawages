unit dmucurrentperiod;

interface

uses
  System.SysUtils, System.Classes, dmu, dmucalcperiods,
  Dialogs, Variants, Generics.Collections, Vcl.Forms,
  System.Generics.Collections, MemTableDataEh, MemTableEh,
  Data.DB, FMACrudDisp, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt;

type
  Tdmcurrentperiod = class(TDataModule)
    period_settings: TFDTable;
    CRUDDisp: TFMACRUDDisp;
    MemData: TMemTableEh;
    procedure DataModuleCreate(Sender: TObject);
  private
    FInitializedForm: TDictionary<TForm,string>;
    FCalcPeriodDate: TDateTime;
    FFilterStartDate: TDateTime;
    FFilterEndDate: TDateTime;
    procedure SetCalcPeriodDate(value: TDateTime);
    procedure OpenSettings;
    procedure CloseSettings;
  public
    Context: TDictionary<string,Variant>;
    CalcPeriodId: Variant;
    IsPeriodClosed: boolean;
    procedure RegisterInitializedForm(form: TForm);
    procedure ReinitializeForms;
    procedure UpdateContext;
    procedure SetDefaultPeriod;
    procedure SetFilterPeriod(start_date, end_date: TDateTime);
    function ChangePeriod(calc_period: Variant; save_setting: boolean): boolean;
    procedure ValidatePeriod;
    procedure SavePeriodSettings;
    procedure SetCalcPeriod(out dtCalcPeriodDate: TDateTime; out dtBeginDate: TDateTime; out dtEndDate: TDateTime);
    property CalcPeriodDate: TDateTime read FCalcPeriodDate write SetCalcPeriodDate;
    property FilterStartDate: TDateTime read FFilterStartDate;
    property FilterEndDate: TDateTime read FFilterEndDate;
  end;

var
  dmcurrentperiod: Tdmcurrentperiod;

implementation

uses initstated;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}


function Tdmcurrentperiod.ChangePeriod(calc_period: Variant; save_setting: boolean): boolean;
var errormessage: string; e: string;
begin
  if not Assigned(dmcalcperiods) then exit;
  if not Assigned(MemData) then exit;
  with CRUDDisp.CrudAgent do
  begin
    ParamTransform.ResetContext;
    ParamTransform.AddPatternContext('/*wherefilter*/', 'date_trunc(''month'',period_begin) = date_trunc(''month'',:period_begin)');
    ParamTransform.AddParameterContext('period_begin', ftDate, calc_period);
  end;
  if not CRUDDisp.GetList(nil,errormessage) then
  begin
    MessageDlg('������ ��������� �������� �������:'+#13+#10+errormessage, mtError, [mbOK], 0);
    result := false;
    exit;
  end;
  if MemData.Eof then
  begin
    ShowMessage('������ ������� ��� ���.'+#13+#10+'�������� ������ ����� ������ ������������.');
    result := false;
  end else
  begin
    ValidatePeriod;
    if save_setting then SavePeriodSettings;
    result := true;
  end;
end;

procedure Tdmcurrentperiod.SetDefaultPeriod;
var errormessage: string;
begin
  OpenSettings;
  with dmcalcperiods.CrudAgent do
  begin
    ParamTransform.ResetContext;
    ParamTransform.AddPatternContext('/*wherefilter*/', 'id = :id');
    ParamTransform.AddParameterContext('id', ftInteger, period_settings.FieldByName('period_id').Value);
  end;
  CloseSettings;
  CRUDDisp.GetList(nil, errormessage);
  if not CRUDDisp.GetList(nil,errormessage) then
  begin
    MessageDlg('������ ��������� ���������� �������:'+#13+#10+errormessage, mtError, [mbOK], 0);
    exit;
  end;
  if MemData.Eof then
  begin
    with dmcalcperiods.CrudAgent do
    begin
      ParamTransform.ResetContext;
      ParamTransform.AddPatternContext('/*orderby*/', 'period_begin desc');
    end;
    CRUDDisp.GetList(nil, errormessage);
    if not CRUDDisp.GetList(nil,errormessage) then
    begin
      MessageDlg('������ ��������� ���������� �������:'+#13+#10+errormessage, mtError, [mbOK], 0);
      exit;
    end;
    if MemData.Eof then
    begin
      ShowMessage('�� ����� �� ���� ��������� ������.'+#13+#10+'�������� ������ ����� ������ ������������.');
    end;
  end;
  if not MemData.Eof then
  begin
    ValidatePeriod;
  end;
end;

procedure Tdmcurrentperiod.SetCalcPeriod(out dtCalcPeriodDate: TDateTime; out dtBeginDate: TDateTime; out dtEndDate: TDateTime);
begin
  dtCalcPeriodDate := self.CalcPeriodDate;
  dtBeginDate := self.CalcPeriodDate - 15;
  dtEndDate := self.CalcPeriodDate + 35;
  SetFilterPeriod(dtBeginDate, dtEndDate);
end;

procedure Tdmcurrentperiod.SetFilterPeriod(start_date, end_date: TDateTime);
begin
  FFilterStartDate := start_date;
  FFilterEndDate := end_date;
  UpdateContext;
end;

procedure Tdmcurrentperiod.UpdateContext;
begin
  context.Clear;
  context.Add('calc_period_id', CalcPeriodId);
  context.Add('filter_start_date', self.FFilterStartDate);
  context.Add('filter_end_date', self.FFilterEndDate);
end;

procedure Tdmcurrentperiod.CloseSettings;
begin
  dm.connSettings.Close;
end;

procedure Tdmcurrentperiod.DataModuleCreate(Sender: TObject);
begin
  context := TDictionary<string, Variant>.Create;
  FInitializedForm := TDictionary<TForm, string>.Create;
end;

procedure Tdmcurrentperiod.OpenSettings;
begin
  dm.connSettings.Close;
  dm.connSettings.Open;
  period_settings.Exclusive := false;
  period_settings.Open;
end;

procedure Tdmcurrentperiod.RegisterInitializedForm(form: TForm);
begin
  FInitializedForm.AddOrSetValue(form,'');
end;

procedure Tdmcurrentperiod.ReinitializeForms;
var form: TForm;
begin
 for form in FInitializedForm.Keys do
 begin
  if form is TFormInitStated then
  begin
    TFormInitStated(form).Reset;
  end;
 end;
end;

procedure Tdmcurrentperiod.SetCalcPeriodDate(value: TDateTime);
begin
  FCalcPeriodDate := value;
end;

procedure Tdmcurrentperiod.ValidatePeriod;
begin
  CalcPeriodDate := MemData.FieldByName('period_begin').AsDateTime;
  CalcPeriodId := MemData.FieldByName('id').AsInteger;
  IsPeriodClosed := not (MemData.FieldByName('date_closed').Value=null);
end;

procedure Tdmcurrentperiod.SavePeriodSettings;
begin
  OpenSettings;
  if period_settings.RecordCount=0 then
  begin
    period_settings.Append;
    period_settings.Post;
  end;
  period_settings.Edit;
  period_settings.FieldByName('period_id').Value := CalcPeriodId;
  period_settings.Post;
  CloseSettings;
end;

end.
