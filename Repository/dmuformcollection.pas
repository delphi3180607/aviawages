unit dmuformcollection;

interface

uses
  System.SysUtils, System.Classes, System.Generics.Collections, Vcl.Forms, InitStated;

type
  TdmFormCollection = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    SelectFormCollection: TDictionary<string, TFormInitStated>;
  end;

var
  dmFormCollection: TdmFormCollection;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TdmFormCollection.DataModuleCreate(Sender: TObject);
begin
  SelectFormCollection := TDictionary<string, TFormInitStated>.Create;
end;

end.
