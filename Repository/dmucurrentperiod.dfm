object dmcurrentperiod: Tdmcurrentperiod
  OnCreate = DataModuleCreate
  Height = 280
  Width = 434
  object period_settings: TFDTable
    Connection = DM.connSettings
    ResourceOptions.AssignedValues = [rvEscapeExpand]
    TableName = 'current_period'
    Left = 50
    Top = 35
  end
  object CRUDDisp: TFMACRUDDisp
    Fields = <>
    MemData = MemData
    CrudAgent = dmCalcPeriods.CrudAgent
    ReadOnly = False
    Left = 114
    Top = 108
  end
  object MemData: TMemTableEh
    Params = <>
    Left = 40
    Top = 104
  end
end
